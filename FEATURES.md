# Library Features

## Convolution Algorithms

`triNNity` implements many different convolution algorithms suitable for
convolutions of different sizes and shapes, including:

- direct convolution (loop based)
- GEMM-based convolution
  - im2col/im2row variants
  - kn2col/kn2row variants
  - specialized 1x1 GEMM convolution
  - low-memory variants (MEC)
- FFT convolution
  - FFTW and LomontFFT supported
  - strided FFT convolution fully supported
- Winograd convolution
  - k=3 and k=5 supported
  - vectorized Winograd using GCC vector extensions

## Specialized Convolutions

`triNNity` supports group convolutions, sparse convolutions, and dilated
convolutions, which may be required in specialized networks.

## Data types and layouts

`triNNity` supports many data types and layouts for convolution, in particular,
direct and GEMM based convolutions support any standard C++ datatype for the
image, kernel, intermediate products, and output. They do not have to match,
for example, if you need to perform signed 8-bit convolution with an unsigned
8-bit kernel producing a signed 32-bit result.

## Backend libraries and frameworks

`triNNity` can transparently use various backend libraries and frameworks to
accelerate your convolutional layers, including:

- OpenBLAS/ATLAS/GotoBLAS/any CBLAS compatible library
  - transparent support for row or column major storage without changing code
- Intel MKL
- OpenMP (via the C++ compiler)
