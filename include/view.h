/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_VIEW_H
#define TRINNITY_VIEW_H

#include <triNNity/config.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace internal {

/**
 * This namespace contains memory manipulation functions and multidimensional views used to represent tensors.
 */

namespace memory {

/**
 * This is the superclass for all View types.
 * A View type is a multidimensional view of a flat memory region
 * which stores a tensor. The View type provides the indexing operations (operator[])
 * used to manipulate data in the tensor.
 *
 * \tparam T
 *
 * \parblock
 * The primitive type of the data in the tensors.
 * \endparblock
 *
 */

template <typename T>
class View {
protected:
  //! Pointer to the contiguous 1 dimensional data in memory
  T* data;
public:

  //! Return a pointer to the beginning of the memory region storing the tensor (the first item in the tensor)
  virtual T* begin() = 0;

  //! Return a pointer to the end of the memory region storing the tensor (one past the last item in the tensor)
  virtual T* end() = 0;

  //! Return the number of elements in the tensor
  virtual unsigned size() = 0;

  View(T* d) : data(d) { TRINNITY_ASSERT(d != nullptr); }
};

template <typename T, unsigned DIM1, unsigned DIM0>
class View2D : public View<T> {
public:

  virtual T* begin() {
    return this->data;
  }

  virtual T* end() {
    return this->data + DIM1*DIM0;
  }

  virtual unsigned size() {
    return DIM1*DIM0;
  }

  T* operator[](unsigned idx) {
    return this->data + (idx * DIM0);
  }

  const T* operator[](unsigned idx) const {
    return this->data + (idx * DIM0);
  }

  View2D(T* d) : View<T>(d) {}
};

template <typename T, unsigned DIM2, unsigned DIM1, unsigned DIM0>
class View3D : public View<T> {
public:

  virtual T* begin() {
    return this->data;
  }

  virtual T* end() {
    return this->data + DIM2*DIM1*DIM0;
  }

  virtual unsigned size() {
    return DIM2*DIM1*DIM0;
  }

  View2D<T, DIM1, DIM0> operator[](unsigned idx) {
    return View2D<T, DIM1, DIM0>(this->data + (idx * (DIM1*DIM0)));
  }

  const View2D<T, DIM1, DIM0> operator[](unsigned idx) const {
    return View2D<T, DIM1, DIM0>(this->data + (idx * (DIM1*DIM0)));
  }

  View3D(T* d) : View<T>(d) {}
};

template <typename T, unsigned DIM3, unsigned DIM2, unsigned DIM1, unsigned DIM0>
class View4D : public View<T> {
public:

  virtual T* begin() {
    return this->data;
  }

  virtual T* end() {
    return this->data + DIM3*DIM2*DIM1*DIM0;
  }

  virtual unsigned size() {
    return DIM3*DIM2*DIM1*DIM0;
  }

  View3D<T, DIM2, DIM1, DIM0> operator[](unsigned idx) {
    return View3D<T, DIM2, DIM1, DIM0>(this->data + (idx * (DIM2*DIM1*DIM0)));
  }

  const View3D<T, DIM2, DIM1, DIM0> operator[](unsigned idx) const {
    return View3D<T, DIM2, DIM1, DIM0>(this->data + (idx * (DIM2*DIM1*DIM0)));
  }

  View4D(T* d) : View<T>(d) {}
};

template <typename T, unsigned DIM4, unsigned DIM3, unsigned DIM2, unsigned DIM1, unsigned DIM0>
class View5D : public View<T> {
public:

  virtual T* begin() {
    return this->data;
  }

  virtual T* end() {
    return this->data + DIM4*DIM3*DIM2*DIM1*DIM0;
  }

  virtual unsigned size() {
    return DIM4*DIM3*DIM2*DIM1*DIM0;
  }

  View4D<T, DIM3, DIM2, DIM1, DIM0> operator[](unsigned idx) {
    return View4D<T, DIM3, DIM2, DIM1, DIM0>(this->data + (idx * (DIM3*DIM2*DIM1*DIM0)));
  }

  const View4D<T, DIM3, DIM2, DIM1, DIM0> operator[](unsigned idx) const {
    return View4D<T, DIM3, DIM2, DIM1, DIM0>(this->data + (idx * (DIM3*DIM2*DIM1*DIM0)));
  }

  View5D(T* d) : View<T>(d) {}
};

}

}

}

#endif // TRINNITY_VIEW_H
