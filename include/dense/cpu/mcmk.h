/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_MCMK_H
#define TRINNITY_DENSE_CPU_MCMK_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/dense/cpu/impl.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace dense {

/**
 * Direct convolution
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors
 * \endparblock
 * \tparam A
 * \parblock
 * The primitive type to use for accumulation
 * \endparblock
 * \tparam O
 * \parblock
 * The primitive type to use for output
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor
 * \endparblock
 * \tparam var
 * \parblock
 * The direct convolution method to use to compute the output
 * \endparblock
 * \tparam img_layout
 * \parblock
 * The layout that the input image tensor is in
 * \endparblock
 * \tparam krn_layout
 * \parblock
 * The layout that the kernel tensor is in
 * \endparblock
 * \tparam out_layout
 * \parblock
 * The layout that the output tensor should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k_w
 * \parblock
 * The width of the convolution kernel
 * \endparblock
 * \tparam k_h
 * \parblock
 * The height of the convolution kernel
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 */

template <typename T, typename A, typename O, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k_w, unsigned k_h,
          conv_direct_impl_t var, boundary_t bound,
          triNNity::layout::feature_map_layout_t img_layout,
          triNNity::layout::parameter_layout_t krn_layout,
          triNNity::layout::feature_map_layout_t out_layout,
          unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void convolution_forward_direct(T * __restrict__ img, KD * __restrict__ kernel, O * __restrict__ out) {

  TRINNITY_STATIC_ASSERT(krn_layout == triNNity::layout::OIHW);
  TRINNITY_STATIC_ASSERT(img_layout == triNNity::layout::CHW || img_layout == triNNity::layout::HWC);
  TRINNITY_STATIC_ASSERT(out_layout == img_layout);

  switch(var) {

    case CONV_MULTI_SUM_SINGLE: {
      switch(img_layout) {
        case triNNity::layout::CHW: {
          dense::cpu::impl::direct_loop_sum_single_chw<T, A, O, KD, ifm_w, ifm_h, ifms, ofms, k_w, k_h, bound, stride_w, stride_h>(img, kernel, out);
        } break;

        case triNNity::layout::HWC: {
          dense::cpu::impl::direct_loop_sum_single_hwc<T, A, O, KD, ifm_w, ifm_h, ifms, ofms, k_w, k_h, bound, stride_w, stride_h>(img, kernel, out);
        } break;
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

/**
 * Indirect GEMM-based convolution without building an input patch matrix.
 *
 * Pure GEMM-based convolution is performed by translating the convolution
 * directly into calls to GEMM without transforming the input. As such,
 * it can only support convolutions with stride 1.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor
 * \endparblock
 * \tparam var
 * \parblock
 * The indirect GEMM-based convolution method to use
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Which GEMM implementation to use
 * \endparblock
 * \tparam gemm_arr
 * \parblock
 * Which arrangement of arguments GEMM should use
 * \endparblock
 * \tparam gemm_transp
 * \parblock
 * What transposition of the A and B matrices GEMM should use
 * \endparblock
 * \tparam img_layout
 * \parblock
 * The layout that the input image tensor is in
 * \endparblock
 * \tparam krn_layout
 * \parblock
 * The layout that the kernel tensor is in
 * \endparblock
 * \tparam out_layout
 * \parblock
 * The layout that the output tensor should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */

template <typename T, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k,
          conv_gemm_impl_t var, boundary_t bound,
          gemm_variant_t gemm_var, gemm_transpose_t gemm_transp, gemm_arrangement_t gemm_arr,
          triNNity::layout::feature_map_layout_t img_layout,
          triNNity::layout::parameter_layout_t krn_layout,
          triNNity::layout::feature_map_layout_t out_layout,
          unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
static TRINNITY_INLINE void convolution_forward_gemm(T * __restrict__ img, KD * __restrict__ kernel, T * __restrict__ out) {

  switch(gemm_arr) {
    case triNNity::GEMM_IK: {
      switch(gemm_transp) {

        case triNNity::GEMM_A_B: {
          switch(var) {

            case CONV_MULTI_KN2COL_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_GEMM, krn_layout == triNNity::layout::IHWO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::kn2col_A_B_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::HWC);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_A_BT: {
          switch(var) {

            case CONV_MULTI_KN2COL_AS_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_AS_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_AS_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_KN2COL_AS_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::kn2col_as_A_BT_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::HWC);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_B: {
          switch(var) {

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::HWC);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_BT: {
          switch(var) {

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::HWC);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
    } break;

    case triNNity::GEMM_KI: {
      switch(gemm_transp) {

        case triNNity::GEMM_A_B: {
          switch(var) {
            case CONV_MULTI_KN2ROW_AS_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AS_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AS_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AS_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::kn2row_as_A_B_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_KN2ROW_AA_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, (bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD));
              dense::cpu::impl::kn2row_aa_A_B_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_A_BT: {
          switch(var) {

            case CONV_MULTI_KN2ROW_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::kn2row_A_BT_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_KN2ROW_AA_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
              dense::cpu::impl::kn2row_aa_A_BT_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWOI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_B: {
          switch(var) {

            case CONV_MULTI_KN2ROW_AA_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, (bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD));
              dense::cpu::impl::kn2row_aa_AT_B_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_BT: {
          switch(var) {

            case CONV_MULTI_KN2ROW_AA_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_KN2ROW_AA_GEMM, (bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD));
              dense::cpu::impl::kn2row_aa_AT_BT_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_CONV_1x1_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, krn_layout == triNNity::layout::HWIO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_CONV_1x1_GEMM, out_layout == triNNity::layout::CHW);
              TRINNITY_ASSERT_ON(var==CONV_MULTI_CONV_1x1_GEMM, k == 1);
              dense::cpu::impl::conv_1x1_gemm<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, gemm_transp, gemm_arr, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

/**
 * Indirect GEMM-based convolution by building an input patch matrix
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor
 * \endparblock
 * \tparam var
 * \parblock
 * The indirect GEMM-based convolution method to use
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Which GEMM implementation to use
 * \endparblock
 * \tparam gemm_arr
 * \parblock
 * Which arrangement of arguments GEMM should use
 * \endparblock
 * \tparam gemm_transp
 * \parblock
 * What transposition of the A and B matrices GEMM should use
 * \endparblock
 * \tparam img_layout
 * \parblock
 * The layout that the input image tensor is in
 * \endparblock
 * \tparam krn_layout
 * \parblock
 * The layout that the kernel tensor is in
 * \endparblock
 * \tparam out_layout
 * \parblock
 * The layout that the output tensor should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */

template <typename T, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k,
          conv_patch_gemm_impl_t var, boundary_t bound,
          gemm_variant_t gemm_var, gemm_transpose_t gemm_transp, gemm_arrangement_t gemm_arr,
          patch_impl_t patch,
          triNNity::layout::feature_map_layout_t img_layout,
          triNNity::layout::parameter_layout_t krn_layout,
          triNNity::layout::feature_map_layout_t out_layout,
          unsigned stride_w = 1, unsigned stride_h = 1,
          unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
static TRINNITY_INLINE void convolution_forward_patch_gemm(T * __restrict__ img, KD * __restrict__ kernel, T * __restrict__ out) {

  switch(gemm_arr) {
    case triNNity::GEMM_IK: {
      switch(gemm_transp) {

        case triNNity::GEMM_A_B: {
          switch(var) {

            case CONV_MULTI_IM2ROW_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, (img_layout == triNNity::layout::CHW  && patch == triNNity::PATCH_SCAN) || (img_layout == triNNity::layout::HWC  && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, (krn_layout == triNNity::layout::IHWO && patch == triNNity::PATCH_SCAN) || (krn_layout == triNNity::layout::HWIO && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, out_layout == triNNity::layout::HWC);
              dense::cpu::impl::im2row_A_B_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_A_BT: {
          switch(var) {

            case CONV_MULTI_IM2ROW_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, (img_layout == triNNity::layout::CHW  && patch == triNNity::PATCH_SCAN) || (img_layout == triNNity::layout::HWC  && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, (krn_layout == triNNity::layout::OIHW && patch == triNNity::PATCH_SCAN) || (krn_layout == triNNity::layout::OHWI && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2ROW_GEMM, out_layout == triNNity::layout::HWC);
              dense::cpu::impl::im2row_A_BT_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_MEC_ROW_PARTITION: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_MEC_ROW_PARTITION, img_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_MEC_ROW_PARTITION, krn_layout == triNNity::layout::OHWI);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_MEC_ROW_PARTITION, out_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_MEC_ROW_PARTITION, patch == triNNity::PATCH_ANY);
              dense::cpu::impl::mec_row_partition<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, stride_w, stride_h>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_B: {
          switch(var) {

            case CONV_MULTI_IM2COL_GEMM: {
              TRINNITY_ASSERT_ON((var == CONV_MULTI_IM2COL_GEMM) && (patch == triNNity::PATCH_COPY_SHORT || patch == triNNity::PATCH_COPY_LONG), (stride_w == 1 && stride_h == 1));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, krn_layout == triNNity::layout::IHWO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, out_layout == triNNity::layout::HWC);
              dense::cpu::impl::im2col_AT_B_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_BT: {
          switch(var) {

            case CONV_MULTI_IM2COL_GEMM: {
              TRINNITY_ASSERT_ON((var == CONV_MULTI_IM2COL_GEMM) && (patch == triNNity::PATCH_COPY_SHORT || patch == triNNity::PATCH_COPY_LONG), (stride_w == 1 && stride_h == 1));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, krn_layout == triNNity::layout::OIHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_IK && var==CONV_MULTI_IM2COL_GEMM, out_layout == triNNity::layout::HWC);
              dense::cpu::impl::im2col_AT_BT_I_K<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
    } break;

    case triNNity::GEMM_KI: {
      switch(gemm_transp) {

        case triNNity::GEMM_A_B: {
          switch(var) {
            case CONV_MULTI_IM2COL_GEMM: {
              TRINNITY_ASSERT_ON((var == CONV_MULTI_IM2COL_GEMM) && (patch == triNNity::PATCH_COPY_SHORT || patch == triNNity::PATCH_COPY_LONG), (stride_w == 1 && stride_h == 1));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, krn_layout == triNNity::layout::OIHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::im2col_A_B_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            case CONV_MULTI_MEC_COL: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_MEC_COL, img_layout == triNNity::layout::HCW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_MEC_COL, krn_layout == triNNity::layout::OHIW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_MEC_COL, out_layout == triNNity::layout::HWC);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_B && gemm_arr==GEMM_KI && var==CONV_MULTI_MEC_COL, patch == triNNity::PATCH_ANY);
              dense::cpu::impl::mec_col<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, stride_w, stride_h>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_A_BT: {
          switch(var) {

            case CONV_MULTI_IM2ROW_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, (img_layout == triNNity::layout::CHW  && patch == triNNity::PATCH_SCAN) || (img_layout == triNNity::layout::HWC  && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, (krn_layout == triNNity::layout::OIHW && patch == triNNity::PATCH_SCAN) || (krn_layout == triNNity::layout::OHWI && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_A_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::im2row_A_BT_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_B: {
          switch(var) {

            case CONV_MULTI_IM2COL_GEMM: {
              TRINNITY_ASSERT_ON((var == CONV_MULTI_IM2COL_GEMM) && (patch == triNNity::PATCH_COPY_SHORT || patch == triNNity::PATCH_COPY_LONG), (stride_w == 1 && stride_h == 1));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, img_layout == triNNity::layout::CHW);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, krn_layout == triNNity::layout::IHWO);
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_B && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2COL_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::im2col_AT_B_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        case triNNity::GEMM_AT_BT: {
          switch(var) {

            case CONV_MULTI_IM2ROW_GEMM: {
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, (img_layout == triNNity::layout::CHW  && patch == triNNity::PATCH_SCAN) || (img_layout == triNNity::layout::HWC  && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, (krn_layout == triNNity::layout::IHWO && patch == triNNity::PATCH_SCAN) || (krn_layout == triNNity::layout::HWIO && patch == triNNity::PATCH_COPY_SHORT));
              TRINNITY_STATIC_ASSERT_ON(gemm_transp==GEMM_AT_BT && gemm_arr==GEMM_KI && var==CONV_MULTI_IM2ROW_GEMM, out_layout == triNNity::layout::CHW);
              dense::cpu::impl::im2row_AT_BT_K_I<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, k, bound, patch, stride_w, stride_h, gemm_block_i, gemm_block_j, gemm_block_k>(img, kernel, out);
            } break;

            default: {
              TRINNITY_ERROR("Not implemented");
            } break;
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

}

}

#endif // TRINNITY_DENSE_CPU_MCMK_H
