/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_GEMM_H
#define TRINNITY_DENSE_CPU_GEMM_H

// This is a bunch of preprocessor nonsense that
// figures out what GEMM implementation to use.
// We use a wrapper function gemm() as the single point of entry.


#if defined(TRINNITY_USE_ARMCL_GEMM)
extern "C" {
  #include <cblas.h>
}
#include "arm_compute/core/NEON/kernels/assembly/arm_gemm.hpp"
#include "arm_compute/runtime/CPUUtils.h"
#include <mutex>

static TRINNITY_INLINE bool is_cblas_transpose(CBLAS_TRANSPOSE transpose) {
  return transpose == CblasNoTrans ? false : true;
}

static const arm_compute::CPUInfo& get_ci() {
  static arm_compute::CPUInfo cpuInfo;
  static bool cpuInfoInitialized = false;
  if (!cpuInfoInitialized) {
    static std::mutex cpuInfoInitMutex;
    std::lock_guard<std::mutex> lck(cpuInfoInitMutex);
    if (!cpuInfoInitialized) {
      arm_compute::get_cpu_configuration(cpuInfo);
    }
    cpuInfoInitialized = true;
  }
  return cpuInfo;
}

void arm_cblas_init() {
  get_ci();
}

template<typename IN_TYPE, typename OUT_TYPE>
void cblas_gemm_armcl(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                      CBLAS_TRANSPOSE TransB, const int M, const int N, const int K,
                      const OUT_TYPE alpha, const IN_TYPE *A, const int lda,
                      const IN_TYPE *B, const int ldb, const OUT_TYPE beta, OUT_TYPE *C,
                      const int ldc) {
  TRINNITY_ASSERT(layout == CblasRowMajor);

  bool trA = is_cblas_transpose(TransA);
  bool trB = is_cblas_transpose(TransB);
  const int max_threads = 1;

  auto gemm_operation = arm_gemm::gemm<IN_TYPE, OUT_TYPE>(get_ci(), M, N, K, 1, 1, trA, trB, alpha, beta, max_threads, false);
  size_t workingSize = gemm_operation->get_working_size();
  uint8_t* tmpMemory = new uint8_t[workingSize];
  gemm_operation->set_working_space(tmpMemory);
  gemm_operation->set_arrays(A, lda, 0, 0, B, ldb, 0, C, ldc, 0, 0);
  auto windowSize = gemm_operation->get_window_size();
  gemm_operation->execute(0, windowSize, 0);

  delete [] tmpMemory;
}

template<typename IN_TYPE, typename OUT_TYPE, typename PASSED_IN_TYPE, typename PASSED_OUT_TYPE>
void cblas_gemm_armcl_complex(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                              CBLAS_TRANSPOSE TransB, const int M, const int N, const int K,
                              const PASSED_OUT_TYPE *alpha, const PASSED_IN_TYPE *A, const int lda,
                              const PASSED_IN_TYPE *B, const int ldb, const PASSED_OUT_TYPE *beta, PASSED_OUT_TYPE *C,
                              const int ldc) {
  TRINNITY_ASSERT(layout == CblasRowMajor);

  bool trA = is_cblas_transpose(TransA);
  bool trB = is_cblas_transpose(TransB);
  const int max_threads = 1;

  auto gemm_operation = arm_gemm::gemm<IN_TYPE, OUT_TYPE>(get_ci(), M, N, K, 1, 1, trA, trB, *(reinterpret_cast<IN_TYPE*>(alpha)), *(reinterpret_cast<IN_TYPE*>(beta)), max_threads, false);
  size_t workingSize = gemm_operation->get_working_size();
  uint8_t* tmpMemory = new uint8_t[workingSize];
  gemm_operation->set_working_space(tmpMemory);
  gemm_operation->set_arrays((reinterpret_cast<IN_TYPE*>(A)), lda, 0, 0, (reinterpret_cast<IN_TYPE*>(B)), ldb, 0, (reinterpret_cast<OUT_TYPE*>(C)), ldc, 0, 0);
  auto windowSize = gemm_operation->get_window_size();
  gemm_operation->execute(0, windowSize, 0);

  delete [] tmpMemory;
}
#elif defined(TRINNITY_USE_MKL_GEMM)
#include <mkl.h>
#elif defined(TRINNITY_USE_CBLAS_GEMM)
extern "C" {
  #include <cblas.h>
}
#elif defined(TRINNITY_USE_LIBXSMM_GEMM)
#include <libxsmm.h>

#define TRINNITY_CBLAS_COLUMN_MAJOR

#elif defined(TRINNITY_USE_CUBLAS_GEMM)
#include <cublas_v2.h>
#include <cuda_runtime.h>

#elif defined(TRINNITY_USE_CLBLAS_GEMM)
#include <clBLAS.h>

#if defined(TRINNITY_CLBLAS_USE_CPU)
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_CPU
#elif defined(TRINNITY_CLBLAS_USE_GPU)
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_GPU
#else
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_DEFAULT
#endif

typedef struct {
  size_t offset_A;
  size_t offset_B;
  size_t offset_C;
  cl_uint num_command_queues;
  cl_command_queue * command_queues;
  cl_uint num_events_in_wait_list;
  cl_event * event_wait_list;
  cl_event * events;
} Trinnity_Clblas_Handle;

#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#include <clblast_c.h>

#if defined(TRINNITY_CLBLAS_USE_CPU)
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_CPU
#elif defined(TRINNITY_CLBLAS_USE_GPU)
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_GPU
#else
#define TRINNITY_CL_DEVICE_TYPE CL_DEVICE_TYPE_DEFAULT
#endif

typedef struct {
  size_t offset_A;
  size_t offset_B;
  size_t offset_C;
  cl_command_queue * command_queues;
  cl_event * events;
} Trinnity_Clblas_Handle;

#endif

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/dense/cpu/primitive.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

#include <complex>

#if defined(TRINNITY_BLAS_REPORT_PARAMS)
#include <iostream>
#include <fstream>
#if !defined(TRINNITY_BLAS_REPORT_FILE)
#define TRINNITY_BLAS_REPORT_FILE "triNNity-blas-report.csv"
#endif
#endif


#if defined(TRINNITY_USE_MKL_GEMM) || defined(TRINNITY_USE_CBLAS_GEMM) || defined (TRINNITY_USE_CLBLAS_GEMM) || defined (TRINNITY_USE_CLBLAST_GEMM) || defined(TRINNITY_USE_ARMCL_GEMM)
// These can use either layout, so default to row-major storage if unset
#if !defined(TRINNITY_CBLAS_COLUMN_MAJOR) && !defined(TRINNITY_CBLAS_ROW_MAJOR)
#define TRINNITY_CBLAS_ROW_MAJOR
#endif

#elif defined(TRINNITY_USE_LIBXSMM_GEMM) || defined(TRINNITY_USE_CUBLAS_GEMM)
// for these column major is the only supported mode
#undef TRINNITY_CBLAS_ROW_MAJOR
#define TRINNITY_CBLAS_COLUMN_MAJOR
#endif

/*
* Different GEMM libraries make conflicting assumptions about the layout of matrices
* being passed to GEMM.
*
* There are two ways to lay out the matrices in memory: row-major order, where all elements
* in a row are consecutive in memory, and column-major order, where all elements down a column
* are consecutive in memory
*
* Row-major layout is the default for multidimensional arrays in C and C++, while column-major is
* the default in FORTRAN.
*
* The order in which the parameters to GEMM are passed depend on how the matrices are laid out.
*
* In order to simplify programming with GEMM, the TRINNITY_xGEMM macro presents a row-major interface to GEMM only.
*
* All code in the library assumes that matrix layout in memory is row-major, and calls GEMM with the correct
* row-major arguments. However, some libraries ONLY support the column-major style.
*
* Fortunately, there is a straightforward way of exchanging the arguments in the GEMM call so that a column-major GEMM can
* be used with row-major data. That's the purpose of the TRINNITY_xGEMM macro.
*
* If TRINNITY_CBLAS_ROW_MAJOR is set, we assume that the underlying matrices are actually
* stored in row-major order, and we just call GEMM normally. If TRINNITY_CBLAS_COLUMN_MAJOR is set,
* we translate the arguments we get into the corresponding column-major arguments. This means we don't
* have to deal with array layout problems everywhere we call GEMM in the library.
*
* Different BLAS libraries also sometimes have different requirements about which parameters
* are passed by reference, and which are passed by value. We also handle this in the TRINNITY_xGEMM macro.
*/

#if defined(TRINNITY_CBLAS_COLUMN_MAJOR) // Assume GEMM wants arrays in column major layout, so transform the calls

// DGEMM
#if defined(TRINNITY_USE_LIBXSMM_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
libxsmm_dgemm(transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", &n, &m, &k, &alpha, B, &ldb, A, &lda, &beta, C, &ldc);
#elif defined(TRINNITY_USE_CUBLAS_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cublasDgemm(handle, transf_B==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, transf_A==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, n, m, k, &alpha, B, ldb, A, lda, &beta, C, ldc);
#elif defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasDgemm (clblasColumnMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastDgemm (CLBlastLayoutColMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl<double, double>(CblasColMajor, transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", n, m, k, alpha, B, ldb, A, lda, beta, C, ldc);
#else
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_dgemm(CblasColMajor, transf_B, transf_A, n, m, k, alpha, B, ldb, A, lda, beta, C, ldc)
#endif

// SGEMM
#if defined(TRINNITY_USE_LIBXSMM_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
libxsmm_sgemm(transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", &n, &m, &k, &alpha, B, &ldb, A, &lda, &beta, C, &ldc);
#elif defined(TRINNITY_USE_CUBLAS_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cublasSgemm(handle, transf_B==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, transf_A==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, n, m, k, &alpha, B, ldb, A, lda, &beta, C, ldc);
#elif defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasSgemm (clblasColumnMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastSgemm(CLBlastLayoutColMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl<float, float>(CblasColMajor, transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", n, m, k, alpha, B, ldb, A, lda, beta, C, ldc);
#else
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_sgemm(CblasColMajor, transf_B, transf_A, n, m, k, alpha, B, ldb, A, lda, beta, C, ldc)
#endif

// CGEMM
#if defined(TRINNITY_USE_LIBXSMM_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
libxsmm_cgemm(transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", &n, &m, &k, &alpha, B, &ldb, A, &lda, &beta, C, &ldc);
#elif defined(TRINNITY_USE_CUBLAS_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cublasCgemm(handle, transf_B==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, transf_A==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, n, m, k, &alpha, B, ldb, A, lda, &beta, C, ldc);
#elif defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasCgemm (clblasColumnMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastCgemm(CLBlastLayoutColMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl_complex<std::complex<float>, std::complex<float>, float, float>(CblasColMajor, transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", n, m, k, alpha, B, ldb, A, lda, beta, C, ldc);
#else
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_cgemm(CblasColMajor, transf_B, transf_A, n, m, k, alpha, B, ldb, A, lda, beta, C, ldc)
#endif

// ZGEMM
#if defined(TRINNITY_USE_LIBXSMM_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
libxsmm_zgemm(transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", &n, &m, &k, &alpha, B, &ldb, A, &lda, &beta, C, &ldc);
#elif defined(TRINNITY_USE_CUBLAS_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cublasZgemm(handle, transf_B==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, transf_A==CUBLAS_OP_T ? CUBLAS_OP_T : CUBLAS_OP_N, n, m, k, &alpha, B, ldb, A, lda, &beta, C, ldc);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastZgemm(CLBlastLayoutColMajor, transf_B, transf_A, n, m, k, alpha, B, (handle)->offset_B, ldb, A, (handle)->offset_A, lda, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl_complex<std::complex<double>, std::complex<double>, double, double>(CblasColMajor, transf_B==CblasTrans ? "T":"N", transf_A==CblasTrans ? "T":"N", n, m, k, alpha, B, ldb, A, lda, beta, C, ldc);
#else
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_zgemm(CblasColMajor, transf_B, transf_A, n, m, k, alpha, B, ldb, A, lda, beta, C, ldc)
#endif

/* The MKL gives us two special extra GEMM calls: s16s16s32 GEMM, and s8u8s32 GEMM. */
#if defined(TRINNITY_USE_MKL_GEMM)
#define TRINNITY_xGEMM(spec, handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_##spec(CblasColMajor, transf_B, transf_A, CblasFixOffset, n, m, k, alpha, B, ldb, 0, A, lda, 0, beta, C, ldc, handle)
#endif

#elif defined(TRINNITY_CBLAS_ROW_MAJOR) // Assume GEMM wants arrays in row-major layout, so don't transform the calls

// DGEMM
#if defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasDgemm (clblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastDgemm (CLBlastLayoutRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl<double, double>(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#else
#define TRINNITY_DGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_dgemm(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#endif

// SGEMM
#if defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasSgemm (clblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastSgemm (CLBlastLayoutRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl<float, float>(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#else
#define TRINNITY_SGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_sgemm(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#endif

// CGEMM
#if defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasCgemm (clblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastCgemm (CLBlastLayoutRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl_complex<std::complex<float>, std::complex<float>, float, float>(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#else
#define TRINNITY_CGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_cgemm(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#endif

// ZGEMM
#if defined(TRINNITY_USE_CLBLAS_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
clblasZgemm (clblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->num_command_queues, (handle)->command_queues, (handle)->num_events_in_wait_list, (handle)->event_wait_list, (handle)->events);
#elif defined(TRINNITY_USE_CLBLAST_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
CLBlastZgemm (CLBlastLayoutRowMajor, transf_A, transf_B, m, n, k, alpha, A, (handle)->offset_A, lda, B, (handle)->offset_B, ldb, beta, C, (handle)->offset_C, ldc, (handle)->command_queues, (handle)->events);
#elif defined(TRINNITY_USE_ARMCL_GEMM)
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_armcl_complex<std::complex<double>, std::complex<double>, double, double>(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#else
#define TRINNITY_ZGEMM(handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_zgemm(CblasRowMajor, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
#endif

/* The MKL gives us two special extra GEMM calls: s16s16s32 GEMM, and s8u8s32 GEMM. */
#if defined(TRINNITY_USE_MKL_GEMM)
#define TRINNITY_xGEMM(spec, handle, transf_A, transf_B, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) \
cblas_gemm_##spec(CblasRowMajor, transf_A, transf_B, CblasFixOffset, m, n, k, alpha, A, lda, 0, B, ldb, 0, beta, C, ldc, handle)
#endif

#else
#error Must define either TRINNITY_CBLAS_ROW_MAJOR or TRINNITY_CBLAS_COLUMN_MAJOR
#endif

namespace triNNity {

namespace dense {

namespace cpu {

namespace internal {

/*
*  All of our GEMM functions require the dimensions passed to be the dimensions *as declared in the calling subprogram*.
*  We never do automatic transposition of matrices.
*/

#if defined(TRINNITY_USE_MKL_GEMM) || defined(TRINNITY_USE_CBLAS_GEMM) || defined(TRINNITY_USE_LIBXSMM_GEMM) || defined(TRINNITY_USE_ARMCL_GEMM)

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const double* __restrict__ a, const double* __restrict__ b, gemm_accumulate_t accum, double* __restrict__ c) {

  double d_accum, d_alpha;
  d_alpha = 1.0;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0;
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_DGEMM(nullptr, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_DGEMM(nullptr, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_DGEMM(nullptr, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_DGEMM(nullptr, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const float* __restrict__ a, const float* __restrict__ b, gemm_accumulate_t accum, float* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_SGEMM(nullptr, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_SGEMM(nullptr, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_SGEMM(nullptr, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_SGEMM(nullptr, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<double>* __restrict__ a, const std::complex<double>* __restrict__ b, gemm_accumulate_t accum, std::complex<double>* __restrict__ c) {

  std::complex<double> d_accum(0.0, 0.0);
  std::complex<double> d_alpha(1.0, 0.0);

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = std::complex<double>(1.0, 0.0);
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = std::complex<double>(1.0, 0.0);
      d_alpha = std::complex<double>(-1.0, 0.0);
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = std::complex<double>(0.0, 0.0);
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "ZGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_ZGEMM(nullptr, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, reinterpret_cast<double*>(&d_alpha), reinterpret_cast<const double*>(a), mat_a_w, reinterpret_cast<const double*>(b), mat_b_w, reinterpret_cast<double*>(&d_accum), reinterpret_cast<double*>(c), mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "ZGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_ZGEMM(nullptr, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, reinterpret_cast<double*>(&d_alpha), reinterpret_cast<const double*>(a), mat_a_w, reinterpret_cast<const double*>(b), mat_b_w, reinterpret_cast<double*>(&d_accum), reinterpret_cast<double*>(c), mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "ZGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_ZGEMM(nullptr, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, reinterpret_cast<double*>(&d_alpha), reinterpret_cast<const double*>(a), mat_a_w, reinterpret_cast<const double*>(b), mat_b_w, reinterpret_cast<double*>(&d_accum), reinterpret_cast<double*>(c), mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "ZGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_ZGEMM(nullptr, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, reinterpret_cast<double*>(&d_alpha), reinterpret_cast<const double*>(a), mat_a_w, reinterpret_cast<const double*>(b), mat_b_w, reinterpret_cast<double*>(&d_accum), reinterpret_cast<double*>(c), mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<float>* __restrict__ a, const std::complex<float>* __restrict__ b, gemm_accumulate_t accum, std::complex<float>* __restrict__ c) {
  std::complex<float> d_accum(0.0f, 0.0f);
  std::complex<float> d_alpha(1.0f, 0.0f);

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = std::complex<float>(1.0f, 0.0f);
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = std::complex<float>(1.0f, 0.0f);
      d_alpha = std::complex<float>(-1.0f, 0.0f);
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = std::complex<float>(0.0f, 0.0f);
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "CGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_CGEMM(nullptr, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, reinterpret_cast<float*>(&d_alpha), reinterpret_cast<const float*>(a), mat_a_w, reinterpret_cast<const float*>(b), mat_b_w, reinterpret_cast<float*>(&d_accum), reinterpret_cast<float*>(c), mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "CGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_CGEMM(nullptr, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, reinterpret_cast<float*>(&d_alpha), reinterpret_cast<const float*>(a), mat_a_w, reinterpret_cast<const float*>(b), mat_b_w, reinterpret_cast<float*>(&d_accum), reinterpret_cast<float*>(c), mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "CGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_CGEMM(nullptr, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, reinterpret_cast<float*>(&d_alpha), reinterpret_cast<const float*>(a), mat_a_w, reinterpret_cast<const float*>(b), mat_b_w, reinterpret_cast<float*>(&d_accum), reinterpret_cast<float*>(c), mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "CGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_CGEMM(nullptr, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, reinterpret_cast<float*>(&d_alpha), reinterpret_cast<const float*>(a), mat_a_w, reinterpret_cast<const float*>(b), mat_b_w, reinterpret_cast<float*>(&d_accum), reinterpret_cast<float*>(c), mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

#if defined(TRINNITY_USE_MKL_GEMM)

/* The MKL gives us two special GEMM calls: s16s16s32 GEMM, and s8u8s32 GEMM. */

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const int16_t* __restrict__ a, const int16_t* __restrict__ b, gemm_accumulate_t accum, int32_t* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  int32_t d_oc[1] = {0};

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S16S16S32" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s16s16s32, d_oc, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S16S16S32" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s16s16s32, d_oc, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S16S16S32" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s16s16s32, d_oc, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S16S16S32" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s16s16s32, d_oc, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void blas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const int8_t* __restrict__ a, const uint8_t* __restrict__ b, gemm_accumulate_t accum, int32_t* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  int32_t d_oc[1] = {0};

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S8U8S32" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s8u8s32, d_oc, CblasNoTrans, CblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S8U8S32" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s8u8s32, d_oc, CblasNoTrans, CblasTrans,   mat_a_h, mat_b_h, mat_a_w, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S8U8S32" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s8u8s32, d_oc, CblasTrans, CblasNoTrans,   mat_a_w, mat_b_w, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_w);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "GEMM_S8U8S32" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      TRINNITY_xGEMM(s8u8s32, d_oc, CblasTrans, CblasTrans,     mat_a_w, mat_b_h, mat_a_h, d_alpha, a, mat_a_w, b, mat_b_w, d_accum, c, mat_b_h);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

#endif // defined(TRINNITY_USE_MKL_GEMM)

#endif // defined(TRINNITY_USE_MKL_GEMM) || defined(TRINNITY_USE_CBLAS_GEMM) || defined(TRINNITY_USE_LIBXSMM_GEMM) || defined(TRINNITY_USE_ARMCL_GEMM)

#if defined(TRINNITY_USE_CUBLAS_GEMM)

template <gemm_transpose_t transp>
static TRINNITY_INLINE void cublas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const double* __restrict__ a, const double* __restrict__ b, gemm_accumulate_t accum, double* __restrict__ c) {}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void cublas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<float>* __restrict__ a, const std::complex<float>* __restrict__ b, gemm_accumulate_t accum, std::complex<float>* __restrict__ c) {}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void cublas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<double>* __restrict__ a, const std::complex<double>* __restrict__ b, gemm_accumulate_t accum, std::complex<double>* __restrict__ c) {}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void cublas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const float* __restrict__ a, const float* __restrict__ b, gemm_accumulate_t accum, float* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  cublasHandle_t handle;
  cublasCreate(&handle);

  // Allocate device arrays
  float *d_A, *d_B, *d_C;
  cudaMalloc(&d_A, mat_a_h * mat_a_w * sizeof(float));
  cudaMalloc(&d_B, mat_b_h * mat_b_w * sizeof(float));

  // Push arrays
  cudaMemcpy(d_A, a, mat_a_h * mat_a_w * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_B, b, mat_b_h * mat_b_w * sizeof(float), cudaMemcpyHostToDevice);

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      cudaMalloc(&d_C, mat_a_h * mat_b_w * sizeof(float));
      TRINNITY_SGEMM(handle, CUBLAS_OP_N, CUBLAS_OP_N, mat_a_h, mat_b_w, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      cudaMemcpy(c, d_C, mat_a_h * mat_b_w * sizeof(float), cudaMemcpyDeviceToHost);
    } break;

    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      cudaMalloc(&d_C, mat_a_h * mat_b_h * sizeof(float));
      TRINNITY_SGEMM(handle, CUBLAS_OP_N, CUBLAS_OP_T,   mat_a_h, mat_b_h, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      cudaMemcpy(c, d_C, mat_a_h * mat_b_h * sizeof(float), cudaMemcpyDeviceToHost);
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      cudaMalloc(&d_C, mat_a_w * mat_b_w * sizeof(float));
      TRINNITY_SGEMM(handle, CUBLAS_OP_T, CUBLAS_OP_N,   mat_a_w, mat_b_w, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      cudaMemcpy(c, d_C, mat_a_w * mat_b_w * sizeof(float), cudaMemcpyDeviceToHost);
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      cudaMalloc(&d_C, mat_a_w * mat_b_h * sizeof(float));
      TRINNITY_SGEMM(handle, CUBLAS_OP_T, CUBLAS_OP_T,     mat_a_w, mat_b_h, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      cudaMemcpy(c, d_C, mat_a_w * mat_b_h * sizeof(float), cudaMemcpyDeviceToHost);
    } break;
  }

  // Free device buffers
  cudaFree(d_A);
  cudaFree(d_B);
  cudaFree(d_C);

  cublasDestroy(handle);
}

#endif // defined(TRINNITY_USE_CUBLAS_GEMM)

#if defined(TRINNITY_USE_CLBLAS_GEMM)
template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const double* __restrict__ a, const double* __restrict__ b, gemm_accumulate_t accum, double* __restrict__ c) {

  double d_accum, d_alpha;
  d_alpha = 1.0;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0;
      d_alpha = -1.0;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0;
    } break;
  }

  cl_platform_id platform = 0;
  cl_device_id device_id = 0;
  cl_context_properties context_properties[3] = {CL_CONTEXT_PLATFORM, 0, 0 };
  cl_context context = 0;
  cl_command_queue queue = 0;
  cl_mem d_A, d_B, d_C;
  cl_event event = NULL;
  cl_int err;
  Trinnity_Clblas_Handle handle;

  // Setup OpenCL environment
  err = clGetPlatformIDs(1, &platform, NULL);
  err = clGetDeviceIDs(platform, TRINNITY_CL_DEVICE_TYPE, 1, &device_id, NULL);
  context_properties[1] = (cl_context_properties)platform;
  context = clCreateContext(context_properties, 1, &device_id, NULL, NULL, &err);
  queue = clCreateCommandQueue(context, device_id, 0, &err);

  // Setup clBLAS
  err = clblasSetup();

  // Allocate device arrays
  d_A = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_a_h * mat_a_w * sizeof(double), NULL, &err);
  d_B = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_b_h * mat_b_w * sizeof(double), NULL, &err);

  // Place matrices using blocking write operations
  err = clEnqueueWriteBuffer(queue, d_A, CL_TRUE, 0, mat_a_h * mat_a_w * sizeof(double), a, 0, NULL, NULL);
  err = clEnqueueWriteBuffer(queue, d_B, CL_TRUE, 0, mat_b_h * mat_b_w * sizeof(double), b, 0, NULL, NULL);

  handle.offset_A = 0;
  handle.offset_B = 0;
  handle.offset_C = 0;
  handle.num_command_queues = 1;
  handle.command_queues = &queue;
  handle.num_events_in_wait_list = 0;
  handle.event_wait_list = NULL;
  handle.events = &event;

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_w * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), clblasNoTrans, clblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      }
    } break;
    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_h * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), clblasNoTrans, clblasTrans, mat_a_h, mat_b_h, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      }
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_w * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), clblasTrans, clblasNoTrans, mat_a_w, mat_b_w, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      }
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_h * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), clblasTrans, clblasTrans, mat_a_w, mat_b_h, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
  // Release resources
  clReleaseEvent(event);
  clReleaseMemObject(d_C);
  clReleaseMemObject(d_B);
  clReleaseMemObject(d_A);
  clblasTeardown();
  clReleaseCommandQueue(queue);
  clReleaseContext(context);
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const float* __restrict__ a, const float* __restrict__ b, gemm_accumulate_t accum, float* __restrict__ c) {

  float d_accum, d_alpha;
  d_alpha = 1.0f;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  cl_platform_id platform = 0;
  cl_device_id device_id = 0;
  cl_context_properties context_properties[3] = {CL_CONTEXT_PLATFORM, 0, 0 };
  cl_context context = 0;
  cl_command_queue queue = 0;
  cl_mem d_A, d_B, d_C;
  cl_event event = NULL;
  cl_int err;
  Trinnity_Clblas_Handle handle;

  // Setup OpenCL environment
  err = clGetPlatformIDs(1, &platform, NULL);
  err = clGetDeviceIDs(platform, TRINNITY_CL_DEVICE_TYPE, 1, &device_id, NULL);
  context_properties[1] = (cl_context_properties)platform;
  context = clCreateContext(context_properties, 1, &device_id, NULL, NULL, &err);
  queue = clCreateCommandQueue(context, device_id, 0, &err);

  // Setup clBLAS
  err = clblasSetup();

  // Allocate device arrays
  d_A = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_a_h * mat_a_w * sizeof(float), NULL, &err);
  d_B = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_b_h * mat_b_w * sizeof(float), NULL, &err);

  // Place matrices using blocking write operations
  err = clEnqueueWriteBuffer(queue, d_A, CL_TRUE, 0, mat_a_h * mat_a_w * sizeof(float), a, 0, NULL, NULL);
  err = clEnqueueWriteBuffer(queue, d_B, CL_TRUE, 0, mat_b_h * mat_b_w * sizeof(float), b, 0, NULL, NULL);

  handle.offset_A = 0;
  handle.offset_B = 0;
  handle.offset_C = 0;
  handle.num_command_queues = 1;
  handle.command_queues = &queue;
  handle.num_events_in_wait_list = 0;
  handle.event_wait_list = NULL;
  handle.events = &event;

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_w * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), clblasNoTrans, clblasNoTrans, mat_a_h, mat_b_w, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      }
    } break;
    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_h * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), clblasNoTrans, clblasTrans, mat_a_h, mat_b_h, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      }
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_w * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), clblasTrans, clblasNoTrans, mat_a_w, mat_b_w, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      }
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_h * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), clblasTrans, clblasTrans, mat_a_w, mat_b_h, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
  // Release resources
  clReleaseEvent(event);
  clReleaseMemObject(d_C);
  clReleaseMemObject(d_B);
  clReleaseMemObject(d_A);
  clblasTeardown();
  clReleaseCommandQueue(queue);
  clReleaseContext(context);
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<double>* __restrict__ a, const std::complex<double>* __restrict__ b, gemm_accumulate_t accum, std::complex<double>* __restrict__ c) {}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblas_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<float>* __restrict__ a, const std::complex<float>* __restrict__ b, gemm_accumulate_t accum, std::complex<float>* __restrict__ c) {}

#endif // if defined(TRINNITY_USE_CLBLAS_GEMM)

#if defined(TRINNITY_USE_CLBLAST_GEMM)

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblast_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const double* __restrict__ a, const double* __restrict__ b, gemm_accumulate_t accum, double* __restrict__ c) {
  double d_accum, d_alpha;
  d_alpha = 1.0;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0;
      d_alpha = -1.0;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0;
    } break;
  }

  cl_platform_id platform = 0;
  cl_device_id device_id = 0;
  cl_context_properties context_properties[3] = {CL_CONTEXT_PLATFORM, 0, 0 };
  cl_context context = 0;
  cl_command_queue queue = 0;
  cl_mem d_A, d_B, d_C;
  cl_event event = NULL;
  cl_int err;
  Trinnity_Clblas_Handle handle;

  // Setup OpenCL environment
  err = clGetPlatformIDs(1, &platform, NULL);
  err = clGetDeviceIDs(platform, TRINNITY_CL_DEVICE_TYPE, 1, &device_id, NULL);
  context_properties[1] = (cl_context_properties)platform;
  context = clCreateContext(context_properties, 1, &device_id, NULL, NULL, &err);
  queue = clCreateCommandQueue(context, device_id, 0, &err);

  // Allocate device arrays
  d_A = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_a_h * mat_a_w * sizeof(double), NULL, &err);
  d_B = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_b_h * mat_b_w * sizeof(double), NULL, &err);

  // Place matrices using blocking write operations
  err = clEnqueueWriteBuffer(queue, d_A, CL_TRUE, 0, mat_a_h * mat_a_w * sizeof(double), a, 0, NULL, NULL);
  err = clEnqueueWriteBuffer(queue, d_B, CL_TRUE, 0, mat_b_h * mat_b_w * sizeof(double), b, 0, NULL, NULL);

  handle.offset_A = 0;
  handle.offset_B = 0;
  handle.offset_C = 0;

  handle.command_queues = &queue;
  handle.events = &event;

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_w * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), CLBlastTransposeNo, CLBlastTransposeNo, mat_a_h, mat_b_w, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(double), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;
    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_h * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), CLBlastTransposeNo, CLBlastTransposeYes, mat_a_h, mat_b_h, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(double), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_w * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), CLBlastTransposeYes, CLBlastTransposeNo, mat_a_w, mat_b_w, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(double), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "DGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_h * sizeof(double), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(double), c, 0, NULL, NULL);
      err = TRINNITY_DGEMM((&handle), CLBlastTransposeYes, CLBlastTransposeYes, mat_a_w, mat_b_h, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(double), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
  // Release resources
  clReleaseMemObject(d_C);
  clReleaseMemObject(d_B);
  clReleaseMemObject(d_A);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblast_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const float* __restrict__ a, const float* __restrict__ b, gemm_accumulate_t accum, float* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  switch (accum) {
    case GEMM_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMM_SUBTRACT_ACCUMULATE: {
      d_accum = 1.0f;
      d_alpha = -1.0f;
    } break;

    case GEMM_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  cl_platform_id platform = 0;
  cl_device_id device_id = 0;
  cl_context_properties context_properties[3] = {CL_CONTEXT_PLATFORM, 0, 0 };
  cl_context context = 0;
  cl_command_queue queue = 0;
  cl_mem d_A, d_B, d_C;
  cl_event event = NULL;
  cl_int err;
  Trinnity_Clblas_Handle handle;

  // Setup OpenCL environment
  err = clGetPlatformIDs(1, &platform, NULL);
  err = clGetDeviceIDs(platform, TRINNITY_CL_DEVICE_TYPE, 1, &device_id, NULL);
  context_properties[1] = (cl_context_properties)platform;
  context = clCreateContext(context_properties, 1, &device_id, NULL, NULL, &err);
  queue = clCreateCommandQueue(context, device_id, 0, &err);

  // Allocate device arrays
  d_A = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_a_h * mat_a_w * sizeof(float), NULL, &err);
  d_B = clCreateBuffer(context, CL_MEM_READ_ONLY, mat_b_h * mat_b_w * sizeof(float), NULL, &err);

  // Place matrices using blocking write operations
  err = clEnqueueWriteBuffer(queue, d_A, CL_TRUE, 0, mat_a_h * mat_a_w * sizeof(float), a, 0, NULL, NULL);
  err = clEnqueueWriteBuffer(queue, d_B, CL_TRUE, 0, mat_b_h * mat_b_w * sizeof(float), b, 0, NULL, NULL);

  handle.offset_A = 0;
  handle.offset_B = 0;
  handle.offset_C = 0;

  handle.command_queues = &queue;
  handle.events = &event;

  switch (transp) {
    case GEMM_A_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "N" << ", " << mat_a_h << ", " << mat_b_w << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_w * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), CLBlastTransposeNo, CLBlastTransposeNo, mat_a_h, mat_b_w, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_w * sizeof(float), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;
    case GEMM_A_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "N" << ", " << "T" << ", " << mat_a_h << ", " << mat_b_h << ", " << mat_a_w << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_h * mat_b_h * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), CLBlastTransposeNo, CLBlastTransposeYes, mat_a_h, mat_b_h, mat_a_w, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_h * mat_b_h * sizeof(float), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    case GEMM_AT_B: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "N" << ", " << mat_a_w << ", " << mat_b_w << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_w * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), CLBlastTransposeYes, CLBlastTransposeNo, mat_a_w, mat_b_w, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_w);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_w * sizeof(float), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    case GEMM_AT_BT: {
      #if defined(TRINNITY_BLAS_REPORT_PARAMS)
      std::ofstream report;
      report.open(TRINNITY_BLAS_REPORT_FILE, std::ofstream::out | std::ofstream::app);
      report << "SGEMM" << ", " << "T" << ", " << "T" << ", " << mat_a_w << ", " << mat_b_h << ", " << mat_a_h << std::endl;
      report.flush();
      report.close();
      #endif
      d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, mat_a_w * mat_b_h * sizeof(float), NULL, &err);
      err = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(float), c, 0, NULL, NULL);
      err = TRINNITY_SGEMM((&handle), CLBlastTransposeYes, CLBlastTransposeYes, mat_a_w, mat_b_h, mat_a_h, d_alpha, d_A, mat_a_w, d_B, mat_b_w, d_accum, d_C, mat_b_h);
      // Wait and read result
      if constexpr (err == CL_SUCCESS) {
        err = clWaitForEvents(1, &event);
        err = clEnqueueReadBuffer(queue, d_C, CL_TRUE, 0, mat_a_w * mat_b_h * sizeof(float), c, 0, NULL, NULL);
        clReleaseEvent(event);
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
  // Release resources
  clReleaseMemObject(d_C);
  clReleaseMemObject(d_B);
  clReleaseMemObject(d_A);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);
}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblast_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<double>* __restrict__ a, const std::complex<double>* __restrict__ b, gemm_accumulate_t accum, std::complex<double>* __restrict__ c) {}

template <gemm_transpose_t transp>
static TRINNITY_INLINE void clblast_gemm_wrap (signed mat_a_w, signed mat_a_h, signed mat_b_w, signed mat_b_h, const std::complex<float>* __restrict__ a, const std::complex<float>* __restrict__ b, gemm_accumulate_t accum, std::complex<float>* __restrict__ c) {}

#endif // if defined(TRINNITY_USE_CLBLAST_GEMM)

/* If no backend GEMM provider library has been selected, use these built-in GEMM routines */

template <typename T, typename KD, typename C,
          gemm_accumulate_t accum=GEMM_ACCUMULATE,
          gemm_transpose_t transp=GEMM_A_B>
static TRINNITY_INLINE void gemm_simple(const unsigned mat_a_w, const unsigned mat_a_h,
                                        const unsigned mat_b_w, const unsigned mat_b_h,
                                        const T * __restrict__ a,
                                        const KD * __restrict__ b,
                                        C *__restrict__ c) {
  switch (transp) {
    case GEMM_A_B: {
      for (unsigned i = 0; i < mat_a_h; i+=1) { // for each row of a
        for(unsigned j = 0; j < mat_b_w; j+=1) { // for each column of b
          C sum = static_cast<C>(0);

          for(unsigned k=0; k < mat_b_h; k+=1) { // for each element in that column of b
            sum += (C)a[i*mat_a_w + k] * (C)b[k*mat_b_w + j];
          }

          switch (accum) {
            case GEMM_ACCUMULATE: {
              c[i*mat_b_w + j] += sum;
            } break;

            case GEMM_SUBTRACT_ACCUMULATE: {
              c[i*mat_b_w + j] -= sum;
            } break;

            case GEMM_NO_ACCUMULATE: {
              c[i*mat_b_w + j] = sum;
            } break;
          }
        }
      }
    } break;

    case GEMM_A_BT: {
      for(unsigned i = 0; i < mat_a_h; i+=1) { // for each row of a
        for(unsigned j = 0; j < mat_b_h; j+=1) { // for each row of b
          C sum = static_cast<C>(0);
          for(unsigned k = 0; k <mat_b_w; k+=1) { // for each column in a and b
            sum += (C) a[i*mat_b_w + k] * (C)b[j*mat_b_w + k];
          }
          switch (accum) {
            case GEMM_ACCUMULATE: {
              c[i*mat_b_h + j] += sum;
            } break;

            case GEMM_SUBTRACT_ACCUMULATE: {
              c[i*mat_b_h + j] -= sum;
            } break;

            case GEMM_NO_ACCUMULATE: {
              c[i*mat_b_h + j] = sum;
            } break;
          }
        }
      }
    } break;

    case GEMM_AT_B: {
      for (unsigned i = 0; i < mat_a_w; i+=1) { // for each column of a
        for (unsigned j = 0; j < mat_b_w; j+=1) { // for each column of b
          C sum = static_cast<C>(0);
          for (unsigned k = 0; k < mat_a_h; k+=1) { // for each row of a and b
            sum += (C) a[k*mat_a_w + i] * (C)b[k*mat_b_w + j];
          }
          switch (accum) {
            case GEMM_ACCUMULATE: {
              c[i*mat_b_w + j] += sum;
            } break;

            case GEMM_SUBTRACT_ACCUMULATE: {
              c[i*mat_b_w + j] -= sum;
            } break;

            case GEMM_NO_ACCUMULATE: {
              c[i*mat_b_w + j] = sum;
            } break;
          }
        }
      }
    } break;

    case GEMM_AT_BT: {
      for (unsigned i = 0; i < mat_a_w; i+=1) { // for each column of a
        for (unsigned j = 0; j < mat_b_h; j+=1) { // for each row of b
          C sum = static_cast<C>(0);
          for (unsigned k = 0; k < mat_a_h; k+=1) { // for each row of a
            sum += (C)a[k*mat_a_w + i] * (C)b[j*mat_b_w + k];
          }
          switch (accum) {
            case GEMM_ACCUMULATE: {
              c[i*mat_b_h + j] += sum;
            } break;

            case GEMM_SUBTRACT_ACCUMULATE: {
              c[i*mat_b_h + j] -= sum;
            } break;

            case GEMM_NO_ACCUMULATE: {
              c[i*mat_b_h + j] = sum;
            }
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <typename T, typename KD, typename C,
gemm_accumulate_t accum=GEMM_ACCUMULATE,
gemm_transpose_t transp=GEMM_A_B,
unsigned block_size_i=1,
unsigned block_size_j=1,
unsigned block_size_k=1>
static TRINNITY_INLINE void gemm_blocked(const unsigned mat_a_w, const unsigned mat_a_h,
  const unsigned mat_b_w, const unsigned mat_b_h,
  const T * __restrict__ a,
  const KD * __restrict__ b,
  C *__restrict__ c) {
    switch (transp) {
      case GEMM_A_B: {
        for (unsigned i = 0; i < mat_a_h; i+=block_size_i) {
          for (unsigned j = 0; j < mat_b_w; j+=block_size_j) {
            TRINNITY_ASSERT(mat_a_h % block_size_i == 0);
            TRINNITY_ASSERT(mat_b_w % block_size_j == 0);

            switch (accum) {
              case GEMM_NO_ACCUMULATE: {
                for (unsigned ii = 0; ii < block_size_i; ii++) {
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    c[(i+ii)*mat_b_w + j + jj] = 0;
                  }
                }
              } break;
              default: break;
            }

            for (unsigned k = 0; k < mat_a_w; k+=block_size_k) {
              TRINNITY_ASSERT(mat_a_w % block_size_k == 0);

              for (unsigned ii = 0; ii < block_size_i; ii++) {
                C sums[block_size_j] = {0};
                T priv_A, priv_B[block_size_j];
                const T *sub_a = &a[(i+ii)*mat_a_w + k];
                for (unsigned kk = 0; kk < block_size_k; kk++) {
                  priv_A = sub_a[kk];
                  const T *sub_b = &b[(k+kk)*mat_b_w + j];
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    priv_B[jj] = sub_b[jj];
                  }
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    sums[jj] += (C)priv_A * (C)priv_B[jj];
                  }
                }
                for (unsigned jj = 0; jj < block_size_j; jj++) {
                  c[(i+ii)*mat_b_w + j + jj] += sums[jj];
                }
              }
            }
          }
        }
      } break;

      case GEMM_A_BT: {
        for (unsigned i = 0; i < mat_a_h; i+=block_size_i) {
          for (unsigned j = 0; j < mat_b_h; j+=block_size_j) {
            TRINNITY_ASSERT(mat_a_h % block_size_i == 0);
            TRINNITY_ASSERT(mat_b_h % block_size_j == 0);

            switch (accum) {
              case GEMM_NO_ACCUMULATE: {
                for (unsigned ii = 0; ii < block_size_i; ii++) {
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    c[(i+ii)*mat_b_h + (j+jj)] = 0;
                  }
                }
              } break;
              default: break;
            }
            for (unsigned k = 0; k < mat_b_w; k+=block_size_k) {
              TRINNITY_ASSERT(mat_b_w % block_size_k == 0);

              for (unsigned ii = 0; ii < block_size_i; ii++) {
                C sums[block_size_j] = {0};
                T priv_A, priv_B[block_size_j];
                const T *sub_a = &a[(i+ii)*mat_a_w + k];
                for (unsigned kk = 0; kk < block_size_k; kk++) {
                  priv_A = sub_a[kk];
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    const T *sub_b = &b[(j+jj)*mat_b_w + (k+kk)];
                    priv_B[jj] = sub_b[0];
                  }
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    sums[jj] += (C)priv_A * (C)priv_B[jj];
                  }
                }
                for (unsigned jj = 0; jj < block_size_j; jj++) {
                  c[(i+ii)*mat_b_h + (j+jj)] += sums[jj];
                }
              }
            }
          }
        }
      } break;

      case GEMM_AT_B: {
        for (unsigned i = 0; i < mat_a_w; i+=block_size_i) {
          for (unsigned j = 0; j < mat_b_w; j+=block_size_j) {
            TRINNITY_ASSERT(mat_a_w % block_size_i == 0);
            TRINNITY_ASSERT(mat_b_w % block_size_j == 0);

            switch (accum) {
              case GEMM_NO_ACCUMULATE: {
                for (unsigned ii = 0; ii < block_size_i; ii++) {
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    c[(i+ii)*mat_b_w + (j+jj)] = 0;
                  }
                }
              } break;
              default : break;
            }
            for (unsigned k = 0; k < mat_a_h; k+=block_size_k) {
              TRINNITY_ASSERT(mat_a_h % block_size_k == 0);

              for (unsigned ii = 0; ii < block_size_i; ii++) {
                C sums[block_size_j] = {0};
                T priv_A, priv_B[block_size_j];
                for (unsigned kk = 0; kk < block_size_k; kk++) {
                  const T *sub_a = &a[(k+kk)*mat_a_w + (i+ii)];
                  priv_A = sub_a[0];
                  const T *sub_b = &b[(k+kk)*mat_b_w + j];
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    priv_B[jj] = sub_b[jj];
                  }
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    sums[jj] += (C)priv_A * (C)priv_B[jj];
                  }
                }
                for (unsigned jj = 0; jj < block_size_j; jj++) {
                  c[(i+ii)*mat_b_w + (j+jj)] += sums[jj];
                }
              }
            }
          }
        }
      } break;

      case GEMM_AT_BT: {
        for (unsigned i = 0; i < mat_a_w; i+=block_size_i) {
          for (unsigned j = 0; j < mat_b_h; j+=block_size_j) {
            TRINNITY_ASSERT(mat_a_w % block_size_i == 0);
            TRINNITY_ASSERT(mat_b_h % block_size_j == 0);

            switch (accum) {
              case GEMM_NO_ACCUMULATE: {
                for (unsigned ii = 0; ii < block_size_i; ii++) {
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    c[(i+ii)*mat_b_h + (j+jj)] = 0;
                  }
                }
              } break;
              default: break;
            }
            for (unsigned k = 0; k < mat_a_h; k+=block_size_k) {
              TRINNITY_ASSERT(mat_a_h % block_size_k == 0);

              for (unsigned ii = 0; ii < block_size_i; ii++) {
                C sums[block_size_j] = {0};
                T priv_A, priv_B[block_size_j];
                for (unsigned kk = 0; kk < block_size_k; kk++) {
                  const T *sub_a = &a[(k+kk)*mat_a_w + (i+ii)];
                  priv_A = sub_a[0];
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    const T *sub_b = &b[(j+jj)*mat_b_w + (k+kk)];
                    priv_B[jj] = sub_b[0];
                  }
                  for (unsigned jj = 0; jj < block_size_j; jj++) {
                    sums[jj] += (C)priv_A * (C)priv_B[jj];
                  }
                }
                for (unsigned jj = 0; jj < block_size_j; jj++) {
                  c[(i+ii)*mat_b_h + (j+jj)] += sums[jj];
                }
              }
            }
          }
        }
      } break;

      default: {
        TRINNITY_ERROR("Not implemented");
      } break;
    }
  }

}


/**
* This function is a wrapper for the GEMM routine for matrix-matrix multiplication.
*
* \tparam variant
*
* \parblock
* For platforms where a high-performance BLAS library is available, this parameter can be set to GEMM_BLAS to
* perform the matrix multiplication using the BLAS library. In this case, define one of
* \c TRINNITY_USE_CBLAS_GEMM , \c TRINNITY_USE_MKL_GEMM , or \c TRINNITY_USE_CUBLAS_GEMM to select the implementation.
* You will need to ensure that the compiler and linker options are set so that the BLAS library is visible for compilation and linking.
* \endparblock
* \tparam accum
* \parblock
* Whether or not the GEMM routine should accumulate into the output.
* \endparblock
* \tparam transp
* \parblock
* Which of the matrices passed to GEMM are transposed, if any.
* \endparblock
* \tparam block_size_i Blocking factor of the \c i loop for matrix multiplication
* \tparam block_size_j Blocking factor of the \c j loop for matrix multiplication
* \tparam block_size_k Blocking factor of the \c k loop for matrix multiplication
*/
template <typename T, typename KD, typename C,
          gemm_variant_t variant,
          gemm_accumulate_t accum=GEMM_ACCUMULATE,
          gemm_transpose_t transp=GEMM_A_B,
          unsigned block_size_i=4,
          unsigned block_size_j=1,
          unsigned block_size_k=4>
static TRINNITY_INLINE void gemm(const unsigned mat_a_w, const unsigned mat_a_h,
                                 const unsigned mat_b_w, const unsigned mat_b_h,
                                 const T * __restrict__ a,
                                 const KD * __restrict__ b,
                                 C *__restrict__ c) {
  switch(variant) {
    case GEMM_SIMPLE: {
      triNNity::dense::cpu::internal::gemm_simple<T, KD, C, accum, transp>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, c);
    } break;

    case GEMM_BLOCKED: {
      triNNity::dense::cpu::internal::gemm_blocked<T, KD, C, accum, transp, block_size_i, block_size_j, block_size_k>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, c);
    } break;

    #if defined(TRINNITY_USE_CBLAS_GEMM) || defined(TRINNITY_USE_MKL_GEMM) || defined(TRINNITY_USE_LIBXSMM_GEMM) || defined(TRINNITY_USE_ARMCL_GEMM)
    case GEMM_BLAS: {
      triNNity::dense::cpu::internal::blas_gemm_wrap<transp>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, accum, c);
    } break;
    #elif defined(TRINNITY_USE_CUBLAS_GEMM)
    case GEMM_BLAS: {
      triNNity::dense::cpu::internal::cublas_gemm_wrap<transp>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, accum, c);
    } break;
    #elif defined(TRINNITY_USE_CLBLAS_GEMM)
    case GEMM_BLAS: {
      triNNity::dense::cpu::internal::clblas_gemm_wrap<transp>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, accum, c);
    } break;
    #elif defined(TRINNITY_USE_CLBLAST_GEMM)
    case GEMM_BLAS: {
      triNNity::dense::cpu::internal::clblast_gemm_wrap<transp>(mat_a_w, mat_a_h, mat_b_w, mat_b_h, a, b, accum, c);
    } break;
    #endif

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template< typename T,
          typename KD,
          typename C,
          gemm_variant_t variant,
          gemm_transpose_t transp=GEMM_A_B>
static TRINNITY_INLINE double gemm_working_space(const unsigned mat_a_w, const unsigned mat_a_h,
                                                 const unsigned mat_b_w, const unsigned mat_b_h) {
  double totalSpace = 0;
  switch(variant) {
    case GEMM_SIMPLE: { } break;
    case GEMM_BLOCKED: { } break;

    #if defined(TRINNITY_USE_CBLAS_GEMM) || defined(TRINNITY_USE_MKL_GEMM) || defined(TRINNITY_USE_LIBXSMM_GEMM) || defined(TRINNITY_USE_ARMCL_GEMM)
    case GEMM_BLAS: { } break;
    #elif defined(TRINNITY_USE_CUBLAS_GEMM)
    case GEMM_BLAS: {
      totalSpace += sizeof(float) * (mat_a_h * mat_a_w);
      totalSpace += sizeof(float) * (mat_b_h * mat_b_w);
      switch (transp) {
        case GEMM_A_B: totalSpace += sizeof(float) * (mat_a_h * mat_b_w); break;
        case GEMM_A_BT: totalSpace += sizeof(float) * (mat_a_h * mat_b_h); break;
        case GEMM_AT_B: totalSpace += sizeof(float) * (mat_a_w * mat_b_w); break;
        case GEMM_AT_BT: totalSpace += sizeof(float) * (mat_a_w * mat_b_h); break;
      }
    } break;
    #elif defined(TRINNITY_USE_CLBLAS_GEMM) || defined(TRINNITY_USE_CLBLAST_GEMM)
    case GEMM_BLAS: {
      totalSpace += sizeof(T) * (mat_a_h * mat_a_w);
      totalSpace += sizeof(T) * (mat_b_h * mat_b_w);
      switch (transp) {
        case GEMM_A_B: totalSpace += sizeof(T) * (mat_a_h * mat_b_w); break;
        case GEMM_A_BT: totalSpace += sizeof(T) * (mat_a_h * mat_b_h); break;
        case GEMM_AT_B: totalSpace += sizeof(T) * (mat_a_w * mat_b_w); break;
        case GEMM_AT_BT: totalSpace += sizeof(T) * (mat_a_w * mat_b_h); break;
      }
    } break;
    #endif
  }
  return totalSpace;
}

}

}

}

#endif // TRINNITY_DENSE_CPU_GEMM_H
