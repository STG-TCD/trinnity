/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_CONFIG_H
#define TRINNITY_DENSE_CPU_CONFIG_H

#include <triNNity/config.h>

namespace triNNity {

/**
 * Names for different kinds of direct convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_SUM_SINGLE,
} conv_direct_impl_t;

/**
 * Names for different kinds of indirect GEMM-based convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_KN2ROW_GEMM,
  CONV_MULTI_KN2ROW_AS_GEMM,
  CONV_MULTI_KN2ROW_AA_GEMM,
  CONV_MULTI_KN2COL_GEMM,
  CONV_MULTI_KN2COL_AS_GEMM,
  CONV_MULTI_CONV_1x1_GEMM
} conv_gemm_impl_t;

/**
 * Names for different kinds of indirect patch-building GEMM-based convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_IM2ROW_GEMM,
  CONV_MULTI_IM2COL_GEMM,
  CONV_MULTI_MEC_ROW_PARTITION,
  CONV_MULTI_MEC_COL
} conv_patch_gemm_impl_t;

/**
 * Different methods of building the patch matrix for indirect GEMM-based convolutions.
 */

typedef enum {
  PATCH_ANY,        /*!< Use any patch building strategy (implementation specific) */
  PATCH_SCAN,       /*!< Scan every input point to build each patch */
  PATCH_COPY_LONG,  /*!< Use block copying to build patches simultaneously : copies from the image in large chunks */
  PATCH_COPY_SHORT, /*!< Use block copying to build patches simultaneously : copies from the image in small chunks */
  PATCH_COPY_SELF   /*!< Use block copying to build patches simultaneously : copies from previously created patches */
} patch_impl_t;

/**
 * Different methods of doing Local Response Normalization
 */

typedef enum {
  LRN_SIMPLE,        /*!< Use the simple pointwise algorithm */
} lrn_impl_t;

/**
 * Specifier for windowed primitive operations to select between those with and without kernels.
 */

typedef enum {
  WINDOW_KERNEL,  /*!< The windowed primitive operation uses a kernel */
  WINDOW_NOKERNEL /*!< The windowed primitive operation uses no kernel */
} window_kernel_t;

/**
 * Names for different kinds of windowed primitive operations implemented by the library.
 */

typedef enum {
  WINDOW_CONV2D,
  WINDOW_MAXPOOL,
  WINDOW_MINPOOL,
  WINDOW_AVGPOOL
} window_op_t;

/**
 * Specifier for behaviour of windowed primitive operations with respect to their output.
 */

typedef enum {
  WINDOW_ASSIGN,  /*!< The windowed primitive operation should overwrite output data points */
  WINDOW_ACCUM    /*!< The windowed primitive operation should accumulate into output data points */
} window_output_t;

/**
 * Names for different kinds of convolution boundary handling implemented by the library.
 */

typedef enum {
  BOUND_IMPLICIT_MIRROR,  /*!< Use mirroring for boundary pixels but don't construct the padded input */
  BOUND_IMPLICIT_PAD,     /*!< Use zero-padding for boundary pixels but don't construct the padded input */
  BOUND_EXPLICIT,         /*!< Assume input has been explicitly padded by the programmer */
  BOUND_UNDEF,            /*!< Output pixels which depend on boundary pixels in the input are undefined (can have any value) */
} boundary_t;

/**
 * Names for different kinds of explicit padding strategy.
 */

typedef enum {
  PAD_ZERO,   /*!<  Use zero padding */
  PAD_MIRROR, /*!<  Use mirroring */
  PAD_FAST,   /*!<  Use a fast padding strategy that pads with don't-care values */
} padding_t;

/**
 * Names for variants of GEMM implemented by the library.
 */

typedef enum {
  GEMM_SIMPLE,      /*!< Use a simple n-cubed GEMM implementation */
  GEMM_BLOCKED,     /*!< Use our blocked GEMM implementation */
  GEMM_BLAS         /*!< Use the BLAS GEMM implementation (requires linking with a BLAS library) */
} gemm_variant_t;

/**
 * What transposition of the A and B matrices to assume for GEMM operations.
 */

typedef enum {
  GEMM_A_B,   /*!< Neither A nor B transposed */
  GEMM_A_BT,  /*!< B but not A transposed */
  GEMM_AT_B,  /*!< A but not B transposed */
  GEMM_AT_BT  /*!< Both A and B transposed */
} gemm_transpose_t;

/**
 *  Specifier for indirect GEMM-based convolutions to select between different ways of combining the input feature maps and the kernel.
 *  This is required because matrix multiplication is not commutative, so (A x B) is different than (B x A).
 */

typedef enum {
  GEMM_IK,  /*!< Multiply the input feature maps by the kernel */
  GEMM_KI   /*!< Multiply the kernel by the input feature maps */
} gemm_arrangement_t;

/**
 * GEMM generalizes matrix multiplication as C = a(AB) + b(C) where a and b are scalars, and A, B, and C are matrices.
 *
 * This allows us to encode some important behaviour by setting the a and b parameters appropriately.
 * We refer to the behaviour by name, using this enumeration.
 */

typedef enum {
  GEMM_ACCUMULATE,          /*!< Accumulate into the output matrix (a=1, b=1) */
  GEMM_SUBTRACT_ACCUMULATE, /*!< Accumulate into the output matrix using subtraction (a=-1, b=1)*/
  GEMM_NO_ACCUMULATE        /*!< Overwrite the output matrix (a=1, b=0) */
} gemm_accumulate_t;

/**
 * How to do the offload to a GPU or accelerator for convolution.
 */

typedef enum {
  OFFLOAD_NONE, /*!< No offload should be done, everything runs on the host */
  OFFLOAD_GEMM, /*!< Only GEMM should be performed on the device */
  OFFLOAD_CONV  /*!< The entire convolution should be performed on the device */
} offload_t;

/**
 * Names for variants of GEMV implemented by the library.
 */

typedef enum {
  GEMV_SIMPLE,  /*!< Use a simple n-squared GEMV implementation */
  GEMV_BLAS     /*!< Use the BLAS GEMV implementation (requires linking with a BLAS library) */
} gemv_variant_t;

/**
 * What transposition of the A matrix to assume for GEMV operations.
 */

typedef enum {
  GEMV_A_B,   /*!< A not transposed */
  GEMV_AT_B,  /*!< A transposed */
} gemv_transpose_t;

/**
 * GEMV generalizes matrix-vector multiplication as C = a(AB) + b(C) where a and b are scalars, A and C are matrices, and B is a vector.
 *
 * This allows us to encode some important behaviour by setting the a and b parameters appropriately.
 * We refer to the behaviour by name, using this enumeration.
 */

typedef enum {
  GEMV_ACCUMULATE,    /*!< Accumulate into the output vector (a=1, b=1)  */
  GEMV_NO_ACCUMULATE  /*!< Overwrite the output vector (a=1, b=0)  */
} gemv_accumulate_t;

/**
 * Names for kinds of activation function implemented by the library
 */

typedef enum {
  ACTIVATION_NONE,  /*!< No activation */
  ACTIVATION_RELU   /*!< ReLU activation */
} activation_t;

/**
 * Names for different EltwiseLayer operations
 */

typedef enum {
  ELTWISE_ADD,
  ELTWISE_MUL,
  ELTWISE_SUB,
  ELTWISE_DIV,
  ELTWISE_MOD,
  ELTWISE_MAX,
  ELTWISE_MIN,
  ELTWISE_L1_NORM,
  ELTWISE_L2_NORM,
  ELTWISE_AVG,
  ELTWISE_BIT_AND,
  ELTWISE_BIT_OR,
  ELTWISE_BIT_XOR,
  ELTWISE_LOG_AND,
  ELTWISE_LOG_OR,
  ELTWISE_BIT_NAND,
  ELTWISE_BIT_NOR,
  ELTWISE_BIT_XNOR,
  ELTWISE_LOG_NAND,
  ELTWISE_LOG_NOR
} eltwise_op_t;

namespace layout {

/**
 * Describes the layout of an input or output feature map.
 * The feature map is a multi-dimensional tensor with some named dimensions:
 *
 * C is the number of channels (or feature maps),
 * H is the height of the feature maps,
 * W is the width of the feature maps.
 *
 * Some special formats have extra named dimensions (detailed in the individual format comments).
 * These descriptors don't specify a size for any of the dimensions; just the relative order of the dimensions (layout).
 */

typedef enum {
  CHW,    /*!< Tensor layout [channels][height][width] */
  HWC,    /*!< Tensor layout [height][width][channels] */
  HCW,    /*!< Tensor layout [height][channels][width] */
} feature_map_layout_t;

/**
 * Describes the layout of the multi-dimensional parameter tensor for a multi-channel multi-kernel convolution.
 *
 * O is the number of output feature maps (ofms),
 * I is the number of input feature maps (ifms),
 * H is the kernel height,
 * W is the kernel width.
 *
 * Some special formats have extra named dimensions (detailed in the individual format comments).
 *
 * These descriptors don't specify a size for any of the dimensions; just the relative order of the dimensions (layout).
 */

typedef enum {
  OIHW,   /*!< Tensor layout [ofms][ifms][height][width] */
  HWOI,   /*!< Tensor layout [height][width][ofms][ifms] */
  IHWO,   /*!< Tensor layout [ifms][height][width][ofms] */
  HWIO,   /*!< Tensor layout [height][width][ifms][ofms] */
  OHWI,   /*!< Tensor layout [ofms][height][width][ifms] */
  OHIW,   /*!< Tensor layout [ofms][height][ifms][width] */
} parameter_layout_t;

}

}

#endif // TRINNITY_DENSE_CPU_CONFIG_H
