/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_LAYER_H
#define TRINNITY_DENSE_CPU_LAYER_H

#include <cstddef>
#include <type_traits>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/layer.h>

#include <triNNity/dense/cpu/gemv.h>
#include <triNNity/dense/cpu/mcmk.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/memory.h>
#include <triNNity/dense/cpu/primitive.h>

namespace triNNity {

namespace layer {

/**
 * A layer which performs an element-wise operation on two input tensors.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam op
 * \parblock
 * The operation to perform
 * \endparblock
 * \tparam count
 * \parblock
 * The number of elements in the input tensors
 * \endparblock
 *
 */

template<typename T, triNNity::eltwise_op_t op, unsigned count>
struct EltwiseLayer: public Layer<T, T> {

  T *input_A, *input_B;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp simd
    #endif
    for (unsigned ix = 0; ix < count; ix++) {
      if constexpr(std::is_integral<T>::value) {
        switch (op) {
          case ELTWISE_ADD: { this->output[ix] = this->input_A[ix] + this->input_B[ix]; } break;
          case ELTWISE_MUL: { this->output[ix] = this->input_A[ix] * this->input_B[ix]; } break;
          case ELTWISE_SUB: { this->output[ix] = this->input_A[ix] - this->input_B[ix]; } break;
          case ELTWISE_DIV: { this->output[ix] = this->input_A[ix] / this->input_B[ix]; } break;
          case ELTWISE_MOD: { this->output[ix] = this->input_A[ix] % this->input_B[ix]; } break;
          case ELTWISE_MAX: { this->output[ix] = std::max(this->input_A[ix], this->input_B[ix]); } break;
          case ELTWISE_MIN: { this->output[ix] = std::min(this->input_A[ix], this->input_B[ix]); } break;
          case ELTWISE_L1_NORM: { this->output[ix] = std::abs(this->input_A[ix]) - std::abs(this->input_B[ix]); } break;
          case ELTWISE_L2_NORM: { this->output[ix] = std::sqrt(this->input_A[ix]*this->input_A[ix] - this->input_B[ix]*this->input_B[ix]); } break;
          case ELTWISE_AVG: { this->output[ix] = (this->input_A[ix] + this->input_B[ix]) / static_cast<T>(2); } break;
          case ELTWISE_BIT_AND: { this->output[ix] = this->input_A[ix] & this->input_B[ix]; } break;
          case ELTWISE_BIT_OR:  { this->output[ix] = this->input_A[ix] | this->input_B[ix]; } break;
          case ELTWISE_BIT_XOR: { this->output[ix] = this->input_A[ix] ^ this->input_B[ix]; } break;
          case ELTWISE_LOG_AND: { this->output[ix] = this->input_A[ix] && this->input_B[ix]; } break;
          case ELTWISE_LOG_OR:  { this->output[ix] = this->input_A[ix] || this->input_B[ix]; } break;
          case ELTWISE_BIT_NAND: { this->output[ix] = ~(this->input_A[ix] & this->input_B[ix]); } break;
          case ELTWISE_BIT_NOR: { this->output[ix] = ~(this->input_A[ix] | this->input_B[ix]); } break;
          case ELTWISE_BIT_XNOR: { this->output[ix] = ~(this->input_A[ix] ^ this->input_B[ix]); } break;
          case ELTWISE_LOG_NAND: { this->output[ix] = !(this->input_A[ix] && this->input_B[ix]); } break;
          case ELTWISE_LOG_NOR: { this->output[ix] = !(this->input_A[ix] || this->input_B[ix]); } break;

          default: {
            TRINNITY_ERROR("Unsupported op in EltwiseLayer");
          } break;
        }
      } else {
        switch (op) {
          case ELTWISE_ADD: { this->output[ix] = this->input_A[ix] + this->input_B[ix]; } break;
          case ELTWISE_MUL: { this->output[ix] = this->input_A[ix] * this->input_B[ix]; } break;
          case ELTWISE_SUB: { this->output[ix] = this->input_A[ix] - this->input_B[ix]; } break;
          case ELTWISE_DIV: { this->output[ix] = this->input_A[ix] / this->input_B[ix]; } break;
          case ELTWISE_MAX: { this->output[ix] = std::max(this->input_A[ix], this->input_B[ix]); } break;
          case ELTWISE_MIN: { this->output[ix] = std::min(this->input_A[ix], this->input_B[ix]); } break;
          case ELTWISE_L1_NORM: { this->output[ix] = std::abs(this->input_A[ix]) - std::abs(this->input_B[ix]); } break;
          case ELTWISE_L2_NORM: { this->output[ix] = std::sqrt(this->input_A[ix]*this->input_A[ix] - this->input_B[ix]*this->input_B[ix]); } break;
          case ELTWISE_AVG: { this->output[ix] = (this->input_A[ix] + this->input_B[ix]) / static_cast<T>(2); } break;

          default: {
            TRINNITY_ERROR("Unsupported op in EltwiseLayer");
          } break;
        }
      }
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    switch (op) {
      case ELTWISE_L1_NORM: { return static_cast<double>(2*count); } break;
      case ELTWISE_L2_NORM: { return static_cast<double>(4*count); } break;
      case ELTWISE_AVG: { return static_cast<double>(2*count); } break;
      default: { return static_cast<double>(count); } break;
    }
  }

  virtual double working_space() { return 0; }

  EltwiseLayer(T *inA, T *inB) {
    this->input_A = inA;
    this->input_B = inB;
    this->output = new T[count]();
    this->output_inplace = false;
  }

  EltwiseLayer(T *inA, T *inB, T *out) {
    this->input_A = inA;
    this->input_B = inB;
    this->output = out;
    this->output_inplace = true;
  }

  ~EltwiseLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which reorganizes the input tensor to produce a flat output tensor.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 *
 */

template<typename T,
         unsigned ifms,
         unsigned ifm_w,
         unsigned ifm_h>
struct FlattenLayer: public Layer<T, T> {
public:

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    if (this->output_inplace) {
      if (this->input == this->output) {}
      else {
        this->output = this->input;
      }
    } else {
      this->output = this->input;
      this->output_inplace = true;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  FlattenLayer(T *in) {
    this->input = in;
    this->output_inplace = false;
  }

  FlattenLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~FlattenLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which reorganizes the input tensor to produce the output tensor.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam in_fmt
 * \parblock
 * The layout of the input tensor
 * \endparblock
 * \tparam out_fmt
 * \parblock
 * The layout of the output tensor
 * \endparblock
 *
 */

template<typename T,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t in_fmt,
         triNNity::layout::feature_map_layout_t out_fmt>
struct TransformLayer: public Layer<T, T> {
public:

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (in_fmt) {
      case triNNity::layout::CHW: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            std::copy_n(this->input, ifms*ifm_h*ifm_w, this->output);
          } break;

          case triNNity::layout::HWC: {
            triNNity::dense::cpu::transform::image_chw_to_hwc<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          case triNNity::layout::HCW: {
            triNNity::dense::cpu::transform::image_chw_to_hcw<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      case triNNity::layout::HWC: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            triNNity::dense::cpu::transform::image_hwc_to_chw<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          case triNNity::layout::HWC: {
            std::copy_n(this->input, ifms*ifm_h*ifm_w, this->output);
          } break;

          case triNNity::layout::HCW: {
            triNNity::dense::cpu::transform::image_hwc_to_hcw<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      case triNNity::layout::HCW: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            triNNity::dense::cpu::transform::image_hcw_to_chw<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          case triNNity::layout::HWC: {
            triNNity::dense::cpu::transform::image_hcw_to_hwc<T, ifms, ifm_h, ifm_w>(this->input, this->output);
          } break;

          case triNNity::layout::HCW: {
            std::copy_n(this->input, ifms*ifm_h*ifm_w, this->output);
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      default: {
        TRINNITY_ERROR("Not implemented");
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  TransformLayer(T *in) {
    this->input = in;
    this->output_inplace = false;
    this->output = new T[ifms*ifm_h*ifm_w]();
  }

  TransformLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~TransformLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which concatenates the two input tensors channelwise to produce the output tensor.
 *
 * The input tensors must be in the same layout, and the output tensor is produced in that layout.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifmsA
 * \parblock
 * The number of input feature maps for input A
 * \endparblock
 * \tparam ifmsB
 * \parblock
 * The number of input feature maps for input B
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam in_fmt
 * \parblock
 * The layout of the input tensor
 * \endparblock
 *
 */

template<typename T,
         unsigned ifmsA, unsigned ifmsB, unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t in_fmt>
struct ChannelwiseConcatLayer: public Layer<T, T> {

  const T* input_A;
  const T* input_B;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (in_fmt) {
      case triNNity::layout::CHW: {
        auto localInputALayout = triNNity::internal::memory::View3D<const T, ifmsA, ifm_h, ifm_w>(this->input_A);
        auto localInputBLayout = triNNity::internal::memory::View3D<const T, ifmsB, ifm_h, ifm_w>(this->input_B);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifmsA+ifmsB, ifm_h, ifm_w>(this->output);

        for (unsigned c = 0; c < ifmsA; c++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[c][h][w] = localInputALayout[c][h][w];
            }
          }
        }

        for (unsigned c = ifmsA; c < ifmsA+ifmsB; c++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[c][h][w] = localInputBLayout[c-ifmsA][h][w];
            }
          }
        }
      } break;

      case triNNity::layout::HWC: {
        auto localInputALayout = triNNity::internal::memory::View3D<const T, ifm_h, ifm_w, ifmsA>(this->input_A);
        auto localInputBLayout = triNNity::internal::memory::View3D<const T, ifm_h, ifm_w, ifmsB>(this->input_B);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifmsA+ifmsB>(this->output);

        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned w = 0; w < ifm_w; w++) {
            for (unsigned c = 0; c < ifmsA; c++) {
              localOutputLayout[h][w][c] = localInputALayout[h][w][c];
            }

            for (unsigned c = ifmsA; c < ifmsA+ifmsB; c++) {
              localOutputLayout[h][w][c] = localInputBLayout[h][w][c-ifmsA];
            }
          }
        }
      } break;

      case triNNity::layout::HCW: {
        auto localInputALayout = triNNity::internal::memory::View3D<const T, ifm_h, ifmsA, ifm_w>(this->input_A);
        auto localInputBLayout = triNNity::internal::memory::View3D<const T, ifm_h, ifmsB, ifm_w>(this->input_B);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifmsA+ifmsB, ifm_w>(this->output);

        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned w = 0; w < ifm_w; w++) {
            for (unsigned c = 0; c < ifmsA; c++) {
              localOutputLayout[h][c][w] = localInputALayout[h][c][w];
            }

            for (unsigned c = ifmsA; c < ifmsA+ifmsB; c++) {
              localOutputLayout[h][c][w] = localInputBLayout[h][c-ifmsA][w];
            }
          }
        }
      } break;

      default: {
        TRINNITY_ERROR("Not implemented");
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  ChannelwiseConcatLayer(T *inA, T* inB) {
    this->input_A = inA;
    this->input_B = inB;
    this->output = new T[(ifmsA+ifmsB)*ifm_h*ifm_w]();
    this->output_inplace = false;
  }

  ChannelwiseConcatLayer(T *inA, T* inB, T *out) {
    this->input_A = inA;
    this->input_B = inB;
    this->output = out;
    this->output_inplace = true;
  }

  ~ChannelwiseConcatLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs activation.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam act_type
 * \parblock
 * The type of activation that the layer should perform
 * \endparblock
 *
 */

template<typename T,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         activation_t act_type>
struct ActivationLayer: public Layer<T, T> {

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch(act_type) {

      case ACTIVATION_RELU: {
        if (this->input == this->output) {
          triNNity::dense::cpu::primitive::relu_inplace<T, ifms*ifm_h*ifm_w>(this->output);
        } else {
          triNNity::dense::cpu::primitive::relu<T, ifms*ifm_h*ifm_w>(this->input, this->output);
        }
      } break;

      case ACTIVATION_NONE: {
        // No activation, just copy the stuff over to the output
        std::copy_n(this->input, ifms*ifm_h*ifm_w, this->output);
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(ifm_h*ifm_w*ifms);
  }

  virtual double working_space() { return 0; }

  ActivationLayer() {
    this->input = nullptr;
    this->output = nullptr;
  }

  ActivationLayer(T *in) {
    this->input = in;
    this->output = new T[ifms*ifm_h*ifm_w]();
    this->output_inplace = false;
  }

  ActivationLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~ActivationLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};


/**
 * A layer which performs the channel-wise Local response normalization as outlined
 * in the Alexnet paper.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The order in which the input and output tensors are laid out
 * \endparblock
 *
 */
template<typename T,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::lrn_impl_t lrn_type = triNNity::LRN_SIMPLE>
struct ChannelwiseLRNLayer: public Layer<T, T> {
  T k;
  unsigned n;
  T alpha;
  T beta;
  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (lrn_type) {

      case triNNity::LRN_SIMPLE: {
        triNNity::dense::cpu::primitive::channelwiseLRN<T, ifms, ifm_w, ifm_h, image_layout>(this->input, this->output, k, n, alpha, beta);
      } break;

      default: {
        TRINNITY_ERROR("ChannelwiseLRNLayer: unrecognized LRN algorithm selected");
      }
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  ChannelwiseLRNLayer(T *in, T k_value, unsigned n_value, T alpha_value, T beta_value)
  : k(k_value), n(n_value), alpha(alpha_value), beta(beta_value)
  {
    this->input = in;
    this->output = new T[ifms*ifm_h*ifm_w]();
    this->output_inplace = false;
  }

  ChannelwiseLRNLayer(T *in, T k_value, unsigned n_value, T alpha_value, T beta_value, T *out)
  : k(k_value), n(n_value), alpha(alpha_value), beta(beta_value)
  {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~ChannelwiseLRNLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs Batch Normalization with minibatches == 1, inference normalization only.
 *
 * To do batch normalization during inference we need the image to normalize as well as the mean and variance for every channel across the entire training
 * batch (i.e the population mean and variance of the values in each channel), so we'll have C mean values and C variance values all of which were
 * calculated from N*H*W pixels each.
 *
 * We also need the gamma and beta parameters for each channel. These are training parameters that are calculated alongside
 * the kernels during training, a different set of C gamma and beta values will be calculated for every batch normalization node in the network.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input tensor
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The order in which the input and output tensors are laid out
 * \endparblock
 */
template<typename T,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t image_layout>
struct BatchNormalizationLayer: public Layer<T, T> {
  T *means;
  T *variances;
  T *gammas;
  T *betas;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    triNNity::dense::cpu::primitive::batchNormalization<T, ifms, ifm_w, ifm_h, image_layout>(this->input, this->output, means, variances, gammas, betas);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(3*ifms + 4*ifms*ifm_h*ifm_w);
  }

  virtual double working_space() { return 0; }

  /**
   * \param in
   * \parblock
   * The input image/tensor
   * \endparblock
   * \param ms
   * \parblock
   * The pre-calculated means to be used with each channel/feature map of the input
   * \endparblock
   * \param vs
   * \parblock
   * The pre-calculated variances to be used with each channel/feature map of the input
   * \endparblock
   * \param gs
   * \parblock
   * The pre-calculated gamma/scale values to be used with each channel/feature map of the input
   * \endparblock
   * \param bs
   * \parblock
   * The pre-calculated beta/shift values to be used with each channel/feature map of the input
   * \endparblock
   */
  BatchNormalizationLayer(T *in, T *ms, T *vs, T *gs, T *bs)
  : means(ms), variances(vs), gammas(gs), betas(bs)
  {
    this->input = in;
    this->output = new T[ifms*ifm_h*ifm_w]();
    this->output_inplace = false;
  }

  BatchNormalizationLayer(T *in, T *ms, T *vs, T *gs, T *bs, T *out)
  : means(ms), variances(vs), gammas(gs), betas(bs)
  {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~BatchNormalizationLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which applies bias.
 * Expects a 1*C*H*W kernel, which is pointwise added to the input.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam layout
 * \parblock
 * The order in which the input and output tensors are laid out
 * \endparblock
 */

template<typename T, typename KD,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t layout>
struct FCBiasLayer: public Layer<T, T> {

  KD* kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (layout) {
      case triNNity::layout::CHW: {
        auto localInputLayout  = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(this->input);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(this->output);
        auto localKernelLayout = triNNity::internal::memory::View4D<T, 1, ifms, ifm_h, ifm_w>(this->kernel);

        for (unsigned c = 0; c < ifms; c++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[c][h][w] = localInputLayout[c][h][w] + localKernelLayout[0][c][h][w];
            }
          }
        }
      } break;
      case triNNity::layout::HWC: {
        auto localInputLayout  = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(this->input);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(this->output);
        auto localKernelLayout = triNNity::internal::memory::View4D<T, 1, ifm_h, ifm_w, ifms>(this->kernel);

        for (unsigned w = 0; w < ifm_w; w++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned c = 0; c < ifms; c++) {
              localOutputLayout[h][w][c] = localInputLayout[h][w][c] + localKernelLayout[0][h][w][c];
            }
          }
        }
      } break;
      default: {
        TRINNITY_ERROR("not implemented - FCBiasLayer - unsupported layout");
      }
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(ifm_h*ifm_w*ifms);
  }

  virtual double working_space() { return 0; }

  FCBiasLayer(T *in, KD* biases) {
    this->input = in;
    this->output = new T[ifms*ifm_h*ifm_w]();
    this->output_inplace = false;
    kernel = biases;
  }

  FCBiasLayer(T *in, KD* biases, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
    kernel = biases;
  }

  ~FCBiasLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which applies bias.
 * Expects a C item kernel, each point of which is added to every value in the corresponding channel of the input.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the kernel.
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam layout
 * \parblock
 * The order in which the input and output tensors are laid out
 * \endparblock
 */

template<typename T, typename KD,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         triNNity::layout::feature_map_layout_t layout>
struct ConvolutionalBiasLayer: public Layer<T, T> {
  KD* kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch(layout) {

      case (triNNity::layout::CHW): {
        auto localInputLayout  = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(this->input);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(this->output);

        for (unsigned c = 0; c < ifms; c++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[c][h][w] = localInputLayout[c][h][w] + this->kernel[c];
            }
          }
        }
      } break;

      case (triNNity::layout::HWC): {
        auto localInputLayout  = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(this->input);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(this->output);

        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned w = 0; w < ifm_w; w++) {
            for (unsigned c = 0; c < ifms; c++) {
              localOutputLayout[h][w][c] = localInputLayout[h][w][c] + this->kernel[c];
            }
          }
        }
      } break;

      case (triNNity::layout::HCW): {
        auto localInputLayout  = triNNity::internal::memory::View3D<T, ifm_h, ifms, ifm_w>(this->input);
        auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifms, ifm_w>(this->output);

        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned c = 0; c < ifms; c++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[h][c][w] = localInputLayout[h][c][w] + this->kernel[c];
            }
          }
        }
      } break;

    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(ifm_h*ifm_w*ifms);
  }

  virtual double working_space() { return 0; }

  ConvolutionalBiasLayer(T *in, KD* biases) {
    this->input = in;
    this->output = new T[ifms*ifm_h*ifm_w]();
    this->output_inplace = false;
    kernel = biases;
  }

  ConvolutionalBiasLayer(T *in, KD* biases, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
    kernel = biases;
  }

  ~ConvolutionalBiasLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs indirect GEMM-based convolution.
 *
 * Pure GEMM-based convolution is performed by translating the convolution
 * directly into calls to GEMM without transforming the input. As such,
 * it can only support convolutions with stride 1.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam gemm_method
 * \parblock
 * The indirect GEMM-based convolution method the layer should use to compute the output
 * \endparblock
 * \tparam gemm_impl
 * \parblock
 * Which GEMM implementation the layer should use
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam gemm_transpose
 * \parblock
 * Which GEMM argument transposition the layer should use
 * \endparblock
 * \tparam gemm_arrangement
 * \parblock
 * Which GEMM argument order the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */
template<typename T, typename KD,
         triNNity::conv_gemm_impl_t gemm_method,
         triNNity::gemm_variant_t gemm_impl,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type,
         triNNity::gemm_transpose_t gemm_transpose,
         triNNity::gemm_arrangement_t gemm_arrangement,
         unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
struct GEMMConvolutionalLayer: public Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    // Run the convolution
    triNNity::dense::convolution_forward_gemm<T, KD, ifm_w, ifm_h, ifms, ofms, k,
                                            gemm_method, bound_type,
                                            gemm_impl, gemm_transpose, gemm_arrangement,
                                            image_layout, kernel_layout, output_layout,
                                            gemm_block_i, gemm_block_j, gemm_block_k>
                                            (this->input, kernel, this->output);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*ifm_h*ifm_w*ifms*ofms*k*k);
    return conv_flops;
  }

  virtual double working_space() {
    return triNNity::dense::cpu::impl::conv_gemm_working_space<T, KD, T, ifm_w, ifm_h, ifms, ofms, k, 1, 1,
      gemm_method, gemm_impl, gemm_transpose, bound_type>();
  }

  GEMMConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    this->output_inplace = false;
    kernel = weights;
  }

  GEMMConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    kernel = weights;
  }

  ~GEMMConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs indirect GEMM-based convolution.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam gemm_method
 * \parblock
 * The indirect GEMM-based convolution method the layer should use to compute the output
 * \endparblock
 * \tparam gemm_impl
 * \parblock
 * Which GEMM implementation the layer should use
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam patch_type
 * \parblock
 * The patch building approach that the layer should use
 * \endparblock
 * \tparam gemm_transpose
 * \parblock
 * Which GEMM argument transposition the layer should use
 * \endparblock
 * \tparam gemm_arrangement
 * \parblock
 * Which GEMM argument order the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */
template<typename T, typename KD,
         triNNity::conv_patch_gemm_impl_t gemm_method,
         triNNity::gemm_variant_t gemm_impl,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type,
         triNNity::patch_impl_t patch_type,
         triNNity::gemm_transpose_t gemm_transpose,
         triNNity::gemm_arrangement_t gemm_arrangement,
         unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
struct PatchGEMMConvolutionalLayer: public Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    // Run the convolution
    triNNity::dense::convolution_forward_patch_gemm<T, KD, ifm_w, ifm_h, ifms, ofms, k,
                                                  gemm_method, bound_type,
                                                  gemm_impl, gemm_transpose, gemm_arrangement,
                                                  patch_type,
                                                  image_layout, kernel_layout, output_layout,
                                                  stride_w, stride_h,
                                                  gemm_block_i, gemm_block_j, gemm_block_k>
                                                  (this->input, kernel, this->output);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*triNNity::uceil<ifm_h, stride_h>()*triNNity::uceil<ifm_w, stride_w>()*ifms*ofms*k*k);
    return conv_flops;
  }

  virtual double working_space() {
    return triNNity::dense::cpu::impl::conv_gemm_patch_working_space<T, KD, T, ifm_w, ifm_h, ifms, ofms, k, stride_w, stride_h,
      gemm_method, gemm_impl, gemm_transpose, bound_type>();

  }

  PatchGEMMConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    this->output_inplace = false;
    kernel = weights;
  }

  PatchGEMMConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    kernel = weights;
  }

  ~PatchGEMMConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs direct convolution.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam A
 * \parblock
 * The type to use for accumulations
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam direct_method
 * \parblock
 * The direct convolution method the layer should use to compute the output
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 *
 */

template<typename T, typename A, typename O, typename KD,
         triNNity::conv_direct_impl_t direct_method,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k_w, unsigned k_h,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = BOUND_IMPLICIT_PAD>
struct DirectConvolutionalLayer: public Layer<T, O> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    // Run the convolution
    triNNity::dense::convolution_forward_direct<T, A, O, KD, ifm_w, ifm_h, ifms, ofms, k_w, k_h,
                                                direct_method, bound_type,
                                                image_layout, kernel_layout, output_layout,
                                                stride_w, stride_h>
                                                (this->input, kernel, this->output);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*triNNity::uceil<ifm_h, stride_h>()*triNNity::uceil<ifm_w, stride_w>()*ifms*ofms*k_w*k_h);
    return conv_flops;
  }

  virtual double working_space() { return 0; }

  DirectConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(ofms > 0);
    TRINNITY_STATIC_ASSERT(ofm_w > 0);
    TRINNITY_STATIC_ASSERT(ofm_h > 0);

    this->input = in;
    this->output = new O[ofms*ofm_h*ofm_w]();
    this->output_inplace = false;
    kernel = weights;
  }

  DirectConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(ofms > 0);
    TRINNITY_STATIC_ASSERT(ofm_w > 0);
    TRINNITY_STATIC_ASSERT(ofm_h > 0);

    this->input = in;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    kernel = weights;
  }

  ~DirectConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs pooling.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam pool_op
 * \parblock
 * What pooling operation to use
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The window size k of the pooling operation
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the pooling operation
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the pooling operation
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 */

template<typename T,
         triNNity::window_op_t pool_op,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k_w, unsigned k_h,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = BOUND_IMPLICIT_PAD>
struct PoolingLayer: public Layer<T, T> {

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    auto localImageLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(this->input);
    auto localOutputLayout = triNNity::internal::memory::View3D<T, ofms, ofm_h, ofm_w>(this->output);

    for(unsigned ifm_it = 0; ifm_it < ifms; ifm_it++) {
      if constexpr (ifm_w >= k_w && ifm_h >= k_h) {
        triNNity::dense::cpu::primitive::window2D<T, T, T, T, k_w, k_h,
                                                  pool_op, triNNity::WINDOW_NOKERNEL,
                                                  ifm_w, ifm_h,
                                                  stride_w, stride_h,
                                                  triNNity::WINDOW_ASSIGN>
                                                  (localImageLayout[ifm_it].begin(), nullptr, localOutputLayout[ifm_it].begin());
      }

      triNNity::dense::cpu::primitive::window2DB<T, T, T, T, k_w, k_h,
                                                 pool_op, triNNity::WINDOW_NOKERNEL, bound_type,
                                                 ifm_w, ifm_h,
                                                 stride_w, stride_h,
                                                 triNNity::WINDOW_ASSIGN>
                                                 (localImageLayout[ifm_it].begin(), nullptr, localOutputLayout[ifm_it].begin());
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(ofms*ofm_h*ofm_w*(k_h*k_w));
  }

  virtual double working_space() { return 0; }

  PoolingLayer(T *in) {
    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    this->output_inplace = false;
  }

  PoolingLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~PoolingLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs FC (fully connected layer).
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam gemv_impl
 * \parblock
 * What implementation of GEMV to use
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 *
 */

template<typename T, typename KD,
         triNNity::gemv_variant_t gemv_impl,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned ofms>
struct FCLayer: public Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    triNNity::dense::cpu::gemv<T, KD, T, gemv_impl,
                             triNNity::GEMV_NO_ACCUMULATE, triNNity::GEMV_A_B>
                             (ifms*ifm_h*ifm_w, ofms, kernel, this->input, this->output);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double fc_flops = static_cast<double>(2*ifm_h*ifm_w*ifms*ofms);
    return fc_flops;
  }

  virtual double working_space() { return 0; }

  FCLayer(T *in, KD *weights) {
    this->input = in;
    this->output = new T[ofms]();
    this->output_inplace = false;
    kernel = weights;
  }

  FCLayer(T *in, KD *weights, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
    kernel = weights;
  }

  ~FCLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs ReLU + sparse FC (fully connected layer).
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam gemv_impl
 * \parblock
 * What implementation of GEMV to use
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam act_type
 * \parblock
 * The type of activation that the layer should perform
 * \endparblock
 *
 */

template<typename T, typename KD,
         triNNity::gemv_variant_t gemv_impl,
         unsigned ifms,
         unsigned ifm_w, unsigned ifm_h,
         unsigned ofms,
         activation_t act_type>
struct FusedReLUFCLayer: public Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch(act_type) {

      case ACTIVATION_RELU: {
        // Perform relu + sparse FC
      } break;

      case ACTIVATION_NONE: {
        // No activation, just do a regular FC
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  FusedReLUFCLayer(T *in, KD *weights) {
    this->input = in;
    this->output = new T[ofms]();
    this->output_inplace = false;
    kernel = weights;
  }

  FusedReLUFCLayer(T *in, KD *weights, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
    kernel = weights;
  }

  ~FusedReLUFCLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which computes the softmax loss function.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam labels
 * \parblock
 * How many labels are being used
 * \endparblock
 *
 */

template<typename T,
         unsigned labels>
struct SoftmaxLayer: public Layer<T, T> {

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    triNNity::dense::cpu::primitive::softmax<T, labels>(this->input, this->output);

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return static_cast<double>(7*labels);
  }

  virtual double working_space() { return 0; }

  SoftmaxLayer(T *in) {
    this->input = in;
    this->output = new T[labels]();
    this->output_inplace = false;
  }

  SoftmaxLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~SoftmaxLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which generates random convolutional kernels
 *
 * \tparam KD
 * \parblock
 * The primitive type of the data in the kernel
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel should be generated in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 *
 */

template <typename KD,
          triNNity::layout::parameter_layout_t kernel_layout,
          unsigned ifms,
          unsigned k,
          unsigned ofms>
struct RandomKernelGenerationLayer: public Layer<KD, KD> {

  virtual void execute(){}

  RandomKernelGenerationLayer(double kernel_value_range) {
    this->output = triNNity::dense::cpu::memory::buildRandomKernel<KD, k, ofms, ifms, kernel_layout>(kernel_value_range);
    this->output_inplace = false;
  }

  ~RandomKernelGenerationLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which generates random sparse convolutional kernels
 *
 * \tparam KD
 * \parblock
 * The primitive type of the data in the kernel
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel should be generated in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 *
 */

template <typename KD,
          triNNity::layout::parameter_layout_t kernel_layout,
          unsigned ifms,
          unsigned k,
          unsigned ofms>
struct SparseRandomKernelGenerationLayer: public Layer<KD, KD> {

  virtual void execute(){}

  SparseRandomKernelGenerationLayer(double sparsity, double kernel_value_range) {
    this->output = triNNity::dense::cpu::memory::buildSparseRandomKernel<KD, k, ofms, ifms, kernel_layout>(sparsity, kernel_value_range);
    this->output_inplace = false;
  }

  ~SparseRandomKernelGenerationLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which generates constant convolutional kernels
 *
 * \tparam KD
 * \parblock
 * The primitive type of the data in the kernel
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel should be generated in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 *
 */

template <typename KD,
          triNNity::layout::parameter_layout_t kernel_layout,
          unsigned ifms,
          unsigned k,
          unsigned ofms>
struct ConstantKernelGenerationLayer: public Layer<KD, KD> {

  virtual void execute(){}

  ConstantKernelGenerationLayer(KD value) {
    this->output = new KD[ofms*ifms*k*k]();
    this->output_inplace = false;
    std::fill_n(this->output, ofms*ifms*k*k, value);
  }

  ~ConstantKernelGenerationLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which applies padding to input images
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the image
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the image is in
 * \endparblock
 * \tparam padding
 * \parblock
 * The padding strategy to use
 * \endparblock
 */

template <typename T,
          unsigned k,
          unsigned ifms,
          unsigned ifm_h,
          unsigned ifm_w,
          triNNity::layout::feature_map_layout_t image_layout,
          triNNity::padding_t padding>
struct PaddingLayer: public Layer<T, T> {

  virtual void execute() {
    triNNity::dense::cpu::memory::pad<T, k, ifms, ifm_h, ifm_w, image_layout, padding>(this->input, this->output);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  PaddingLayer(T *in) {
    this->input = in;
    this->output = new T[ifms*(ifm_h+(k-1))*(ifm_w+(k-1))]();
    this->output_inplace = false;
  }

  PaddingLayer(T *in, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
  }

  ~PaddingLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

}

}

#endif // TRINNITY_DENSE_CPU_LAYER_H
