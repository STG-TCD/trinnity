/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_PRIMITIVE_H
#define TRINNITY_DENSE_CPU_PRIMITIVE_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace dense {

namespace cpu {

namespace primitive {

template <typename T, unsigned sz>
static TRINNITY_INLINE T sum(const T * __restrict__ input) {
  T sum = input[0];
  for (unsigned i = 1; i < sz; i++) {
    sum += input[i];
  }
  return sum;
}

template <typename T, unsigned sz>
static TRINNITY_INLINE void relu(const T * __restrict__ input, T * __restrict__ output) {
  for (unsigned i = 0; i < sz; i++) {
    output[i] = std::max(static_cast<T>(0), input[i]);
  }
}

template <typename T, unsigned sz>
static TRINNITY_INLINE void relu_inplace(T * __restrict__ data) {
  for (unsigned i = 0; i < sz; i++) {
    data[i] = std::max(static_cast<T>(0), data[i]);
  }
}

template <typename T, unsigned ifms, unsigned ifm_w, unsigned ifm_h, triNNity::layout::feature_map_layout_t image_layout>
static TRINNITY_INLINE void channelwiseLRN(const T * __restrict__ input, T * __restrict__ output, T k, unsigned n, T alpha, T beta) {
  constexpr signed ifms_s = static_cast<signed>(ifms);
  switch(image_layout) {
    case triNNity::layout::HWC: {
      auto localInputLayout = triNNity::internal::memory::View3D<const T, ifm_h, ifm_w, ifms>(input);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(output);

      for (unsigned h = 0; h < ifm_h; h++) {
        for (unsigned w = 0; w < ifm_w; w++) {
          for (unsigned c = 0; c < ifms; c++) {
            T sum = 0;
            unsigned start_j = std::max(0, static_cast<signed>(c)-static_cast<signed>((n/2)));
            unsigned end_j = std::min(ifms_s-1, static_cast<signed>(c)+static_cast<signed>((n/2)));
            for (unsigned j = start_j; j <= end_j; j++) {
              sum += localInputLayout[h][w][j] * localInputLayout[h][w][j];
            }
            sum *= alpha / static_cast<T>(n);
            sum += k;
            sum = std::pow(sum, beta);
            localOutputLayout[h][w][c] = localInputLayout[h][w][c] / sum;
          }
        }
      }
    } break;

    case triNNity::layout::CHW: {
      auto localInputLayout = triNNity::internal::memory::View3D<const T, ifms, ifm_h, ifm_w>(input);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(output);

      for (unsigned c = 0; c < ifms; c++) {
        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned w = 0; w < ifm_w; w++) {
            localOutputLayout[c][h][w] = 0;
          }
        }
      }

      for (unsigned c = 0; c < ifms; c++) {
        unsigned start_j = std::max(0, static_cast<signed>(c)-static_cast<signed>((n/2)));
        unsigned end_j   = std::min(ifms_s-1, static_cast<signed>(c)+static_cast<signed>((n/2)));
        for (unsigned j = start_j; j <= end_j; j++) {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              localOutputLayout[c][h][w] += localInputLayout[j][h][w] * localInputLayout[j][h][w];
            }
          }
        }
      }

      for (unsigned c = 0; c < ifms; c++) {
        for (unsigned h = 0; h < ifm_h; h++) {
          for (unsigned w = 0; w < ifm_w; w++) {
            T sum = localOutputLayout[c][h][w];
            sum *= alpha / static_cast<T>(n);
            sum += k;
            sum = std::pow(sum, beta);
            localOutputLayout[c][h][w] = localInputLayout[c][h][w] / sum;
          }
        }
      }

    } break;

    default: {
      TRINNITY_ERROR("Unsupported layout - channelwiseLRN");
    }

  }
}

template <typename T, unsigned sz>
static TRINNITY_INLINE void softmax(const T * __restrict__ input, T * __restrict__ output) {
  const T alpha = *(std::max_element(input, input+sz));

  T denom = static_cast<T>(0);
  for (unsigned j = 0; j < sz; j++) {
    denom += std::exp(input[j] - alpha);
  }

  for (unsigned i = 0; i < sz; i++) {
    T numer = std::exp(input[i] - alpha);
    output[i] = numer / denom;
  }
}

template <typename T, unsigned k_w, unsigned k_h, unsigned img_w, unsigned img_h, boundary_t bound>
static TRINNITY_INLINE void makeConvPatch(const unsigned w_loc, const unsigned h_loc, const T * __restrict__ img, T * __restrict__ buf) {
  constexpr signed krad_w = (signed) k_w/2;
  constexpr signed krad_h = (signed) k_h/2;

  for (signed ky = -(krad_h); ky < (krad_h)+1; ky++) {
    for (signed kx = -(krad_w); kx < (krad_w)+1; kx++) {
      switch (bound) {
        case BOUND_UNDEF:
        case BOUND_IMPLICIT_PAD:
        case BOUND_IMPLICIT_MIRROR: {
          buf[(k_w*(ky+(k_h/2))) + (kx+(k_w/2))] = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[h_loc+ky][w_loc+kx];
        } break;
        case BOUND_EXPLICIT: {
          buf[(k_w*(ky+(k_h/2))) + (kx+(k_w/2))] = triNNity::internal::memory::View2D<const T, img_h+k_h-1, img_w+k_w-1>(img)[h_loc+ky+krad_h][w_loc+kx+krad_w];
        } break;
        default: {
          TRINNITY_ERROR("Not implemented.");
        } break;
      }
    }
  }
}

template <typename T, unsigned k_w, unsigned k_h, unsigned img_w, unsigned img_h, boundary_t bound>
static TRINNITY_INLINE void makeConvPatchBoundary(const unsigned w_loc, const unsigned h_loc, const T * __restrict__ img, T * __restrict__ buf) {
  constexpr signed simg_h = (signed) img_h;
  constexpr signed simg_w = (signed) img_w;
  constexpr signed krad_w = (signed) k_w/2;
  constexpr signed krad_h = (signed) k_h/2;

  if constexpr (bound != BOUND_UNDEF) {
    for (signed ky = -(krad_h); ky <= (krad_h); ky++) {
      for (signed kx = -(krad_w); kx <= (krad_w); kx++) {
        signed rr = ky + h_loc;
        signed cc = kx + w_loc;

        T pixel;
        switch (bound) {
          case BOUND_IMPLICIT_MIRROR: {
            if (rr<0) {
              rr = abs(rr) - 1;
            } else if (rr>=simg_h) {
              rr = img_h - 1 - (rr%img_h);
            }
            if (cc<0) {
              cc = abs(cc) - 1;
            } else if (cc>=simg_w) {
              cc = img_w - 1 - (cc%img_w);
            }
            pixel = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[rr][cc];
          } break;
          case BOUND_UNDEF:
          case BOUND_IMPLICIT_PAD: {
            if (rr<0 || rr>=simg_h || cc<0 || cc>=simg_w) {
              pixel = 0;
            } else {
              pixel = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[rr][cc];
            }
          } break;
          case BOUND_EXPLICIT: {
            rr += (signed)(k_h/2);
            cc += (signed)(k_w/2);
            pixel = triNNity::internal::memory::View2D<const T, img_h+k_h-1, img_w+k_w-1>(img)[rr][cc];
          } break;

          default: {
            TRINNITY_ERROR("Not implemented.");
          }
        }
        buf[k_w*(ky+(krad_h)) + (kx+(krad_w))] = pixel;

      }
    }
  }
}

template <typename T, unsigned k_w, unsigned k_h, unsigned img_w, unsigned img_h, unsigned img_c, boundary_t bound>
static TRINNITY_INLINE void makeConvPatchBoundaryHWC(const unsigned w_loc, const unsigned h_loc, const unsigned d, const T * __restrict__ img, T * __restrict__ buf) {
  constexpr signed simg_h = (signed) img_h;
  constexpr signed simg_w = (signed) img_w;
  constexpr signed krad_w = (signed) k_w/2;
  constexpr signed krad_h = (signed) k_h/2;

  if constexpr (bound != BOUND_UNDEF) {
    for (signed ky = -(krad_h); ky <= (krad_h); ky++) {
      for (signed kx = -(krad_w); kx <= (krad_w); kx++) {
        signed rr = ky + h_loc;
        signed cc = kx + w_loc;

        T pixel;
        switch (bound) {
          case BOUND_IMPLICIT_MIRROR: {
            if (rr<0) {
              rr = abs(rr) - 1;
            } else if (rr>=simg_h) {
              rr = img_h - 1 - (rr%img_h);
            }
            if (cc<0) {
              cc = abs(cc) - 1;
            } else if (cc>=simg_w) {
              cc = img_w - 1 - (cc%img_w);
            }
            pixel = triNNity::internal::memory::View3D<const T, img_h, img_w, img_c>(img)[rr][cc][d];
          } break;
          case BOUND_UNDEF:
          case BOUND_IMPLICIT_PAD: {
            if (rr<0 || rr>=simg_h || cc<0 || cc>=simg_w) {
              pixel = 0;
            } else {
              pixel = triNNity::internal::memory::View3D<const T, img_h, img_w, img_c>(img)[rr][cc][d];
            }
          } break;
          case BOUND_EXPLICIT: {
            rr += (signed)(k_h/2);
            cc += (signed)(k_w/2);
            pixel = triNNity::internal::memory::View3D<const T, img_h+k_h-1, img_w+k_w-1, img_c>(img)[rr][cc][d];
          } break;

          default: {
            TRINNITY_ERROR("Not implemented.");
          }
        }
        buf[k_w*(ky+(krad_h)) + (kx+(krad_w))] = pixel;

      }
    }
  }
}

template <typename T, unsigned k, unsigned img_w, unsigned img_h>
static TRINNITY_INLINE void makePoolPatch(const unsigned w_loc, const unsigned h_loc, const T * __restrict__ img, T * __restrict__ buf) {
  for (signed ky = 0; ky < ((signed)k); ky++) {
    for (signed kx = 0; kx < ((signed)k); kx++) {
      buf[(k*ky) + (kx)] = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[h_loc+ky][w_loc+kx];
    }
  }
}

template <typename T, unsigned k, unsigned img_w, unsigned img_h, boundary_t bound>
static TRINNITY_INLINE void makePoolPatchBoundary(const unsigned w_loc, const unsigned h_loc, const T * __restrict__ img, T * __restrict__ buf) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;

  for (signed ky = -((signed)k/2); ky <= ((signed)k/2); ky++) {
    for (signed kx = -((signed)k/2); kx <= ((signed)k/2); kx++) {
      signed rr = ky + h_loc;
      signed cc = kx + w_loc;

      T pixel;
      switch (bound) {
        case BOUND_UNDEF: break;
        case BOUND_IMPLICIT_MIRROR: {
          if (rr<0) {
            rr = abs(rr) - 1;
          } else if (rr>=simg_h) {
            rr = img_h - 1 - (rr%img_h);
          }
          if (cc<0) {
            cc = abs(cc) - 1;
          } else if (cc>=simg_w) {
            cc = img_w - 1 - (cc%img_w);
          }
          pixel = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[rr][cc];
        } break;
        case BOUND_IMPLICIT_PAD: {
          if (rr<0 || rr>=simg_h || cc<0 || cc>=simg_w) {
            pixel = 0;
          } else {
            pixel = triNNity::internal::memory::View2D<const T, img_h, img_w>(img)[rr][cc];
          }
        } break;
        case BOUND_EXPLICIT: {
          rr += (signed)(k/2);
          cc += (signed)(k/2);
          pixel = triNNity::internal::memory::View2D<const T, img_h+k-1, img_w+k-1>(img)[rr][cc];
        } break;

        default: {
          TRINNITY_ERROR("Not implemented.");
        }
      }
      buf[k*(ky+((signed)k/2)) + (kx+((signed)k/2))] = pixel;
    }
  }
}

template <typename T, typename KD, typename A, typename O, unsigned k_w, unsigned k_h, unsigned out_w, unsigned out_h, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void doConv2D(const T * __restrict__ buf, const KD * __restrict__ kern, O * __restrict__ out, const unsigned x, const unsigned y) {
  A result;

  #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
  result = (A)buf[0] * (A)kern[0];
  #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
  result = (KD)buf[0] * (KD)kern[0];
  #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
  result = (T)buf[0] * (T)kern[0];
  #else
  result = buf[0] * kern[0];
  #endif

  for (unsigned i = 1; i < k_h*k_w; i++) {
    #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
    result += (A)buf[i] * (A)kern[i];
    #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
    result += (KD)buf[i] * (KD)kern[i];
    #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
    result += (T)buf[i] * (T)kern[i];
    #else
    result += buf[i] * kern[i];
    #endif
  }

  auto dest = triNNity::internal::memory::View2D<O, out_h, out_w>(out);

  switch (out_behaviour) {
    case WINDOW_ACCUM: {
      dest[y][x] += (O)result;
    } break;

    case WINDOW_ASSIGN: {
      dest[y][x] = (O)result;
    } break;

    default: {
      TRINNITY_ERROR("Unknown output behaviour selected for windowed operation");
    } break;
  }
}

template <typename T, typename KD, typename A, typename O, unsigned k_w, unsigned k_h, unsigned out_w, unsigned out_h, unsigned out_m, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void doConv2DHWC(const T * __restrict__ buf, const KD * __restrict__ kern, O * __restrict__ out, const unsigned x, const unsigned y, const unsigned m) {
  A result;

  #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
  result = (A)buf[0] * (A)kern[0];
  #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
  result = (KD)buf[0] * (KD)kern[0];
  #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
  result = (T)buf[0] * (T)kern[0];
  #else
  result = buf[0] * kern[0];
  #endif

  for (unsigned i = 1; i < k_h*k_w; i++) {
    #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
    result += (A)buf[i] * (A)kern[i];
    #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
    result += (KD)buf[i] * (KD)kern[i];
    #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
    result += (T)buf[i] * (T)kern[i];
    #else
    result += buf[i] * kern[i];
    #endif
  }

  auto dest = triNNity::internal::memory::View3D<O, out_h, out_w, out_m>(out);

  switch (out_behaviour) {
    case WINDOW_ACCUM: {
      dest[y][x][m] += (O)result;
    } break;

    case WINDOW_ASSIGN: {
      dest[y][x][m] = (O)result;
    } break;

    default: {
      TRINNITY_ERROR("Unknown output behaviour selected for windowed operation");
    } break;
  }
}

template <typename T, typename O, unsigned k, unsigned out_w, unsigned out_h, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void doMaxPool2D(const T * __restrict__ buf, O * __restrict__ out, const unsigned x, const unsigned y) {
  T result = buf[0];
  for (unsigned i = 1; i < k*k; i++) {
    if (result < buf[i]) {
      result = buf[i];
    }
  }

  auto dest = triNNity::internal::memory::View2D<O, out_h, out_w>(out);
  switch (out_behaviour) {
    case WINDOW_ACCUM: {
      dest[y][x] += (O)result;
    } break;

    case WINDOW_ASSIGN: {
      dest[y][x] = (O)result;
    } break;

    default: {
      TRINNITY_ERROR("Unknown output behaviour selected for windowed operation");
    } break;
  }
}

template <typename T, typename O, unsigned k, unsigned out_w, unsigned out_h, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void doMinPool2D(const T * __restrict__ buf, O * __restrict__ out, const unsigned x, const unsigned y) {
  T result = buf[0];
  for (unsigned i = 1; i < k*k; i++) {
    if (result > buf[i]) {
      result = buf[i];
    }
  }

  auto dest = triNNity::internal::memory::View2D<O, out_h, out_w>(out);
  switch (out_behaviour) {
    case WINDOW_ACCUM: {
      dest[y][x] += (O)result;
    } break;

    case WINDOW_ASSIGN: {
      dest[y][x] = (O)result;
    } break;

    default: {
      TRINNITY_ERROR("Unknown output behaviour selected for windowed operation");
    } break;
  }
}

template <typename T, typename O, unsigned k, unsigned out_w, unsigned out_h, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void doAvgPool2D(const T * __restrict__ buf, O * __restrict__ out, const unsigned x, const unsigned y) {
  T result = sum<T, k*k>(buf) / static_cast<T>(k*k);

  auto dest = triNNity::internal::memory::View2D<O, out_h, out_w>(out);
  switch (out_behaviour) {
    case WINDOW_ACCUM: {
      dest[y][x] += (O)result;
    } break;

    case WINDOW_ASSIGN: {
      dest[y][x] = (O)result;
    } break;

    default: {
      TRINNITY_ERROR("Unknown output behaviour selected for windowed operation");
    } break;
  }
}

template <typename T, typename A, typename O, typename KD, unsigned k_w, unsigned k_h, unsigned prim_op, unsigned have_kernel, boundary_t bound, unsigned img_w, unsigned img_h, unsigned img_c, unsigned img_m, unsigned stride_w = 1, unsigned stride_h = 1, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void window2DBHWC(const T * __restrict__ img, const KD * __restrict__ kernel, O * __restrict__ out, const unsigned d, const unsigned m) {
  switch (bound) {
    case BOUND_UNDEF: break;

    default: {
      KD kern[k_h*k_w];

      switch (have_kernel) {
        case WINDOW_KERNEL: {
          for (unsigned ky = 0; ky < k_h; ky++) {
            for (unsigned kx = 0; kx < k_w; kx++) {
              kern[ky*k_w + kx] = triNNity::internal::memory::View2D<const KD, k_h, k_w>(kernel)[ky][kx];
            }
          }
        } break;

        default: break;
      }

      // This function only does the boundary pixels
      // But "boundary" is different for different operations
      switch (prim_op) {
        case WINDOW_CONV2D: {
          // The top rows of the image
          for (signed y = 0; y < std::min(img_h, k_h/2); y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundaryHWC<T, k_w, k_h, img_w, img_h, img_c, bound>(x, y, d, img, buf);
              doConv2DHWC<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), img_m, out_behaviour>(buf, kern, out, x/stride_w, y/stride_h, m);
            }
          }
          // The left columns of the image
          for (signed y = stride_h*triNNity::uceil<k_h/2, stride_h>(); y < ((signed)img_h)-((signed)k_h/2); y+=stride_h) {
            for (signed x = 0; x < (k_w/2); x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundaryHWC<T, k_w, k_h, img_w, img_h, img_c, bound>(x, y, d, img, buf);
              doConv2DHWC<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), img_m, out_behaviour>(buf, kern, out, x/stride_w, y/stride_h, m);
            }
          }
          // The right columns of the image
          for (signed y = stride_h*triNNity::uceil<k_h/2, stride_h>(); y < ((signed)img_h)-((signed)k_h/2); y+=stride_h) {
            for (signed x = stride_w*triNNity::sceil<((signed)img_w) - ((signed)k_w/2), stride_w>(); x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundaryHWC<T, k_w, k_h, img_w, img_h, img_c, bound>(x, y, d, img, buf);
              doConv2DHWC<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), img_m, out_behaviour>(buf, kern, out, x/stride_w, y/stride_h, m);
            }
          }
          // The bottom rows of the image
          /*
              The complex expression to initialize y is needed to make sure that the 'bottom rows' of
              the image do not overlap with the 'top rows' calculated in the above nested loop. The
              conditional boolean checks if the highest (possible) row index of the 'top rows' overlaps
              with what could be the lowest row index (stride_h*triNNity::uceil<img_h - ((signed)k_h/2), stride_h>())
              of the bottom row
          */
          for (signed y =
                (((signed)k_h/2) - 1) >= stride_h*triNNity::sceil<((signed)img_h) - ((signed)k_h/2), stride_h>() ?
                stride_h*triNNity::uceil<k_h/2, stride_h>() : stride_h*triNNity::sceil<((signed)img_h) - ((signed)k_h/2), stride_h>();
              y < img_h; y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundaryHWC<T, k_w, k_h, img_w, img_h, img_c, bound>(x, y, d, img, buf);
              doConv2DHWC<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), img_m, out_behaviour>(buf, kern, out, x/stride_w, y/stride_h, m);
            }
          }
        } break;
      }
    } break;
  }
}

template <typename T, typename A, typename O, typename KD, unsigned k_w, unsigned k_h, unsigned prim_op, unsigned have_kernel, boundary_t bound, unsigned img_w, unsigned img_h, unsigned stride_w = 1, unsigned stride_h = 1, window_output_t out_behaviour = WINDOW_ASSIGN>
static TRINNITY_INLINE void window2DB(const T * __restrict__ img, const KD * __restrict__ kernel, O * __restrict__ out) {
  switch (bound) {
    case BOUND_UNDEF: break;

    default: {
      KD kern[k_h*k_w];

      switch (have_kernel) {
        case WINDOW_KERNEL: {
          for (unsigned ky = 0; ky < k_h; ky++) {
            for (unsigned kx = 0; kx < k_w; kx++) {
              kern[ky*k_w + kx] = triNNity::internal::memory::View2D<const KD, k_h, k_w>(kernel)[ky][kx];
            }
          }
        } break;

        default: break;
      }

      // This function only does the boundary pixels
      // But "boundary" is different for different operations
      switch (prim_op) {
        case WINDOW_CONV2D: {
          // The top rows of the image
          for (signed y = 0; y < std::min(img_h, k_h/2); y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundary<T, k_w, k_h, img_w, img_h, bound>(x, y, img, buf);
              doConv2D<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, kern, out, x/stride_w, y/stride_h);
            }
          }
          // The left columns of the image
          for (signed y = stride_h*triNNity::uceil<k_h/2, stride_h>(); y < ((signed)img_h)-((signed)k_h/2); y+=stride_h) {
            for (signed x = 0; x < (k_w/2); x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundary<T, k_w, k_h, img_w, img_h, bound>(x, y, img, buf);
              doConv2D<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, kern, out, x/stride_w, y/stride_h);
            }
          }
          // The right columns of the image
          for (signed y = stride_h*triNNity::uceil<k_h/2, stride_h>(); y < ((signed)img_h)-((signed)k_h/2); y+=stride_h) {
            for (signed x = stride_w*triNNity::sceil<((signed)img_w) - ((signed)k_w/2), stride_w>(); x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundary<T, k_w, k_h, img_w, img_h, bound>(x, y, img, buf);
              doConv2D<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, kern, out, x/stride_w, y/stride_h);
            }
          }
          // The bottom rows of the image
          /*
              The complex expression to initialize y is needed to make sure that the 'bottom rows' of
              the image do not overlap with the 'top rows' calculated in the above nested loop. The
              conditional boolean checks if the highest (possible) row index of the 'top rows' overlaps
              with what could be the lowest row index (stride_h*triNNity::uceil<img_h - ((signed)k_h/2), stride_h>())
              of the bottom row
          */
          for (signed y =
                (((signed)k_h/2) - 1) >= stride_h*triNNity::sceil<((signed)img_h) - ((signed)k_h/2), stride_h>() ?
                stride_h*triNNity::uceil<k_h/2, stride_h>() : stride_h*triNNity::sceil<((signed)img_h) - ((signed)k_h/2), stride_h>();
              y < img_h; y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k_h*k_w];
              makeConvPatchBoundary<T, k_w, k_h, img_w, img_h, bound>(x, y, img, buf);
              doConv2D<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, kern, out, x/stride_w, y/stride_h);
            }
          }
        } break;

        // Note: Pooling operations only have right and bottom boundaries, because pooling isn't symmetric like convolution.
        case WINDOW_MAXPOOL: {
          TRINNITY_STATIC_ASSERT(k_w == k_h);
          constexpr signed k = k_w;
          // The right columns of the image
          for (unsigned y = 0; y < img_h; y+=stride_h) {
            for (signed x = stride_w*triNNity::uceil<img_w - k, stride_w>(); x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doMaxPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
          // The bottom rows of the image
          for (signed y = stride_h*triNNity::uceil<img_h - k, stride_h>(); y < img_h; y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doMaxPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
        } break;

        case WINDOW_MINPOOL: {
          TRINNITY_STATIC_ASSERT(k_w == k_h);
          constexpr signed k = k_w;
          // The right columns of the image
          for (signed y = 0; y < img_h; y+=stride_h) {
            for (signed x = stride_w*triNNity::uceil<img_w - k, stride_w>(); x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doMinPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
          // The bottom rows of the image
          for (signed y = stride_h*triNNity::uceil<img_h - k, stride_h>(); y < img_h; y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doMinPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
        } break;

        case WINDOW_AVGPOOL: {
          TRINNITY_STATIC_ASSERT(k_w == k_h);
          constexpr signed k = k_w;
          // The right columns of the image
          for (signed y = 0; y < img_h; y+=stride_h) {
            for (signed x = stride_w*triNNity::uceil<img_w - k, stride_w>(); x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doAvgPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
          // The bottom rows of the image
          for (signed y = stride_h*triNNity::uceil<img_h - k, stride_h>(); y < img_h; y+=stride_h) {
            for (signed x = 0; x < img_w; x+=stride_w) {
              T buf[k*k];
              makePoolPatchBoundary<T, k, img_w, img_h, bound>(x, y, img, buf);
              doAvgPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
            }
          }
        } break;
      }
    } break;
  }
}

template <typename T, typename A, typename O, typename KD, unsigned k_w, unsigned k_h, unsigned prim_op, unsigned have_kernel, unsigned img_w, unsigned img_h, unsigned stride_w = 1, unsigned stride_h = 1, window_output_t out_behaviour = WINDOW_ASSIGN, boundary_t bound = BOUND_IMPLICIT_PAD >
static TRINNITY_INLINE void window2D(const T * __restrict__ img, const KD * __restrict__ kernel, O * __restrict__ out) {

  KD kern[k_h*k_w];

  switch (have_kernel) {
    case WINDOW_KERNEL: {
      for (unsigned ky = 0; ky < k_h; ky++) {
        for (unsigned kx = 0; kx < k_w; kx++) {
          kern[ky*k_w + kx] = triNNity::internal::memory::View2D<const KD, k_h, k_w>(kernel)[ky][kx];
        }
      }
    } break;

    default: break;
  }

  // This function only does the non-boundary pixels
  // But "boundary" is different for different operations
  switch (prim_op) {
    case WINDOW_CONV2D: {
      for (signed y = stride_h*triNNity::uceil<k_h/2, stride_h>(); y < img_h - (k_h/2); y+=stride_h) {
        for (signed x = stride_w*triNNity::uceil<k_w/2, stride_w>(); x < img_w - (k_w/2); x+=stride_w) {
          T buf[k_h*k_w];
          makeConvPatch<T, k_w, k_h, img_w, img_h, bound>(x, y, img, buf);
          doConv2D<T, KD, A, O, k_w, k_h, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, kern, out, x/stride_w, y/stride_h);
        }
      }
    } break;

    case WINDOW_MAXPOOL: {
      TRINNITY_STATIC_ASSERT(k_w == k_h);
      constexpr signed k = k_w;
      for (signed y = 0; y < img_h - k; y+=stride_h) {
        for (signed x = 0; x < img_w - k; x+=stride_w) {
          T buf[k*k];
          makePoolPatch<T, k, img_w, img_h>(x, y, img, buf);
          doMaxPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
        }
      }
    } break;

    case WINDOW_MINPOOL: {
      TRINNITY_STATIC_ASSERT(k_w == k_h);
      constexpr signed k = k_w;
      for (signed y = 0; y < img_h - k; y+=stride_h) {
        for (signed x = 0; x < img_w - k; x+=stride_w) {
          T buf[k*k];
          makePoolPatch<T, k, img_w, img_h>(x, y, img, buf);
          doMinPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
        }
      }
    } break;

    case WINDOW_AVGPOOL: {
      TRINNITY_STATIC_ASSERT(k_w == k_h);
      constexpr signed k = k_w;
      for (signed y = 0; y < img_h - k; y+=stride_h) {
        for (signed x = 0; x < img_w - k; x+=stride_w) {
          T buf[k*k];
          makePoolPatch<T, k, img_w, img_h>(x, y, img, buf);
          doAvgPool2D<T, O, k, triNNity::uceil<img_w, stride_w>(), triNNity::uceil<img_h, stride_h>(), out_behaviour>(buf, out, x/stride_w, y/stride_h);
        }
      }
    } break;
  }
}

/**
 * Batch Normalization with mini-batches=1, inference Normalization only.
 * To do batch Normalization during inference we need the image to normalize as well as the mean and variance for every channel across the entire training
 * batch (i.e the population mean and variance of the values in each channel, so we'll have C mean values and C variance values all of which were
 * calculated from N*H*W pixels each). We also need the gamma and beta parameters for each channel. These are training parameters that are calculated alongside
 * the kernels during training, a different set of C gamma and beta values will be calculated for every batch Normalization node in the network.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input tensor
 * \endparblock
 * \tparam channels
 * \parblock
 * The number of channels/feature maps in the image/tensor
 * \endparblock
 * \tparam img_h
 * \parblock
 * The height of the input image / feature maps
 * \endparblock
 * \tparam img_w
 * \parblock
 * The width of the input image / feature maps
 * \endparblock
 * \tparam img_layout
 * \parblock
 * The layout that the input image/tensor should be in
 * \endparblock
 * \param img
 * \parblock
 * The input image/tensor
 * \endparblock
 * \param out
 * \parblock
 * The output image/tensor
 * \endparblock
 * \param popMeans
 * \parblock
 * The pre-calculated means to be used with each channel/feature map of the input
 * \endparblock
 * \param popVars
 * \parblock
 * The pre-calculated variances to be used with each channel/feature map of the input
 * \endparblock
 * \param gammas
 * \parblock
 * The pre-calculated gamma/scale values to be used with each channel/feature map of the input
 * \endparblock
 * \param betas
 * \parblock
 * The pre-calculated beta/shift values to be used with each channel/feature map of the input
 * \endparblock
 */
template <typename T, unsigned channels, unsigned img_h, unsigned img_w, triNNity::layout::feature_map_layout_t img_layout>
static TRINNITY_INLINE void batchNormalization(const T * __restrict__ img,  T * __restrict__ out, const T * popMeans, const T * popVars, const T * gammas, const T * betas) {
  switch (img_layout) {
    case triNNity::layout::CHW: {
      auto image = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(img);
      auto output = triNNity::internal::memory::View3D<T, channels, img_h, img_w>(out);
      for (unsigned c = 0; c < channels; c++) {
        T sqve = 1 / sqrt((nullptr == popVars ? 0 : popVars[c]) + 0.0001);
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            output[c][h][w] = (nullptr == gammas ? 1 : gammas[c]) *
                              (image[c][h][w] - (nullptr == popMeans ? 0 : popMeans[c])) *
                              sqve +
                              (nullptr == betas ? 0 : betas[c]);
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("not implemented.");
    }
  }
}

namespace dilated {

//TODO: merge these into a more general dilate(kernel, output) function that takes the layout of the kernel as a template parameter

/*
template<typename T, unsigned k, unsigned channels, unsigned kernels, unsigned dilation>
static TRINNITY_INLINE void makeDilatedKernel_oihw(const T * __restrict__ kernel, T * __restrict__ out) {
  constexpr unsigned dk = k + (dilation*(k-1));
  auto ker = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel);
  auto dker = triNNity::internal::memory::View4D<T, kernels, channels, dk, dk>(out);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      unsigned yPos = 0;
      for (unsigned ky = 0; ky < k; ky++) {
        unsigned xPos = 0;
        for (unsigned kx = 0; kx < k; kx++) { //row with values
          dker[m][c][yPos][xPos] = ker[m][c][ky][kx]; xPos++;
          if (kx != k-1){
            for (unsigned dx = 0; dx < dilation; dx++) { //all zero columns
              dker[m][c][yPos][xPos] = 0; xPos++;
            }
          }
        }
        yPos++;
        if (ky != k-1) {
          for (unsigned dy = 0; dy < dilation; dy++) { //all zero rows
            for (unsigned i = 0; i < dk; i++) {
              dker[m][c][yPos][i] = 0;
            }
            yPos++;
          }
        }
      }
    }
  }
}

template<typename T, unsigned k, unsigned channels, unsigned kernels, unsigned dilation>
static TRINNITY_INLINE void makeDilatedKernel_hwoi(const T * __restrict__ kernel, T * __restrict__ out) {
  constexpr unsigned dk = k + (dilation*(k-1));
  auto ker = triNNity::internal::memory::View4D<const T, k, k, kernels, channels>(kernel);
  auto dker = triNNity::internal::memory::View4D<T, dk, dk, kernels, channels>(out);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      unsigned yPos = 0;
      for (unsigned ky = 0; ky < k; ky++) {
        unsigned xPos = 0;
        for (unsigned kx = 0; kx < k; kx++) { //row with values
          dker[yPos][xPos][m][c] = ker[ky][kx][m][c]; xPos++;
          if (kx != k-1){
            for (unsigned dx = 0; dx < dilation; dx++) { //all zero columns
              dker[yPos][xPos][m][c] = 0; xPos++;
            }
          }
        }
        yPos++;
        if (ky != k-1) {
          for (unsigned dy = 0; dy < dilation; dy++) { //all zero rows
            for (unsigned i = 0; i < dk; i++) {
              dker[yPos][i][m][c] = 0;
            }
            yPos++;
          }
        }
      }
    }
  }
}

*/
}

}

}

}

}

#endif // TRINNITY_DENSE_CPU_PRIMITIVE_H
