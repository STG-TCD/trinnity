/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_IMPL_H
#define TRINNITY_DENSE_CPU_IMPL_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/dense/cpu/gemm.h>
#include <triNNity/dense/cpu/primitive.h>
#include <triNNity/dense/cpu/shape.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace dense {

namespace cpu {

namespace impl {

template <typename T,
          typename KD,
          typename O,
          unsigned img_w,
          unsigned img_h,
          unsigned channels,
          unsigned kernels,
          unsigned k,
          unsigned stride_w, unsigned stride_h,
          triNNity::conv_gemm_impl_t gemm_method,
          triNNity::gemm_variant_t gemm_impl,
          triNNity::gemm_transpose_t gemm_transpose,
          triNNity::boundary_t bound_type>
static TRINNITY_INLINE double conv_gemm_working_space() {
  double totalCPUSpace = 0;
  constexpr unsigned TS = sizeof(T);
  constexpr unsigned KDS = sizeof(KD);
  constexpr unsigned OS = sizeof(O);
  constexpr unsigned real_img_h = img_h + (bound_type==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned real_img_w = img_w + (bound_type==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned krad = k/2;
  constexpr unsigned padded_h = img_h + krad*2;
  constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();
  constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();

  switch (gemm_method) {
    case CONV_MULTI_CONV_1x1_GEMM: { break; }
    case CONV_MULTI_KN2COL_GEMM:
    case CONV_MULTI_KN2ROW_GEMM: {
      totalCPUSpace += (OS * real_img_h*real_img_w*k*k*kernels);
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(channels, (k*k)*kernels, channels, real_img_h*real_img_w);
      break;
    }
    case CONV_MULTI_KN2COL_AS_GEMM: {
      totalCPUSpace += (OS * real_img_h*real_img_w*kernels);
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(channels, real_img_h*real_img_w, channels, kernels);
      break;
    }
    case CONV_MULTI_KN2ROW_AS_GEMM: {
      totalCPUSpace += (OS * img_h*real_img_w*kernels);
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(channels, kernels, img_h*real_img_w, channels);
      break;
    }
    case CONV_MULTI_KN2ROW_AA_GEMM: {
      constexpr unsigned extra_rows = std::ceil((double)(img_w * std::floor((double)k/2)) / (double)(img_h*img_w));
      constexpr signed pad = 10;
      totalCPUSpace += (OS * (kernels + 2*extra_rows + pad)*img_h*img_w);
      totalCPUSpace += (TS * (channels*krad*img_w));
      totalCPUSpace += (TS * (channels*krad*img_h));
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(channels, kernels, img_h*img_w, channels);
      break;
    }
  }
  return totalCPUSpace;
}


template <typename T,
          typename KD,
          typename O,
          unsigned img_w,
          unsigned img_h,
          unsigned channels,
          unsigned kernels,
          unsigned k,
          unsigned stride_w, unsigned stride_h,
          triNNity::conv_patch_gemm_impl_t gemm_method,
          triNNity::gemm_variant_t gemm_impl,
          triNNity::gemm_transpose_t gemm_transpose,
          triNNity::boundary_t bound_type>
static TRINNITY_INLINE double conv_gemm_patch_working_space() {
  double totalCPUSpace = 0;
  constexpr unsigned TS = sizeof(T);
  constexpr unsigned KDS = sizeof(KD);
  constexpr unsigned OS = sizeof(O);
  constexpr unsigned real_img_h = img_h + (bound_type==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned real_img_w = img_w + (bound_type==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned krad = k/2;
  constexpr unsigned padded_h = img_h + krad*2;
  constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();
  constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();

  switch (gemm_method) {
    case CONV_MULTI_IM2COL_GEMM:
    case CONV_MULTI_IM2ROW_GEMM: {
      totalCPUSpace += (TS * (out_h*out_w*k*k*channels));
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, GEMM_A_B>((channels*k*k), out_h*out_w, kernels, (channels*k*k));
      break;
    }
    case CONV_MULTI_MEC_COL: {
      totalCPUSpace += (TS * (padded_h * k * channels * out_w));
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(out_w, k*channels*k, k*channels*k, kernels);
      break;
    }
    case CONV_MULTI_MEC_ROW_PARTITION: {
      totalCPUSpace += (TS * ((out_w * padded_h * k * channels) + (out_w * k*k * channels)));
      totalCPUSpace += gemm_working_space<T, KD, O, gemm_impl, gemm_transpose>(k*k*channels, out_w, k*k*channels, kernels);
      break;
    }
  }
  return totalCPUSpace;
}


template <typename T, typename A, typename O, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k_w, unsigned k_h, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void direct_loop_sum_single_chw(const T * __restrict__ img, const KD * __restrict__ kernel, O * __restrict__ out) {

  // Loop inspired by the Xeon Phi optimization book, no privatisation
  constexpr unsigned real_img_h = img_h + (bound==BOUND_EXPLICIT ? (2*k_h)/2 : 0);
  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? (2*k_w)/2 : 0);

  auto localKernelLayout = triNNity::internal::memory::View4D<const KD, kernels, channels, k_h, k_w>(kernel);
  auto localImageLayout  = triNNity::internal::memory::View3D<const T, channels, real_img_h, real_img_w>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<O, kernels, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>()>(out);

  constexpr bool h_small = (((signed)img_h)-(k_h/2)) <= 0;
  constexpr bool w_small = (((signed)img_w)-(k_w/2)) <= 0;

  constexpr signed h_bound = h_small ? (stride_h*triNNity::uceil<k_h/2, stride_h>()) : ((signed)img_h)-(k_h/2);
  constexpr signed w_bound = w_small ? (stride_w*triNNity::uceil<k_w/2, stride_w>()) : ((signed)img_w)-(k_w/2);

  for (unsigned m = 0; m < kernels; m++) {

    // These two loops should get fused
    for (signed h = stride_h*triNNity::uceil<k_h/2, stride_h>(); h < h_bound; h+=stride_h) {
      for (signed w = stride_w*triNNity::uceil<k_w/2, stride_w>(); w < w_bound; w+=stride_w) {
        localOutputLayout[m][h/stride_h][w/stride_w] = static_cast<O>(0);

        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp simd collapse(2)
        #endif
        for (signed y = 0; y < k_h; y++) {
          for (signed x = 0; x < k_w; x++) {
            localOutputLayout[m][h/stride_h][w/stride_w] += (O)
            #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
            ((A)localImageLayout[0][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
             *
             (A)localKernelLayout[m][0][y][x]);
            #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
            ((KD)localImageLayout[0][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
             *
             (KD)localKernelLayout[m][0][y][x]);
            #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
            ((T)localImageLayout[0][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
             *
             (T)localKernelLayout[m][0][y][x]);
            #else
            (localImageLayout[0][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
             *
             localKernelLayout[m][0][y][x]);
            #endif
          }
        }
      }
    }

    for (unsigned d = 1; d < channels; d++) {
      for (signed h = stride_h*triNNity::uceil<k_h/2, stride_h>(); h < h_bound; h += stride_h) {
        for (signed w = stride_w*triNNity::uceil<k_w/2, stride_w>(); w < w_bound; w += stride_w) {
          for (signed y = 0; y < k_h; y++) {
            #if defined(TRINNITY_USE_OPENMP)
            #pragma omp simd
            #endif
            for (signed x = 0; x < k_w; x++) {
              localOutputLayout[m][h/stride_h][w/stride_w] += (O)
              #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
              ((A)localImageLayout[d][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
               *
               (A)localKernelLayout[m][d][y][x]);
              #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
              ((KD)localImageLayout[d][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
               *
               (KD)localKernelLayout[m][d][y][x]);
              #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
              ((T)localImageLayout[d][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
               *
               (T)localKernelLayout[m][d][y][x]);
              #else
              (localImageLayout[d][(h + y) - (k_h/2)][(w + x) - (k_w/2)]
               *
               localKernelLayout[m][d][y][x]);
              #endif
            }
          }
        }
      }
    }
  }

  switch (bound) {
    case BOUND_UNDEF: break;

    default: {
      for(unsigned kernel_it = 0; kernel_it < kernels; kernel_it++) {
        triNNity::dense::cpu::primitive::window2DB<T, A, O, KD, k_w, k_h, WINDOW_CONV2D, WINDOW_KERNEL, bound, img_w, img_h, stride_w, stride_h, WINDOW_ASSIGN>(localImageLayout[0].begin(), localKernelLayout[kernel_it][0].begin(), localOutputLayout[kernel_it].begin());

        for (unsigned chan_it = 1; chan_it < channels; chan_it++) {
          triNNity::dense::cpu::primitive::window2DB<T, A, O, KD, k_w, k_h, WINDOW_CONV2D, WINDOW_KERNEL, bound, img_w, img_h, stride_w, stride_h, WINDOW_ACCUM>(localImageLayout[chan_it].begin(), localKernelLayout[kernel_it][chan_it].begin(), localOutputLayout[kernel_it].begin());
        }
      }
    } break;
  }
}

template <typename T, typename A, typename O, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k_w, unsigned k_h, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void direct_loop_sum_single_hwc(const T * __restrict__ img, const KD * __restrict__ kernel, O * __restrict__ out) {

  // Loop inspired by the Xeon Phi optimization book, no privatisation
  constexpr unsigned real_img_h = img_h + (bound==BOUND_EXPLICIT ? (2*k_h)/2 : 0);
  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? (2*k_w)/2 : 0);

  auto localKernelLayout = triNNity::internal::memory::View4D<const KD, kernels, channels, k_h, k_w>(kernel);
  auto localImageLayout  = triNNity::internal::memory::View3D<const T, real_img_h, real_img_w, channels>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<O, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>(), kernels>(out);

  constexpr bool h_small = (((signed)img_h)-(k_h/2)) <= 0;
  constexpr bool w_small = (((signed)img_w)-(k_w/2)) <= 0;

  constexpr signed h_bound = h_small ? (stride_h*triNNity::uceil<k_h/2, stride_h>()) : ((signed)img_h)-(k_h/2);
  constexpr signed w_bound = w_small ? (stride_w*triNNity::uceil<k_w/2, stride_w>()) : ((signed)img_w)-(k_w/2);

  // These two loops should get fused
  for (signed h = stride_h*triNNity::uceil<k_h/2, stride_h>(); h < h_bound; h+=stride_h) {
    for (signed w = stride_w*triNNity::uceil<k_w/2, stride_w>(); w < w_bound; w+=stride_w) {
      for (unsigned m = 0; m < kernels; m++) {
        localOutputLayout[h/stride_h][w/stride_w][m] = static_cast<O>(0);

        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp simd collapse(2)
        #endif
        for (signed y = 0; y < k_h; y++) {
          for (signed x = 0; x < k_w; x++) {
            localOutputLayout[h/stride_h][w/stride_w][m] += (O)
            #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
            ((A)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][0]
             *
             (A)localKernelLayout[m][0][y][x]);
            #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
            ((KD)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][0]
             *
             (KD)localKernelLayout[m][0][y][x]);
            #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
            ((T)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][0]
             *
             (T)localKernelLayout[m][0][y][x]);
            #else
            (localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][0]
             *
             localKernelLayout[m][0][y][x]);
            #endif
          }
        }
      }
    }
  }

  for (signed h = stride_h*triNNity::uceil<k_h/2, stride_h>(); h < h_bound; h += stride_h) {
    for (signed w = stride_w*triNNity::uceil<k_w/2, stride_w>(); w < w_bound; w += stride_w) {
      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned d = 1; d < channels; d++) {
          for (signed y = 0; y < k_h; y++) {
            #if defined(TRINNITY_USE_OPENMP)
            #pragma omp simd
            #endif
            for (signed x = 0; x < k_w; x++) {
              localOutputLayout[h/stride_h][w/stride_w][m] += (O)
              #if defined(TRINNITY_DIRECT_MUL_PRECISION_A)
              ((A)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][d]
               *
               (A)localKernelLayout[m][d][y][x]);
              #elif defined(TRINNITY_DIRECT_MUL_PRECISION_K)
              ((KD)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][d]
               *
               (KD)localKernelLayout[m][d][y][x]);
              #elif defined(TRINNITY_DIRECT_MUL_PRECISION_I)
              ((T)localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][d]
               *
               (T)localKernelLayout[m][d][y][x]);
              #else
              (localImageLayout[(h + y) - (k_h/2)][(w + x) - (k_w/2)][d]
               *
               localKernelLayout[m][d][y][x]);
              #endif
            }
          }
        }
      }
    }
  }

  switch (bound) {
    case BOUND_UNDEF: break;

    default: {
      for(unsigned kernel_it = 0; kernel_it < kernels; kernel_it++) {
        triNNity::dense::cpu::primitive::window2DBHWC<T, A, O, KD, k_w, k_h, WINDOW_CONV2D, WINDOW_KERNEL, bound, img_w, img_h, channels, kernels, stride_w, stride_h, WINDOW_ASSIGN>(localImageLayout.begin(), localKernelLayout[kernel_it][0].begin(), localOutputLayout.begin(), 0, kernel_it);

        for (unsigned chan_it = 1; chan_it < channels; chan_it++) {
          triNNity::dense::cpu::primitive::window2DBHWC<T, A, O, KD, k_w, k_h, WINDOW_CONV2D, WINDOW_KERNEL, bound, img_w, img_h, channels, kernels, stride_w, stride_h, WINDOW_ACCUM>(localImageLayout.begin(), localKernelLayout[kernel_it][chan_it].begin(), localOutputLayout.begin(), chan_it, kernel_it);
        }
      }
    } break;
  }
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2row_A_B_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  T* patch_matrix;
  if constexpr (patch == PATCH_COPY_SHORT) {
    patch_matrix = triNNity::dense::cpu::transform::im2row_hwc_to_hwkkc<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  } else {
    patch_matrix = triNNity::dense::cpu::transform::im2row_chw_to_hwckk<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  }

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>((channels*k*k), (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), kernels, (channels*k*k), patch_matrix, kernel, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2row_AT_BT_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  T* patch_matrix;
  if constexpr (patch == PATCH_COPY_SHORT) {
    patch_matrix = triNNity::dense::cpu::transform::im2row_hwc_to_hwkkc<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  } else {
    patch_matrix = triNNity::dense::cpu::transform::im2row_chw_to_hwckk<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  }

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_BT, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, (channels*k*k), (channels*k*k), (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), kernel, patch_matrix, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2row_A_BT_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  T* patch_matrix;
  if constexpr (patch == PATCH_COPY_SHORT) {
    patch_matrix = triNNity::dense::cpu::transform::im2row_hwc_to_hwkkc<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  } else {
    patch_matrix = triNNity::dense::cpu::transform::im2row_chw_to_hwckk<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  }

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>((channels*k*k), (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), (channels*k*k), kernels, patch_matrix, kernel, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2row_A_BT_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  T* patch_matrix;
  if constexpr (patch == PATCH_COPY_SHORT) {
    patch_matrix = triNNity::dense::cpu::transform::im2row_hwc_to_hwkkc<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  } else {
    patch_matrix = triNNity::dense::cpu::transform::im2row_chw_to_hwckk<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);
  }

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>((channels*k*k), kernels, (channels*k*k), (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), kernel, patch_matrix, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2col_AT_B_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  T* patch_matrix = triNNity::dense::cpu::transform::im2col_chw_to_ckkhw<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_B, gemm_block_i, gemm_block_j, gemm_block_k>((triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), (channels*k*k), kernels, (channels*k*k), patch_matrix, kernel, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2col_AT_B_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  T* patch_matrix = triNNity::dense::cpu::transform::im2col_chw_to_ckkhw<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_B, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, (channels*k*k), (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), (channels*k*k), kernel, patch_matrix, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2col_AT_BT_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  T* patch_matrix = triNNity::dense::cpu::transform::im2col_chw_to_ckkhw<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_BT, gemm_block_i, gemm_block_j, gemm_block_k>((triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), (channels*k*k), (channels*k*k), kernels, patch_matrix, kernel, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void im2col_A_B_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  T* patch_matrix = triNNity::dense::cpu::transform::im2col_chw_to_ckkhw<T, img_w, img_h, channels, kernels, k, bound, patch, stride_w, stride_h>(img);

  gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>((channels*k*k), kernels, (triNNity::uceil<img_h, stride_h>())*(triNNity::uceil<img_w, stride_w>()), (channels*k*k), kernel, patch_matrix, out);

  delete [] patch_matrix;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_A_BT_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  #if !defined(TRINNITY_ARMV7_KLUDGE)
  constexpr unsigned real_img_h = img_h + (bound==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);

  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, img_h, img_w>(out);

  T* tempOutput = new T[k*k*kernels*real_img_h*real_img_w]();

  auto tempOutputLayout = triNNity::internal::memory::View4D<T, k*k, kernels, real_img_h, real_img_w>(tempOutput);

  gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, (k*k)*kernels, channels, real_img_h*real_img_w, kernel, img, tempOutput);

  // Avoid having to zero-initialize by peeling one GEMM and having it overwrite the output
  // This has to be the GEMM which does not need to have its output shifted.
  // That's the GEMM for kernel point (k/2, k/2).

  for (signed o_it = (k*k)/2; o_it == (k*k)/2; o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] = tempOutputLayout[o_it][k_it][pixelRow][pixelColumn];
        }
      }
    }
  }

  for (signed o_it = 0; o_it < ((signed)((k*k)/2)); o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += tempOutputLayout[o_it][k_it][pixelRow][pixelColumn];
        }
      }
    }
  }

  for (signed o_it = ((k*k)/2)+1; o_it < ((signed)(k*k)); o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += tempOutputLayout[o_it][k_it][pixelRow][pixelColumn];
        }
      }
    }
  }

  delete [] tempOutput;
  #endif
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2col_A_B_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  #if !defined(TRINNITY_ARMV7_KLUDGE)
  constexpr unsigned real_img_h = img_h + (bound==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);

  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, img_h, img_w>(out);

  T* tempOutput = new T[real_img_h*real_img_w*k*k*kernels]();

  auto tempOutputLayout = triNNity::internal::memory::View4D<T, real_img_h, real_img_w, k*k, kernels>(tempOutput);

  gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, real_img_h*real_img_w, (k*k)*kernels, channels, img, kernel, tempOutput);

  // Avoid having to zero-initialize by peeling one GEMM and having it overwrite the output
  // This has to be the GEMM which does not need to have its output shifted.
  // That's the GEMM for kernel point (k/2, k/2).

  for (signed o_it = (k*k)/2; o_it == ((signed)((k*k)/2)); o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] = tempOutputLayout[pixelRow][pixelColumn][o_it][k_it];
        }
      }
    }
  }

  for (signed o_it = 0; o_it < ((signed)((k*k)/2)); o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += tempOutputLayout[pixelRow][pixelColumn][o_it][k_it];
        }
      }
    }
  }

  for (signed o_it = ((k*k)/2)+1; o_it < ((signed)(k*k)); o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += tempOutputLayout[pixelRow][pixelColumn][o_it][k_it];
        }
      }
    }
  }

  delete [] tempOutput;
  #endif
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_as_A_B_K_I(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);

  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, img_h, img_w>(out);

  // Avoid having to zero-initialize by peeling one GEMM and having it overwrite the output
  // This has to be the GEMM which does not need to have its output shifted.
  // That's the GEMM for kernel point (k/2, k/2).

  gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, img_h*real_img_w, channels, localKernelLayout[(k*k)/2], img, out);

  T* gemmOutput = new T[kernels*img_h*real_img_w](); // zero initialized by operator new ([C++11: 5.3.4/15])
  auto gemmOutputLayout = triNNity::internal::memory::View3D<T, kernels, img_h, real_img_w>(gemmOutput);

  for (signed o_it = 0; o_it < ((signed)(k*k)/2); o_it++) {

    gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, img_h*real_img_w, channels, localKernelLayout[o_it], img, gemmOutput);

    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(img_h, img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += gemmOutputLayout[k_it][pixelRow][pixelColumn];
        }
      }
    }
  }

  for (signed o_it = ((k*k)/2)+1; o_it < ((signed)(k*k)); o_it++) {

    gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, img_h*real_img_w, channels, localKernelLayout[o_it], img, gemmOutput);

    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(img_h, img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += gemmOutputLayout[k_it][pixelRow][pixelColumn];
        }
      }
    }
  }

  delete [] gemmOutput;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2col_as_A_BT_I_K(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  constexpr unsigned real_img_h = img_h + (bound==BOUND_EXPLICIT ? k-1 : 0);
  constexpr unsigned real_img_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);

  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, img_h, img_w>(out);

  T* gemmOutput = new T[real_img_h*real_img_w*kernels](); // zero initialized by operator new ([C++11: 5.3.4/15])
  auto gemmOutputLayout = triNNity::internal::memory::View3D<T, real_img_h, real_img_w, kernels>(gemmOutput);

  // Avoid having to zero-initialize by peeling one GEMM and having it overwrite the output
  // This has to be the GEMM which does not need to have its output shifted.
  // That's the GEMM for kernel point (k/2, k/2).

  gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, real_img_h*real_img_w, channels, kernels, img, localKernelLayout[(k*k)/2], gemmOutput);

  for (signed o_it = (k*k)/2; o_it == (k*k)/2; o_it++) {
    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] = gemmOutputLayout[pixelRow][pixelColumn][k_it];
        }
      }
    }
  }

  for (signed o_it = 0; o_it < ((signed)(k*k)/2); o_it++) {

    gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, real_img_h*real_img_w, channels, kernels, img, localKernelLayout[o_it], gemmOutput);

    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += gemmOutputLayout[pixelRow][pixelColumn][k_it];
        }
      }
    }
  }

  for (signed o_it = ((k*k)/2)+1; o_it < ((signed)(k*k)); o_it++) {

    gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, real_img_h*real_img_w, channels, kernels, img, localKernelLayout[o_it], gemmOutput);

    const signed destRowOffset = ((signed)(k/2)) - o_it/k;
    const signed destColumnOffset = ((signed)(k/2)) - o_it%k;
    const signed startRow = std::max(0, 0-destRowOffset);
    const signed endRow = std::min(real_img_h, real_img_h-destRowOffset);
    const signed startCol = std::max(0, 0-destColumnOffset);
    const signed endCol = std::min(real_img_w, real_img_w-destColumnOffset);
    for (unsigned k_it=0; k_it<kernels; ++k_it) {
      for (signed pixelRow=startRow; pixelRow<endRow; pixelRow++) {
        for (signed pixelColumn=startCol; pixelColumn<endCol; pixelColumn++) {
          signed kRowCenter = pixelRow+destRowOffset;
          signed kColCenter = pixelColumn+destColumnOffset;
          localOutputLayout[k_it][kRowCenter][kColCenter] += gemmOutputLayout[pixelRow][pixelColumn][k_it];
        }
      }
    }
  }

  delete [] gemmOutput;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_aa_A_B_K_I(T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  // Note: this is the img_manip_cols implementation
  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);

  unsigned extra_rows = std::ceil((double)(img_w * std::floor((double)k/2)) / (double)(img_h*img_w));

  constexpr signed pad = 10;

  T* tempOutput;
  tempOutput = new T[(kernels + 2*extra_rows + pad)*img_h*img_w](); // zero initialized by operator new ([C++11: 5.3.4/15])

  T* start_address = tempOutput + (unsigned) (std::floor((double)k/2)*(img_h*img_w+img_w+1));
  T* current_address = start_address;

  constexpr unsigned krad = k/2;
  T* rowBufferBacking = new T[channels*krad*img_w];
  auto rowBuffer = triNNity::internal::memory::View2D<T, channels, krad*img_w>(rowBufferBacking);
  T* colBufferBacking = new T[channels*krad*img_h];
  auto colBuffer = triNNity::internal::memory::View2D<T, channels, krad*img_h>(colBufferBacking);
  auto imgLayout = triNNity::internal::memory::View2D<T, channels, img_h*img_w>(img);
  unsigned savedKR = krad;
  unsigned savedKC = krad;

  for(unsigned k_c = 0; k_c < k; k_c++ ) {
    for(unsigned k_r = 0; k_r < k; k_r++ ) {
      unsigned o_it = k_r*k+k_c;
      unsigned offset = k_r*img_w;
      current_address = start_address-(offset+k_c);

      const unsigned prevTopRemoval =    savedKR > krad ? savedKR - krad : 0;
      const unsigned prevBottomRemoval = krad > savedKR ? krad - savedKR : 0;
      const unsigned prevLeftRemoval =   savedKC > krad ? savedKC - krad : 0;
      const unsigned prevRightRemoval =  krad > savedKC ? krad - savedKC : 0;
      const unsigned nextTopRemoval =    k_r > krad ? k_r - krad : 0;
      const unsigned nextBottomRemoval = krad > k_r ? krad - k_r : 0;
      const unsigned nextLeftRemoval =   k_c > krad ? k_c - krad : 0;
      const unsigned nextRightRemoval =  krad > k_c ? krad - k_c : 0;
      for (unsigned c = 0; c < channels; c++)
      {
        {  // restore previous value removal
          //restore the right columns
          unsigned cb_it = krad-prevRightRemoval;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = img_w-prevRightRemoval; w < img_w-nextRightRemoval; w++) {
              imgLayout[c][h*img_w + w] = colBuffer[c][cb_it];
              cb_it += krad;
            }
          }
          //restore the bottom rows
          unsigned rb_it = 0;
          for (unsigned h = img_h-prevBottomRemoval; h < img_h-nextBottomRemoval; h++) {
            rb_it = (krad-(img_h-h))*img_w;
            for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
              imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
              rb_it++;
            }
          }
          //restore the top rows
          if (nextTopRemoval == 0) {
            for (unsigned h = 0; h < prevTopRemoval; h++) {
              rb_it = h*img_w;
              for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
                imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
                rb_it++;
              }
            }
          }
        }

        { // zero out img values this kernel pixel shouldn't reach
          //punch out the right columns
          unsigned cb_it = 0;
          if (nextRightRemoval > prevRightRemoval) {
            for (unsigned h = 0; h < img_h; h++) {
              for (unsigned w = img_w-nextRightRemoval; w < img_w-prevRightRemoval; w++) {
                colBuffer[c][cb_it] = imgLayout[c][h*img_w + w];
                imgLayout[c][h*img_w + w] = 0;
                cb_it++;
              }
            }
          }
          //punch out the left columns
          cb_it = prevLeftRemoval*img_h;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = prevLeftRemoval; w < nextLeftRemoval; w++) {
              colBuffer[c][cb_it] = imgLayout[c][h*img_w + w];
              imgLayout[c][h*img_w + w] = 0;
              cb_it++;
            }
          }
          //punch out the bottom rows
          unsigned rb_it = 0;
          if (nextBottomRemoval > prevBottomRemoval) {
            for (unsigned h = img_h-nextBottomRemoval; h < img_h-prevBottomRemoval; h++) {
              rb_it = (krad-(img_h-h))*img_w;
              for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
                rowBuffer[c][rb_it] = imgLayout[c][h*img_w + w];
                imgLayout[c][h*img_w + w] = 0;
                rb_it++;
              }
            }
          }
          //punch out the top rows
          rb_it = prevTopRemoval*img_w;
          for (unsigned h = prevTopRemoval; h < nextTopRemoval; h++) {
            for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
              rowBuffer[c][rb_it] = imgLayout[c][h*img_w + w];
              imgLayout[c][h*img_w + w] = 0;
              rb_it++;
            }
          }
        }
        savedKR = k_r;
        savedKC = k_c;
      }/**/

      gemm<T, T, T, gemm_var, GEMM_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, img_h*img_w, channels, localKernelLayout[o_it], img, current_address);

      current_address--;
    }
  }

  T* the_data = tempOutput;
  T* first_e = the_data + (unsigned) (std::floor((double)k/2)*img_w*img_h);

  std::copy_n(first_e, kernels*img_w*img_h, out);
  delete [] tempOutput;

  // Restore the image back to how it was when it was passed in
  for(unsigned c = 0; c < channels; c++) {
    unsigned rb_it = 0;
    for (unsigned h = 0; h < krad; h++) {
      rb_it = h*img_w;
      for (unsigned w = krad; w < img_w; w++) {
         imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
         rb_it++;
      }
    }
    unsigned cb_it = 0;
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned w = 0; w < krad; w++) {
        imgLayout[c][h*img_w + w] = colBuffer[c][w*img_h + h];
        cb_it++;
      }
    }
  }

  delete [] rowBufferBacking;
  delete [] colBufferBacking;

}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_aa_A_BT_K_I(T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);

  unsigned extra_rows = std::ceil((double)(img_w * std::floor((double)k/2)) / (double)(img_h*img_w));

  constexpr signed pad = 10;

  T* tempOutput;
  tempOutput = new T[(kernels + 2*extra_rows + pad)*img_h*img_w](); // zero initialized by operator new ([C++11: 5.3.4/15])

  T* start_address = tempOutput + (unsigned) (std::floor((double)k/2)*(img_h*img_w+img_w+1));
  T* current_address = start_address;

  constexpr unsigned krad = k/2;
  T* rowBufferBacking = new T[channels*krad*img_w];
  auto rowBuffer = triNNity::internal::memory::View2D<T, krad*img_w, channels>(rowBufferBacking);
  T* colBufferBacking = new T[channels*krad*img_h];
  auto colBuffer = triNNity::internal::memory::View2D<T, krad*img_h, channels>(colBufferBacking);
  auto imgLayout = triNNity::internal::memory::View2D<T, img_h*img_w, channels>(img);
  unsigned savedKR = krad;
  unsigned savedKC = krad;

  for(unsigned k_c = 0; k_c < k; k_c++ ) {
    for(unsigned k_r = 0; k_r < k; k_r++ ) {
      unsigned o_it = k_r*k+k_c;
      unsigned offset = k_r*img_w;
      current_address = start_address-(offset+k_c);

      const unsigned prevTopRemoval =    savedKR > krad ? savedKR - krad : 0;
      const unsigned prevBottomRemoval = krad > savedKR ? krad - savedKR : 0;
      const unsigned prevLeftRemoval =   savedKC > krad ? savedKC - krad : 0;
      const unsigned prevRightRemoval =  krad > savedKC ? krad - savedKC : 0;
      const unsigned nextTopRemoval =    k_r > krad ? k_r - krad : 0;
      const unsigned nextBottomRemoval = krad > k_r ? krad - k_r : 0;
      const unsigned nextLeftRemoval =   k_c > krad ? k_c - krad : 0;
      const unsigned nextRightRemoval =  krad > k_c ? krad - k_c : 0;
      {
        {  // restore previous value removal
          //restore the right columns
          unsigned cb_it = krad-prevRightRemoval;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = img_w-prevRightRemoval; w < img_w-nextRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = colBuffer[cb_it][c]; }
              cb_it += krad;
            }
          }
          //restore the bottom rows
          unsigned rb_it = 0;
          for (unsigned h = img_h-prevBottomRemoval; h < img_h-nextBottomRemoval; h++) {
            rb_it = (krad-(img_h-h))*img_w;
            for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
              rb_it++;
            }
          }
          //restore the top rows
          if (nextTopRemoval == 0) {
            for (unsigned h = 0; h < prevTopRemoval; h++) {
              rb_it = h*img_w;
              for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
                rb_it++;
              }
            }
          }
        }

        { // zero out img values this kernel pixel shouldn't reach
          //punch out the right columns
          unsigned cb_it = 0;
          if (nextRightRemoval > prevRightRemoval) {
            for (unsigned h = 0; h < img_h; h++) {
              for (unsigned w = img_w-nextRightRemoval; w < img_w-prevRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) {
                  colBuffer[cb_it][c] = imgLayout[h*img_w + w][c];
                  imgLayout[h*img_w + w][c] = 0;
                }
                cb_it++;
              }
            }
          }
          //punch out the left columns
          cb_it = prevLeftRemoval*img_h;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = prevLeftRemoval; w < nextLeftRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) {
                colBuffer[cb_it][c] = imgLayout[h*img_w + w][c];
                imgLayout[h*img_w + w][c] = 0;
              }
              cb_it++;
            }
          }
          //punch out the bottom rows
          unsigned rb_it = 0;
          if (nextBottomRemoval > prevBottomRemoval) {
            for (unsigned h = img_h-nextBottomRemoval; h < img_h-prevBottomRemoval; h++) {
              rb_it = (krad-(img_h-h))*img_w;
              for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) {
                  rowBuffer[rb_it][c] = imgLayout[h*img_w + w][c];
                  imgLayout[h*img_w + w][c] = 0;
                }
                rb_it++;
              }
            }
          }
          //punch out the top rows
          rb_it = prevTopRemoval*img_w;
          for (unsigned h = prevTopRemoval; h < nextTopRemoval; h++) {
            for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) {
                rowBuffer[rb_it][c] = imgLayout[h*img_w + w][c];
                imgLayout[h*img_w + w][c] = 0;
              }
              rb_it++;
            }
          }
        }
        savedKR = k_r;
        savedKC = k_c;
      }

      gemm<T, T, T, gemm_var, GEMM_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, channels, img_h*img_w, localKernelLayout[o_it], img, current_address);

    }
  }

  T* the_data = tempOutput;
  T* first_e = the_data + (unsigned) (std::floor((double)k/2)*img_w*img_h);

  std::copy_n(first_e, kernels*img_w*img_h, out);
  delete [] tempOutput;

  // Restore the image back to how it was when it was passed in
  {
    unsigned rb_it = 0;
    for (unsigned h = 0; h < krad; h++) {
      rb_it = h*img_w;
      for (unsigned w = krad; w < img_w; w++) {
         for(unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
         rb_it++;
      }
    }
    unsigned cb_it = 0;
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned w = 0; w < krad; w++) {
        for(unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = colBuffer[w*img_h + h][c]; }
        cb_it++;
      }
    }
  }

  delete [] rowBufferBacking;
  delete [] colBufferBacking;
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_aa_AT_B_K_I(T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  // Note: this is the img_manip_cols implementation
  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);

  unsigned extra_rows = std::ceil((double)(img_w * std::floor((double)k/2)) / (double)(img_h*img_w));

  constexpr signed pad = 10;

  T* tempOutput;
  tempOutput = new T[(kernels + 2*extra_rows + pad)*img_h*img_w](); // zero initialized by operator new ([C++11: 5.3.4/15])

  T* start_address = tempOutput + (unsigned) (std::floor((double)k/2)*(img_h*img_w+img_w+1));
  T* current_address = start_address;

  constexpr unsigned krad = k/2;
  T* rowBufferBacking = new T[channels*krad*img_w];
  auto rowBuffer = triNNity::internal::memory::View2D<T, channels, krad*img_w>(rowBufferBacking);
  T* colBufferBacking = new T[channels*krad*img_h];
  auto colBuffer = triNNity::internal::memory::View2D<T, channels, krad*img_h>(colBufferBacking);
  auto imgLayout = triNNity::internal::memory::View2D<T, channels, img_h*img_w>(img);
  unsigned savedKR = krad;
  unsigned savedKC = krad;

  for(unsigned k_c = 0; k_c < k; k_c++ ) {
    for(unsigned k_r = 0; k_r < k; k_r++ ) {
      unsigned o_it = k_r*k+k_c;
      unsigned offset = k_r*img_w;
      current_address = start_address-(offset+k_c);

      const unsigned prevTopRemoval =    savedKR > krad ? savedKR - krad : 0;
      const unsigned prevBottomRemoval = krad > savedKR ? krad - savedKR : 0;
      const unsigned prevLeftRemoval =   savedKC > krad ? savedKC - krad : 0;
      const unsigned prevRightRemoval =  krad > savedKC ? krad - savedKC : 0;
      const unsigned nextTopRemoval =    k_r > krad ? k_r - krad : 0;
      const unsigned nextBottomRemoval = krad > k_r ? krad - k_r : 0;
      const unsigned nextLeftRemoval =   k_c > krad ? k_c - krad : 0;
      const unsigned nextRightRemoval =  krad > k_c ? krad - k_c : 0;
      for (unsigned c = 0; c < channels; c++)
      {
        {  // restore previous value removal
          //restore the right columns
          unsigned cb_it = krad-prevRightRemoval;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = img_w-prevRightRemoval; w < img_w-nextRightRemoval; w++) {
              imgLayout[c][h*img_w + w] = colBuffer[c][cb_it];
              cb_it += krad;
            }
          }
          //restore the bottom rows
          unsigned rb_it = 0;
          for (unsigned h = img_h-prevBottomRemoval; h < img_h-nextBottomRemoval; h++) {
            rb_it = (krad-(img_h-h))*img_w;
            for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
              imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
              rb_it++;
            }
          }
          //restore the top rows
          if (nextTopRemoval == 0) {
            for (unsigned h = 0; h < prevTopRemoval; h++) {
              rb_it = h*img_w;
              for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
                imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
                rb_it++;
              }
            }
          }
        }

        { // zero out img values this kernel pixel shouldn't reach
          //punch out the right columns
          unsigned cb_it = 0;
          if (nextRightRemoval > prevRightRemoval) {
            for (unsigned h = 0; h < img_h; h++) {
              for (unsigned w = img_w-nextRightRemoval; w < img_w-prevRightRemoval; w++) {
                colBuffer[c][cb_it] = imgLayout[c][h*img_w + w];
                imgLayout[c][h*img_w + w] = 0;
                cb_it++;
              }
            }
          }
          //punch out the left columns
          cb_it = prevLeftRemoval*img_h;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = prevLeftRemoval; w < nextLeftRemoval; w++) {
              colBuffer[c][cb_it] = imgLayout[c][h*img_w + w];
              imgLayout[c][h*img_w + w] = 0;
              cb_it++;
            }
          }
          //punch out the bottom rows
          unsigned rb_it = 0;
          if (nextBottomRemoval > prevBottomRemoval) {
            for (unsigned h = img_h-nextBottomRemoval; h < img_h-prevBottomRemoval; h++) {
              rb_it = (krad-(img_h-h))*img_w;
              for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
                rowBuffer[c][rb_it] = imgLayout[c][h*img_w + w];
                imgLayout[c][h*img_w + w] = 0;
                rb_it++;
              }
            }
          }
          //punch out the top rows
          rb_it = prevTopRemoval*img_w;
          for (unsigned h = prevTopRemoval; h < nextTopRemoval; h++) {
            for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
              rowBuffer[c][rb_it] = imgLayout[c][h*img_w + w];
              imgLayout[c][h*img_w + w] = 0;
              rb_it++;
            }
          }
        }
        savedKR = k_r;
        savedKC = k_c;
      }/**/

      gemm<T, T, T, gemm_var, GEMM_ACCUMULATE, GEMM_AT_B, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, channels, img_h*img_w, channels, localKernelLayout[o_it], img, current_address);

      current_address--;
    }
  }

  T* the_data = tempOutput;
  T* first_e = the_data + (unsigned) (std::floor((double)k/2)*img_w*img_h);

  std::copy_n(first_e, kernels*img_w*img_h, out);
  delete [] tempOutput;

  // Restore the image back to how it was when it was passed in
  for(unsigned c = 0; c < channels; c++) {
    unsigned rb_it = 0;
    for (unsigned h = 0; h < krad; h++) {
      rb_it = h*img_w;
      for (unsigned w = krad; w < img_w; w++) {
         imgLayout[c][h*img_w + w] = rowBuffer[c][rb_it];
         rb_it++;
      }
    }
    unsigned cb_it = 0;
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned w = 0; w < krad; w++) {
        imgLayout[c][h*img_w + w] = colBuffer[c][w*img_h + h];
        cb_it++;
      }
    }
  }

  delete [] rowBufferBacking;
  delete [] colBufferBacking;

}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void kn2row_aa_AT_BT_K_I(T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {

  auto localKernelLayout = triNNity::internal::memory::View2D<const KD, k*k, kernels*channels>(kernel);

  unsigned extra_rows = std::ceil((double)(img_w * std::floor((double)k/2)) / (double)(img_h*img_w));

  constexpr signed pad = 10;

  T* tempOutput;
  tempOutput = new T[(kernels + 2*extra_rows + pad)*img_h*img_w](); // zero initialized by operator new ([C++11: 5.3.4/15])

  T* start_address = tempOutput + (unsigned) (std::floor((double)k/2)*(img_h*img_w+img_w+1));
  T* current_address = start_address;

  constexpr unsigned krad = k/2;
  T* rowBufferBacking = new T[channels*krad*img_w];
  auto rowBuffer = triNNity::internal::memory::View2D<T, krad*img_w, channels>(rowBufferBacking);
  T* colBufferBacking = new T[channels*krad*img_h];
  auto colBuffer = triNNity::internal::memory::View2D<T, krad*img_h, channels>(colBufferBacking);
  auto imgLayout = triNNity::internal::memory::View2D<T, img_h*img_w, channels>(img);
  unsigned savedKR = krad;
  unsigned savedKC = krad;

  for(unsigned k_c = 0; k_c < k; k_c++ ) {
    for(unsigned k_r = 0; k_r < k; k_r++ ) {
      unsigned o_it = k_r*k+k_c;
      unsigned offset = k_r*img_w;
      current_address = start_address-(offset+k_c);

      const unsigned prevTopRemoval =    savedKR > krad ? savedKR - krad : 0;
      const unsigned prevBottomRemoval = krad > savedKR ? krad - savedKR : 0;
      const unsigned prevLeftRemoval =   savedKC > krad ? savedKC - krad : 0;
      const unsigned prevRightRemoval =  krad > savedKC ? krad - savedKC : 0;
      const unsigned nextTopRemoval =    k_r > krad ? k_r - krad : 0;
      const unsigned nextBottomRemoval = krad > k_r ? krad - k_r : 0;
      const unsigned nextLeftRemoval =   k_c > krad ? k_c - krad : 0;
      const unsigned nextRightRemoval =  krad > k_c ? krad - k_c : 0;
      {
        {  // restore previous value removal
          //restore the right columns
          unsigned cb_it = krad-prevRightRemoval;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = img_w-prevRightRemoval; w < img_w-nextRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = colBuffer[cb_it][c]; }
              cb_it += krad;
            }
          }
          //restore the bottom rows
          unsigned rb_it = 0;
          for (unsigned h = img_h-prevBottomRemoval; h < img_h-nextBottomRemoval; h++) {
            rb_it = (krad-(img_h-h))*img_w;
            for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
              rb_it++;
            }
          }
          //restore the top rows
          if (nextTopRemoval == 0) {
            for (unsigned h = 0; h < prevTopRemoval; h++) {
              rb_it = h*img_w;
              for (unsigned w = prevLeftRemoval; w < img_w - prevRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
                rb_it++;
              }
            }
          }
        }

        { // zero out img values this kernel pixel shouldn't reach
          //punch out the right columns
          unsigned cb_it = 0;
          if (nextRightRemoval > prevRightRemoval) {
            for (unsigned h = 0; h < img_h; h++) {
              for (unsigned w = img_w-nextRightRemoval; w < img_w-prevRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) {
                  colBuffer[cb_it][c] = imgLayout[h*img_w + w][c];
                  imgLayout[h*img_w + w][c] = 0;
                }
                cb_it++;
              }
            }
          }
          //punch out the left columns
          cb_it = prevLeftRemoval*img_h;
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = prevLeftRemoval; w < nextLeftRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) {
                colBuffer[cb_it][c] = imgLayout[h*img_w + w][c];
                imgLayout[h*img_w + w][c] = 0;
              }
              cb_it++;
            }
          }
          //punch out the bottom rows
          unsigned rb_it = 0;
          if (nextBottomRemoval > prevBottomRemoval) {
            for (unsigned h = img_h-nextBottomRemoval; h < img_h-prevBottomRemoval; h++) {
              rb_it = (krad-(img_h-h))*img_w;
              for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
                for (unsigned c = 0; c < channels; c++) {
                  rowBuffer[rb_it][c] = imgLayout[h*img_w + w][c];
                  imgLayout[h*img_w + w][c] = 0;
                }
                rb_it++;
              }
            }
          }
          //punch out the top rows
          rb_it = prevTopRemoval*img_w;
          for (unsigned h = prevTopRemoval; h < nextTopRemoval; h++) {
            for (unsigned w = nextLeftRemoval; w < img_w - nextRightRemoval; w++) {
              for (unsigned c = 0; c < channels; c++) {
                rowBuffer[rb_it][c] = imgLayout[h*img_w + w][c];
                imgLayout[h*img_w + w][c] = 0;
              }
              rb_it++;
            }
          }
        }
        savedKR = k_r;
        savedKC = k_c;
      }

      gemm<T, T, T, gemm_var, GEMM_ACCUMULATE, GEMM_AT_BT, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, channels, channels, img_h*img_w, localKernelLayout[o_it], img, current_address);

    }
  }

  T* the_data = tempOutput;
  T* first_e = the_data + (unsigned) (std::floor((double)k/2)*img_w*img_h);

  std::copy_n(first_e, kernels*img_w*img_h, out);
  delete [] tempOutput;

  // Restore the image back to how it was when it was passed in
  {
    unsigned rb_it = 0;
    for (unsigned h = 0; h < krad; h++) {
      rb_it = h*img_w;
      for (unsigned w = krad; w < img_w; w++) {
         for(unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = rowBuffer[rb_it][c]; }
         rb_it++;
      }
    }
    unsigned cb_it = 0;
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned w = 0; w < krad; w++) {
        for(unsigned c = 0; c < channels; c++) { imgLayout[h*img_w + w][c] = colBuffer[w*img_h + h][c]; }
        cb_it++;
      }
    }
  }

  delete [] rowBufferBacking;
  delete [] colBufferBacking;
}

//im2col like function inspired by https://arxiv.org/pdf/1706.06873.pdf
template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void mec_col(const T * __restrict__ img, const KD * __restrict__ ker, T * __restrict__ out) {
  const unsigned krad = k/2;
  const unsigned padded_h = img_h + krad*2;
  const unsigned real_img_h = img_h + (bound==triNNity::BOUND_EXPLICIT ? krad*2 : 0);
  const unsigned real_img_w = img_w + (bound==triNNity::BOUND_EXPLICIT ? krad*2 : 0);
  const unsigned out_h = triNNity::uceil<img_h, stride_h>();
  const unsigned out_w = triNNity::uceil<img_w, stride_w>();

  auto image  = triNNity::internal::memory::View3D<const T, real_img_h, channels, real_img_w>(img);
  auto kernel = triNNity::internal::memory::View4D<const T, kernels, k, k, channels>(ker);
  auto output = triNNity::internal::memory::View3D<T, out_h, out_w, kernels>(out);

  T * lecBacking = new T[padded_h * k * channels * out_w];
  auto lec = triNNity::internal::memory::View4D<T, padded_h, channels, k, out_w>(lecBacking);

  {
    //filling up lec
    switch (bound) {
      case BOUND_UNDEF:
      case BOUND_EXPLICIT: {
        for (unsigned h = 0; h < padded_h; h++) {
          for (unsigned c = 0; c < channels; c++) {
            for (unsigned ki = 0; ki < k; ki++) {
              #if defined(TRINNITY_USE_OPENMP)
              #pragma omp simd
              #endif
              for (unsigned w = 0; w < out_w; w++) {
                lec[h][c][ki][w] = image[h][c][ki + w*stride_w];
              }
            }
          }
        }

      } break;

      case BOUND_IMPLICIT_PAD: {
        for (unsigned h = 0; h < krad; h++) {
          for (unsigned c = 0; c < channels; c++) {
            for (unsigned ki = 0; ki < k; ki++) {
              #if defined(TRINNITY_USE_OPENMP)
              #pragma omp simd
              #endif
              for (unsigned ow = 0; ow < out_w; ow++) {
                lec[h][c][ki][ow] = 0;
              }
            }
          }
        }
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned c = 0; c < channels; c++) {
            for (unsigned ki = 0; ki < krad; ki++) {
              unsigned wCounter = 0;
              for (unsigned iw = 0; iw < krad-ki; iw+=stride_w) {
                lec[h+krad][c][ki][wCounter] = 0;
                wCounter++;
              }
              for (unsigned iw = wCounter*stride_w; iw < img_w; iw+=stride_w) {
                lec[h+krad][c][ki][wCounter] = image[h][c][ki + iw - krad];
                wCounter++;
              }
            }
            #if defined(TRINNITY_USE_OPENMP)
            #pragma omp simd
            #endif
            for (unsigned ow = 0; ow < out_w; ow++) {
              lec[h+krad][c][krad][ow] = image[h][c][ow*stride_w];
            }
            for (unsigned ki = 0; ki < krad; ki++) {
              unsigned wCounter = 0;
              for (unsigned iw = 0; iw < img_w-(ki+1); iw+=stride_w) {
                lec[h+krad][c][ki+krad+1][wCounter] = image[h][c][ki+krad+1 + iw - krad];
                wCounter++;
              }
              #if defined(TRINNITY_USE_OPENMP)
              #pragma omp simd
              #endif
              for (unsigned ow = wCounter; ow < out_w; ow++) {
                lec[h+krad][c][ki+krad+1][ow] = 0;
              }
            }
          }
        }
        for (unsigned h = 0; h < krad; h++) {
          for (unsigned c = 0; c < channels; c++) {
            for (unsigned ki = 0; ki < k; ki++) {
              #if defined(TRINNITY_USE_OPENMP)
              #pragma omp simd
              #endif
              for (unsigned ow = 0; ow < out_w; ow++) {
                lec[h+krad+img_h][c][ki][ow] = 0;
              }
            }
          }
        }
      } break;

      default: {
        TRINNITY_ERROR("not implemented");
      } break;

    }

  }

  //using lec
  for (unsigned oh = 0; oh < out_h; oh++) {
    gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_BT>(out_w, k*channels*k, k*channels*k, kernels, &lecBacking[(oh*stride_h*k)*channels*out_w + 0], &kernel[0][0][0][0], &output[oh][0][0]);
  }

}

//im2col like function inspired by https://arxiv.org/pdf/1706.06873.pdf,
template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void mec_row_partition(const T * __restrict__ img, const KD * __restrict__ ker, T * __restrict__ out) {
  const unsigned krad = k/2;
  const unsigned padded_h = img_h + krad*2;
  const unsigned real_img_h = img_h + (bound==triNNity::BOUND_EXPLICIT ? krad*2 : 0);
  const unsigned real_img_w = img_w + (bound==triNNity::BOUND_EXPLICIT ? krad*2 : 0);
  const unsigned out_h = triNNity::uceil<img_h, stride_h>();
  const unsigned out_w = triNNity::uceil<img_w, stride_w>();

  auto image  = triNNity::internal::memory::View3D<const T, real_img_h, real_img_w, channels>(img);
  auto output = triNNity::internal::memory::View3D<T, out_h, out_w, kernels>(out);

  T * lecBacking = new T[out_w * padded_h * k * channels];
  auto lec = triNNity::internal::memory::View4D<T, out_w, padded_h, k, channels>(lecBacking);
  auto flec = triNNity::internal::memory::View3D<T, out_w, padded_h*k, channels>(lecBacking);

  T * paritionBacking = new T[out_w * k*k * channels];
  auto partition = triNNity::internal::memory::View3D<T, out_w, k*k, channels>(paritionBacking);

  switch (bound) {
    case BOUND_UNDEF:
    case BOUND_IMPLICIT_PAD: {
      {
        //filling up lec
        for (unsigned ow = 0; ow < out_w; ow++) {
          unsigned hPos = 0;
          for (unsigned h = 0; h < krad; h++) { //implicit top rows of zeroes on image
            for (unsigned i = 0; i < k; i++) {
              memset(lec[ow][hPos][i], 0, channels*sizeof(T));
            }
            hPos++;
          }

          for (unsigned h = 0; h < img_h; h++) { //actual rows of the image
            int wPos = 0;
            const signed startW = static_cast<signed>(ow*stride_w) - krad;
            const unsigned endW = (ow*stride_w + krad)+1;
            if (startW < 0) { //implicit zero columns on the left of the image
              for (int i = startW; i < 0; i++) {
                memset(lec[ow][hPos][wPos], 0, channels*sizeof(T));
                wPos++;
              }
            }
            //The actual columns of the image
            const unsigned imgSW = (startW < 0 ? 0 : startW);
            const unsigned imgEW = (endW >= img_h ? img_h : endW);
            for (unsigned i = imgSW; i < imgEW; i++) {
              memcpy(lec[ow][hPos][wPos], image[h][i], channels*sizeof(T));
              wPos++;
            }
            if (endW >= img_w) {//implicit zero columns on the right of the image
              for (unsigned i = img_w; i < endW; i++) {
                memset(lec[ow][hPos][wPos], 0, channels*sizeof(T));
                wPos++;
              }
            }
            hPos++;
          }

          for (unsigned h = 0; h < krad; h++) { //implicit bottom rows of zeroes on image
            for (unsigned i = 0; i < k; i++) {
              memset(lec[ow][hPos][i], 0, channels*sizeof(T));
            }
            hPos++;
          }
        }
      }
    } break;

    case BOUND_EXPLICIT: {
      for (unsigned h = 0; h < padded_h; h++) {
        for (unsigned w = 0; w < out_w; w++) {
          for (unsigned ki = 0; ki < k; ki++) {
            for (unsigned c = 0; c < channels; c++) {
              //auto image  = triNNity::internal::memory::View3D<const T, real_img_h, real_img_w, channels>(img);
              //auto lec = triNNity::internal::memory::View4D<T, out_w, padded_h, k, channels>(lecBacking);
              lec[w][h][ki][c] = image[h][ki + w*stride_w][c];
            }
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("not implemented.");
    } break;
  }

  //gemming the partitions
  for (unsigned oh = 0; oh < out_h; oh++) {
    for (unsigned ow = 0; ow < out_w; ow++) {
      for (unsigned kk = 0; kk < k*k; kk++) {
        memcpy(partition[ow][kk], flec[ow][oh*stride_h*k + kk], channels*sizeof(T));
      }
    }
    gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(k*k*channels, out_w, k*k*channels, kernels, partition[0][0], ker, &output[oh][0][0]);
  }


  delete [] paritionBacking;
  delete [] lecBacking;

}

template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, gemm_transpose_t gemm_transp, gemm_arrangement_t gemm_arr, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k>
static TRINNITY_INLINE void conv_1x1_gemm(const T * __restrict__ img, const KD * __restrict__ kernel, T * __restrict__ out) {
  switch (gemm_arr) {
    case GEMM_IK: {
      switch (gemm_transp) {
        case GEMM_A_BT: {
          //HWC, MC, IK --> HWC
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, img_h*img_w, channels, kernels, img, kernel, out);
        } break;
        case GEMM_A_B: {
          //HWC, CM, IK --> HWC
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, img_h*img_w, kernels, channels, img, kernel, out);
        } break;
        case GEMM_AT_BT: {
          //CHW, MC, IK --> HWC
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_BT, gemm_block_i, gemm_block_j, gemm_block_k>(img_h*img_w, channels, channels, kernels, img, kernel, out);
        } break;
        case GEMM_AT_B: {
          //CHW, CM, IK --> HWC
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_B, gemm_block_i, gemm_block_j, gemm_block_k>(img_h*img_w, channels, kernels, channels, img, kernel, out);
        } break;
        default: {
          TRINNITY_ERROR("not implemented.");
        } break;
      }
    } break;

    case GEMM_KI: {
      switch (gemm_transp) {
        case GEMM_A_BT: {
          //MC, HWC, KI --> CHW
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, channels, img_h*img_w, kernel, img, out);
        } break;
        case GEMM_A_B: {
          //MC, CHW, KI --> CHW
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_B, gemm_block_i, gemm_block_j, gemm_block_k>(channels, kernels, img_h*img_w, channels, kernel, img, out);
        } break;
        case GEMM_AT_BT: {
          //CM, HWC, KI --> CHW
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_BT, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, channels, channels, img_h*img_w, kernel, img, out);
        } break;
        case GEMM_AT_B: {
          //CM, CHW, KI --> CHW
          gemm<T, T, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_AT_B, gemm_block_i, gemm_block_j, gemm_block_k>(kernels, channels, img_h*img_w, channels, kernel, img, out);
        } break;
        default: {
          TRINNITY_ERROR("not implemented.");
        } break;
      }
    } break;

    default: {
      TRINNITY_ERROR("not implemented");
    } break;
  }
}

}

}

}

}

#endif // TRINNITY_DENSE_CPU_IMPL_H
