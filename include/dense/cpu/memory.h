/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_MEMORY_H
#define TRINNITY_DENSE_CPU_MEMORY_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace dense {

namespace cpu {

namespace memory {

/**
 * Build a kernel in the specified format which is populated with random data.
 *
 * Kernel data is uniform random noise within a symmetric band around zero.
 * The bandwidth of the noise is controlled using the scale parameter.
 *
 * For example, setting scale to 2 will produce a kernel with values uniformly randomly distributed between -2 and +2
 *
 * \tparam KD
 *
 * \parblock
 * The primitive type of the kernel data.
 * \endparblock
 * \tparam k_w
 * \parblock
 * The width of the convolution kernel
 * \endparblock
 * \tparam k_h
 * \parblock
 * The height of the convolution kernel
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam shape
 * \parblock
 * The format of the kernel data
 * \endparblock
 *
 * \param scale
 * \parblock
 * The bandwidth of the kernel values [-scale, +scale]
 * \endparblock
 */

template <typename KD, unsigned k_w, unsigned k_h, unsigned ofms, unsigned ifms, triNNity::layout::parameter_layout_t shape>
static TRINNITY_INLINE KD* buildRandomKernel(double scale) {
  std::random_device rd;
  KD *kernel = new KD[ofms * ifms * k_h * k_w]();

  switch (shape) {
    case triNNity::layout::OIHW: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, ofms, ifms, k_h, k_w>(kernel);

      for (signed kx = 0; kx < ((signed)ofms); kx++) {
        for (signed x = 0; x < ((signed)ifms); x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              localKernelLayout[kx][x][i][j] = static_cast<KD>((distribution(gen) - scale));
            }
          }
        }
      }
    } break;

    case triNNity::layout::HWOI: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, k_h, k_w, ofms, ifms>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              localKernelLayout[i][j][kx][x] = static_cast<KD>((distribution(gen) - scale));
            }
          }
        }
      }
    } break;

    case triNNity::layout::IHWO: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, ifms, k_h, k_w, ofms>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              localKernelLayout[x][j][i][kx] = static_cast<KD>((distribution(gen) - scale));
            }
          }
        }
      }
    } break;

    case triNNity::layout::HWIO: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, k_h, k_w, ifms, ofms>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              localKernelLayout[i][j][x][kx] = static_cast<KD>((distribution(gen) - scale));
            }
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }

  return kernel;
}

/**
 * Build a kernel in the specified format which is populated with random data.
 *
 * Kernel data is uniform random noise within a symmetric band around zero.
 * The bandwidth of the noise is controlled using the scale parameter.
 *
 * For example, setting scale to 2 will produce a kernel with values uniformly randomly distributed between -2 and +2
 *
 * This function additionally takes a sparsity parameter between 0.0 and 1.0 which controls how sparse the generated data will be.
 * For example, setting sparsity to 0.5 will result in a kernel where 50% of the values are zero.
 * Sparsity ratios below 0 or abov 1 result in an all-zeroes kernel.
 *
 * \tparam KD
 *
 * \parblock
 * The primitive type of the kernel data.
 * \endparblock
 * \tparam k_w
 * \parblock
 * The width of the convolution kernel
 * \endparblock
 * \tparam k_h
 * \parblock
 * The height of the convolution kernel
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam shape
 * \parblock
 * The format of the kernel data
 * \endparblock
 *
 * \param sparsity
 * \parblock
 * Sparsity ratio of the generated kernel
 * \endparblock
 * \param scale
 * \parblock
 * The bandwidth of the kernel values [-scale, +scale]
 * \endparblock
 */

template <typename KD, unsigned k_w, unsigned k_h, unsigned ofms, unsigned ifms, triNNity::layout::parameter_layout_t shape>
static TRINNITY_INLINE KD* buildSparseRandomKernel(double sparsity, double scale) {
  std::random_device rd;
  KD *kernel = new KD[ofms * ifms * k_h * k_w]();

  switch (shape) {
    case triNNity::layout::OIHW: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, ofms, ifms, k_h, k_w>(kernel);

      for (signed kx = 0; kx < ((signed)ofms); kx++) {
        for (signed x = 0; x < ((signed)ifms); x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);
          std::uniform_real_distribution<double> sparseness(0.0, 1.0);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              if (sparseness(gen) >= std::abs(sparsity)) {
                localKernelLayout[kx][x][i][j] = static_cast<KD>((distribution(gen) - scale));
              }
            }
          }
        }
      }
    } break;

    case triNNity::layout::HWOI: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, k_h, k_w, ofms, ifms>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          std::default_random_engine gen(rd());
          std::uniform_real_distribution<double> distribution(0.0, 2.0 * scale);
          std::uniform_real_distribution<double> sparseness(0.0, 1.0);

          for (unsigned i = 0; i < k_h; i++) {
            for (unsigned j = 0; j < k_w; j++) {
              if (sparseness(gen) >= std::abs(sparsity)) {
                localKernelLayout[i][j][kx][x] = static_cast<KD>((distribution(gen) - scale));
              }
            }
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }

  return kernel;
}

/**
 * Build an identity convolution kernel in the specified format.
 *
 *
 * \tparam KD
 *
 * \parblock
 * The primitive type of the kernel data.
 * \endparblock
 * \tparam k
 * \parblock
 * The radix of the convolution kernel
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam shape
 * \parblock
 * The format of the kernel data
 * \endparblock
 */

template <typename KD, unsigned k, unsigned ofms, unsigned ifms, triNNity::layout::parameter_layout_t shape>
static TRINNITY_INLINE KD* buildIdentityKernel() {
  std::random_device rd;
  KD *kernel = new KD[ofms * ifms * k * k]();

  switch (shape) {
    case triNNity::layout::OIHW: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, ofms, ifms, k, k>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          for (unsigned i = 0; i < k; i++) {
            for (unsigned j = 0; j < k; j++) {
              localKernelLayout[kx][x][i][j] = static_cast<KD>(0);
            }
          }
          localKernelLayout[kx][x][k/2][k/2] = static_cast<KD>(1);
        }
      }
    } break;

    case triNNity::layout::HWOI: {
      auto localKernelLayout = triNNity::internal::memory::View4D<KD, k, k, ofms, ifms>(kernel);

      for (signed kx = 0; kx < (signed)ofms; kx++) {
        for (signed x = 0; x < (signed)ifms; x++) {
          for (unsigned i = 0; i < k; i++) {
            for (unsigned j = 0; j < k; j++) {
              localKernelLayout[i][j][kx][x] = static_cast<KD>(0);
            }
          }
          localKernelLayout[k/2][k/2][kx][x] = static_cast<KD>(1);
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }

  return kernel;
}

/**
 *  Apply padding to an input array to obtain an explicitly padded output.
 *
 *  NB: We assume the kernel is symmetric (k x k)
 */

template <typename T, unsigned k, unsigned ifms, unsigned ifm_h, unsigned ifm_w, triNNity::layout::feature_map_layout_t shape, triNNity::padding_t padding>
static TRINNITY_INLINE void pad(const T * __restrict__ img, T *__restrict__ output) {

  switch(shape) {

    case triNNity::layout::CHW: {
      auto localImageLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h, ifm_w>(img);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, ifms, ifm_h+(k-1), ifm_w+(k-1)>(output);

      switch(padding) {
        case triNNity::PAD_FAST: {
          for (unsigned c = 0; c < ifms; c++) {
            for (unsigned h = 0; h < ifm_h; h++) {
              for (unsigned w = 0; w < ifm_w; w++) {
                localOutputLayout[c][h+(k-1)][w+(k-1)] = localImageLayout[c][h][w];
              }
            }
          }
        } break;

        // A good strategy for the rest of the padding cases is to recursively call ourselves with PAD_FAST first,
        // Then iterate over the output again just afterwards - there's a good chance the output will be in cache.

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }
    } break;

    case triNNity::layout::HWC: {
      auto localImageLayout = triNNity::internal::memory::View3D<T, ifm_h, ifm_w, ifms>(img);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h+(k-1), ifm_w+(k-1), ifms>(output);

      switch(padding) {
        case triNNity::PAD_FAST: {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned w = 0; w < ifm_w; w++) {
              for (unsigned c = 0; c < ifms; c++) {
                localOutputLayout[h+(k-1)][w+(k-1)][c] = localImageLayout[h][w][c];
              }
            }
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }
    } break;

    case triNNity::layout::HCW: {
      auto localImageLayout = triNNity::internal::memory::View3D<T, ifm_h, ifms, ifm_w>(img);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, ifm_h+(k-1), ifms, ifm_w+(k-1)>(output);

      switch(padding) {
        case triNNity::PAD_FAST: {
          for (unsigned h = 0; h < ifm_h; h++) {
            for (unsigned c = 0; c < ifms; c++) {
              for (unsigned w = 0; w < ifm_w; w++) {
                localOutputLayout[h+(k-1)][c][w+(k-1)] = localImageLayout[h][c][w];
              }
            }
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

}

}

}

}

#endif // TRINNITY_DENSE_CPU_MEMORY_H
