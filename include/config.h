/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_CONFIG_H
#define TRINNITY_CONFIG_H

#if defined(DEBUG)
#define TRINNITY_INLINE
#else
#define TRINNITY_INLINE inline
//__attribute__((always_inline))
#endif

#if defined(TRINNITY_NO_EXCEPTIONS)
#define TRINNITY_ERROR(x) fprintf(stderr, "triNNity: %s:%d %s\n", __FILE__, __LINE__, x); exit(1)
#else
#define TRINNITY_ERROR(x) fprintf(stderr, "triNNity: %s:%d %s\n", __FILE__, __LINE__, x); throw std::runtime_error(x)
#endif

#if defined(TRINNITY_NO_ASSERTS)
#define TRINNITY_ASSERT(x)
#define TRINNITY_ASSERT_ON(x, y)
#define TRINNITY_STATIC_ASSERT(x)
#define TRINNITY_STATIC_ASSERT_ON(x, y)
#else
#define TRINNITY_ASSERT(x) assert(x)
#define TRINNITY_ASSERT_ON(x, y) assert(x ? y : true)
#define TRINNITY_STATIC_ASSERT(x) static_assert(x)
#define TRINNITY_STATIC_ASSERT_ON(x, y) static_assert(x ? y : true)
#endif

#define TRINNITY_CUDA_ASSERT(err, file, line) \
{ \
   if constexpr (err != cudaSuccess) \
   { \
      fprintf(stderr,"triNNity (CUDA): %s:%d %s\n", file, line, cudaGetErrorString(err)); \
      exit(err); \
   } \
}

#define TRINNITY_CUDA_CHECK(ret) TRINNITY_CUDA_ASSERT((ret), __FILE__, __LINE__)

#if defined(TRINNITY_DISABLE_LAYER_TIMING)
#define TRINNITY_TIMESTAMP(x)
#else
#define TRINNITY_TIMESTAMP(x) clock_gettime(CLOCK_MONOTONIC, x)
#endif

#if defined(TRINNITY_NO_DEALLOCATE_LAYERS)
#define TRINNITY_DELETE(x) delete x
#else
#define TRINNITY_DELETE(x)
#endif

#if defined(TRINNITY_NO_DEALLOCATE_LAYERS)
#define TRINNITY_DELETE_ARRAY(x) delete [] x
#else
#define TRINNITY_DELETE_ARRAY(x)
#endif

namespace triNNity {

/*
 * Integer ceiling division
 */

template<unsigned numerator, unsigned denominator>
static constexpr unsigned uceil() {
  return (numerator/denominator) + ((numerator%denominator) > 0);
}

template<signed numerator, signed denominator>
static constexpr signed sceil() {
  return (numerator/denominator) + ((numerator%denominator) > 0);
}

template<typename T>
static constexpr T static_min(T a, T b) {
  return (a <= b ? a : b);
}

}

#endif // TRINNITY_CONFIG_H
