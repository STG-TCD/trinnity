/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_SHAPE_H
#define TRINNITY_SPECTRAL_CPU_SHAPE_H

#include <iostream>

#include <triNNity/config.h>
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/view.h>

#if defined(TRINNITY_USE_FFTW)
#include <fftw3.h>
#if !defined(TRINNITY_FFTW_MODE)
#define TRINNITY_FFTW_MODE FFTW_ESTIMATE
#endif
#endif

#include <type_traits>
#include <complex>

#include <triNNity/spectral/cpu/primitive.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace spectral {

namespace cpu {

namespace transform {


template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned k, unsigned stride_w = 1>
static TRINNITY_INLINE void image_polyphase_transform(const T * __restrict__ image_normal, T * __restrict__ image_stride) {

  // constexpr unsigned img_w_s = triNNity::uceil<img_w, stride_w>() + (k/2) - (k/2)/stride_w;
  constexpr unsigned img_w_s = triNNity::uceil<img_w, stride_w>() + (k/2);

  auto v_image_normal = triNNity::internal::memory::View3D<const T, channels,           img_h, img_w>(image_normal);
  auto v_image_stride = triNNity::internal::memory::View4D<      T, channels, stride_w, img_h, img_w_s>(image_stride);

  for (unsigned c = 0; c < channels; c++) {
    for (unsigned h = 0; h < img_h; h++) {

      unsigned s = (k/2) % stride_w;
      unsigned f = (k/2) - (k/2)/stride_w;

      for (unsigned w = 0; w < img_w; w++) {
        /////////////////////////////////////////////////////////////////////////
        // if constexpr (f >= img_w_s) {
        //   std::cout << "(f=" << f << ")" << std::endl;
        //   TRINNITY_ERROR("ERROR: img f has gone out of bounds.");
        // }
        // else if constexpr (s >= stride_w) {
        //   std::cout << "(s=" << s << ")" << std::endl;
        //   TRINNITY_ERROR("ERROR: img s has gone out of bounds.");
        // }
        /////////////////////////////////////////////////////////////////////////
        v_image_stride[c][s][h][f] = v_image_normal[c][h][w];
        if (s == 0) {
          s = stride_w - 1;
          f++;
        } else {
          s--;
        }
      }
    }
  }
}

template <typename T, unsigned k, unsigned kernels, unsigned channels, unsigned stride_w = 1>
static TRINNITY_INLINE void kernel_polyphase_transform(const T * __restrict__ kernel_normal, T * __restrict__ kernel_stride) {

  auto v_kernel_normal = triNNity::internal::memory::View4D<const T, kernels, channels,           k, k>(kernel_normal);
  auto v_kernel_stride = triNNity::internal::memory::View5D<      T, kernels, channels, stride_w, k, k>(kernel_stride);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned y = 0; y < k; y++) {
        for (unsigned x = 0; x < k; x++) {
          ///////////////////////////////////////////////////////////////////////
          // if constexpr (x%stride_w >= stride_w) {
          //   TRINNITY_ERROR("ERROR: kernel s has gone out of bounds.");
          // }
          // else if constexpr (x/stride_w >= k) {
          //   TRINNITY_ERROR("ERROR: kernel x/stride has gone out of bounds.");
          // }
          ///////////////////////////////////////////////////////////////////////
          v_kernel_stride[m][c][x%stride_w][y][x/stride_w] = v_kernel_normal[m][c][y][k - x - 1];
        }
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////
//  ______ ______ _______   _______                   __                          //
// |  ____|  ____|__   __| |__   __|                 / _|                         //
// | |__  | |__     | |       | |_ __ __ _ _ __  ___| |_ ___  _ __ _ __ ___  ___  //
// |  __| |  __|    | |       | | '__/ _` | '_ \/ __|  _/ _ \| '__| '_ ` _ \/ __| //
// | |    | |       | |       | | | | (_| | | | \__ \ || (_) | |  | | | | | \__ \ //
// |_|    |_|       |_|       |_|_|  \__,_|_| |_|___/_| \___/|_|  |_| |_| |_|___/ //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned k, unsigned fourier_domain_length, unsigned stride_w>
static TRINNITY_INLINE void image_chw_to_chf_complex(const T * __restrict__ image_space, std::complex<T> * __restrict__ image_freq) {

  // constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>() + (k/2) - (k/2)/stride_w;
  constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>() + (k/2);
  // constexpr unsigned out_w = fourier_domain_length;

  T *image_stride  = new T[channels*stride_w*img_h*out_w]();
  image_polyphase_transform<T, img_h, img_w, channels, k, stride_w>(image_space, image_stride);

  auto v_image_space = triNNity::internal::memory::View4D<             T , channels, stride_w, img_h, out_w>(image_stride);
  auto v_image_freq  = triNNity::internal::memory::View4D<std::complex<T>, channels, stride_w, img_h, fourier_domain_length>(image_freq);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      fftwf_complex *freq, *space;
      fftwf_plan p;
      space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
      p = fftwf_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned f = 0; f < out_w; f++) {
              space[f][0] = (float) v_image_space[c][s][h][f];
              space[f][1] = 0.0;
            }
            for (unsigned f = out_w; f < fourier_domain_length; f++) {
              space[f][0] = 0.0;
              space[f][1] = 0.0;
            }
            fftwf_execute(p);
            for (unsigned f = 0; f < fourier_domain_length; f++) {
              v_image_freq[c][s][h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
            }
          }
        }
      }

      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      fftw_complex *freq, *space;
      fftw_plan p;
      space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
      p = fftw_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned f = 0; f < out_w; f++) {
              space[f][0] = (double) v_image_space[c][s][h][f];
              space[f][1] = 0.0;
            }
            for (unsigned f = out_w; f < fourier_domain_length; f++) {
              space[f][0] = 0.0;
              space[f][1] = 0.0;
            }
            fftw_execute(p);
            for (unsigned f = 0; f < fourier_domain_length; f++) {
              v_image_freq[c][s][h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
            }
          }
        }
      }

      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    } else {
      TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_complex_fftw - std::is_same<T, ?>::value");
    }
  #else
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned s = 0; s < stride_w; s++) {
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned f = 0; f < out_w; f++) {
            v_image_freq[c][s][h][f].real(v_image_space[c][s][h][f]);
            v_image_freq[c][s][h][f].imag(0.0);
          }
          for (unsigned f = out_w; f < fourier_domain_length; f++) {
            v_image_freq[c][s][h][f].real(0.0);
            v_image_freq[c][s][h][f].imag(0.0);
          }
          triNNity::spectral::cpu::primitive::forwardComplexFFT<T, fourier_domain_length>((T *) v_image_freq[c][s][h]);
        }
      }
    }
  #endif
  delete [] image_stride;
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned k, unsigned fourier_domain_length, unsigned stride_w>
static TRINNITY_INLINE void image_chw_to_chf_real(const T * __restrict__ image_space, std::complex<T> * __restrict__ image_freq) {

  constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>() + (k/2);

  T *image_stride  = new T[channels*stride_w*img_h*out_w]();
  image_polyphase_transform<T, img_h, img_w, channels, k, stride_w>(image_space, image_stride);

  auto v_image_space = triNNity::internal::memory::View4D<             T , channels, stride_w, img_h, out_w>(image_stride);
  auto v_image_freq  = triNNity::internal::memory::View4D<std::complex<T>, channels, stride_w, img_h, (fourier_domain_length/2 + 1)>(image_freq);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      float *space;
      fftwf_complex *freq;
      fftwf_plan p;
      space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * (fourier_domain_length/2 + 1));

      p = fftwf_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned f = 0; f < out_w; f++) {
              space[f] = (float) v_image_space[c][s][h][f];
            }
            for (unsigned f = out_w; f < fourier_domain_length; f++) {
              space[f] = 0.0;
            }
            fftwf_execute(p);
            for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
              v_image_freq[c][s][h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
            }
          }
        }
      }
      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      double *space;
      fftw_complex *freq;
      fftw_plan p;
      space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (fourier_domain_length/2 + 1));

      p = fftw_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned f = 0; f < out_w; f++) {
              space[f] = (double) v_image_space[c][s][h][f];
            }
            for (unsigned f = out_w; f < fourier_domain_length; f++) {
              space[f] = 0.0;
            }
            fftw_execute(p);
            for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
              v_image_freq[c][s][h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
            }
          }
        }
      }
      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    } else {
    }
  #else
    T *freq = new T[fourier_domain_length];
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned s = 0; s < stride_w; s++) {
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned f = 0; f < out_w; f++) {
            freq[f] = v_image_space[c][s][h][f];
          }
          for (unsigned f = out_w; f < fourier_domain_length; f++) {
            freq[f] = 0.0;
          }
          triNNity::spectral::cpu::primitive::forwardRealFFT<T, fourier_domain_length>(freq);
          v_image_freq[c][s][h][0].real(freq[0]);
          v_image_freq[c][s][h][0].imag(0.0);
          v_image_freq[c][s][h][fourier_domain_length/2].real(freq[1]);
          v_image_freq[c][s][h][fourier_domain_length/2].imag(0.0);
          for (unsigned f = 2; f < fourier_domain_length; f++) {
            if (f % 2 == 0) {
              v_image_freq[c][s][h][f/2].real(freq[f]);
            } else {
              v_image_freq[c][s][h][f/2].imag(freq[f]);
            }
          }
        }
      }
    }
    delete [] freq;
  #endif
  delete [] image_stride;
}

template <typename T, unsigned k, unsigned kernels, unsigned channels, unsigned fourier_domain_length, unsigned stride_w>
static TRINNITY_INLINE void kernel_oihw_to_oihf_complex(const T * __restrict__ kernel_space, std::complex<T> * __restrict__ kernel_freq) {

  T *kernel_stride  = new T[kernels*channels*stride_w*k*k]();
  kernel_polyphase_transform<T, k, kernels, channels, stride_w>(kernel_space, kernel_stride);

  auto v_kernel_space  = triNNity::internal::memory::View5D<             T , kernels, channels, stride_w, k, k>(kernel_stride);
  auto v_kernel_freq   = triNNity::internal::memory::View5D<std::complex<T>, kernels, channels, stride_w, k, fourier_domain_length>(kernel_freq);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      fftwf_complex *freq, *space;
      fftwf_plan p;
      space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);

      p = fftwf_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned c = 0; c < channels; c++) {
          for (unsigned s = 0; s < stride_w; s++) {
            for (unsigned y = 0; y < k; y++) {
              for (unsigned f = 0; f < k; f++) {
                // space[f][0] = (float) v_kernel_space[m][c][s][y][k-f-1];
                space[f][0] = (float) v_kernel_space[m][c][s][y][f];
                space[f][1] = 0.0;
              }
              for (unsigned f = k; f < fourier_domain_length; f++) {
                space[f][0] = 0.0;
                space[f][1] = 0.0;
              }
              fftwf_execute(p);
              for (unsigned f = 0; f < fourier_domain_length; f++) {
                v_kernel_freq[m][c][s][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
              }
            }
          }
        }
      }

      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      fftw_complex *freq, *space;
      fftw_plan p;
      space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);

      p = fftw_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned c = 0; c < channels; c++) {
          for (unsigned s = 0; s < stride_w; s++) {
            for (unsigned y = 0; y < k; y++) {
              for (unsigned f = 0; f < k; f++) {
                // space[f][0] = (double) v_kernel_space[m][c][s][y][k-f-1];
                space[f][0] = (double) v_kernel_space[m][c][s][y][f];
                space[f][1] = 0.0;
              }
              for (unsigned f = k; f < fourier_domain_length; f++) {
                space[f][0] = 0.0;
                space[f][1] = 0.0;
              }
              fftw_execute(p);
              for (unsigned f = 0; f < fourier_domain_length; f++) {
                v_kernel_freq[m][c][s][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
              }
            }
          }
        }
      }

      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    }
    else {
      TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_real_fftw - std::is_same<T, ?>::value");
    }
  #else
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned y = 0; y < k; y++) {
            for (unsigned f = 0; f < k; f++) {
              // v_kernel_freq[m][c][y][f].real(v_kernel_space[m][c][s][y][k-f-1]);
              v_kernel_freq[m][c][s][y][f].real(v_kernel_space[m][c][s][y][f]);
              v_kernel_freq[m][c][s][y][f].imag(0.0);
            }
            for (unsigned f = k; f < fourier_domain_length; f++) {
              v_kernel_freq[m][c][s][y][f].real(0.0);
              v_kernel_freq[m][c][s][y][f].imag(0.0);
            }
              triNNity::spectral::cpu::primitive::forwardComplexFFT<T, fourier_domain_length>((T *) v_kernel_freq[m][c][s][y]);
          }
        }
      }
    }
  #endif
  delete [] kernel_stride;
}

template <typename T, unsigned k, unsigned kernels, unsigned channels, unsigned fourier_domain_length, unsigned stride_w>
static TRINNITY_INLINE void kernel_oihw_to_oihf_real(const T * __restrict__ kernel_space, std::complex<T> * __restrict__ kernel_freq) {

  T *kernel_stride  = new T[kernels*channels*stride_w*k*k]();
  kernel_polyphase_transform<T, k, kernels, channels, stride_w>(kernel_space, kernel_stride);

  auto v_kernel_space  = triNNity::internal::memory::View5D<const T,         kernels, channels, stride_w, k, k>(kernel_stride);
  auto v_kernel_freq   = triNNity::internal::memory::View5D<std::complex<T>, kernels, channels, stride_w, k, (fourier_domain_length/2 + 1)>(kernel_freq);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      float *space;
      fftwf_complex *freq;
      fftwf_plan p;
      space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * ((fourier_domain_length/2) + 1));

      p = fftwf_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned c = 0; c < channels; c++) {
          for (unsigned s = 0; s < stride_w; s++) {
            for (unsigned y = 0; y < k; y++) {
              for (unsigned f = 0; f < k; f++) {
                // space[f] = (float) v_kernel_space[m][c][s][y][k-f-1];
                space[f] = (float) v_kernel_space[m][c][s][y][f];
              }
              for (unsigned f = k; f < fourier_domain_length; f++) {
                space[f] = 0.0;
              }
              fftwf_execute(p);
              for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
                v_kernel_freq[m][c][s][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
              }
            }
          }
        }
      }

      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      double *space;
      fftw_complex *freq;
      fftw_plan p;
      space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * ((fourier_domain_length/2) + 1));

      p = fftw_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned c = 0; c < channels; c++) {
          for (unsigned s = 0; s < stride_w; s++) {
            for (unsigned y = 0; y < k; y++) {
              for (unsigned f = 0; f < k; f++) {
                // space[f] = v_kernel_space[m][c][s][y][k-f-1];
                space[f] = v_kernel_space[m][c][s][y][f];
              }
              for (unsigned f = k; f < fourier_domain_length; f++) {
                space[f] = 0.0;
              }
              fftw_execute(p);
              for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
                v_kernel_freq[m][c][s][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
              }
            }
          }
        }
      }

      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    } else {
      TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_real_fftw - std::is_same<T, ?>::value");
    }
  #else
    T *freq = new T[fourier_domain_length];
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned y = 0; y < k; y++) {
            for (unsigned f = 0; f < k; f++) {
              // freq[f] = v_kernel_space[m][c][s][y][k-f-1];
              freq[f] = v_kernel_space[m][c][s][y][f];
            }
            for (unsigned f = k; f < fourier_domain_length; f++) {
              freq[f] = 0.0;
            }
            triNNity::spectral::cpu::primitive::forwardRealFFT<T, fourier_domain_length>(freq);
            v_kernel_freq[m][c][s][y][0].real(freq[0]);
            v_kernel_freq[m][c][s][y][0].imag(0.0);
            v_kernel_freq[m][c][s][y][fourier_domain_length/2].real(freq[1]);
            v_kernel_freq[m][c][s][y][fourier_domain_length/2].imag(0.0);
            for (unsigned f = 2; f < fourier_domain_length; f++) {
              if (f % 2 == 0) {
                v_kernel_freq[m][c][s][y][f/2].real(freq[f]);
              } else {
                v_kernel_freq[m][c][s][y][f/2].imag(freq[f]);
              }
            }
          }
        }
      }
    }
    delete [] freq;
  #endif
  delete [] kernel_stride;
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned k, unsigned fourier_domain_length, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void out_mhf_to_mhw_complex(const std::complex<T> * __restrict__ out_freq, T * __restrict__ out_space) {

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_freq   = triNNity::internal::memory::View3D<const std::complex<T>, kernels, out_h, fourier_domain_length>(out_freq);
  auto v_out_space  = triNNity::internal::memory::View3D<                   T , kernels, out_h, out_w>(out_space);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      fftwf_complex *freq, *space;
      fftwf_plan p;
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
      space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);

      p = fftwf_plan_dft_1d(fourier_domain_length, freq, space, FFTW_BACKWARD, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < out_h; h++) {
          for (unsigned f = 0; f < fourier_domain_length; f++) {
            freq[f][0] = (float) v_out_freq[m][h][f].real();
            freq[f][1] = (float) v_out_freq[m][h][f].imag();
          }
          fftwf_execute(p);
          for (unsigned w = 0; w < out_w; w++) {
            v_out_space[m][h][w] = (T) space[w + k/2][0] / (T)fourier_domain_length;
          }
        }
      }
      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      fftw_complex *freq, *space;
      fftw_plan p;
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
      space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);

      p = fftw_plan_dft_1d(fourier_domain_length, freq, space, FFTW_BACKWARD, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < out_h; h++) {
          for (unsigned f = 0; f < fourier_domain_length; f++) {
            freq[f][0] = (double) v_out_freq[m][h][f].real();
            freq[f][1] = (double) v_out_freq[m][h][f].imag();
          }
          fftw_execute(p);
          for (unsigned w = 0; w < out_w; w++) {
            v_out_space[m][h][w] = (T) space[w + k/2][0] / (T)fourier_domain_length;
          }
        }
      }
      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    }
    else {
    }
  #else
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        triNNity::spectral::cpu::primitive::inverseComplexFFT<T, fourier_domain_length>((T *) v_out_freq[m][h]);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = v_out_freq[m][h][w + k/2].real();
          // v_out_space[m][h][w] = v_out_freq[m][h][w + k/2].real() / (T)fourier_domain_length;
        }
      }
    }
  #endif
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned k, unsigned fourier_domain_length, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void out_mhf_to_mhw_real(const std::complex<T>  * __restrict__ out_freq, T * __restrict__ out_space) {

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_freq   = triNNity::internal::memory::View3D<const std::complex<T>, kernels, out_h, (fourier_domain_length/2 + 1)>(out_freq);
  auto v_out_space  = triNNity::internal::memory::View3D<                   T , kernels, out_h, out_w>(out_space);

  #if defined(TRINNITY_USE_FFTW)
    if constexpr (std::is_same<T, float>::value) {
      fftwf_complex *freq;
      float *space;
      fftwf_plan p;
      freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * ((fourier_domain_length/2) + 1));
      space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);

      p = fftwf_plan_dft_c2r_1d(fourier_domain_length, freq, space, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < out_h; h++) {
          for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
            freq[f][0] = (float) v_out_freq[m][h][f].real();
            freq[f][1] = (float) v_out_freq[m][h][f].imag();
          }
          fftwf_execute(p);
          for (unsigned w = 0; w < out_w; w++) {
            v_out_space[m][h][w] = (T) space[w + k/2] / (T)fourier_domain_length;
          }
        }
      }
      fftwf_destroy_plan(p);
      fftwf_free(space);
      fftwf_free(freq);
    }
    else if constexpr (std::is_same<T, double>::value) {
      fftw_complex *freq;
      double *space;
      fftw_plan p;
      freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * ((fourier_domain_length/2) + 1));
      space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);

      p = fftw_plan_dft_c2r_1d(fourier_domain_length, freq, space, TRINNITY_FFTW_MODE);

      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < out_h; h++) {
          for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
            freq[f][0] = (double) v_out_freq[m][h][f].real();
            freq[f][1] = (double) v_out_freq[m][h][f].imag();
          }
          fftw_execute(p);
          for (unsigned w = 0; w < out_w; w++) {
            v_out_space[m][h][w] = (T) space[w + k/2] / (T)fourier_domain_length;
          }
        }
      }
      fftw_destroy_plan(p);
      fftw_free(space);
      fftw_free(freq);
    } else {
      TRINNITY_ERROR("not implemented - out_mhfp_to_mhw_real_fftw - std::is_same<T, ?>::value");
    }
  #else
    T *freq = new T[fourier_domain_length];
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        freq[0] = v_out_freq[m][h][0].real();
        freq[1] = v_out_freq[m][h][fourier_domain_length/2].real();
        for (unsigned f = 2; f < fourier_domain_length; f++) {
          if (f % 2 == 0) {
            freq[f] = v_out_freq[m][h][f/2].real();
          } else {
            freq[f] = v_out_freq[m][h][f/2].imag();
          }
        }
        triNNity::spectral::cpu::primitive::inverseRealFFT<T, fourier_domain_length>(freq);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = freq[w + k/2];
        }
      }
    }
    delete [] freq;
  #endif
}


//////////////////////////////////////////////////////////////////////////////////////////
//  ______ ______ _______         _______                   __                          //
// |  ____|  ____|__   __|       |__   __|                 / _|                         //
// | |__  | |__     | |_      __    | |_ __ __ _ _ __  ___| |_ ___  _ __ _ __ ___  ___  //
// |  __| |  __|    | \ \ /\ / /    | | '__/ _` | '_ \/ __|  _/ _ \| '__| '_ ` _ \/ __| //
// | |    | |       | |\ V  V /     | | | | (_| | | | \__ \ || (_) | |  | | | | | \__ \ //
// |_|    |_|       |_| \_/\_/      |_|_|  \__,_|_| |_|___/_| \___/|_|  |_| |_| |_|___/ //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void image_chw_to_chfp_complex_fftw(const T * __restrict__ image_space, std::complex<T> * __restrict__ image_freq) {
  #if defined(TRINNITY_USE_FFTW)
  static constexpr unsigned image_freq_h = triNNity::uceil<img_h, stride_h>();

  auto v_image_space  = triNNity::internal::memory::View3D<const T,         channels, img_h,        img_w>(image_space);
  auto v_image_freq   = triNNity::internal::memory::View3D<std::complex<T>, channels, image_freq_h, fourier_domain_length>(image_freq);

  if constexpr (std::is_same<T, float>::value) {
    fftwf_complex *freq, *space;
    fftwf_plan p;
    space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
    p = fftwf_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

    for (unsigned c = 0; c < channels; c++) {
      for (unsigned h = 0; h < img_h; h+=stride_h) {
        for (unsigned f = 0; f < img_w; f++) {
          space[f][0] = (float) v_image_space[c][h][f];
          space[f][1] = 0.0;
        }
        for (unsigned f = img_w; f < fourier_domain_length; f++) {
          space[f][0] = 0.0;
          space[f][1] = 0.0;
        }
        fftwf_execute(p);
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          v_image_freq[c][h/stride_h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
        }
      }
    }

    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    fftw_complex *freq, *space;
    fftw_plan p;
    space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
    p = fftw_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

    for (unsigned c = 0; c < channels; c++) {
      for (unsigned h = 0; h < img_h; h+=stride_h) {
        for (unsigned f = 0; f < img_w; f++) {
          space[f][0] = (double) v_image_space[c][h][f];
          space[f][1] = 0.0;
        }
        for (unsigned f = img_w; f < fourier_domain_length; f++) {
          space[f][0] = 0.0;
          space[f][1] = 0.0;
        }
        fftw_execute(p);
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          v_image_freq[c][h/stride_h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
        }
      }
    }

    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - image_chw_to_chfp_complex_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - image_chw_to_chfp_complex_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void image_chw_to_chfp_real_fftw(const T * __restrict__ image_space, std::complex<T> * __restrict__ image_freq) {
  #if defined(TRINNITY_USE_FFTW)
  static constexpr unsigned image_freq_h = triNNity::uceil<img_h, stride_h>();

  auto v_image_space  = triNNity::internal::memory::View3D<const T,         channels, img_h,        img_w>(image_space);
  auto v_image_freq   = triNNity::internal::memory::View3D<std::complex<T>, channels, image_freq_h, (fourier_domain_length/2 + 1)>(image_freq);

  if constexpr (std::is_same<T, float>::value) {
    float *space;
    fftwf_complex *freq;
    fftwf_plan p;
    space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * (fourier_domain_length/2 + 1));

    p = fftwf_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

    for (unsigned c = 0; c < channels; c++) {
      for (unsigned h = 0; h < img_h; h+=stride_h) {
        for (unsigned f = 0; f < img_w; f++) {
          space[f] = (float) v_image_space[c][h][f];
        }
        for (unsigned f = img_w; f < fourier_domain_length; f++) {
          space[f] = 0.0;
        }
        fftwf_execute(p);
        for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
          v_image_freq[c][h/stride_h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
        }
      }
    }
    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    double *space;
    fftw_complex *freq;
    fftw_plan p;
    space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (fourier_domain_length/2 + 1));

    p = fftw_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

    for (unsigned c = 0; c < channels; c++) {
      for (unsigned h = 0; h < img_h; h+=stride_h) {
        for (unsigned f = 0; f < img_w; f++) {
          space[f] = (double) v_image_space[c][h][f];
        }
        for (unsigned f = img_w; f < fourier_domain_length; f++) {
          space[f] = 0.0;
        }
        fftw_execute(p);
        for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
          v_image_freq[c][h/stride_h][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
        }
      }
    }
    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - image_chw_to_chfp_real_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - image_chw_to_chfp_real_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}

template <typename T, unsigned k, unsigned kernels, unsigned channels, unsigned fourier_domain_length>
static TRINNITY_INLINE void kernel_oihw_to_oihfp_complex_fftw(const T * __restrict__ kernel_space, std::complex<T> * __restrict__ kernel_freq) {

  #if defined(TRINNITY_USE_FFTW)
  auto v_kernel_space  = triNNity::internal::memory::View4D<       const T , kernels, channels, k, k>(kernel_space);
  auto v_kernel_freq   = triNNity::internal::memory::View4D<std::complex<T>, kernels, channels, k, fourier_domain_length>(kernel_freq);

  if constexpr (std::is_same<T, float>::value) {
    fftwf_complex *freq, *space;
    fftwf_plan p;
    space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);

    p = fftwf_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned y = 0; y < k; y++) {
          for (unsigned f = 0; f < k; f++) {
            space[f][0] = (float) v_kernel_space[m][c][y][k-f-1];
            space[f][1] = 0.0;
          }
          for (unsigned f = k; f < fourier_domain_length; f++) {
            space[f][0] = 0.0;
            space[f][1] = 0.0;
          }
          fftwf_execute(p);
          for (unsigned f = 0; f < fourier_domain_length; f++) {
            // v_kernel_freq[m][c][y][2*f]     = (T) freq[f][0];
            // v_kernel_freq[m][c][y][2*f + 1] = (T) freq[f][1];
            v_kernel_freq[m][c][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
          }
        }
      }
    }

    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    fftw_complex *freq, *space;
    fftw_plan p;
    space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);

    p = fftw_plan_dft_1d(fourier_domain_length, space, freq, FFTW_FORWARD, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned y = 0; y < k; y++) {
          for (unsigned f = 0; f < k; f++) {
            space[f][0] = (double) v_kernel_space[m][c][y][k-f-1];
            space[f][1] = 0.0;
          }
          for (unsigned f = k; f < fourier_domain_length; f++) {
            space[f][0] = 0.0;
            space[f][1] = 0.0;
          }
          fftw_execute(p);
          for (unsigned f = 0; f < fourier_domain_length; f++) {
            // v_kernel_freq[m][c][y][2*f]     = (T) freq[f][0];
            // v_kernel_freq[m][c][y][2*f + 1] = (T) freq[f][1];
            v_kernel_freq[m][c][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
          }
        }
      }
    }

    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_complex_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_complex_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}

template <typename T, unsigned k, unsigned kernels, unsigned channels, unsigned fourier_domain_length>
static TRINNITY_INLINE void kernel_oihw_to_oihfp_real_fftw(const T * __restrict__ kernel_space, std::complex<T> * __restrict__ kernel_freq) {
  #if defined(TRINNITY_USE_FFTW)
  auto v_kernel_space  = triNNity::internal::memory::View4D<const T,         kernels, channels, k, k>(kernel_space);
  auto v_kernel_freq   = triNNity::internal::memory::View4D<std::complex<T>, kernels, channels, k, (fourier_domain_length/2 + 1)>(kernel_freq);

  if constexpr (std::is_same<T, float>::value) {
    float *space;
    fftwf_complex *freq;
    fftwf_plan p;
    space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * ((fourier_domain_length/2) + 1));

    p = fftwf_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned y = 0; y < k; y++) {
          for (unsigned f = 0; f < k; f++) {
            space[f] = (float) v_kernel_space[m][c][y][k-f-1];
          }
          for (unsigned f = k; f < fourier_domain_length; f++) {
            space[f] = 0.0;
          }
          fftwf_execute(p);
          // v_kernel_freq[m][c][y][0] = (T) freq[0][0];
          // v_kernel_freq[m][c][y][1] = (T) freq[fourier_domain_length/2][0];
          // for (unsigned f = 1; f < fourier_domain_length/2; f++) {
          for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
            // v_kernel_freq[m][c][y][2*f]     = (T) freq[f][0];
            // v_kernel_freq[m][c][y][2*f + 1] = (T) freq[f][1];
            v_kernel_freq[m][c][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
          }
        }
      }
    }

    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    double *space;
    fftw_complex *freq;
    fftw_plan p;
    space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * ((fourier_domain_length/2) + 1));

    p = fftw_plan_dft_r2c_1d(fourier_domain_length, space, freq, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned y = 0; y < k; y++) {
          for (unsigned f = 0; f < k; f++) {
            space[f] = v_kernel_space[m][c][y][k-f-1];
          }
          for (unsigned f = k; f < fourier_domain_length; f++) {
            space[f] = 0.0;
          }
          fftw_execute(p);
          // v_kernel_freq[m][c][y][0] = (T) freq[0][0];
          // v_kernel_freq[m][c][y][1] = (T) freq[fourier_domain_length/2][0];
          // for (unsigned f = 1; f < fourier_domain_length/2; f++) {
          for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
            // v_kernel_freq[m][c][y][2*f]     = (T) freq[f][0];
            // v_kernel_freq[m][c][y][2*f + 1] = (T) freq[f][1];
            v_kernel_freq[m][c][y][f] = std::complex<T>( (T)freq[f][0], (T)freq[f][1] );
          }
        }
      }
    }

    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_real_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - kernel_oihw_to_oihfp_real_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned k, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void out_mhfp_to_mhw_complex_fftw(const std::complex<T> * __restrict__ out_freq, T * __restrict__ out_space) {
  #if defined(TRINNITY_USE_FFTW)
  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_freq   = triNNity::internal::memory::View3D<const std::complex<T>, kernels, out_h, fourier_domain_length>(out_freq);
  auto v_out_space  = triNNity::internal::memory::View3D<T,                     kernels, out_h, out_w>(out_space);

  if constexpr (std::is_same<T, float>::value) {
    fftwf_complex *freq, *space;
    fftwf_plan p;
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);
    space = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * fourier_domain_length);

    p = fftwf_plan_dft_1d(fourier_domain_length, freq, space, FFTW_BACKWARD, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          freq[f][0] = (float) v_out_freq[m][h][f].real();
          freq[f][1] = (float) v_out_freq[m][h][f].imag();
        }
        fftwf_execute(p);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = (T) space[w + k/2][0] / (T)fourier_domain_length;
        }
      }
    }
    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    fftw_complex *freq, *space;
    fftw_plan p;
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);
    space = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fourier_domain_length);

    p = fftw_plan_dft_1d(fourier_domain_length, freq, space, FFTW_BACKWARD, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          freq[f][0] = (double) v_out_freq[m][h][f].real();
          freq[f][1] = (double) v_out_freq[m][h][f].imag();
        }
        fftw_execute(p);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = (T) space[w + k/2][0] / (T)fourier_domain_length;
        }
      }
    }
    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - out_mhfp_to_mhw_complex_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - out_mhfp_to_mhw_complex_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned k, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void out_mhfp_to_mhw_real_fftw(const std::complex<T> * __restrict__ out_freq, T * __restrict__ out_space) {
  #if defined(TRINNITY_USE_FFTW)
  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_freq   = triNNity::internal::memory::View3D<const std::complex<T>, kernels, out_h, (fourier_domain_length/2 + 1)>(out_freq);
  auto v_out_space  = triNNity::internal::memory::View3D<T,                     kernels, out_h, out_w>(out_space);

  if constexpr (std::is_same<T, float>::value) {
    fftwf_complex *freq;
    float *space;
    fftwf_plan p;
    freq  = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex) * ((fourier_domain_length/2) + 1));
    space = (float *)         fftwf_malloc(sizeof(float) * fourier_domain_length);

    p = fftwf_plan_dft_c2r_1d(fourier_domain_length, freq, space, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
          freq[f][0] = (float) v_out_freq[m][h][f].real();
          freq[f][1] = (float) v_out_freq[m][h][f].imag();
        }
        fftwf_execute(p);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = (T) space[w + k/2] / (T)fourier_domain_length;
        }
      }
    }
    fftwf_destroy_plan(p);
    fftwf_free(space);
    fftwf_free(freq);
  }
  else if constexpr (std::is_same<T, double>::value) {
    fftw_complex *freq;
    double *space;
    fftw_plan p;
    freq  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * ((fourier_domain_length/2) + 1));
    space = (double *)       fftw_malloc(sizeof(double) * fourier_domain_length);

    p = fftw_plan_dft_c2r_1d(fourier_domain_length, freq, space, TRINNITY_FFTW_MODE);

    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned h = 0; h < out_h; h++) {
        for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
          freq[f][0] = (float) v_out_freq[m][h][f].real();
          freq[f][1] = (float) v_out_freq[m][h][f].imag();
        }
        fftw_execute(p);
        for (unsigned w = 0; w < out_w; w++) {
          v_out_space[m][h][w] = (T) space[w + k/2] / (T)fourier_domain_length;
        }
      }
    }
    fftw_destroy_plan(p);
    fftw_free(space);
    fftw_free(freq);
  } else {
    TRINNITY_ERROR("not implemented - out_mhfp_to_mhw_real_fftw - std::is_same<T, ?>::value");
  }
  #else
    TRINNITY_ERROR("not implemented - out_mhfp_to_mhw_real_fftw - must #define TRINNITY_USE_FFTW to use");
  #endif
}


//////////////////////////////////////////////////////////////////////////////////////////
//   _____ ______ __  __ __  __   _______                   __                          //
//  / ____|  ____|  \/  |  \/  | |__   __|                 / _|                         //
// | |  __| |__  | \  / | \  / |    | |_ __ __ _ _ __  ___| |_ ___  _ __ _ __ ___  ___  //
// | | |_ |  __| | |\/| | |\/| |    | | '__/ _` | '_ \/ __|  _/ _ \| '__| '_ ` _ \/ __| //
// | |__| | |____| |  | | |  | |    | | | | (_| | | | \__ \ || (_) | |  | | | | | \__ \ //
//  \_____|______|_|  |_|_|  |_|    |_|_|  \__,_|_| |_|___/_| \___/|_|  |_| |_| |_|___/ //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned channels, unsigned img_h, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void image_chfp_to_pfhc_complex(const std::complex<T> * __restrict__ img_chfp, T * __restrict__ img_pfhc) {

  auto v_img_chfp  = triNNity::internal::memory::View3D<const std::complex<T>, channels, img_h, fourier_domain_length>(img_chfp);
  auto v_img_pfhc  = triNNity::internal::memory::View4D<T,                  2, fourier_domain_length, img_h, channels>(img_pfhc);

  for (signed c = 0; c < channels; c++) {
    for (signed h = 0; h < img_h; h++) {
      for (signed f = 0; f < fourier_domain_length; f++) {
        v_img_pfhc[0][f][h][c] = v_img_chfp[c][h][f].real();
        v_img_pfhc[1][f][h][c] = v_img_chfp[c][h][f].imag();
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned fourier_domain_length, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void image_chfp_to_pfhc_real(const std::complex<T> * __restrict__ img_chfp, T * __restrict__ img_pfhc) {

  auto v_img_chfp  = triNNity::internal::memory::View3D<const std::complex<T>, channels, img_h, (fourier_domain_length/2 + 1)>(img_chfp);
  auto v_img_pfhc  = triNNity::internal::memory::View4D<T,                  2, (fourier_domain_length/2 + 1), img_h, channels>(img_pfhc);

  for (unsigned c = 0; c < channels; c++) {
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned f = 0; f < fourier_domain_length/2 + 1; f++) {
        v_img_pfhc[0][f][h][c] = v_img_chfp[c][h][f].real();
        v_img_pfhc[1][f][h][c] = v_img_chfp[c][h][f].imag();
      }
    }
  }
}

template <typename T, unsigned kernels, unsigned channels, unsigned k, unsigned fourier_domain_length>
static TRINNITY_INLINE void kernel_oihfp_to_pfhoi_complex(const std::complex<T> * __restrict__ kernel_oihfp, T * __restrict__ kernel_pfhoi) {

  auto v_kernel_oihfp  = triNNity::internal::memory::View4D<const std::complex<T>, kernels, channels, k, fourier_domain_length>(kernel_oihfp);
  auto v_kernel_pfhoi  = triNNity::internal::memory::View4D<T,                  2, fourier_domain_length, k*kernels,  channels>(kernel_pfhoi);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned y = 0; y < k; y++) {
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          v_kernel_pfhoi[0][f][m*k + y][c] = v_kernel_oihfp[m][c][y][f].real();
          v_kernel_pfhoi[1][f][m*k + y][c] = v_kernel_oihfp[m][c][y][f].imag();
        }
      }
    }
  }
}

template <typename T, unsigned kernels, unsigned channels, unsigned k, unsigned fourier_domain_length>
static TRINNITY_INLINE void kernel_oihfp_to_pfhoi_real(const std::complex<T> * __restrict__ kernel_oihfp, T * __restrict__ kernel_pfhoi) {

  auto v_kernel_oihfp  = triNNity::internal::memory::View4D<const std::complex<T>, kernels, channels, k, (fourier_domain_length/2 + 1)>(kernel_oihfp);
  auto v_kernel_pfhoi  = triNNity::internal::memory::View4D<T,                  2, (fourier_domain_length/2 + 1), k*kernels,  channels>(kernel_pfhoi);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned y = 0; y < k; y++) {
        for (unsigned f = 0; f < fourier_domain_length/2 + 1; f++) {
          v_kernel_pfhoi[0][f][m*k + y][c] = v_kernel_oihfp[m][c][y][f].real();
          v_kernel_pfhoi[1][f][m*k + y][c] = v_kernel_oihfp[m][c][y][f].imag();
        }
      }
    }
  }
}

template <typename T, unsigned fourier_domain_length, unsigned img_h, unsigned k, unsigned kernels, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void out_pfhym_to_mhfp_complex(const T * __restrict__ out_pfhym, std::complex<T> * __restrict__ out_mhfp) {

  auto v_out_pfhym  = triNNity::internal::memory::View4D<const T,      2, fourier_domain_length, img_h, k*kernels>(out_pfhym);
  auto v_out_mhfp   = triNNity::internal::memory::View3D<std::complex<T>, kernels,  img_h,   fourier_domain_length>(out_mhfp);

  for (unsigned f = 0; f < fourier_domain_length; f++) {
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned y = 0; y < k; y++) {
          v_out_mhfp[m][h][f].real() += v_out_pfhym[0][f][h][m*k + y];
          v_out_mhfp[m][h][f].imag() += v_out_pfhym[1][f][h][m*k + y];
        }
      }
    }
  }
}

template <typename T, unsigned fourier_domain_length, unsigned img_h, unsigned k, unsigned kernels, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void out_pfhym_to_mhfp_real(const T * __restrict__ out_pfhym, std::complex<T> * __restrict__ out_mhfp) {

  auto v_out_pfhym  = triNNity::internal::memory::View4D<const T,      2, (fourier_domain_length/2 + 1), img_h, k*kernels>(out_pfhym);
  auto v_out_mhfp   = triNNity::internal::memory::View3D<std::complex<T>, kernels,  img_h,   (fourier_domain_length/2 + 1)>(out_mhfp);

  for (unsigned f = 0; f < fourier_domain_length/2 + 1; f++) {
    for (unsigned h = 0; h < img_h; h++) {
      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned y = 0; y < k; y++) {
          v_out_mhfp[m][h][f].real() += v_out_pfhym[0][f][h][m*k + y];
          v_out_mhfp[m][h][f].imag() += v_out_pfhym[1][f][h][m*k + y];
        }
      }
    }
  }
}


template <typename T, unsigned channels, unsigned img_h, unsigned fourier, unsigned stride_w, unsigned fft_type>
static TRINNITY_INLINE void image_chfp_to_fhcp(const std::complex<T> * __restrict__ img, std::complex<T> *img_fhcp) {

  constexpr unsigned fourier_domain_length = ((spectral_fft_type_t) fft_type == triNNity::FFT_C2C) ? fourier
                                           : ((spectral_fft_type_t) fft_type == triNNity::FFT_R2C) ? (fourier/2 + 1)
                                           : 0;

  if constexpr (fourier_domain_length == 0) {
    TRINNITY_ERROR("ERROR: fft_type is somehow neither R2C or C2C.");
  }

  auto v_img       = triNNity::internal::memory::View4D<const std::complex<T>, channels, stride_w, img_h, fourier_domain_length>(img);
  auto v_img_fhcp  = triNNity::internal::memory::View4D<      std::complex<T>, fourier_domain_length, img_h, channels, stride_w>(img_fhcp);

  for (unsigned c = 0; c < channels; c++) {
    for (unsigned s = 0; s < stride_w; s++) {
      for (unsigned h = 0; h < img_h; h++) {
        for (unsigned f = 0; f < fourier_domain_length; f++) {
          v_img_fhcp[f][h][c][s] = v_img[c][s][h][f];
        }
      }
    }
  }
}

/*template <typename T, unsigned channels, unsigned img_h, unsigned fourier_domain_length, unsigned stride_w>
static TRINNITY_INLINE void image_chfp_to_fhcp_real(const std::complex<T> * __restrict__ img, std::complex<T> *img_fhcp) {

  auto v_img       = triNNity::internal::memory::View3D<const std::complex<T>, channels, img_h, (fourier_domain_length/2 + 1)>(img);
  auto v_img_fhcp  = triNNity::internal::memory::View3D<std::complex<T>,       (fourier_domain_length/2 + 1), img_h, channels>(img_fhcp);

  for (signed c = 0; c < channels; c++) {
    for (signed h = 0; h < img_h; h++) {
      for (signed f = 0; f < (fourier_domain_length/2 + 1); f++) {
        v_img_fhcp[f][h][c] = v_img[c][h][f];
      }
    }
  }
}*/

template <typename T, unsigned kernels, unsigned channels, unsigned k, unsigned fourier, unsigned stride_w, unsigned fft_type>
static TRINNITY_INLINE void kernel_oihfp_to_fohip(const std::complex<T> * __restrict__ kernel, std::complex<T> *kernel_fhoip) {

  constexpr unsigned fourier_domain_length = ((spectral_fft_type_t) fft_type == triNNity::FFT_C2C) ? fourier
                                           : ((spectral_fft_type_t) fft_type == triNNity::FFT_R2C) ? (fourier/2 + 1)
                                           : 0;

  if constexpr (fourier_domain_length == 0) {
    TRINNITY_ERROR("ERROR: fft_type is somehow neither R2C or C2C.");
  }

  auto v_kernel        = triNNity::internal::memory::View5D<const std::complex<T>, kernels, channels, stride_w, k, fourier_domain_length>(kernel);
  auto v_kernel_fhoip  = triNNity::internal::memory::View4D<      std::complex<T>, fourier_domain_length, k*kernels, channels, stride_w>(kernel_fhoip);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned s = 0; s < stride_w; s++) {
        for (unsigned y = 0; y < k; y++) {
          for (unsigned f = 0; f < fourier_domain_length; f++) {
            v_kernel_fhoip[f][m*k + y][c][s] = v_kernel[m][c][s][y][f];
          }
        }
      }
    }
  }
}

/*template <typename T, unsigned kernels, unsigned channels, unsigned k, unsigned fourier_domain_length>
static TRINNITY_INLINE void kernel_oihfp_to_fohip_real(const std::complex<T> * __restrict__ kernel, std::complex<T> *kernel_fhoip) {

  auto v_kernel        = triNNity::internal::memory::View4D<const std::complex<T>, kernels, channels, k, (fourier_domain_length/2 + 1)>(kernel);
  auto v_kernel_fhoip  = triNNity::internal::memory::View3D<std::complex<T>      , (fourier_domain_length/2 + 1),  k*kernels, channels>(kernel_fhoip);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned y = 0; y < k; y++) {
        for (unsigned f = 0; f < fourier_domain_length/2 + 1; f++) {
          v_kernel_fhoip[f][m*k + y][c] = v_kernel[m][c][y][f];
        }
      }
    }
  }
}*/

template <typename T, unsigned fourier, unsigned img_h, unsigned k, unsigned kernels, unsigned stride_w, unsigned stride_h, unsigned fft_type>
static TRINNITY_INLINE void out_fhoyp_to_ohfp(const std::complex<T> * __restrict__ out_fhmyp, std::complex<T> * __restrict__ out_mhfp) {

  constexpr unsigned fourier_domain_length = ((spectral_fft_type_t) fft_type == triNNity::FFT_C2C) ? fourier
                                           : ((spectral_fft_type_t) fft_type == triNNity::FFT_R2C) ? (fourier/2 + 1)
                                           : 0;

  if constexpr (fourier_domain_length == 0) {
    TRINNITY_ERROR("ERROR: fft_type is somehow neither R2C or C2C.");
  }

  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_fhmyp = triNNity::internal::memory::View4D<const std::complex<T>, fourier_domain_length, img_h, kernels, k>(out_fhmyp);
  auto v_out_mhfp  = triNNity::internal::memory::View3D<      std::complex<T>, kernels,   out_h,   fourier_domain_length>(out_mhfp);

  for (unsigned f = 0; f < fourier_domain_length; f++) {
    for (unsigned h = 0; h < img_h; h+=stride_h) {
      for (unsigned m = 0; m < kernels; m++) {
        v_out_mhfp[m][h/stride_h][f] = std::complex<T>(0.0,0.0);
        for (unsigned y = 0; y < k; y++) {
          signed ypos = h + y - k/2;
          if (ypos >= 0 && ypos < (signed)img_h) {
              v_out_mhfp[m][h/stride_h][f] += v_out_fhmyp[f][ypos][m][y];
          }
        }
      }
    }
  }
}

/*template <typename T, unsigned fourier_domain_length, unsigned img_h, unsigned k, unsigned kernels, unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void out_fhoyp_to_ohfp_real(const std::complex<T> * __restrict__ out_fhmyp, std::complex<T> * __restrict__ out_mhfp) {

  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  auto v_out_fhmyp = triNNity::internal::memory::View4D<const std::complex<T>, (fourier_domain_length/2 + 1), img_h, kernels, k>(out_fhmyp);
  auto v_out_mhfp  = triNNity::internal::memory::View3D<std::complex<T>,       kernels,   out_h,   (fourier_domain_length/2 + 1)>(out_mhfp);

  for (unsigned f = 0; f < (fourier_domain_length/2) + 1; f++) {
    for (unsigned h = 0; h < out_h; h++) {
      for (unsigned m = 0; m < kernels; m++) {
        v_out_mhfp[m][h][f] = std::complex<T>(0.0,0.0);
        for (unsigned y = 0; y < k; y++) {
          signed ypos = h + y - k/2;
          if constexpr (ypos >= 0 && ypos < out_h) {
              v_out_mhfp[m][h][f] += v_out_fhmyp[f][ypos][m][y];
          }
        }
      }
    }
  }
}*/

////////////////////////////////////////////////////
//           _                                 _  //
//          (_)                               | | //
// __      ___ _ __   ___   __ _ _ __ __ _  __| | //
// \ \ /\ / / | '_ \ / _ \ / _` | '__/ _` |/ _` | //
//  \ V  V /| | | | | (_) | (_| | | | (_| | (_| | //
//   \_/\_/ |_|_| |_|\___/ \__, |_|  \__,_|\__,_| //
//                          __/ |                 //
//                         |___/                  //
////////////////////////////////////////////////////


template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2_3_kernel_to_winograd(const KDV * __restrict__ kernel, KDV * __restrict__ out) {
  TRINNITY_ASSERT((channels%kdv_lanes) == 0);
  auto ker = triNNity::internal::memory::View4D<const KDV, 3,3, kernels, channels/kdv_lanes>(kernel);
  auto kerTrans = triNNity::internal::memory::View4D<KDV, 3, 4, kernels, channels/kdv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for collapse(2)
  #endif
  for (unsigned j = 0; j < 3; j++) {
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        kerTrans[j][0][m][c] =  ker[j][0][m][c];
        kerTrans[j][1][m][c] = (ker[j][0][m][c]
                                  + ker[j][2][m][c]
                                  + ker[j][1][m][c]) * 0.5f;
        kerTrans[j][2][m][c] = (ker[j][0][m][c]
                                  + ker[j][2][m][c]
                                  - ker[j][1][m][c]) * 0.5f;
        kerTrans[j][3][m][c] =  ker[j][2][m][c];
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2_3_image_to_winograd(const TV * __restrict__ img, TV * __restrict__ out) {
  TRINNITY_ASSERT((channels%tv_lanes) == 0);
  constexpr signed simg_h = (signed)img_h;
  constexpr unsigned tiledW = img_w/2 + (img_w%2);
  auto image = triNNity::internal::memory::View3D<const TV, img_h, img_w, channels/tv_lanes>(img);
  auto imgTrans = triNNity::internal::memory::View4D<TV, 4, (img_h+2), tiledW, channels/tv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel
  #endif
  {
    TV * barBacking = new TV[4 * channels/tv_lanes];
    auto bar = triNNity::internal::memory::View2D<TV, 4, channels/tv_lanes>(barBacking);
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (signed h = -1; h < ((signed)img_h) + 1; h++) {
      for (unsigned w = 0; w < tiledW; w++) {
        if (h < 0 || h >= simg_h) {
          for (unsigned c = 0; c < channels/tv_lanes; c++) {
            bar[0][c] = 0;
            bar[1][c] = 0;
            bar[2][c] = 0;
            bar[3][c] = 0;
          }
        } else {
          if ((w*2 - 1) < 0 || (w*2-1) >= img_w) {  memset(bar[0], 0, sizeof(TV)*channels/tv_lanes);
          } else {                                  memcpy(bar[0], image[h][w*2-1], sizeof(TV)*channels/tv_lanes); }
          if ((w*2    ) < 0 || (w*2  ) >= img_w) {  memset(bar[1], 0, sizeof(TV)*channels/tv_lanes);
          } else {                                  memcpy(bar[1], image[h][w*2  ], sizeof(TV)*channels/tv_lanes); }
          if ((w*2 + 1) < 0 || (w*2+1) >= img_w) {  memset(bar[2], 0, sizeof(TV)*channels/tv_lanes);
          } else {                                  memcpy(bar[2], image[h][w*2+1], sizeof(TV)*channels/tv_lanes); }
          if ((w*2 + 2) < 0 || (w*2+2) >= img_w) {  memset(bar[3], 0, sizeof(TV)*channels/tv_lanes);
          } else {                                  memcpy(bar[3], image[h][w*2+2], sizeof(TV)*channels/tv_lanes); }
        }
        for (unsigned c = 0; c < channels/tv_lanes; c++) {
          imgTrans[0][h+1][w][c] = bar[0][c] - bar[2][c];
          imgTrans[1][h+1][w][c] = bar[1][c] + bar[2][c];
          imgTrans[2][h+1][w][c] = bar[2][c] - bar[1][c];
          imgTrans[3][h+1][w][c] = bar[1][c] - bar[3][c];
        }
      }
    }

    delete [] barBacking;
  }//end OMP parallel
}



template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2x2_3x3_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 3*3, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 4*4, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2x2_3x3_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_2x2_3x3_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_2x2_3x3_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2x2_3x3_kernel_to_winograd_ptrs(const KDV * kernel, KDV * out[16]) {
  TRINNITY_ASSERT((channels%kdv_lanes) == 0);
  auto ker = triNNity::internal::memory::View3D<const KDV, 3*3, kernels, channels/kdv_lanes>(kernel);
  out[0] = &ker[0][0][0];
  out[3] = &ker[2][0][0];
  out[12] = &ker[6][0][0];
  out[15] = &ker[8][0][0];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel
  #endif
  {
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[1][m*(channels/kdv_lanes) + c]  = (ker[0][m][c]+ker[2][m][c]+ker[1][m][c])*static_cast<KD>(0.5f);
        out[2][m*(channels/kdv_lanes) + c]  = (ker[0][m][c]+ker[2][m][c]-ker[1][m][c])*static_cast<KD>(0.5f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[4][m*(channels/kdv_lanes) + c]  = (ker[0][m][c]+ker[6][m][c]+ker[3][m][c])*static_cast<KD>(0.5f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[5][m*(channels/kdv_lanes) + c]  = ((ker[0][m][c]+ker[6][m][c]+ker[3][m][c])
                                    +(ker[2][m][c]+ker[8][m][c]+ker[5][m][c])
                                    +(ker[1][m][c]+ker[7][m][c]+ker[4][m][c]))*static_cast<KD>(0.25f);
        out[6][m*(channels/kdv_lanes) + c]  = ((ker[0][m][c]+ker[6][m][c]+ker[3][m][c])
                                    + (ker[2][m][c]+ker[8][m][c]+ker[5][m][c])
                                    - (ker[1][m][c]+ker[7][m][c]+ker[4][m][c]))*static_cast<KD>(0.25f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[7][m*(channels/kdv_lanes) + c]  = (ker[2][m][c]+ker[8][m][c]+ker[5][m][c])*static_cast<KD>(0.5f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[8][m*(channels/kdv_lanes) + c]  = (ker[0][m][c]+ker[6][m][c]-ker[3][m][c])*static_cast<KD>(0.5f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[9][m*(channels/kdv_lanes) + c]  = ((ker[0][m][c]+ker[6][m][c]-ker[3][m][c])
                                    + (ker[2][m][c]+ker[8][m][c]-ker[5][m][c])
                                    + (ker[1][m][c]+ker[7][m][c]-ker[4][m][c]))*static_cast<KD>(0.25f);
        out[10][m*(channels/kdv_lanes) + c] = ((ker[0][m][c]+ker[6][m][c]-ker[3][m][c])
                                    + (ker[2][m][c]+ker[8][m][c]-ker[5][m][c])
                                    - (ker[1][m][c]+ker[7][m][c]-ker[4][m][c]))*static_cast<KD>(0.25f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[11][m*(channels/kdv_lanes) + c] = (ker[2][m][c]+ker[8][m][c]-ker[5][m][c])*static_cast<KD>(0.5f);
      }
    }
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        out[13][m*(channels/kdv_lanes) + c] = (ker[6][m][c]+ker[8][m][c]+ker[7][m][c])*static_cast<KD>(0.5f);
        out[14][m*(channels/kdv_lanes) + c] = (ker[6][m][c]+ker[8][m][c]-ker[7][m][c])*static_cast<KD>(0.5f);
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_3x3_image_to_winograd_transformation(const T * __restrict__ tile[4*4], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 4*4, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[0][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_3x3_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 3/2;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[4*4];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=2){
    for (signed outw=0; outw < simg_w; outw+=2){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+4; th+=1){
        for (signed tw=w; tw < w+4; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/2;
      signed wc = (outw)/2;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_2x2_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_2x2_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_3x3_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto in = triNNity::internal::memory::View4D<T, 4*4, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(-((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[5][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_3x3_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[2*2];
        winograd_2x2_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*2 < 2 ? img_h-h*2 : 2); th++) {
          for (size_t tw = 0; tw < (img_w-w*2 < 2 ? img_w-w*2 : 2); tw++) {
            std::memcpy(&(output[h*2+th][w*2+tw][m]), &(outDetrans[th*2+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[2*2];
        winograd_2x2_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*2 < 2 ? img_h-h*2 : 2); th++) {
          for (size_t tw = 0; tw < (img_w-w*2 < 2 ? img_w-w*2 : 2); tw++) {
            std::memcpy(&(output[h*2+th][w*2+tw][m]), &(outDetrans[th*2+tw]), sizeof(T));
          }
        }
      }
    }
  }
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV>
static TRINNITY_INLINE void winograd_3_3_kernel_to_winograd(const KDV * __restrict__ kernel, KDV * __restrict__ out) {
  TRINNITY_ASSERT((channels%kdv_lanes) == 0);
  constexpr KD oneSixth = 0.1666666;
  constexpr KD oneThird = 0.3333333;
  constexpr KD twoThirds = 0.6666666;
  auto ker = triNNity::internal::memory::View4D<const KDV, 3,3, kernels, channels/kdv_lanes>(kernel);
  auto kerTrans = triNNity::internal::memory::View4D<KDV, 3, 5, kernels, channels/kdv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for collapse(2)
  #endif
  for (unsigned j = 0; j < 3; j++) {
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        kerTrans[j][0][m][c] = ker[j][0][m][c]*static_cast<KD>(0.5);
        kerTrans[j][1][m][c] = (ker[j][1][m][c]
                                    - (ker[j][2][m][c]
                                        + ker[j][0][m][c]))*oneSixth;
        kerTrans[j][2][m][c] = (ker[j][0][m][c]
                                    + ker[j][1][m][c]
                                    + ker[j][2][m][c])*static_cast<KD>(-0.5f);
        kerTrans[j][3][m][c] = oneSixth*ker[j][0][m][c]
                                    + oneThird*ker[j][1][m][c]
                                    + twoThirds*ker[j][2][m][c];
        kerTrans[j][4][m][c] = ker[j][2][m][c];
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3_3_image_to_winograd(const TV * __restrict__ img, TV * __restrict__ out) {
  TRINNITY_ASSERT(channels%tv_lanes==0);
  constexpr signed simg_h = (signed)img_h;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  auto image = triNNity::internal::memory::View3D<const TV, img_h, img_w, channels/tv_lanes>(img);
  auto imgTrans = triNNity::internal::memory::View4D<TV, 5, (img_h+2), tiledW, channels/tv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel
  #endif
  {
    TV * barBacking = new TV[5 * channels/tv_lanes];
    auto bar = triNNity::internal::memory::View2D<TV, 5, channels/tv_lanes>(barBacking);
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (signed h = -1; h < ((signed)img_h) + 1; h++) {
      for (unsigned w = 0; w < tiledW; w++) {
        if (h < 0 || h >= simg_h) {
          for (unsigned c = 0; c < channels/tv_lanes; c++) {
            bar[0][c] = 0;
            bar[1][c] = 0;
            bar[2][c] = 0;
            bar[3][c] = 0;
            bar[4][c] = 0;
          }
        } else {
          for (unsigned c = 0; c < channels/tv_lanes; c++) {
            bar[0][c] = ((w*3 - 1) < 0 || (w*3-1) >= img_w) ? 0 : image[h][w*3-1][c];
            bar[1][c] = ((w*3    ) < 0 || (w*3  ) >= img_w) ? 0 : image[h][w*3  ][c];
            bar[2][c] = ((w*3 + 1) < 0 || (w*3+1) >= img_w) ? 0 : image[h][w*3+1][c];
            bar[3][c] = ((w*3 + 2) < 0 || (w*3+2) >= img_w) ? 0 : image[h][w*3+2][c];
            bar[4][c] = ((w*3 + 3) < 0 || (w*3+3) >= img_w) ? 0 : image[h][w*3+3][c];
          }
        }
        for (unsigned c = 0; c < channels/tv_lanes; c++) {
          imgTrans[0][h+1][w][c] = 2*bar[0][c] + bar[3][c] - (bar[1][c] + 2*bar[2][c]);
          imgTrans[1][h+1][w][c] = 2*bar[1][c] + bar[3][c] - 3*bar[2][c];
          imgTrans[2][h+1][w][c] = bar[3][c] - (2*bar[1][c] + bar[2][c]);
          imgTrans[3][h+1][w][c] = bar[3][c] - bar[1][c];
          imgTrans[4][h+1][w][c] = 2*bar[1][c] + bar[4][c] - (bar[2][c] + 2*bar[3][c]);
        }
      }
    }

    delete [] barBacking;
  }

}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_3x3_3x3_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 3*3, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 5*5, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[16][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[17][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[18][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[19][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[20][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[21][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[22][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[23][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[24][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_3x3_3x3_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_3x3_3x3_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_3x3_3x3_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_3x3_image_to_winograd_transformation(const T * __restrict__ tile[5*5], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 5*5, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[0][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[7][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))));
  std::memcpy(&(imgTrans[16][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[17][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[18][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[19][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))));
  std::memcpy(&(imgTrans[20][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))));
  std::memcpy(&(imgTrans[21][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[22][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[23][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[24][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_3x3_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 3/2;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[5*5];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=3){
    for (signed outw=0; outw < simg_w; outw+=3){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+5; th+=1){
        for (signed tw=w; tw < w+5; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/3;
      signed wc = (outw)/3;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_3x3_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_3x3_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_3x3_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto in = triNNity::internal::memory::View4D<T, 5*5, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(in[5][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[4]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[5]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[6]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(in[6][h][w][m]))))))+(-((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[7]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))));
  std::memcpy(&(out[8]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_3x3_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[3*3];
        winograd_3x3_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*3 < 3 ? img_h-h*3 : 3); th++) {
          for (size_t tw = 0; tw < (img_w-w*3 < 3 ? img_w-w*3 : 3); tw++) {
            std::memcpy(&(output[h*3+th][w*3+tw][m]), &(outDetrans[th*3+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[3*3];
        winograd_3x3_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*3 < 3 ? img_h-h*3 : 3); th++) {
          for (size_t tw = 0; tw < (img_w-w*3 < 3 ? img_w-w*3 : 3); tw++) {
            std::memcpy(&(output[h*3+th][w*3+tw][m]), &(outDetrans[th*3+tw]), sizeof(T));
          }
        }
      }
    }
  }
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_4x4_3x3_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 3*3, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 6*6, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (static_cast<KD>((1.0f)/(16.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[16][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[17][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[18][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[19][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[20][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[21][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[22][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[23][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[24][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[25][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[26][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[27][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[28][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))));
  std::memcpy(&(kerTrans[29][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[30][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[31][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[32][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[33][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))));
  std::memcpy(&(kerTrans[34][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[8][m][c]))))))));
  std::memcpy(&(kerTrans[35][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_4x4_3x3_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_4x4_3x3_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_4x4_3x3_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_3x3_image_to_winograd_transformation(const T * __restrict__ tile[6*6], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[0][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[16][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[17][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[18][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[19][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[20][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[21][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[22][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[23][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[24][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[25][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[26][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[27][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[28][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[29][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[30][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[31][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[32][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[33][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[34][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[35][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_3x3_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 3/2;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[6*6];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=4){
    for (signed outw=0; outw < simg_w; outw+=4){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+6; th+=1){
        for (signed tw=w; tw < w+6; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/4;
      signed wc = (outw)/4;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_4x4_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_4x4_3x3_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_3x3_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto in = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[4]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[5]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[6]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[7]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[8]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[9]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[10]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[11]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[12]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[13]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[14]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[15]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_3x3_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[4*4];
        winograd_4x4_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*4 < 4 ? img_h-h*4 : 4); th++) {
          for (size_t tw = 0; tw < (img_w-w*4 < 4 ? img_w-w*4 : 4); tw++) {
            std::memcpy(&(output[h*4+th][w*4+tw][m]), &(outDetrans[th*4+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[4*4];
        winograd_4x4_3x3_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*4 < 4 ? img_h-h*4 : 4); th++) {
          for (size_t tw = 0; tw < (img_w-w*4 < 4 ? img_w-w*4 : 4); tw++) {
            std::memcpy(&(output[h*4+th][w*4+tw][m]), &(outDetrans[th*4+tw]), sizeof(T));
          }
        }
      }
    }
  }
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2_5_kernel_to_winograd(const KDV * __restrict__ kernel, KDV * __restrict__ out) {
  TRINNITY_ASSERT((channels%kdv_lanes) == 0);
  auto ker = triNNity::internal::memory::View4D<const KDV, 5,5, kernels, channels/kdv_lanes>(kernel);
  auto kerTrans = triNNity::internal::memory::View4D<KDV, 5, 6, kernels, channels/kdv_lanes>(out);
  for (unsigned j = 0; j < 5; j++) {
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
        kerTrans[j][0][m][c] =  + static_cast<KD>(1.0/4.0)*( + ker[j][0][m][c]);
        kerTrans[j][1][m][c] =  + static_cast<KD>(1.0/6.0)*( - ker[j][0][m][c] - ker[j][1][m][c] - ker[j][2][m][c] - ker[j][3][m][c] - ker[j][4][m][c]);
        kerTrans[j][2][m][c] =  + static_cast<KD>(1.0/6.0)*( - ker[j][0][m][c] + ker[j][1][m][c] - ker[j][2][m][c] + ker[j][3][m][c] - ker[j][4][m][c]);
        kerTrans[j][3][m][c] =  + static_cast<KD>(1.0/3.0)*( + ker[j][3][m][c]) + static_cast<KD>(1.0/6.0)*( + ker[j][2][m][c]) + static_cast<KD>(1.0/12.0)*( + ker[j][1][m][c]) + static_cast<KD>(1.0/24.0)*( + ker[j][0][m][c]) + static_cast<KD>(2.0/3.0)*( + ker[j][4][m][c]);
        kerTrans[j][4][m][c] =  + static_cast<KD>(1.0/3.0)*( - ker[j][3][m][c]) + static_cast<KD>(1.0/6.0)*( + ker[j][2][m][c]) + static_cast<KD>(1.0/12.0)*( - ker[j][1][m][c]) + static_cast<KD>(1.0/24.0)*( + ker[j][0][m][c]) + static_cast<KD>(2.0/3.0)*( + ker[j][4][m][c]);
        kerTrans[j][5][m][c] =  + ( + ker[j][4][m][c]);

      }
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2_5_image_to_winograd(const TV * __restrict__ img, TV * __restrict__ out) {
  TRINNITY_ASSERT((channels%tv_lanes) == 0);
  constexpr signed simg_h = (signed)img_h;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr signed krad = 5/2;
  constexpr TV zero_vec = 0;
  auto image = triNNity::internal::memory::View3D<const TV, img_h, img_w, channels/tv_lanes>(img);
  auto imgTrans = triNNity::internal::memory::View4D<TV, 6, (img_h+(krad*2)), tiledW, channels/tv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel
  #endif
  {
    TV * barBacking = new TV[6 * channels/tv_lanes];
    auto bar = triNNity::internal::memory::View2D<TV, 6, channels/tv_lanes>(barBacking);
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (signed h = -krad; h < ((signed)img_h) + krad; h++) {
      for (unsigned w = 0; w < tiledW; w++) {
        if (h < 0 || h >= simg_h) {
          for (unsigned b = 0; b < 6; b++) {
            for (unsigned c = 0; c < channels/tv_lanes; c++) {
              bar[b][c] = zero_vec;
            }
          }
        } else {
          for (signed b = -krad; b < 6-krad; b++) {
            if ((w*2 + b) < 0 || (w*2 + b) >= img_w) { memset(bar[b+krad], 0, sizeof(TV)*channels/tv_lanes);
            } else {                                   memcpy(bar[b+krad], image[h][w*2 + b], sizeof(TV)*channels/tv_lanes); }
          }
        }
        for (unsigned c = 0; c < channels/tv_lanes; c++) {
imgTrans[0][h+krad][w][c] =  + ( + bar[4][c]) + static_cast<T>(4.0/1.0)*( + bar[0][c]) + static_cast<T>(5.0/1.0)*( - bar[2][c]);
imgTrans[1][h+krad][w][c] =  + ( + bar[3][c] + bar[4][c]) + static_cast<T>(4.0/1.0)*( - bar[1][c] - bar[2][c]);
imgTrans[2][h+krad][w][c] =  + ( - bar[3][c] + bar[4][c]) + static_cast<T>(4.0/1.0)*( + bar[1][c] - bar[2][c]);
imgTrans[3][h+krad][w][c] =  + ( - bar[2][c] + bar[4][c]) + static_cast<T>(2.0/1.0)*( - bar[1][c] + bar[3][c]);
imgTrans[4][h+krad][w][c] =  + ( - bar[2][c] + bar[4][c]) + static_cast<T>(2.0/1.0)*( + bar[1][c] - bar[3][c]);
imgTrans[5][h+krad][w][c] =  + ( + bar[5][c]) + static_cast<T>(4.0/1.0)*( + bar[1][c]) + static_cast<T>(5.0/1.0)*( - bar[3][c]);

        }
      }
    }
    delete [] barBacking;
  }//end OMP parallel
}



template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_3_5_kernel_to_winograd(const KDV * __restrict__ kernel, KDV * __restrict__ out) {
  TRINNITY_ASSERT((channels%kdv_lanes) == 0);
  auto ker = triNNity::internal::memory::View4D<const KDV, 5,5, kernels, channels/kdv_lanes>(kernel);
  auto kerTrans = triNNity::internal::memory::View4D<KDV, 5, 7, kernels, channels/kdv_lanes>(out);
  for (unsigned j = 0; j < 5; j++) {
    for (unsigned m = 0; m < kernels; m++) {
      for (unsigned c = 0; c < channels/kdv_lanes; c++) {
kerTrans[j][0][m][c] =  + static_cast<KD>(1.0/12.0)*( + ker[j][0][m][c]);
kerTrans[j][1][m][c] =  + static_cast<KD>(1.0/12.0)*( + ker[j][0][m][c] + ker[j][1][m][c] + ker[j][2][m][c] + ker[j][3][m][c] + ker[j][4][m][c]);
kerTrans[j][2][m][c] =  + static_cast<KD>(1.0/24.0)*( + ker[j][0][m][c] - ker[j][1][m][c] + ker[j][2][m][c] - ker[j][3][m][c] + ker[j][4][m][c]);
kerTrans[j][3][m][c] =  + static_cast<KD>(1.0/3.0)*( - ker[j][3][m][c]) + static_cast<KD>(1.0/6.0)*( - ker[j][2][m][c]) + static_cast<KD>(1.0/12.0)*( - ker[j][1][m][c]) + static_cast<KD>(1.0/24.0)*( - ker[j][0][m][c]) + static_cast<KD>(2.0/3.0)*( - ker[j][4][m][c]);
kerTrans[j][4][m][c] =  + static_cast<KD>(1.0/15.0)*( + ker[j][3][m][c]) + static_cast<KD>(1.0/30.0)*( - ker[j][2][m][c]) + static_cast<KD>(1.0/60.0)*( + ker[j][1][m][c]) + static_cast<KD>(1.0/120.0)*( - ker[j][0][m][c]) + static_cast<KD>(2.0/15.0)*( - ker[j][4][m][c]);
kerTrans[j][5][m][c] =  + static_cast<KD>(1.0/40.0)*( + ker[j][1][m][c]) + static_cast<KD>(1.0/120.0)*( + ker[j][0][m][c]) + static_cast<KD>(3.0/40.0)*( + ker[j][2][m][c]) + static_cast<KD>(9.0/40.0)*( + ker[j][3][m][c]) + static_cast<KD>(27.0/40.0)*( + ker[j][4][m][c]);
kerTrans[j][6][m][c] =  + ( + ker[j][4][m][c]);

      }
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3_5_image_to_winograd(const TV * __restrict__ img, TV * __restrict__ out) {
  TRINNITY_ASSERT((channels%tv_lanes) == 0);
  constexpr signed simg_h = (signed)img_h;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr signed krad = 5/2;
  constexpr TV zero_vec = 0;
  auto image = triNNity::internal::memory::View3D<const TV, img_h, img_w, channels/tv_lanes>(img);
  auto imgTrans = triNNity::internal::memory::View4D<TV, 7, (img_h+(krad*2)), tiledW, channels/tv_lanes>(out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel
  #endif
  {
    TV * barBacking = new TV[7 * channels/tv_lanes];
    auto bar = triNNity::internal::memory::View2D<TV, 7, channels/tv_lanes>(barBacking);
    #if defined(TRINNITY_USE_OPENMP)
    #pragma omp for nowait
    #endif
    for (signed h = -krad; h < ((signed)img_h) + krad; h++) {
      for (unsigned w = 0; w < tiledW; w++) {
        if (h < 0 || h >= simg_h) {
          for (unsigned b = 0; b < 7; b++) {
            for (unsigned c = 0; c < channels/tv_lanes; c++) {
              bar[b][c] = zero_vec;
            }
          }
        } else {
          for (signed b = -krad; b < 7-krad; b++) {
            if ((w*3 + b) < 0 || (w*3 + b) >= img_w) { memset(bar[b+krad], 0, sizeof(TV)*channels/tv_lanes);
            } else {                                   memcpy(bar[b+krad], image[h][w*3 + b], sizeof(TV)*channels/tv_lanes); }
          }
        }
        for (unsigned c = 0; c < channels/tv_lanes; c++) {
imgTrans[0][h+krad][w][c] =  + ( - bar[5][c]) + static_cast<T>(3.0/1.0)*( + bar[4][c]) + static_cast<T>(4.0/1.0)*( - bar[1][c]) + static_cast<T>(5.0/1.0)*( + bar[3][c]) + static_cast<T>(12.0/1.0)*( + bar[0][c]) + static_cast<T>(15.0/1.0)*( - bar[2][c]);
imgTrans[1][h+krad][w][c] =  + ( + bar[5][c]) + static_cast<T>(2.0/1.0)*( - bar[4][c]) + static_cast<T>(7.0/1.0)*( - bar[3][c]) + static_cast<T>(8.0/1.0)*( + bar[2][c]) + static_cast<T>(12.0/1.0)*( + bar[1][c]);
imgTrans[2][h+krad][w][c] =  + ( - bar[3][c] + bar[5][c]) + static_cast<T>(4.0/1.0)*( - bar[4][c]) + static_cast<T>(12.0/1.0)*( - bar[1][c]) + static_cast<T>(16.0/1.0)*( + bar[2][c]);
imgTrans[3][h+krad][w][c] =  + ( + bar[2][c] - bar[4][c] + bar[5][c]) + static_cast<T>(6.0/1.0)*( + bar[1][c]) + static_cast<T>(7.0/1.0)*( - bar[3][c]);
imgTrans[4][h+krad][w][c] =  + ( + bar[5][c]) + static_cast<T>(5.0/1.0)*( + bar[2][c] + bar[3][c] - bar[4][c]) + static_cast<T>(6.0/1.0)*( - bar[1][c]);
imgTrans[5][h+krad][w][c] =  + ( + bar[5][c]) + static_cast<T>(4.0/1.0)*( + bar[1][c]) + static_cast<T>(5.0/1.0)*( - bar[3][c]);
imgTrans[6][h+krad][w][c] =  + ( + bar[6][c]) + static_cast<T>(3.0/1.0)*( - bar[5][c]) + static_cast<T>(4.0/1.0)*( + bar[2][c]) + static_cast<T>(5.0/1.0)*( - bar[4][c]) + static_cast<T>(12.0/1.0)*( - bar[1][c]) + static_cast<T>(15.0/1.0)*( + bar[3][c]);

        }
      }
    }
    delete [] barBacking;
  }//end OMP parallel
}



template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2x2_5x5_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 5*5, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 6*6, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (static_cast<KD>((1.0f)/(16.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[16][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[17][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[18][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[19][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[20][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[21][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[22][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[23][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(48.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(96.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[24][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[25][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[26][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[27][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[28][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[29][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(4.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[30][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[31][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[32][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[33][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[34][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[35][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_2x2_5x5_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_2x2_5x5_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_2x2_5x5_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_5x5_image_to_winograd_transformation(const T * __restrict__ tile[6*6], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[0][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[16][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))));
  std::memcpy(&(imgTrans[17][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[18][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[19][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[20][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[21][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[22][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[23][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[24][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[25][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[26][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[27][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[28][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[29][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[30][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[31][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[32][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[33][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[34][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))));
  std::memcpy(&(imgTrans[35][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_5x5_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 5/2;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[6*6];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=2){
    for (signed outw=0; outw < simg_w; outw+=2){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+6; th+=1){
        for (signed tw=w; tw < w+6; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/2;
      signed wc = (outw)/2;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_2x2_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_2x2_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_5x5_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto in = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(-((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_2x2_5x5_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[2*2];
        winograd_2x2_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*2 < 2 ? img_h-h*2 : 2); th++) {
          for (size_t tw = 0; tw < (img_w-w*2 < 2 ? img_w-w*2 : 2); tw++) {
            std::memcpy(&(output[h*2+th][w*2+tw][m]), &(outDetrans[th*2+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[2*2];
        winograd_2x2_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*2 < 2 ? img_h-h*2 : 2); th++) {
          for (size_t tw = 0; tw < (img_w-w*2 < 2 ? img_w-w*2 : 2); tw++) {
            std::memcpy(&(output[h*2+th][w*2+tw][m]), &(outDetrans[th*2+tw]), sizeof(T));
          }
        }
      }
    }
  }
}


template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_3x3_5x5_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 5*5, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 7*7, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(90.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(160.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(90.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(160.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[16][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[17][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(180.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[18][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(320.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(320.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[19][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[20][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[21][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(18.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[22][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[23][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(9.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(18.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(72.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(144.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(288.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(576.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(9.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(9.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[24][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(45.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(90.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(45.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(45.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[25][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(20.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(40.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(80.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(160.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(320.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(20.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(40.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((3.0f)/(320.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((9.0f)/(20.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((9.0f)/(40.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(320.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[26][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[27][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(90.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[28][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(90.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[29][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(180.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[30][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(45.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(90.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(45.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(45.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[31][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(225.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(450.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(225.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(225.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[32][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(100.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(300.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(100.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((9.0f)/(100.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((9.0f)/(200.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(400.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((9.0f)/(800.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[33][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[34][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(160.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[35][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(160.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[36][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(320.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(320.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[37][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(20.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(40.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(80.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(160.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(180.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(320.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(480.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(20.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(40.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((3.0f)/(160.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((3.0f)/(320.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((9.0f)/(20.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((9.0f)/(40.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((9.0f)/(160.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((9.0f)/(320.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[38][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(100.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(300.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(100.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((9.0f)/(100.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((9.0f)/(200.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(400.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((9.0f)/(800.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((9.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[39][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((9.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((27.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((81.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((243.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((729.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[40][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(40.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(40.0f))*((+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((9.0f)/(40.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((27.0f)/(40.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[41][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[42][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(12.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[43][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(24.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[44][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(3.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(6.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(12.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(24.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(3.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[45][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[46][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(40.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(40.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((9.0f)/(40.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((27.0f)/(40.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[47][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[48][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_3x3_5x5_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_3x3_5x5_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_3x3_5x5_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_5x5_image_to_winograd_transformation(const T * __restrict__ tile[7*7], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 7*7, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(-((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[0][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[40][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[7][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((112.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((128.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((112.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((128.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((256.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))));
  std::memcpy(&(imgTrans[16][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((112.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[17][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))));
  std::memcpy(&(imgTrans[18][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[19][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))));
  std::memcpy(&(imgTrans[20][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[21][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[22][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((112.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[23][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[24][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[25][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[26][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[27][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[28][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[29][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))));
  std::memcpy(&(imgTrans[30][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[31][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))));
  std::memcpy(&(imgTrans[32][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[33][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))));
  std::memcpy(&(imgTrans[34][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[35][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[36][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[37][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[38][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[39][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[40][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[41][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))+(+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[42][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((96.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[43][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((192.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))));
  std::memcpy(&(imgTrans[44][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((7.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((21.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((35.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((84.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((105.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[45][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))));
  std::memcpy(&(imgTrans[46][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[47][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))));
  std::memcpy(&(imgTrans[48][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_5x5_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 5/2;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[7*7];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=3){
    for (signed outw=0; outw < simg_w; outw+=3){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+7; th+=1){
        for (signed tw=w; tw < w+7; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/3;
      signed wc = (outw)/3;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_3x3_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_3x3_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_5x5_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto in = triNNity::internal::memory::View4D<T, 7*7, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(-((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[23][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(-((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[4]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(-((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[22][h][w][m]))))))+(-((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(-((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[36][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(in[38][h][w][m]))))))+(-((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[5]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[6]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[7]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[48][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))));
  std::memcpy(&(out[8]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_3x3_5x5_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[3*3];
        winograd_3x3_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*3 < 3 ? img_h-h*3 : 3); th++) {
          for (size_t tw = 0; tw < (img_w-w*3 < 3 ? img_w-w*3 : 3); tw++) {
            std::memcpy(&(output[h*3+th][w*3+tw][m]), &(outDetrans[th*3+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[3*3];
        winograd_3x3_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*3 < 3 ? img_h-h*3 : 3); th++) {
          for (size_t tw = 0; tw < (img_w-w*3 < 3 ? img_w-w*3 : 3); tw++) {
            std::memcpy(&(output[h*3+th][w*3+tw][m]), &(outDetrans[th*3+tw]), sizeof(T));
          }
        }
      }
    }
  }
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_4x4_5x5_kernel_to_winograd_transformation(const KDV * __restrict__ inV, KDV * __restrict__ outV, unsigned c, unsigned m) {
  const KD* in = (const KD*) inV;
  KD* out = (KD*) outV;
  auto ker = triNNity::internal::memory::View3D<const KD, 5*5, kernels, channels>(in);
  auto kerTrans = triNNity::internal::memory::View3D<KD, 8*8, kernels, channels>(out);
  typedef KD KDVV __attribute__ ((vector_size (kdv_lanes*sizeof(KD)), aligned (1)));
  KDVV tempRes;
  tempRes = (static_cast<KD>((1.0f)/(1296.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[0][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1728.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[1][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1728.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[2][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(270.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(540.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(1080.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(2160.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(4320.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[3][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(270.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(540.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(1080.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(2160.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(4320.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[4][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(8640.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(25920.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[5][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(8640.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(25920.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[6][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[7][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1728.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[8][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2304.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[9][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2304.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[10][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[11][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[12][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[13][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[14][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[15][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1728.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[16][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2304.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[17][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(2304.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[18][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[19][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[20][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[21][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[22][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[23][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(270.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(540.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(1080.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(2160.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(4320.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[24][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[25][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[26][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(225.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(450.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(225.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(225.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[27][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(225.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(450.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(225.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(225.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[28][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[29][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[30][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[31][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(270.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(540.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(1080.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(2160.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(4320.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[32][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[33][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(360.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(1440.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(5760.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(-((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[34][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(225.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(450.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(225.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(225.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[35][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(225.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(450.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(900.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((2.0f)/(225.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((4.0f)/(225.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[36][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((+((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((+((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[37][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((+((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((+((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))));
  std::memcpy(&(kerTrans[38][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[39][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(8640.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(25920.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[40][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[41][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[42][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[43][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((-((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((+((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((-((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[44][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(19200.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(57600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(172800.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(518400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((27.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((81.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[45][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(19200.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(57600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(172800.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(518400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(6400.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((27.0f)/(6400.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((81.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[46][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(80.0f))*((+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[47][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(320.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((1.0f)/(960.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(2880.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(8640.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(25920.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))));
  std::memcpy(&(kerTrans[48][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[49][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(1280.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(3840.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(11520.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(34560.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(1280.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[50][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((+((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((-((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((+((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((-((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((+((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[51][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(200.0f))*((+((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((1.0f)/(400.0f))*((-((*((KDVV*)(&(ker[18][m][c]))))))))+(static_cast<KD>((1.0f)/(600.0f))*((-((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(800.0f))*((+((*((KDVV*)(&(ker[13][m][c]))))))))+(static_cast<KD>((1.0f)/(1200.0f))*((+((*((KDVV*)(&(ker[17][m][c]))))))))+(static_cast<KD>((1.0f)/(1600.0f))*((-((*((KDVV*)(&(ker[8][m][c]))))))))+(static_cast<KD>((1.0f)/(1800.0f))*((+((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(2400.0f))*((-((*((KDVV*)(&(ker[12][m][c]))))))))+(static_cast<KD>((1.0f)/(3200.0f))*((+((*((KDVV*)(&(ker[3][m][c]))))))))+(static_cast<KD>((1.0f)/(3600.0f))*((-((*((KDVV*)(&(ker[16][m][c]))))))))+(static_cast<KD>((1.0f)/(4800.0f))*((+((*((KDVV*)(&(ker[7][m][c]))))))))+(static_cast<KD>((1.0f)/(5400.0f))*((-((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(7200.0f))*((+((*((KDVV*)(&(ker[11][m][c]))))))))+(static_cast<KD>((1.0f)/(9600.0f))*((-((*((KDVV*)(&(ker[2][m][c]))))))))+(static_cast<KD>((1.0f)/(10800.0f))*((+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(14400.0f))*((-((*((KDVV*)(&(ker[6][m][c]))))))))+(static_cast<KD>((1.0f)/(21600.0f))*((-((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(28800.0f))*((+((*((KDVV*)(&(ker[1][m][c]))))))))+(static_cast<KD>((1.0f)/(43200.0f))*((+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(86400.0f))*((-((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(200.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))))+(static_cast<KD>((3.0f)/(400.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((3.0f)/(800.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((3.0f)/(1600.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((3.0f)/(3200.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[52][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(-((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(19200.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(+((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(+((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(57600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(-((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(172800.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(+((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(518400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(+((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((27.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((81.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[53][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[8][m][c]))))))+(+((*((KDVV*)(&(ker[12][m][c]))))))+(+((*((KDVV*)(&(ker[16][m][c]))))))+(+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((1.0f)/(19200.0f))*((-((*((KDVV*)(&(ker[3][m][c]))))))+(-((*((KDVV*)(&(ker[7][m][c]))))))+(-((*((KDVV*)(&(ker[11][m][c]))))))+(-((*((KDVV*)(&(ker[15][m][c]))))))))+(static_cast<KD>((1.0f)/(57600.0f))*((+((*((KDVV*)(&(ker[2][m][c]))))))+(+((*((KDVV*)(&(ker[6][m][c]))))))+(+((*((KDVV*)(&(ker[10][m][c]))))))))+(static_cast<KD>((1.0f)/(172800.0f))*((-((*((KDVV*)(&(ker[1][m][c]))))))+(-((*((KDVV*)(&(ker[5][m][c]))))))))+(static_cast<KD>((1.0f)/(518400.0f))*((+((*((KDVV*)(&(ker[0][m][c]))))))))+(static_cast<KD>((3.0f)/(6400.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))+(-((*((KDVV*)(&(ker[13][m][c]))))))+(-((*((KDVV*)(&(ker[17][m][c]))))))+(-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((9.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[18][m][c]))))))+(+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((27.0f)/(6400.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))+(-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((81.0f)/(6400.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[54][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(80.0f))*((+((*((KDVV*)(&(ker[22][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((-((*((KDVV*)(&(ker[21][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[20][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((-((*((KDVV*)(&(ker[23][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[55][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(36.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))));
  std::memcpy(&(kerTrans[56][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(+((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(+((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[57][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(48.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))+(-((*((KDVV*)(&(ker[9][m][c]))))))+(+((*((KDVV*)(&(ker[14][m][c]))))))+(-((*((KDVV*)(&(ker[19][m][c]))))))+(+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[58][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[59][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(15.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((1.0f)/(30.0f))*((-((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(60.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(120.0f))*((-((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((2.0f)/(15.0f))*((-((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[60][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(80.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((+((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((+((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[61][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (static_cast<KD>((1.0f)/(80.0f))*((+((*((KDVV*)(&(ker[14][m][c]))))))))+(static_cast<KD>((1.0f)/(240.0f))*((-((*((KDVV*)(&(ker[9][m][c]))))))))+(static_cast<KD>((1.0f)/(720.0f))*((+((*((KDVV*)(&(ker[4][m][c]))))))))+(static_cast<KD>((3.0f)/(80.0f))*((-((*((KDVV*)(&(ker[19][m][c]))))))))+(static_cast<KD>((9.0f)/(80.0f))*((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[62][m][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((KDVV*)(&(ker[24][m][c]))))))));
  std::memcpy(&(kerTrans[63][m][c]), &tempRes, sizeof(tempRes));
}

template <typename KD, unsigned channels, unsigned kernels, unsigned kdv_lanes=1, typename KDV=KD>
static TRINNITY_INLINE void winograd_4x4_5x5_kernel_to_winograd(const KDV * __restrict__ in, KDV * __restrict__ out) {
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m=0; m < kernels; m+=1){
    for (signed c=0; c < (channels/kdv_lanes)*kdv_lanes; c+=kdv_lanes){
      winograd_4x4_5x5_kernel_to_winograd_transformation<KD, channels, kernels, kdv_lanes, KDV>(in, out, c, m);
    }
    for (signed c=(channels/kdv_lanes)*kdv_lanes; c < channels; c+=1){
      winograd_4x4_5x5_kernel_to_winograd_transformation<KD, channels, kernels, 1, KDV>(in, out, c, m);
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_5x5_image_to_winograd_transformation(const T * __restrict__ tile[8*8], TV * __restrict__ outV, unsigned hc, unsigned wc, unsigned c) {
  T* out = (T*) outV;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto imgTrans = triNNity::internal::memory::View4D<T, 8*8, tiledH, tiledW, channels>(out);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[4][c]))))))+(+((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((686.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[0][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[2][c]))))))+(-((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((2401.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[0][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[40][c]))))))+(+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[1][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))+(+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))+(-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))+(+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[2][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[3][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[4][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[5][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[48][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[16][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[32][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[6][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[56][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[40][c]))))))))+(static_cast<T>((686.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[8][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[24][c]))))))))+(static_cast<T>((2401.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[7][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[53][c]))))))+(-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))+(+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))+(-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[8][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((169.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[9][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((169.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[10][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[11][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[12][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[13][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[14][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[61][c]))))))+(+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[59][c]))))))+(-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[57][c]))))))+(+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[15][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[53][c]))))))+(-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))+(+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(+((*((TVV*)(&(tile[6][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))+(-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))+(-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))+(+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))+(+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))+(-((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[16][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((169.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[17][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((169.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))));
  std::memcpy(&(imgTrans[18][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[19][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[20][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[21][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[53][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[22][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[61][c]))))))+(+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[59][c]))))))+(-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[57][c]))))))+(+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))));
  std::memcpy(&(imgTrans[23][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[24][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[25][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[26][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((162.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((200.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((400.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[27][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((162.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((200.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((400.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[28][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[29][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[30][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[61][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[57][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[59][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[31][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[32][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[33][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((26.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((117.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((130.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((234.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((260.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[34][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((162.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((200.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((400.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[35][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((162.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((200.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((400.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[36][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[37][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[38][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[61][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[57][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[59][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[39][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[40][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[41][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[42][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[43][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[44][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[45][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[46][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[61][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[57][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[59][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[47][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[6][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((+((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((-((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[2][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[4][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((+((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[48][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[49][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[46][c]))))))+(+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(-((*((TVV*)(&(tile[38][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((39.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((52.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((65.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((156.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((195.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[50][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((-((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[51][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((8.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((27.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((30.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((40.0f)/(1.0f))*((-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))))+(static_cast<T>((50.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((80.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((90.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((100.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((120.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((135.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((150.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((216.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((240.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((270.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((300.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[52][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(-((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((+((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((-((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(-((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((+((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[53][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[54][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[22][c]))))))+(+((*((TVV*)(&(tile[50][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[38][c]))))))+(-((*((TVV*)(&(tile[52][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))+(-((*((TVV*)(&(tile[42][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))+(+((*((TVV*)(&(tile[44][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(tile[18][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[20][c]))))))+(-((*((TVV*)(&(tile[34][c]))))))))+(static_cast<T>((25.0f)/(1.0f))*((+((*((TVV*)(&(tile[36][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((45.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((48.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((60.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))+(+((*((TVV*)(&(tile[26][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((75.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((225.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[54][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[62][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[61][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[58][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[60][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[57][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[46][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[59][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[14][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[30][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[42][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((+((*((TVV*)(&(tile[44][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[10][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[12][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[26][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((-((*((TVV*)(&(tile[28][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[55][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[39][c]))))))+(+((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[7][c]))))))+(+((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((-((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[5][c]))))))+(-((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((686.0f)/(1.0f))*((+((*((TVV*)(&(tile[21][c]))))))+(+((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[1][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[3][c]))))))+(+((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((2401.0f)/(1.0f))*((-((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[56][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[47][c]))))))+(+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(-((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[57][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((-((*((TVV*)(&(tile[47][c]))))))+(+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((13.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))+(-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(+((*((TVV*)(&(tile[23][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))+(-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))+(+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((182.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((468.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))+(+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((637.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))+(-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))+(-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(+((*((TVV*)(&(tile[19][c]))))))));
  std::memcpy(&(imgTrans[58][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[59][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((2.0f)/(1.0f))*((-((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((10.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((18.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((20.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((28.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((98.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((126.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((140.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((252.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((280.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((324.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((360.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((441.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((490.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((648.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((720.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((882.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((980.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[60][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((-((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((-((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((-((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((+((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((-((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((+((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((-((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((+((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((+((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((-((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[61][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[55][c]))))))))+(static_cast<T>((3.0f)/(1.0f))*((-((*((TVV*)(&(tile[47][c]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(tile[23][c]))))))))+(static_cast<T>((5.0f)/(1.0f))*((-((*((TVV*)(&(tile[39][c]))))))))+(static_cast<T>((12.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[53][c]))))))))+(static_cast<T>((15.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[49][c]))))))))+(static_cast<T>((42.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[51][c]))))))))+(static_cast<T>((56.0f)/(1.0f))*((-((*((TVV*)(&(tile[21][c]))))))))+(static_cast<T>((70.0f)/(1.0f))*((+((*((TVV*)(&(tile[37][c]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((144.0f)/(1.0f))*((-((*((TVV*)(&(tile[17][c]))))))))+(static_cast<T>((147.0f)/(1.0f))*((-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((168.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))))+(static_cast<T>((180.0f)/(1.0f))*((+((*((TVV*)(&(tile[33][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[19][c]))))))))+(static_cast<T>((210.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))))+(static_cast<T>((245.0f)/(1.0f))*((-((*((TVV*)(&(tile[35][c]))))))))+(static_cast<T>((432.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((540.0f)/(1.0f))*((-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((588.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))))+(static_cast<T>((735.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[62][hc][wc][c]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(tile[63][c]))))))))+(static_cast<T>((14.0f)/(1.0f))*((-((*((TVV*)(&(tile[47][c]))))))+(-((*((TVV*)(&(tile[61][c]))))))))+(static_cast<T>((36.0f)/(1.0f))*((-((*((TVV*)(&(tile[15][c]))))))+(-((*((TVV*)(&(tile[57][c]))))))))+(static_cast<T>((49.0f)/(1.0f))*((+((*((TVV*)(&(tile[31][c]))))))+(+((*((TVV*)(&(tile[59][c]))))))))+(static_cast<T>((196.0f)/(1.0f))*((+((*((TVV*)(&(tile[45][c]))))))))+(static_cast<T>((504.0f)/(1.0f))*((+((*((TVV*)(&(tile[13][c]))))))+(+((*((TVV*)(&(tile[41][c]))))))))+(static_cast<T>((686.0f)/(1.0f))*((-((*((TVV*)(&(tile[29][c]))))))+(-((*((TVV*)(&(tile[43][c]))))))))+(static_cast<T>((1296.0f)/(1.0f))*((+((*((TVV*)(&(tile[9][c]))))))))+(static_cast<T>((1764.0f)/(1.0f))*((-((*((TVV*)(&(tile[11][c]))))))+(-((*((TVV*)(&(tile[25][c]))))))))+(static_cast<T>((2401.0f)/(1.0f))*((+((*((TVV*)(&(tile[27][c]))))))));
  std::memcpy(&(imgTrans[63][hc][wc][c]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned channels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_5x5_image_to_winograd(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr signed simg_h = (signed)img_h;
  constexpr signed simg_w = (signed)img_w;
  constexpr signed krad = 5/2;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  T imgTransZeroArray[channels] = {0};
  T* inS = (T*) in;
  auto image = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(inS);
  const T* tile[8*8];
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed outh=0; outh < simg_h; outh+=4){
    for (signed outw=0; outw < simg_w; outw+=4){
      signed h = outh - krad;
      signed w = outw - krad;
      signed outPos = 0;
      for (signed th=h; th < h+8; th+=1){
        for (signed tw=w; tw < w+8; tw+=1){
          if (th < 0 || tw < 0 || th >= simg_h || tw >= simg_w) {
            tile[outPos] = &(imgTransZeroArray[0]);
            outPos++;
          } else {
            tile[outPos] = &(image[th][tw][0]);
            outPos++;
          }
        }
      }
      signed hc = (outh)/4;
      signed wc = (outw)/4;
      for (signed c=0; c < (channels/tv_lanes)*tv_lanes; c+=tv_lanes){
        winograd_4x4_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, tv_lanes, TV>(tile, out, hc, wc, c);
      }
      for (signed c=(channels/tv_lanes)*tv_lanes; c < channels; c+=1){
        winograd_4x4_5x5_image_to_winograd_transformation<T, img_h, img_w, channels, 1, TV>(tile, out, hc, wc, c);
      }
    }
  }
}
template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_5x5_winograd_to_output_transformation(const TV * __restrict__ inV, TV * __restrict__ outV, unsigned h, unsigned w, unsigned m) {
  TV* out = (TV*) outV;
  T* inS = (T*) inV;
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto in = triNNity::internal::memory::View4D<T, 8*8, tiledH, tiledW, kernels>(inS);
  typedef T TVV __attribute__ ((vector_size (tv_lanes*sizeof(T)), aligned (1)));
  TVV tempRes;
  tempRes = (((+((*((TVV*)(&(in[0][h][w][m]))))))+(+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[40][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[48][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[0]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[48][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[1]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[16][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[32][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[48][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[2]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[8][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[16][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[56][h][w][m]))))))+(+((*((TVV*)(&(in[57][h][w][m]))))))+(+((*((TVV*)(&(in[58][h][w][m]))))))+(+((*((TVV*)(&(in[59][h][w][m]))))))+(+((*((TVV*)(&(in[60][h][w][m]))))))+(+((*((TVV*)(&(in[61][h][w][m]))))))+(+((*((TVV*)(&(in[62][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[24][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[32][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[40][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[48][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[3]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[5][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[4]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((6.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[5]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[6]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[57][h][w][m]))))))+(-((*((TVV*)(&(in[58][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[59][h][w][m]))))))+(-((*((TVV*)(&(in[60][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[61][h][w][m]))))))+(-((*((TVV*)(&(in[62][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[7]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(+((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(+((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[5][h][w][m]))))))+(+((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[8]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((12.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((18.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[9]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((36.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[10]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(+((*((TVV*)(&(in[10][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[57][h][w][m]))))))+(+((*((TVV*)(&(in[58][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(+((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[59][h][w][m]))))))+(+((*((TVV*)(&(in[60][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(+((*((TVV*)(&(in[26][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(+((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[61][h][w][m]))))))+(+((*((TVV*)(&(in[62][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(+((*((TVV*)(&(in[42][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(+((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(+((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(+((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((243.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(+((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[11]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[1][h][w][m]))))))+(-((*((TVV*)(&(in[2][h][w][m]))))))+(+((*((TVV*)(&(in[7][h][w][m]))))))+(+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))+(+((*((TVV*)(&(in[55][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[3][h][w][m]))))))+(-((*((TVV*)(&(in[4][h][w][m]))))))+(+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[5][h][w][m]))))))+(-((*((TVV*)(&(in[6][h][w][m]))))))+(+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[12]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(-((*((TVV*)(&(in[23][h][w][m]))))))))+(static_cast<T>((2.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(-((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((3.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))+(-((*((TVV*)(&(in[55][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))))+(static_cast<T>((16.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((24.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((54.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((81.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[13]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(+((*((TVV*)(&(in[17][h][w][m]))))))+(-((*((TVV*)(&(in[18][h][w][m]))))))+(+((*((TVV*)(&(in[23][h][w][m]))))))))+(static_cast<T>((4.0f)/(1.0f))*((+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(+((*((TVV*)(&(in[33][h][w][m]))))))+(-((*((TVV*)(&(in[34][h][w][m]))))))+(+((*((TVV*)(&(in[39][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(+((*((TVV*)(&(in[19][h][w][m]))))))+(-((*((TVV*)(&(in[20][h][w][m]))))))))+(static_cast<T>((9.0f)/(1.0f))*((+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))+(+((*((TVV*)(&(in[49][h][w][m]))))))+(-((*((TVV*)(&(in[50][h][w][m]))))))+(+((*((TVV*)(&(in[55][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(+((*((TVV*)(&(in[21][h][w][m]))))))+(-((*((TVV*)(&(in[22][h][w][m]))))))))+(static_cast<T>((32.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(+((*((TVV*)(&(in[35][h][w][m]))))))+(-((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((72.0f)/(1.0f))*((+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(+((*((TVV*)(&(in[51][h][w][m]))))))+(-((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((108.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(+((*((TVV*)(&(in[37][h][w][m]))))))+(-((*((TVV*)(&(in[38][h][w][m]))))))))+(static_cast<T>((243.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(+((*((TVV*)(&(in[53][h][w][m]))))))+(-((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[14]), &tempRes, sizeof(tempRes));
  tempRes = (((+((*((TVV*)(&(in[9][h][w][m]))))))+(-((*((TVV*)(&(in[10][h][w][m]))))))+(+((*((TVV*)(&(in[15][h][w][m]))))))+(-((*((TVV*)(&(in[17][h][w][m]))))))+(+((*((TVV*)(&(in[18][h][w][m]))))))+(-((*((TVV*)(&(in[23][h][w][m]))))))+(+((*((TVV*)(&(in[57][h][w][m]))))))+(-((*((TVV*)(&(in[58][h][w][m]))))))+(+((*((TVV*)(&(in[63][h][w][m]))))))))+(static_cast<T>((8.0f)/(1.0f))*((+((*((TVV*)(&(in[11][h][w][m]))))))+(-((*((TVV*)(&(in[12][h][w][m]))))))+(-((*((TVV*)(&(in[19][h][w][m]))))))+(+((*((TVV*)(&(in[20][h][w][m]))))))+(+((*((TVV*)(&(in[25][h][w][m]))))))+(-((*((TVV*)(&(in[26][h][w][m]))))))+(+((*((TVV*)(&(in[31][h][w][m]))))))+(-((*((TVV*)(&(in[33][h][w][m]))))))+(+((*((TVV*)(&(in[34][h][w][m]))))))+(-((*((TVV*)(&(in[39][h][w][m]))))))+(+((*((TVV*)(&(in[59][h][w][m]))))))+(-((*((TVV*)(&(in[60][h][w][m]))))))))+(static_cast<T>((27.0f)/(1.0f))*((+((*((TVV*)(&(in[13][h][w][m]))))))+(-((*((TVV*)(&(in[14][h][w][m]))))))+(-((*((TVV*)(&(in[21][h][w][m]))))))+(+((*((TVV*)(&(in[22][h][w][m]))))))+(+((*((TVV*)(&(in[41][h][w][m]))))))+(-((*((TVV*)(&(in[42][h][w][m]))))))+(+((*((TVV*)(&(in[47][h][w][m]))))))+(-((*((TVV*)(&(in[49][h][w][m]))))))+(+((*((TVV*)(&(in[50][h][w][m]))))))+(-((*((TVV*)(&(in[55][h][w][m]))))))+(+((*((TVV*)(&(in[61][h][w][m]))))))+(-((*((TVV*)(&(in[62][h][w][m]))))))))+(static_cast<T>((64.0f)/(1.0f))*((+((*((TVV*)(&(in[27][h][w][m]))))))+(-((*((TVV*)(&(in[28][h][w][m]))))))+(-((*((TVV*)(&(in[35][h][w][m]))))))+(+((*((TVV*)(&(in[36][h][w][m]))))))))+(static_cast<T>((216.0f)/(1.0f))*((+((*((TVV*)(&(in[29][h][w][m]))))))+(-((*((TVV*)(&(in[30][h][w][m]))))))+(-((*((TVV*)(&(in[37][h][w][m]))))))+(+((*((TVV*)(&(in[38][h][w][m]))))))+(+((*((TVV*)(&(in[43][h][w][m]))))))+(-((*((TVV*)(&(in[44][h][w][m]))))))+(-((*((TVV*)(&(in[51][h][w][m]))))))+(+((*((TVV*)(&(in[52][h][w][m]))))))))+(static_cast<T>((729.0f)/(1.0f))*((+((*((TVV*)(&(in[45][h][w][m]))))))+(-((*((TVV*)(&(in[46][h][w][m]))))))+(-((*((TVV*)(&(in[53][h][w][m]))))))+(+((*((TVV*)(&(in[54][h][w][m]))))))));
  std::memcpy(&(out[15]), &tempRes, sizeof(tempRes));
}

template <typename T, unsigned img_h, unsigned img_w, unsigned kernels, unsigned tv_lanes=1, typename TV=T>
static TRINNITY_INLINE void winograd_4x4_5x5_winograd_to_output(const TV * __restrict__ in, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>((T*)out);
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed h=0; h < tiledH; h+=1){
    for (signed w=0; w < tiledW; w+=1){
      for (signed m=0; m < (kernels/tv_lanes)*tv_lanes; m+=tv_lanes){
        TV outDetrans[4*4];
        winograd_4x4_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, tv_lanes, TV>((const TV*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*4 < 4 ? img_h-h*4 : 4); th++) {
          for (size_t tw = 0; tw < (img_w-w*4 < 4 ? img_w-w*4 : 4); tw++) {
            std::memcpy(&(output[h*4+th][w*4+tw][m]), &(outDetrans[th*4+tw]), sizeof(TV));
          }
        }
      }
      for (signed m=(kernels/tv_lanes)*tv_lanes; m < kernels; m+=1){
        T outDetrans[4*4];
        winograd_4x4_5x5_winograd_to_output_transformation<T, img_h, img_w, kernels, 1, T>((const T*)in, outDetrans, h, w, m);
        for (size_t th = 0; th < (img_h-h*4 < 4 ? img_h-h*4 : 4); th++) {
          for (size_t tw = 0; tw < (img_w-w*4 < 4 ? img_w-w*4 : 4); tw++) {
            std::memcpy(&(output[h*4+th][w*4+tw][m]), &(outDetrans[th*4+tw]), sizeof(T));
          }
        }
      }
    }
  }
}




}

}

}

}

#endif // TRINNITY_SPECTRAL_CPU_SHAPE_H
