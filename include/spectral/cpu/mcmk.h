/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_MCMK_H
#define TRINNITY_SPECTRAL_CPU_MCMK_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/spectral/cpu/impl.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace spectral {

template <typename T, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k, unsigned fourier_domain_length,
          conv_fourier_impl_t var, boundary_t bound,
          gemm_variant_t gemm_var,
          triNNity::layout::spectral_feature_map_layout_t img_layout,
          triNNity::layout::spectral_parameter_layout_t krn_layout,
          triNNity::layout::spectral_feature_map_layout_t out_layout,
          unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void convolution_forward_fourier(std::complex<T> * __restrict__ img, std::complex<KD> * __restrict__ kernel, std::complex<T> * __restrict__ out) {

  // TRINNITY_STATIC_ASSERT( stride_w == 1 || stride_h == 1);

  switch(var) {

    case CONV_MULTI_FFT_STITCHED_REAL: {
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_REAL, krn_layout == triNNity::layout::OIHF);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_REAL, img_layout == triNNity::layout::CHFP);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_REAL, out_layout == triNNity::layout::CHFP);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_REAL, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_stitched<T, KD, ifm_w, ifm_h, ifms, ofms, k, ((fourier_domain_length/2) + 1), bound, stride_w, stride_h>(img, kernel, out);
    } break;

    case CONV_MULTI_FFT_STITCHED_COMPLEX: {
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_COMPLEX, krn_layout == triNNity::layout::OIHF);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_COMPLEX, img_layout == triNNity::layout::CHFP);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_COMPLEX, out_layout == triNNity::layout::CHFP);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_STITCHED_COMPLEX, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_stitched<T, KD, ifm_w, ifm_h, ifms, ofms, k, fourier_domain_length, bound, stride_w, stride_h>(img, kernel, out);
    } break;

    case CONV_MULTI_FFT_FULL_GEMM_REAL: {
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_REAL, krn_layout == triNNity::layout::FOHI);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_REAL, img_layout == triNNity::layout::FHCP);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_REAL, out_layout == triNNity::layout::FHCP);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_REAL, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_full_gemm<T, KD, gemm_var, ifm_h, ifms, ofms, k, ((fourier_domain_length/2) + 1), bound, stride_w>(img, kernel, out);
    } break;

    case CONV_MULTI_FFT_FULL_GEMM_COMPLEX: {
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_COMPLEX, krn_layout == triNNity::layout::FOHI);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_COMPLEX, img_layout == triNNity::layout::FHCP);
      TRINNITY_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_COMPLEX, out_layout == triNNity::layout::FHCP);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_FULL_GEMM_COMPLEX, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_full_gemm<T, KD, gemm_var, ifm_h, ifms, ofms, k, fourier_domain_length, bound, stride_w>(img, kernel, out);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <typename T, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k, unsigned fourier_domain_length,
          conv_fourier_impl_t var, boundary_t bound,
          gemm_variant_t gemm_var,
          triNNity::layout::feature_map_layout_t img_layout,
          triNNity::layout::spectral_parameter_layout_t krn_layout,
          triNNity::layout::feature_map_layout_t out_layout,
          unsigned stride_w = 1, unsigned stride_h = 1>
static TRINNITY_INLINE void convolution_forward_fourier_simple_gemm(T * __restrict__ img, KD * __restrict__ kernel, T * __restrict__ out) {

  static constexpr unsigned out_h = (ifm_h/stride_h) + ((ifm_h%stride_h) > 0);

  switch(var) {

    case CONV_MULTI_FFT_SIMPLE_GEMM_REAL: {
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_REAL, krn_layout == triNNity::layout::OIHF);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_REAL, img_layout == triNNity::layout::CHW);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_REAL, out_layout == triNNity::layout::CHW);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_REAL, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_simple_gemm<T, KD, gemm_var, out_h, ifms, ofms, k, ((fourier_domain_length/2) + 1), bound>(img, kernel, out);
    } break;

    case CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX: {
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX, krn_layout == triNNity::layout::OIHF);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX, img_layout == triNNity::layout::CHW);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX, out_layout == triNNity::layout::CHW);
      TRINNITY_STATIC_ASSERT_ON(var==CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX, bound == triNNity::BOUND_UNDEF || bound == triNNity::BOUND_IMPLICIT_PAD);
      spectral::cpu::impl::spectral_fft_simple_gemm<T, KD, gemm_var, out_h, ifms, ofms, k, fourier_domain_length, bound>(img, kernel, out);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <typename T, typename KD, unsigned ifm_w, unsigned ifm_h, unsigned ifms, unsigned ofms, unsigned k,
          conv_winograd_impl_t var, boundary_t bound,
          gemm_variant_t gemm_var,
          triNNity::layout::feature_map_layout_t img_layout,
          triNNity::layout::parameter_layout_t krn_layout,
          triNNity::layout::feature_map_layout_t out_layout,
          unsigned stride_w = 1, unsigned stride_h = 1,
          unsigned tv_lanes=1, typename TV=T,
          unsigned kdv_lanes=1, typename KDV=KD,
          bool transform_kernel=true>
static TRINNITY_INLINE void convolution_forward_winograd(TV * __restrict__ img, KDV * __restrict__ kernel, TV * __restrict__ out) {

  TRINNITY_STATIC_ASSERT(img_layout == triNNity::layout::HWC);
  TRINNITY_STATIC_ASSERT(krn_layout == triNNity::layout::HWOI);
  TRINNITY_STATIC_ASSERT(out_layout == triNNity::layout::HWC);
  TRINNITY_STATIC_ASSERT(bound==BOUND_UNDEF || bound==BOUND_IMPLICIT_PAD);

  TRINNITY_ASSERT(k == 3 || k == 5);

  switch(var) {

    case CONV_MULTI_WINOGRAD_2: {
      TRINNITY_ASSERT(ifms%kdv_lanes == 0);
      TRINNITY_ASSERT(ifms%tv_lanes == 0);
      TRINNITY_ASSERT(ofms%tv_lanes == 0);
      switch (k) {
        case 3: {
          spectral::cpu::impl::winograd_2_3<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, 1, T, 1, KD, transform_kernel>((T*)img, (KD*)kernel, (T*)out);
        } break;
        case 5: {
          spectral::cpu::impl::winograd_2_5<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, 1, T, 1, KD, transform_kernel>((T*)img, (KD*)kernel, (T*)out);
        } break;
        default: {
          TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
        } break;
      }
    } break;

    case CONV_MULTI_WINOGRAD_2x2: {
      switch (k) {
        case 3: {
          spectral::cpu::impl::winograd_2x2_3x3<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        case 5: {
          spectral::cpu::impl::winograd_2x2_5x5<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        default: {
          TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
        } break;
      }
    } break;

    case CONV_MULTI_WINOGRAD_3: {
      TRINNITY_ASSERT(ifms%kdv_lanes == 0);
      TRINNITY_ASSERT(ifms%tv_lanes == 0);
      TRINNITY_ASSERT(ofms%tv_lanes == 0);
      switch (k) {
        case 3: {
          spectral::cpu::impl::winograd_3_3<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, 1, T, 1, KD, transform_kernel>((T*)img, (KD*)kernel, (T*)out);
        } break;
        case 5: {
          spectral::cpu::impl::winograd_3_5<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, 1, T, 1, KD, transform_kernel>((T*)img, (KD*)kernel, (T*)out);
        } break;
        default: {
          TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
        } break;
      }
    } break;

    case CONV_MULTI_WINOGRAD_3x3: {
      switch (k) {
        case 3: {
          spectral::cpu::impl::winograd_3x3_3x3<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        case 5: {
          spectral::cpu::impl::winograd_3x3_5x5<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        default: {
          TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
        } break;
      }
    } break;

    case CONV_MULTI_WINOGRAD_4x4: {
      switch (k) {
        case 3: {
          spectral::cpu::impl::winograd_4x4_3x3<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        case 5: {
          spectral::cpu::impl::winograd_4x4_5x5<T, KD, ifm_w, ifm_h, gemm_var, ifms, ofms, bound, tv_lanes, TV, kdv_lanes, KDV, transform_kernel>(img, kernel, out);
        } break;
        default: {
          TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
        } break;
      }
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

}

}

#endif // TRINNITY_SPECTRAL_CPU_MCMK_H
