/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_LAYER_H
#define TRINNITY_SPECTRAL_CPU_LAYER_H

#include <cstddef>
#include <cmath>

#include <triNNity/config.h>
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/spectral/cpu/shape.h>
#include <triNNity/spectral/cpu/mcmk.h>
#include <triNNity/dense/cpu/primitive.h>

namespace triNNity {

namespace layer {

template<typename T, typename KD,
         triNNity::conv_fourier_impl_t fft_type,
         triNNity::gemm_variant_t gemm_impl,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = BOUND_UNDEF>
struct FFTConvolutionalLayer: public Layer<T, T> {

  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch(fft_type) {
      case CONV_MULTI_FFT_STITCHED_REAL: {
        TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
        TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_UNDEF || bound_type == triNNity::BOUND_IMPLICIT_PAD);

        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k/2>();

        std::complex<T>* input_fft = new std::complex<T>[ifms*stride_w*ifm_h*(fourier_domain_length/2 + 1)];
        std::complex<KD>* kernel_fft = new std::complex<KD>[ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1)];
        std::complex<T>* output_fft = new std::complex<T>[ofms*ofm_h*(fourier_domain_length/2 + 1)];

        triNNity::spectral::cpu::transform::image_chw_to_chf_real<T, ifm_h, ifm_w, ifms, k, fourier_domain_length, stride_w>(this->input, input_fft);
        triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_real<KD, k, ofms, ifms, fourier_domain_length, stride_w>(this->kernel, kernel_fft);

        triNNity::spectral::convolution_forward_fourier<T, KD, ifm_w, ifm_h, ifms, ofms, k, fourier_domain_length, fft_type, bound_type, gemm_impl,
                                                      triNNity::layout::CHFP, triNNity::layout::OIHF, triNNity::layout::CHFP,
                                                      stride_w, stride_h> (input_fft, kernel_fft, output_fft);

        triNNity::spectral::cpu::transform::out_mhf_to_mhw_real<T, ifm_h, ifm_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_fft, this->output);

        delete [] input_fft;
        delete [] kernel_fft;
        delete [] output_fft;

      } break;

      case CONV_MULTI_FFT_STITCHED_COMPLEX: {
        TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
        TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_UNDEF || bound_type == triNNity::BOUND_IMPLICIT_PAD);

        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k/2>();

        std::complex<T>* input_fft = new std::complex<T>[ifms*stride_w*ifm_h*fourier_domain_length];
        std::complex<KD>* kernel_fft = new std::complex<KD>[ofms*ifms*stride_w*k*fourier_domain_length];
        std::complex<T>* output_fft = new std::complex<T>[ofms*ofm_h*fourier_domain_length];

        triNNity::spectral::cpu::transform::image_chw_to_chf_complex<T, ifm_h, ifm_w, ifms, k, fourier_domain_length, stride_w>(this->input, input_fft);
        triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_complex<KD, k, ofms, ifms, fourier_domain_length, stride_w>(this->kernel, kernel_fft);

        triNNity::spectral::convolution_forward_fourier<T, KD, ifm_w, ifm_h, ifms, ofms, k, fourier_domain_length, fft_type, bound_type, gemm_impl,
                                                      triNNity::layout::CHFP, triNNity::layout::OIHF, triNNity::layout::CHFP,
                                                      stride_w, stride_h> (input_fft, kernel_fft, output_fft);

        triNNity::spectral::cpu::transform::out_mhf_to_mhw_complex<T, ifm_h, ifm_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_fft, this->output);

        delete [] input_fft;
        delete [] kernel_fft;
        delete [] output_fft;

      } break;

      case CONV_MULTI_FFT_FULL_GEMM_REAL: {
        TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
        TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_UNDEF || bound_type == triNNity::BOUND_IMPLICIT_PAD);

        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k>();

        std::complex<T> *image_chf      = new std::complex<T>[ifms*stride_w*ifm_h*(fourier_domain_length/2 + 1)];
        std::complex<T> *ifm_fhcp       = new std::complex<T>[(fourier_domain_length/2 + 1)*ifm_h*ifms*stride_w];
        std::complex<KD> *kernel_oihf   = new std::complex<KD>[ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1)];
        std::complex<KD> *kernel_fhoip  = new std::complex<KD>[(fourier_domain_length/2 + 1)*k*ofms*ifms*stride_w];
        std::complex<T> *output_ohf     = new std::complex<T>[ofms*ofm_h*(fourier_domain_length/2 + 1)]();
        std::complex<T> *output_fhoy    = new std::complex<T>[k*ofms*ifm_h*(fourier_domain_length/2 + 1)];

        triNNity::spectral::cpu::transform::image_chw_to_chf_real<T, ifm_h, ifm_w, ifms, k, fourier_domain_length, stride_w>(this->input, image_chf);
        triNNity::spectral::cpu::transform::image_chfp_to_fhcp<T, ifms, ifm_h, fourier_domain_length, stride_w, triNNity::FFT_R2C>(image_chf, ifm_fhcp);

        triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_real<KD, k, ofms, ifms, fourier_domain_length, stride_w>(this->kernel, kernel_oihf);
        triNNity::spectral::cpu::transform::kernel_oihfp_to_fohip<KD, ofms, ifms, k, fourier_domain_length, stride_w, triNNity::FFT_R2C>(kernel_oihf, kernel_fhoip);

        triNNity::spectral::convolution_forward_fourier<T, KD, ifm_w, ifm_h, ifms, ofms, k, fourier_domain_length,
                                                      fft_type, bound_type, gemm_impl,
                                                      triNNity::layout::FHCP, triNNity::layout::FOHI, triNNity::layout::FHCP,
                                                      stride_w, stride_h>
                                                      (ifm_fhcp, kernel_fhoip, output_fhoy);

        triNNity::spectral::cpu::transform::out_fhoyp_to_ohfp<T, fourier_domain_length, ifm_h, k, ofms, stride_w, stride_h, triNNity::FFT_R2C>(output_fhoy, output_ohf);
        triNNity::spectral::cpu::transform::out_mhf_to_mhw_real<T, ifm_h, ifm_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_ohf, this->output);

        delete [] image_chf;
        delete [] ifm_fhcp;
        delete [] kernel_oihf;
        delete [] kernel_fhoip;
        delete [] output_ohf;
        delete [] output_fhoy;
      } break;

      case CONV_MULTI_FFT_FULL_GEMM_COMPLEX: {
        TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
        TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
        TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_UNDEF || bound_type == triNNity::BOUND_IMPLICIT_PAD);

        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k>();

        std::complex<T> *image_chf      = new std::complex<T>[ifms*stride_w*ifm_h*fourier_domain_length];
        std::complex<T> *ifm_fhcp       = new std::complex<T>[fourier_domain_length*ifm_h*ifms*stride_w];
        std::complex<KD> *kernel_oihf   = new std::complex<KD>[ofms*ifms*stride_w*k*fourier_domain_length];
        std::complex<KD> *kernel_fhoip  = new std::complex<KD>[fourier_domain_length*k*ofms*ifms*stride_w];
        std::complex<T> *output_ohf     = new std::complex<T>[ofms*ofm_h*fourier_domain_length]();
        std::complex<T> *output_fhoy    = new std::complex<T>[k*ofms*ifm_h*fourier_domain_length];

        triNNity::spectral::cpu::transform::image_chw_to_chf_real<T, ifm_h, ifm_w, ifms, k, fourier_domain_length, stride_w>(this->input, image_chf);
        triNNity::spectral::cpu::transform::image_chfp_to_fhcp<T, ifms, ifm_h, fourier_domain_length, stride_w, triNNity::FFT_C2C>(image_chf, ifm_fhcp);

        triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_real<KD, k, ofms, ifms, fourier_domain_length, stride_w>(this->kernel, kernel_oihf);
        triNNity::spectral::cpu::transform::kernel_oihfp_to_fohip<KD, ofms, ifms, k, fourier_domain_length, stride_w, triNNity::FFT_C2C>(kernel_oihf, kernel_fhoip);

        triNNity::spectral::convolution_forward_fourier<T, KD, ifm_w, ifm_h, ifms, ofms, k, fourier_domain_length,
                                                      fft_type, bound_type, gemm_impl,
                                                      triNNity::layout::FHCP, triNNity::layout::FOHI, triNNity::layout::FHCP,
                                                      stride_w, stride_h>
                                                      (ifm_fhcp, kernel_fhoip, output_fhoy);

        triNNity::spectral::cpu::transform::out_fhoyp_to_ohfp<T, fourier_domain_length, ifm_h, k, ofms, stride_w, stride_h, triNNity::FFT_C2C>(output_fhoy, output_ohf);
        triNNity::spectral::cpu::transform::out_mhf_to_mhw_real<T, ifm_h, ifm_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_ohf, this->output);

        delete [] image_chf;
        delete [] ifm_fhcp;
        delete [] kernel_oihf;
        delete [] kernel_fhoip;
        delete [] output_ohf;
        delete [] output_fhoy;
      } break;

      default: {
        TRINNITY_ERROR("Not implemented");
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    static constexpr unsigned in_w = triNNity::uceil<ifm_w, stride_w>();
    static constexpr unsigned in_h = triNNity::uceil<ifm_h, stride_h>();
    static constexpr unsigned out_h = triNNity::uceil<ofm_h, stride_h>();

    static constexpr unsigned fdl = triNNity::spectral::fourierDomainLength<in_w, k/2>();

    // From http://www.fftw.org/speed/ : MFLOP = 5 * N * log2(N)
    // so FLOPS = 5 * N * log2(N) * 1e6
    double fwd_fft_img_flops = static_cast<double>(ifms * in_h * 5 * fdl * log2(fdl)) * 1e6;
    double fwd_fft_krn_flops = static_cast<double>(ofms * ifms * k * 5 * fdl * log2(fdl)) * 1e6;
    double bwd_fft_img_flops = static_cast<double>(ofms * out_h * 5 * fdl * log2(fdl)) * 1e6;
    double mul_flops         = static_cast<double>(2 * k * in_h * in_w * ofms * ifms * fdl);

    return fwd_fft_img_flops + fwd_fft_krn_flops + bwd_fft_img_flops + mul_flops;
  }

  virtual double working_space() {
    double cpuSpace = 0;

    double space_image_chw_to_chf_real = 0;
    double space_kernel_oihw_to_oihf_real = 0;
    double space_out_mhf_to_mhw_real = 0;

    double space_image_chw_to_chf_complex = 0;
    double space_kernel_oihw_to_oihf_complex = 0;
    double space_out_mhf_to_mhw_complex = 0;

    constexpr unsigned out_h = (ifm_h/stride_h) + ((ifm_h%stride_h) > 0);
    constexpr unsigned fourier_domain_length =
        (fft_type==CONV_MULTI_FFT_STITCHED_COMPLEX || fft_type==CONV_MULTI_FFT_STITCHED_REAL)
        ? triNNity::spectral::fourierDomainLength<ifm_w, k/2>()
        : triNNity::spectral::fourierDomainLength<ifm_w, k>();
    //image_chw_to_chf_real
    {
      constexpr unsigned out_w = triNNity::uceil<ifm_w, stride_w>() + (k/2);
      space_image_chw_to_chf_real += sizeof(T) * (ifms*stride_w*ifm_h*out_w);
      #if defined(TRINNITY_USE_FFTW)
        space_image_chw_to_chf_real += sizeof(float) * (fourier_domain_length);
        space_image_chw_to_chf_real += sizeof(fftwf_complex) * (fourier_domain_length/2 + 1);
      #endif
    }
    //kernel_oihw_to_oihf_real
    {
      space_kernel_oihw_to_oihf_real += sizeof(T) * (ofms*ifms*stride_w*k*k);
      #if defined(TRINNITY_USE_FFTW)
        space_kernel_oihw_to_oihf_real += sizeof(float) * (fourier_domain_length);
        space_kernel_oihw_to_oihf_real += sizeof(fftwf_complex) * (fourier_domain_length/2 + 1);
      #endif
    }
    //out_mhf_to_mhw_real
    {
      #if defined(TRINNITY_USE_FFTW)
        space_out_mhf_to_mhw_real += sizeof(float) * (fourier_domain_length);
        space_out_mhf_to_mhw_real += sizeof(fftwf_complex) * (fourier_domain_length/2 + 1);
      #endif
    }
    //image_chw_to_chf_complex
    {
      constexpr unsigned out_w = triNNity::uceil<ifm_w, stride_w>() + (k/2);
      space_image_chw_to_chf_complex += sizeof(T) * (ifms*stride_w*ifm_h*out_w);
      #if defined(TRINNITY_USE_FFTW)
        space_image_chw_to_chf_complex += sizeof(fftwf_complex) * (2*fourier_domain_length);
      #endif
    }
    //kernel_oihw_to_oihf_complex
    {
      space_kernel_oihw_to_oihf_complex += sizeof(T) * (ofms*ifms*stride_w*k*k);
      #if defined(TRINNITY_USE_FFTW)
        space_kernel_oihw_to_oihf_complex += sizeof(fftwf_complex) * (2*fourier_domain_length);
      #endif
    }
    //out_mhf_to_mhw_complex
    {
      #if defined(TRINNITY_USE_FFTW)
        space_out_mhf_to_mhw_complex += sizeof(fftwf_complex) * (2*fourier_domain_length);
      #endif

    }

    switch (fft_type) {
      case CONV_MULTI_FFT_STITCHED_REAL: {
        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k/2>();
        cpuSpace += sizeof(std::complex<T>) * (ifms*stride_w*ifm_h*(fourier_domain_length/2 + 1));
        cpuSpace += sizeof(std::complex<KD>) * (ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1));
        cpuSpace += sizeof(std::complex<T>) * (ofms*ofm_h*(fourier_domain_length/2 + 1));
        cpuSpace += std::max({space_image_chw_to_chf_real, space_kernel_oihw_to_oihf_real, space_out_mhf_to_mhw_real});
      } break;
      case CONV_MULTI_FFT_STITCHED_COMPLEX: {
        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k/2>();
        cpuSpace += sizeof(std::complex<T>) * (ifms*stride_w*ifm_h*fourier_domain_length);
        cpuSpace += sizeof(std::complex<KD>) * (ofms*ifms*stride_w*k*fourier_domain_length);
        cpuSpace += sizeof(std::complex<T>) * (ofms*ofm_h*fourier_domain_length);
        cpuSpace += std::max({space_image_chw_to_chf_complex, space_kernel_oihw_to_oihf_complex, space_out_mhf_to_mhw_complex});
      } break;
      case CONV_MULTI_FFT_FULL_GEMM_REAL: {
        static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<ifm_w, k>();
        cpuSpace += sizeof(std::complex<T> ) * ( ifms*stride_w*ifm_h*(fourier_domain_length/2 + 1) );
        cpuSpace += sizeof(std::complex<T> ) * ( (fourier_domain_length/2 + 1)*ifm_h*ifms*stride_w );
        cpuSpace += sizeof(std::complex<KD>) * ( ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1) );
        cpuSpace += sizeof(std::complex<KD>) * ( (fourier_domain_length/2 + 1)*k*ofms*ifms*stride_w );
        cpuSpace += sizeof(std::complex<T> ) * ( ofms*ofm_h*(fourier_domain_length/2 + 1) );
        cpuSpace += sizeof(std::complex<T> ) * ( k*ofms*ifm_h*(fourier_domain_length/2 + 1) );
        double gemmSpace = triNNity::dense::cpu::gemm_working_space<T, KD, T, gemm_impl, GEMM_A_BT>(ifms, out_h, ifms, k*ofms);
        cpuSpace += std::max({gemmSpace, space_image_chw_to_chf_real, space_kernel_oihw_to_oihf_real, space_out_mhf_to_mhw_real});
      } break;
      case CONV_MULTI_FFT_FULL_GEMM_COMPLEX: {
        cpuSpace += sizeof(std::complex<T> ) * ( ifms*stride_w*ifm_h*fourier_domain_length );
        cpuSpace += sizeof(std::complex<T> ) * ( fourier_domain_length*ifm_h*ifms*stride_w );
        cpuSpace += sizeof(std::complex<KD>) * ( ofms*ifms*stride_w*k*fourier_domain_length );
        cpuSpace += sizeof(std::complex<KD>) * ( fourier_domain_length*k*ofms*ifms*stride_w );
        cpuSpace += sizeof(std::complex<T> ) * ( ofms*ofm_h*fourier_domain_length );
        cpuSpace += sizeof(std::complex<T> ) * ( k*ofms*ifm_h*fourier_domain_length );
        double gemmSpace = triNNity::dense::cpu::gemm_working_space
            <std::complex<T>, std::complex<KD>, std::complex<T>, gemm_impl, GEMM_A_BT>
            (ifms*stride_w, out_h, ifms*stride_w, k*ofms);
        cpuSpace += std::max({gemmSpace, space_image_chw_to_chf_real, space_kernel_oihw_to_oihf_real, space_out_mhf_to_mhw_real});
      } break;
    }

    return cpuSpace;
  }

  FFTConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->kernel = weights;
    this->output = new T[ofms*ofm_h*ofm_w]();
    this->output_inplace = false;
  }

  FFTConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->kernel = weights;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
  }

  ~FFTConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs convolution using Winograd's algorithm.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam wino_impl
 * \parblock
 * Which variant of the Winograd algorithm to use
 * \endparblock
 * \tparam gemm_impl
 * \parblock
 * Which GEMM implementation the layer should use
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 *
 */
template<typename T, typename KD,
         triNNity::conv_winograd_impl_t wino_impl,
         triNNity::gemm_variant_t gemm_impl,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = BOUND_UNDEF,
         unsigned vf = 1,
         bool transform_kernel = true>
struct WinogradConvolutionalLayer: public Layer<T, T> {

  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (vf) {

      case 1: {
        triNNity::spectral::convolution_forward_winograd<T, KD, ifm_w, ifm_h, ifms, ofms, k,
                                                       wino_impl, bound_type, gemm_impl,
                                                       image_layout, kernel_layout, output_layout,
                                                       stride_w, stride_h,
                                                       1, T, 1, KD,
                                                       transform_kernel>
                                                       (this->input, this->kernel, this->output);
      } break;

      default: {
        #if defined(TRINNITY_USE_GCC_VECTOR_EXTS)
        typedef T TV __attribute__ ((vector_size (vf*sizeof(T)), aligned (1)));
        typedef KD KDV __attribute__ ((vector_size (vf*sizeof(KD)), aligned (1)));
        triNNity::spectral::convolution_forward_winograd<T, KD, ifm_w, ifm_h, ifms, ofms, k,
                                                       wino_impl, bound_type, gemm_impl,
                                                       image_layout, kernel_layout, output_layout,
                                                       stride_w, stride_h,
                                                       vf, TV, vf, KDV,
                                                       transform_kernel>
                                                       ((TV*)(this->input), (KDV*)(this->kernel), (TV*)(this->output));
        #else
        triNNity::spectral::convolution_forward_winograd<T, KD, ifm_w, ifm_h, ifms, ofms, k,
                                                       wino_impl, bound_type, gemm_impl,
                                                       image_layout, kernel_layout, output_layout,
                                                       stride_w, stride_h,
                                                       1, T, 1, KD,
                                                       transform_kernel>
                                                       ((T*)(this->input), (KD*)(this->kernel), (T*)(this->output));
        #endif

      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() {

    double cpuSpace = 0;
    double TS = sizeof(T);
    double KDS = sizeof(KD);
    switch (wino_impl) {
      case CONV_MULTI_WINOGRAD_2: {
        constexpr signed tiledW = ifm_w/2 + (ifm_w%2);
        constexpr signed tile_size = (2+k-1);
        if (transform_kernel) { cpuSpace += KDS * (3 * tile_size * ofms * ifms); }
        cpuSpace += TS * (tile_size * (ifm_h+2) * tiledW * ifms);
        cpuSpace += TS * (tile_size * (ifm_h+2) * tiledW * ofms);
      } break;
      case CONV_MULTI_WINOGRAD_3: {
        constexpr signed tiledW = ifm_w/3 + (ifm_w%3>0);
        constexpr signed tile_size = (3+k-1);
        if (transform_kernel) { cpuSpace += KDS * (3 * tile_size * ofms * ifms); }
        cpuSpace += TS * (tile_size * (ifm_h+2) * tiledW * ifms);
        cpuSpace += TS * (tile_size * (ifm_h+2) * tiledW * ofms);
      } break;
      case CONV_MULTI_WINOGRAD_2x2: {
        constexpr unsigned tiledW = ifm_w/2 + (ifm_w%2 > 0);
        constexpr unsigned tiledH = ifm_h/2 + (ifm_h%2 > 0);
        constexpr signed tile_size = (2+k-1);
        if (transform_kernel) { cpuSpace += KDS * (tile_size*tile_size*ofms*ifms); }
        cpuSpace += TS * (tiledH*tiledW*tile_size*tile_size*ifms);
        cpuSpace += TS * (tile_size*tile_size*tiledH*tiledW*ofms);
      } break;
      case CONV_MULTI_WINOGRAD_3x3: {
        constexpr unsigned tiledW = ifm_w/3 + (ifm_w%3 > 0);
        constexpr unsigned tiledH = ifm_h/3 + (ifm_h%3 > 0);
        constexpr signed tile_size = (3+k-1);
        if (transform_kernel) { cpuSpace += KDS * (tile_size*tile_size*ofms*ifms); }
        cpuSpace += TS * (tiledH*tiledW*tile_size*tile_size*ifms);
        cpuSpace += TS * (tile_size*tile_size*tiledH*tiledW*ofms);
      } break;
      case CONV_MULTI_WINOGRAD_4x4: {
        constexpr unsigned tiledW = ifm_w/4 + (ifm_w%4 > 0);
        constexpr unsigned tiledH = ifm_h/4 + (ifm_h%4 > 0);
        constexpr signed tile_size = (4+k-1);
        if (transform_kernel) { cpuSpace += KDS * (tile_size*tile_size*ofms*ifms); }
        cpuSpace += TS * (tiledH*tiledW*tile_size*tile_size*ifms);
        cpuSpace += TS * (tile_size*tile_size*tiledH*tiledW*ofms);
      } break;
    }
    return cpuSpace;
  }

  WinogradConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->kernel = weights;
    this->output = new T[ofm_w*ofm_h*ofms]();
    this->output_inplace = false;
  }

  WinogradConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->kernel = weights;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
  }

  ~WinogradConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

}

}

#endif // TRINNITY_SPECTRAL_CPU_LAYER_H
