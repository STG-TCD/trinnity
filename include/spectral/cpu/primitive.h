/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_PRIMITIVE_H
#define TRINNITY_SPECTRAL_CPU_PRIMITIVE_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/view.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace spectral {

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourier_domain_length0() {
  return (ifm_w + k/2) - 1;
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourier_domain_length1() {
  return fourier_domain_length0<ifm_w, k>() | (fourier_domain_length0<ifm_w, k>() >> 1);
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourier_domain_length2() {
  return fourier_domain_length1<ifm_w, k>() | (fourier_domain_length1<ifm_w, k>() >> 2);
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourier_domain_length3() {
  return fourier_domain_length2<ifm_w, k>() | (fourier_domain_length2<ifm_w, k>() >> 4);
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourier_domain_length4() {
  return fourier_domain_length3<ifm_w, k>() | (fourier_domain_length3<ifm_w, k>() >> 8);
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourierDomainLengthPowerOf2() {
  return (fourier_domain_length4<ifm_w, k>() | (fourier_domain_length4<ifm_w, k>() >> 16)) + 1;
}

template<unsigned ifm_w, unsigned k>
static constexpr unsigned fourierDomainLength() {
  #if defined(TRINNITY_USE_FFTW)
  return 2 * ((ifm_w + k/2) / 2 + 1);
  #else
  return fourierDomainLengthPowerOf2<ifm_w, k>();
  #endif
}

namespace cpu {

namespace primitive {

static constexpr signed TRINNITY_FFT_A = 1;
static constexpr signed TRINNITY_FFT_B = 1;

// Code to implement decently performing FFT for complex and real valued
// signals. See www.lomont.org for a derivation of the relevant algorithms
// from first principles. Copyright Chris Lomont 2010-2012.
// This code and any ports are free for all to use for any reason as long
// as this header is left in place.
// Version 1.1, Sept 2011

template<typename T, unsigned n>
static TRINNITY_INLINE void Reverse(T *data) {
  signed j = 0, k = 0; // Knuth R1: initialize
  signed top = n / 2; // this is Knuth's 2^(n-1)
  while (true) {
    // Knuth R2: swap - swap j+1 and k+2^(n - 1), 2 entries each
    T t = data[j + 2];
    data[j + 2] = data[k + n];
    data[k + n] = t;
    t = data[j + 3];
    data[j + 3] = data[k + n + 1];
    data[k + n + 1] = t;
    if (j > k) {
      // j and k
      t = data[j];
      data[j] = data[k];
      data[k] = t;
      t = data[j + 1];
      data[j + 1] = data[k + 1];
      data[k + 1] = t;
      // j + top + 1 and k+top + 1
      t = data[j + n + 2];
      data[j + n + 2] = data[k + n + 2];
      data[k + n + 2] = t;
      t = data[j + n + 3];
      data[j + n + 3] = data[k + n + 3];
      data[k + n + 3] = t;
    }
    // Knuth R3: advance k
    k += 4;
    if (k >= (signed)n)
      break;
    // Knuth R4: advance j
    signed h = top;
    while (j >= h) {
      j -= h;
      h /= 2;
    }
    j += h;
  } // bit reverse loop
}

template<typename T, unsigned n, bool forward>
static TRINNITY_INLINE void Scale(T *data) {
  // forward scaling if needed
  if constexpr ((forward) && (TRINNITY_FFT_A != 1)) {
    T scale = std::pow(n, (TRINNITY_FFT_A - 1) / 2.0);
    for (unsigned i = 0; i < 2*n; ++i)
      data[i] *= scale;
  }

  // inverse scaling if needed
  if constexpr ((!forward) && (TRINNITY_FFT_A != -1)) {
    T scale = std::pow(n, -(TRINNITY_FFT_A + 1) / 2.0);
    for (unsigned i = 0; i < 2*n; ++i)
      data[i] *= scale;
  }
}

template<typename T, unsigned length, bool forward>
static TRINNITY_INLINE void FFT(T *data) {
  constexpr unsigned n = length / 2;
  // checks n is a power of 2 in 2's complement format
  if constexpr ((length & (length - 1)) != 0) {
    TRINNITY_ERROR("data length in FFT is not a power of 2");
  }

  Reverse<T, n>(data); // bit index data reversal

  // do transform: so single point transforms, then doubles, etc.
  signed sign = forward ? TRINNITY_FFT_B : -TRINNITY_FFT_B;
  unsigned mmax = 1;
  while (n > mmax) {
    unsigned istep = 2 * mmax;
    T theta = sign * M_PI / mmax;
    T wr = 1, wi = 0;
    T wpr = std::cos(theta);
    T wpi = std::sin(theta);
    for (unsigned m = 0; m < istep; m += 2) {
      for (unsigned k = m; k < 2 * n; k += 2 * istep) {
        unsigned j = k + istep;
        T tempr = wr * data[j] - wi * data[j + 1];
        T tempi = wi * data[j] + wr * data[j + 1];
        data[j] = data[k] - tempr;
        data[j + 1] = data[k + 1] - tempi;
        data[k] = data[k] + tempr;
        data[k + 1] = data[k + 1] + tempi;
      }
      T t = wr; // trig recurrence
      wr = wr * wpr - wi * wpi;
      wi = wi * wpr + t * wpi;
    }
    mmax = istep;
  }

  // perform data scaling as needed
  Scale<T, n, forward>(data);
}

template<typename T, unsigned length, bool forward>
static TRINNITY_INLINE void RealFFT(T *data) {
  unsigned n = length; // # of real inputs, 1/2 the complex length
  // checks n is a power of 2 in 2's complement format
  if constexpr ((length & (length - 1)) != 0) {
    TRINNITY_ERROR("data length in RealFFT is not a power of 2");
  }

  auto sign = -1.0; // assume inverse FFT, this controls how algebra below works
  if constexpr (forward) {
    FFT<T, length, true>(data);
    sign = 1.0;
    // scaling - divide by scaling for N/2, then mult by scaling for N
    if constexpr (TRINNITY_FFT_A != 1) {
      auto scale = std::pow(2.0, (TRINNITY_FFT_A - 1) / 2.0);
      for (unsigned i = 0; i < n; ++i)
        data[i] *= scale;
    }
  }

  auto theta = TRINNITY_FFT_B * sign * 2 * M_PI / n;
  auto wpr = std::cos(theta);
  auto wpi = std::sin(theta);
  auto wjr = wpr;
  auto wji = wpi;

  for (unsigned j = 1; j <= n/4; ++j) {
    auto k = n / 2 - j;
    auto tkr = data[2 * k];    // real and imaginary parts of t_k  = t_(n/2 - j)
    auto tki = data[2 * k + 1];
    auto tjr = data[2 * j];    // real and imaginary parts of t_j
    auto tji = data[2 * j + 1];

    auto a = (tjr - tkr) * wji;
    auto b = (tji + tki) * wjr;
    auto c = (tjr - tkr) * wjr;
    auto d = (tji + tki) * wji;
    auto e = (tjr + tkr);
    auto f = (tji - tki);

    // compute entry y[j]
    data[2 * j] = 0.5 * (e + sign * (a + b));
    data[2 * j + 1] = 0.5 * (f + sign * (d - c));

    // compute entry y[k]
    data[2 * k] = 0.5 * (e - sign * (b + a));
    data[2 * k + 1] = 0.5 * (sign * (d - c) - f);

    auto temp = wjr;
    // todo - allow more accurate version here? make option?
    wjr = wjr * wpr - wji * wpi;
    wji = temp * wpi + wji * wpr;
  }

  if constexpr (forward) {
    // compute final y0 and y_{N/2}, store in data[0], data[1]
    auto temp = data[0];
    data[0] += data[1];
    data[1] = temp - data[1];
  }
  else {
    auto temp = data[0]; // unpack the y0 and y_{N/2}, then invert FFT
    data[0] = 0.5 * (temp + data[1]);
    data[1] = 0.5 * (temp - data[1]);
    FFT<T, length, false>(data);
    // scaling - divide by scaling for N, then mult by scaling for N/2
    if constexpr (TRINNITY_FFT_A != -1) // todo - off by factor of 2? this works, but something seems weird
    {
      auto scale = std::pow(2.0, -(TRINNITY_FFT_A + 1) / 2.0)*2;
      for (unsigned i = 0; i < n; ++i)
        data[i] *= scale;
    }
  }
}

// End of Lomont's code

template <typename T, unsigned length>
static TRINNITY_INLINE void forwardRealFFT(T * __restrict__ data) {
  RealFFT<T, length, true>(data);
}

template <typename T, unsigned length>
static TRINNITY_INLINE void inverseRealFFT(T * __restrict__ data) {
  RealFFT<T, length, false>(data);
}

template <typename T, unsigned length>
static TRINNITY_INLINE void forwardComplexFFT(T * __restrict__ data) {
  FFT<T, 2*length, true>(data);
}

template <typename T, unsigned length>
static TRINNITY_INLINE void inverseComplexFFT(T * __restrict__ data) {
  FFT<T, 2*length, false>(data);
}

}

}

}

}

#endif // TRINNITY_SPECTRAL_CPU_PRIMITIVE_H
