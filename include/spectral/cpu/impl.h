/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_IMPL_H
#define TRINNITY_SPECTRAL_CPU_IMPL_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/spectral/cpu/primitive.h>
#include <triNNity/spectral/cpu/shape.h>
#include <triNNity/spectral/cpu/shape.h>

#include <triNNity/dense/cpu/primitive.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/gemm.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace spectral {

namespace cpu {

namespace impl {

template <typename T, typename KD, unsigned width, unsigned height, unsigned channels, unsigned kernels, unsigned k, unsigned fourier_domain_length, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void spectral_fft_stitched(const std::complex<T> * __restrict__ four_img, const std::complex<KD> * __restrict__ four_kernel, std::complex<T> * __restrict__ four_out) {

  static constexpr unsigned out_h = triNNity::uceil<height, stride_h>();

  auto v_four_img    = triNNity::internal::memory::View4D<const std::complex<T>,  channels, stride_w, height,       fourier_domain_length>(four_img);
  auto v_four_kernel = triNNity::internal::memory::View5D<const std::complex<KD>, kernels,  channels, stride_w, k,  fourier_domain_length>(four_kernel);
  auto v_four_out    = triNNity::internal::memory::View3D<      std::complex<T>,  kernels,  out_h,                  fourier_domain_length>(four_out);

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned h = 0; h < height; h+=stride_h) {
      for (unsigned f = 0; f < fourier_domain_length; f++) {
        v_four_out[m][h/stride_h][f] = 0.0;
      }
      for (unsigned c = 0; c < channels; c++) {
        for (unsigned s = 0; s < stride_w; s++) {
          for (unsigned y = 0; y < k; y++) {
            signed ypos = h + y - k/2;
            if (ypos >= 0 && ypos < (signed)height) {
              // Complex number multiplication
              for (unsigned f = 0; f < fourier_domain_length; f++) {
                v_four_out[m][h/stride_h][f] += v_four_img[c][s][ypos][f] * v_four_kernel[m][c][s][y][f];
              }
            }
          }
        }
      }
    }
  }
}


template <typename T, typename KD, unsigned gemm_var, unsigned height, unsigned channels, unsigned kernels, unsigned k, unsigned fourier_domain_length, boundary_t bound>
static TRINNITY_INLINE void spectral_fft_simple_gemm(const T  * __restrict__ img_pfhc, const KD * __restrict__ kernel_pfhoi_real, T  * __restrict__ out_pfhym) {

  auto v_img_pfhc     = triNNity::internal::memory::View4D<const T,  2, fourier_domain_length, height, channels>(img_pfhc);
  auto v_kernel_pfhoi = triNNity::internal::memory::View4D<const KD, 2, fourier_domain_length, k*kernels, channels>(kernel_pfhoi_real);
  auto v_out_pfhym    = triNNity::internal::memory::View4D<T,        2, fourier_domain_length, height, k*kernels>(out_pfhym);

  for (unsigned f = 0; f < fourier_domain_length; f++) {

    // Real part of the output
    triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
      channels, height,          channels, k*kernels,
      v_img_pfhc[0][f].begin(),  v_kernel_pfhoi[0][f].begin(),  v_out_pfhym[0][f].begin());
    triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_SUBTRACT_ACCUMULATE, GEMM_A_BT>(
      channels, height,          channels, k*kernels,
      v_img_pfhc[1][f].begin(),  v_kernel_pfhoi[1][f].begin(),  v_out_pfhym[0][f].begin());

    // Imaginary part of the output
    triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
      channels, height,          channels, k*kernels,
      v_img_pfhc[0][f].begin(),  v_kernel_pfhoi[1][f].begin(),  v_out_pfhym[1][f].begin());
    triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_ACCUMULATE, GEMM_A_BT>(
      channels, height,          channels, k*kernels,
      v_img_pfhc[1][f].begin(),  v_kernel_pfhoi[0][f].begin(),  v_out_pfhym[1][f].begin());
  }
}

template <typename T, typename KD, gemm_variant_t gemm_var, unsigned height, unsigned channels, unsigned kernels, unsigned k, unsigned fourier_domain_length, boundary_t bound, unsigned stride_w>
static TRINNITY_INLINE void spectral_fft_full_gemm(const std::complex<T>  * __restrict__ img_fhcp, const std::complex<KD> * __restrict__ kernel_fhoip, std::complex<T>  * __restrict__ out_fhymp) {

  auto v_img_fhcp     = triNNity::internal::memory::View3D<const std::complex<T>,  fourier_domain_length, height, channels*stride_w>(img_fhcp);
  auto v_kernel_fhoip = triNNity::internal::memory::View3D<const std::complex<KD>, fourier_domain_length, k*kernels, channels*stride_w>(kernel_fhoip);
  auto v_out_fhymp    = triNNity::internal::memory::View3D<std::complex<T>,        fourier_domain_length, height, k*kernels>(out_fhymp);

  for (unsigned f = 0; f < fourier_domain_length; f++) {
    triNNity::dense::cpu::gemm<std::complex<T>, std::complex<KD>, std::complex<T>, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
      channels*stride_w, height,    channels*stride_w, k*kernels,
      v_img_fhcp[f].begin(),        v_kernel_fhoip[f].begin(),        v_out_fhymp[f].begin());
  }
}


/**
 * Winograd f(2, 3).
 * Based off the paper: https://arxiv.org/abs/1509.09308. Also uses the matrices the paper provided.
 * \tparam T
 * \parblock
 * The primitive type that the type TV is a vector version of. T must be a type where casting a TV* to a T* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that
 * is aligned correctly for both). T and TV may be the same type aswell.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type that the type KDV is a vector version of. KDV must be a type where casting a KDV* to a KD* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that is
 * aligned correctly for both). KD and KDV may be the same type aswell.
 * \endparblock
 * \tparam img_w
 * \parblock
 * The logical width of the image (i.e. the width of the image ignoring any explicit padding). The physical width of the image
 * is calculated using img_w, k and the bound parameter passed.
 * \endparblock
 * \tparam img_h
 * \parblock
 * The logical height of the image (i.e. the width of the image ignoring any explicit padding). The physical height of the image
 * is calculated using img_h, k and the bound parameter passed.
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Enum passed to the gemm call controling what gemm implementation is used.
 * \endparblock
 * \tparam channels
 * \parblock
 * Number of input feature maps in the image and in each seperate kernel. Must be evenly divisible by tv_lanes and kdv_lanes.
 * \endparblock
 * \tparam kernels
 * \parblock
 * Number of output feature maps and number of kernels passed in. Must be evenly divisible by tv_lanes.
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the convolution should use. The valid values are:
 * - BOUND_IMPLICIT_PAD
 * - BOUND_UNDEF
 * \endparblock
 * \tparam tv_lanes
 * \parblock
 * How many lanes of type T does a variable of type TV contain (defaults to 1).  channels and kernels must be evenly divisible by this number.
 * \endparblock
 * \tparam TV
 * \parblock
 * A vector version of T (defaults to being T). Also the type of the data in the input and output tensors.
 * \endparblock
 * \tparam kdv_lanes
 * \parblock
 * How many lanes of of type KD does a variable of type KDV contain (defaults to 1).  channels must be evenly divisible by this number.
 * \endparblock
 * \tparam KDV
 * \parblock
 * A vector version of KD (defaults to being KD). Also the type of the data in the kernel tensor.
 * \endparblock
 * \param img
 * \parblock
 * Points to an input image tensor with layout [height][width][ifms].
 * \endparblock
 * \param kernel
 * \parblock
 * Points to a kernel tensor with layout [height][width][ofms][ifms].
 * \endparblock
 * \param out
 * \parblock
 * Points to a preallocated contiguous piece of memory where the result of the convolution will be stored. The result will be stored in a
 * [height][width][ofms] format.
 * \endparblock
 */
template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, boundary_t bound,
          unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_2_3(const TV * __restrict__ img, const KDV * __restrict__ kernel, TV * __restrict__ out) {
  TRINNITY_ASSERT(channels%kdv_lanes == 0);
  TRINNITY_ASSERT(channels%tv_lanes == 0);
  TRINNITY_ASSERT(kernels%tv_lanes == 0);

  switch (bound) {

    case BOUND_UNDEF:
    case BOUND_IMPLICIT_PAD: {

      constexpr signed tiledW = img_w/2 + (img_w%2);
      // Kernel Transformation
      //
      KDV * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KDV[3 * 4 * kernels * channels/kdv_lanes];
        triNNity::spectral::cpu::transform::winograd_2_3_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, kerTransBacking);
      } else {
        kerTransBacking = const_cast<KDV*>(kernel);
      }
      auto kerTrans = triNNity::internal::memory::View4D<KDV, 3, 4, kernels, channels/kdv_lanes>(kerTransBacking);
      // Image Transformation
      //
      TV * imgTransBacking = new TV[4 * (img_h+2) * tiledW * channels/tv_lanes];
      auto imgTrans = triNNity::internal::memory::View4D<TV, 4, (img_h+2), tiledW, channels/tv_lanes>(imgTransBacking);
      triNNity::spectral::cpu::transform::winograd_2_3_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, imgTransBacking);

      // GEMM and output
      //
      auto output = triNNity::internal::memory::View3D<TV, img_h, img_w, kernels/tv_lanes>(out);
      TV * gemmOutBacking = new TV[4 * (img_h+2) * tiledW * kernels/tv_lanes];
      auto gemmOut = triNNity::internal::memory::View4D<TV, 4, img_h+2, tiledW, kernels/tv_lanes>(gemmOutBacking);

      //j = 0 loop unrolled so can use = on first and += on rest
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {
        for (unsigned i = 0; i < 4; i++) {
          triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
            channels, (img_h+2)*tiledW, channels, kernels,
            (T*)imgTrans[i][0][0], (KD*)kerTrans[0][i][0], (T*)gemmOut[i][0][0]);
        }

        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w/2; w++) {
            for (unsigned m = 0; m < kernels/tv_lanes; m++) {
              output[h][w*2][m]     = gemmOut[0][h+0][w][m] +  gemmOut[1][h+0][w][m] + gemmOut[2][h+0][w][m];
              output[h][(w*2)+1][m] = gemmOut[1][h+0][w][m] - (gemmOut[2][h+0][w][m] + gemmOut[3][h+0][w][m]);
            }
          }
        }
        if constexpr (img_w%2) {
          for (unsigned h = 0; h < img_h; h++) {
            unsigned w = tiledW-1; {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                output[h][w*2][m]     = gemmOut[0][h+0][w][m] +  gemmOut[1][h+0][w][m] + gemmOut[2][h+0][w][m];
              }
            }
          }
        }

        for (unsigned j = 1; j < 3; j++) {
          for (unsigned i = 0; i < 4; i++) {
            triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
              channels, (img_h+2)*tiledW, channels, kernels,
              (T*)imgTrans[i][0][0], (KD*)kerTrans[j][i][0], (T*)gemmOut[i][0][0]);
          }

          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = 0; w < img_w/2; w++) {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                output[h][w*2][m]     += gemmOut[0][h+j][w][m] +  gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m];
                output[h][(w*2)+1][m] += gemmOut[1][h+j][w][m] - (gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m]);
              }
            }
          }
          if constexpr (img_w%2) {
            for (unsigned h = 0; h < img_h; h++) {
              unsigned w = tiledW-1; {
                for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                  output[h][w*2][m]     += gemmOut[0][h+j][w][m] +  gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m];
                }
              }
            }
          }
        }

      }
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }

    } break;

    default: {
      TRINNITY_ERROR("not implemented");
    }

  }

}

/**
 * Winograd f(2x2, 3x3).
 * Based off the paper: https://arxiv.org/abs/1509.09308. Also uses the matrices the paper provided.
 * \tparam T
 * \parblock
 * The primitive type that the type TV is a vector version of. T must be a type where casting a TV* to a T* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that
 * is aligned correctly for both). T and TV may be the same type aswell.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type that the type KDV is a vector version of. KDV must be a type where casting a KDV* to a KD* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that is
 * aligned correctly for both). KD and KDV may be the same type aswell.
 * \endparblock
 * \tparam img_w
 * \parblock
 * The logical width of the image (i.e. the width of the image ignoring any explicit padding). The physical width of the image
 * is calculated using img_w, k and the bound parameter passed.
 * \endparblock
 * \tparam img_h
 * \parblock
 * The logical height of the image (i.e. the width of the image ignoring any explicit padding). The physical height of the image
 * is calculated using img_h, k and the bound parameter passed.
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Enum passed to the gemm call controling what gemm implementation is used.
 * \endparblock
 * \tparam channels
 * \parblock
 * Number of input feature maps in the image and in each seperate kernel. Must be evenly divisible by tv_lanes and kdv_lanes.
 * \endparblock
 * \tparam kernels
 * \parblock
 * Number of output feature maps and number of kernels passed in. Must be evenly divisible by tv_lanes.
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the convolution should use. The valid values are:
 * - BOUND_IMPLICIT_PAD
 * - BOUND_UNDEF
 * \endparblock
 * \tparam tv_lanes
 * \parblock
 * How many lanes of type T does a variable of type TV contain (defaults to 1).  channels and kernels must be evenly divisible by this number.
 * \endparblock
 * \tparam TV
 * \parblock
 * A vector version of T (defaults to being T). Also the type of the data in the input and output tensors.
 * \endparblock
 * \tparam kdv_lanes
 * \parblock
 * How many lanes of of type KD does a variable of type KDV contain (defaults to 1).  channels must be evenly divisible by this number.
 * \endparblock
 * \tparam KDV
 * \parblock
 * A vector version of KD (defaults to being KD). Also the type of the data in the kernel tensor.
 * \endparblock
 * \param img
 * \parblock
 * Points to an input image tensor with layout [height][width][ifms].
 * \endparblock
 * \param kernel
 * \parblock
 * Points to a kernel tensor with layout [height][width][ofms][ifms].
 * \endparblock
 * \param out
 * \parblock
 * Points to a preallocated contiguous piece of memory where the result of the convolution will be stored. The result will be stored in a
 * [height][width][ofms] format.
 * \endparblock
 */
template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_2x2_3x3(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[4*4*kernels*channels];
        transform::winograd_2x2_3x3_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*4*4*channels];
      transform::winograd_2x2_3x3_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[4*4*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 4*4, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 4*4, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 4*4, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 4*4; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_2x2_3x3_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}
/**
 * Winograd f(3, 3).
 * Based off the paper: https://arxiv.org/abs/1509.09308.
 * \tparam T
 * \parblock
 * The primitive type that the type TV is a vector version of. T must be a type where casting a TV* to a T* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that
 * is aligned correctly for both). T and TV may be the same type aswell.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type that the type KDV is a vector version of. KDV must be a type where casting a KDV* to a KD* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that is
 * aligned correctly for both). KD and KDV may be the same type aswell.
 * \endparblock
 * \tparam img_w
 * \parblock
 * The logical width of the image (i.e. the width of the image ignoring any explicit padding). The physical width of the image
 * is calculated using img_w, k and the bound parameter passed.
 * \endparblock
 * \tparam img_h
 * \parblock
 * The logical height of the image (i.e. the width of the image ignoring any explicit padding). The physical height of the image
 * is calculated using img_h, k and the bound parameter passed.
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Enum passed to the gemm call controling what gemm implementation is used.
 * \endparblock
 * \tparam channels
 * \parblock
 * Number of input feature maps in the image and in each seperate kernel. Must be evenly divisible by tv_lanes and kdv_lanes.
 * \endparblock
 * \tparam kernels
 * \parblock
 * Number of output feature maps and number of kernels passed in. Must be evenly divisible by tv_lanes.
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the convolution should use. The valid values are:
 * - BOUND_IMPLICIT_PAD
 * - BOUND_UNDEF
 * \endparblock
 * \tparam tv_lanes
 * \parblock
 * How many lanes of type T does a variable of type TV contain (defaults to 1).  channels and kernels must be evenly divisible by this number.
 * \endparblock
 * \tparam TV
 * \parblock
 * A vector version of T (defaults to being T). Also the type of the data in the input and output tensors.
 * \endparblock
 * \tparam kdv_lanes
 * \parblock
 * How many lanes of of type KD does a variable of type KDV contain (defaults to 1).  channels must be evenly divisible by this number.
 * \endparblock
 * \tparam KDV
 * \parblock
 * A vector version of KD (defaults to being KD). Also the type of the data in the kernel tensor.
 * \endparblock
 * \param img
 * \parblock
 * Points to an input image tensor with layout [height][width][ifms].
 * \endparblock
 * \param kernel
 * \parblock
 * Points to a kernel tensor with layout [height][width][ofms][ifms].
 * \endparblock
 * \param out
 * \parblock
 * Points to a preallocated contiguous piece of memory where the result of the convolution will be stored. The result will be stored in a
 * [height][width][ofms] format.
 * \endparblock
 */
template <typename T, typename KD, unsigned img_w, unsigned img_h, gemm_variant_t gemm_var, unsigned channels, unsigned kernels, boundary_t bound,
            unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_3_3(const TV * __restrict__ img, const KDV * __restrict__ kernel, TV * __restrict__ out) {
  TRINNITY_ASSERT(channels%kdv_lanes == 0);
  TRINNITY_ASSERT(channels%tv_lanes == 0);
  TRINNITY_ASSERT(kernels%tv_lanes == 0);
  constexpr signed tiledW = img_w/3 + (img_w%3>0);

  switch (bound) {

    case BOUND_UNDEF:
    case BOUND_IMPLICIT_PAD: {

      // Kernel Transformation
      //
      KDV * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KDV[3 * 5 * kernels * channels/kdv_lanes];
        triNNity::spectral::cpu::transform::winograd_3_3_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, kerTransBacking);
      } else {
        kerTransBacking = const_cast<KDV*>(kernel);
      }
      auto kerTrans = triNNity::internal::memory::View4D<KDV, 3, 5, kernels, channels/kdv_lanes>(kerTransBacking);
      // Image Transformation
      //
      TV * imgTransBacking = new TV[5 * (img_h+2) * tiledW * channels/tv_lanes];
      auto imgTrans = triNNity::internal::memory::View4D<TV, 5, (img_h+2), tiledW, channels/tv_lanes>(imgTransBacking);
      triNNity::spectral::cpu::transform::winograd_3_3_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, imgTransBacking);
      // GEMM and output
      //
      auto output = triNNity::internal::memory::View3D<T, img_h, img_w, kernels>(out);
      T * gemmOutBacking = new TV[5 * (img_h+2) * tiledW * kernels/tv_lanes];
      auto gemmOut = triNNity::internal::memory::View4D<TV, 5, img_h+2, tiledW, kernels/tv_lanes>(gemmOutBacking);

      //j = 0 loop unrolled so can use = on first and += on rest
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {
        for (unsigned i = 0; i < 5; i++) {
          triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
            channels, (img_h+2)*tiledW, channels, kernels,
            (T*)imgTrans[i][0][0], (KD*)kerTrans[0][i][0], (T*)gemmOut[i][0][0]);
        }

        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w/3; w++) {
            for (unsigned m = 0; m < kernels/tv_lanes; m++) {
              output[h][w*3  ][m] = gemmOut[0][h][w][m] + gemmOut[1][h][w][m] +   gemmOut[2][h][w][m] + gemmOut[3][h][w][m];
              output[h][w*3+1][m] = gemmOut[2][h][w][m] +  2*gemmOut[3][h][w][m] - gemmOut[1][h][w][m];
              output[h][w*3+2][m] = gemmOut[1][h][w][m] +    gemmOut[2][h][w][m] + 4*gemmOut[3][h][w][m] + gemmOut[4][h][w][m];
            }
          }
        }
        if constexpr (img_w%3) {
          for (unsigned h = 0; h < img_h; h++) {
            constexpr unsigned w = tiledW-1; {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                output[h][w*3  ][m] = gemmOut[0][h][w][m] + gemmOut[1][h][w][m] + gemmOut[2][h][w][m] + gemmOut[3][h][w][m];
                if (w*3+1 < img_w) { output[h][w*3+1][m] = gemmOut[2][h][w][m] +  2*gemmOut[3][h][w][m] - gemmOut[1][h][w][m]; }
              }
            }
          }
        }

        for (unsigned j = 1; j < 3; j++) {
          for (unsigned i = 0; i < 5; i++) {
            triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
              channels, (img_h+2)*tiledW, channels, kernels,
              (T*)imgTrans[i][0][0], (KD*)kerTrans[j][i][0], (T*)gemmOut[i][0][0]);
          }

          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = 0; w < img_w/3; w++) {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                output[h][w*3  ][m] += gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] +   gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m];
                output[h][w*3+1][m] += gemmOut[2][h+j][w][m] +  2*gemmOut[3][h+j][w][m] - gemmOut[1][h+j][w][m];
                output[h][w*3+2][m] += gemmOut[1][h+j][w][m] +    gemmOut[2][h+j][w][m] + 4*gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m];
              }
            }
          }
          if constexpr (img_w%3) {
            for (unsigned h = 0; h < img_h; h++) {
              constexpr unsigned w = tiledW-1; {
                for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                  output[h][w*3  ][m] += gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m];
                  if (w*3+1 < img_w) { output[h][w*3+1][m] += gemmOut[2][h+j][w][m] +  2*gemmOut[3][h+j][w][m] - gemmOut[1][h+j][w][m]; }
                }
              }
            }
          }
        }

      }

      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }

    } break;

    default: {
      TRINNITY_ERROR("not implemented");
    }

  }

}

/**
 * Winograd f(3x3, 3x3).
 * Based off the paper: https://arxiv.org/abs/1509.09308.
 * \tparam T
 * \parblock
 * The primitive type that the type TV is a vector version of. T must be a type where casting a TV* to a T* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that
 * is aligned correctly for both). T and TV may be the same type aswell.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type that the type KDV is a vector version of. KDV must be a type where casting a KDV* to a KD* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that is
 * aligned correctly for both). KD and KDV may be the same type aswell.
 * \endparblock
 * \tparam img_w
 * \parblock
 * The logical width of the image (i.e. the width of the image ignoring any explicit padding). The physical width of the image
 * is calculated using img_w, k and the bound parameter passed.
 * \endparblock
 * \tparam img_h
 * \parblock
 * The logical height of the image (i.e. the width of the image ignoring any explicit padding). The physical height of the image
 * is calculated using img_h, k and the bound parameter passed.
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Enum passed to the gemm call controling what gemm implementation is used.
 * \endparblock
 * \tparam channels
 * \parblock
 * Number of input feature maps in the image and in each seperate kernel. Must be evenly divisible by tv_lanes and kdv_lanes.
 * \endparblock
 * \tparam kernels
 * \parblock
 * Number of output feature maps and number of kernels passed in. Must be evenly divisible by tv_lanes.
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the convolution should use. The valid values are:
 * - BOUND_IMPLICIT_PAD
 * - BOUND_UNDEF
 * \endparblock
 * \tparam tv_lanes
 * \parblock
 * How many lanes of type T does a variable of type TV contain (defaults to 1).  channels and kernels must be evenly divisible by this number.
 * \endparblock
 * \tparam TV
 * \parblock
 * A vector version of T (defaults to being T). Also the type of the data in the input and output tensors.
 * \endparblock
 * \tparam kdv_lanes
 * \parblock
 * How many lanes of of type KD does a variable of type KDV contain (defaults to 1).  channels must be evenly divisible by this number.
 * \endparblock
 * \tparam KDV
 * \parblock
 * A vector version of KD (defaults to being KD). Also the type of the data in the kernel tensor.
 * \endparblock
 * \param img
 * \parblock
 * Points to an input image tensor with layout [height][width][ifms].
 * \endparblock
 * \param kernel
 * \parblock
 * Points to a kernel tensor with layout [height][width][ofms][ifms].
 * \endparblock
 * \param out
 * \parblock
 * Points to a preallocated contiguous piece of memory where the result of the convolution will be stored. The result will be stored in a
 * [height][width][ofms] format.
 * \endparblock
 */
template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_3x3_3x3(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[5*5*kernels*channels];
        transform::winograd_3x3_3x3_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*5*5*channels];
      transform::winograd_3x3_3x3_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[5*5*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 5*5, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 5*5, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 5*5, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 5*5; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_3x3_3x3_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}


/**
 * Winograd f(4x4, 3x3).
 * Based off the paper: https://arxiv.org/abs/1509.09308.
 * \tparam T
 * \parblock
 * The primitive type that the type TV is a vector version of. T must be a type where casting a TV* to a T* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that
 * is aligned correctly for both). T and TV may be the same type aswell.
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type that the type KDV is a vector version of. KDV must be a type where casting a KDV* to a KD* (and vice versa)
 * is a valid operation resulting in a pointer that can be freely used (assuming the pointer points to a memory address that is
 * aligned correctly for both). KD and KDV may be the same type aswell.
 * \endparblock
 * \tparam img_w
 * \parblock
 * The logical width of the image (i.e. the width of the image ignoring any explicit padding). The physical width of the image
 * is calculated using img_w, k and the bound parameter passed.
 * \endparblock
 * \tparam img_h
 * \parblock
 * The logical height of the image (i.e. the width of the image ignoring any explicit padding). The physical height of the image
 * is calculated using img_h, k and the bound parameter passed.
 * \endparblock
 * \tparam gemm_var
 * \parblock
 * Enum passed to the gemm call controling what gemm implementation is used.
 * \endparblock
 * \tparam channels
 * \parblock
 * Number of input feature maps in the image and in each seperate kernel. Must be evenly divisible by tv_lanes and kdv_lanes.
 * \endparblock
 * \tparam kernels
 * \parblock
 * Number of output feature maps and number of kernels passed in. Must be evenly divisible by tv_lanes.
 * \endparblock
 * \tparam bound
 * \parblock
 * The type of boundary handling that the convolution should use. The valid values are:
 * - BOUND_IMPLICIT_PAD
 * - BOUND_UNDEF
 * \endparblock
 * \tparam tv_lanes
 * \parblock
 * How many lanes of type T does a variable of type TV contain (defaults to 1).  channels and kernels must be evenly divisible by this number.
 * \endparblock
 * \tparam TV
 * \parblock
 * A vector version of T (defaults to being T). Also the type of the data in the input and output tensors.
 * \endparblock
 * \tparam kdv_lanes
 * \parblock
 * How many lanes of of type KD does a variable of type KDV contain (defaults to 1).  channels must be evenly divisible by this number.
 * \endparblock
 * \tparam KDV
 * \parblock
 * A vector version of KD (defaults to being KD). Also the type of the data in the kernel tensor.
 * \endparblock
 * \param img
 * \parblock
 * Points to an input image tensor with layout [height][width][ifms].
 * \endparblock
 * \param kernel
 * \parblock
 * Points to a kernel tensor with layout [height][width][ofms][ifms].
 * \endparblock
 * \param out
 * \parblock
 * Points to a preallocated contiguous piece of memory where the result of the convolution will be stored. The result will be stored in a
 * [height][width][ofms] format.
 * \endparblock
 */
template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_4x4_3x3(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[6*6*kernels*channels];
        transform::winograd_4x4_3x3_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*6*6*channels];
      transform::winograd_4x4_3x3_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[6*6*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 6*6, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 6*6; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_4x4_3x3_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}



template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels, triNNity::boundary_t bound,
          unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_2_5(const TV * __restrict__ img, const KDV * __restrict__ kernel, TV * __restrict__ out) {
  TRINNITY_ASSERT(channels%kdv_lanes == 0);
  TRINNITY_ASSERT(channels%tv_lanes == 0);
  TRINNITY_ASSERT(kernels%tv_lanes == 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      constexpr signed tiledW = img_w/2 + (img_w%2 > 0);
      constexpr signed krad = 5/2;
      // Kernel Transformation
      //
      KDV * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KDV[5 * 6 * kernels * channels/kdv_lanes];
        triNNity::spectral::cpu::transform::winograd_2_5_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, kerTransBacking);
      } else {
        kerTransBacking = const_cast<KDV*>(kernel);
      }
      auto kerTrans = triNNity::internal::memory::View4D<KDV, 5, 6, kernels, channels/kdv_lanes>(kerTransBacking);
      // Image Transformation
      //
      TV * imgTransBacking = new TV[6 * (img_h+(krad*2)) * tiledW * channels/tv_lanes];
      auto imgTrans = triNNity::internal::memory::View4D<TV, 6, (img_h+(krad*2)), tiledW, channels/tv_lanes>(imgTransBacking);
      triNNity::spectral::cpu::transform::winograd_2_5_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, imgTransBacking);
      // GEMM and output
      //
      auto output = triNNity::internal::memory::View3D<TV, img_h, img_w, kernels/tv_lanes>(out);
      TV * gemmOutBacking = new TV[6 * (img_h+(krad*2)) * tiledW * kernels/tv_lanes];
      auto gemmOut = triNNity::internal::memory::View4D<TV, 6, img_h+(krad*2), tiledW, kernels/tv_lanes>(gemmOutBacking);
      //j = 0 loop unrolled so can use = on first and += on rest
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {
        for (unsigned i = 0; i < 6; i++) {
          triNNity::dense::cpu::gemm<T, KD, T, gemm_var, triNNity::GEMM_NO_ACCUMULATE, triNNity::GEMM_A_BT>(
            channels, (img_h+(krad*2))*tiledW, channels, kernels,
            (T*)imgTrans[i][0][0], (KD*)kerTrans[0][i][0], (T*)gemmOut[i][0][0]);
        }
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < tiledW; w++) {
            for (unsigned m = 0; m < kernels/tv_lanes; m++) {
              TV outTemp[2];
              unsigned j = 0;
              outTemp[0] =  + ( + gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m]);
              outTemp[1] =  + ( + gemmOut[1][h+j][w][m] - gemmOut[2][h+j][w][m] + gemmOut[5][h+j][w][m]) + static_cast<T>(2.0/1.0)*( + gemmOut[3][h+j][w][m] - gemmOut[4][h+j][w][m]);

              for (unsigned b = 0; b+(w*2) < img_w && b < 2; b++) {
                output[h][(w*2)+b][m] = outTemp[b];
              }
            }
          }
        }
        for (unsigned j = 1; j < 5; j++) {
          for (unsigned i = 0; i < 6; i++) {
            triNNity::dense::cpu::gemm<T, KD, T, gemm_var, triNNity::GEMM_NO_ACCUMULATE, triNNity::GEMM_A_BT>(
              channels, (img_h+(krad*2))*tiledW, channels, kernels,
              (T*)imgTrans[i][0][0], (KD*)kerTrans[j][i][0], (T*)gemmOut[i][0][0]);
          }
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = 0; w < tiledW; w++) {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                TV outTemp[2];
                outTemp[0] =  + ( + gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m]);
                outTemp[1] =  + ( + gemmOut[1][h+j][w][m] - gemmOut[2][h+j][w][m] + gemmOut[5][h+j][w][m]) + static_cast<T>(2.0/1.0)*( + gemmOut[3][h+j][w][m] - gemmOut[4][h+j][w][m]);

                for (unsigned b = 0; b+(w*2) < img_w && b < 2; b++) {
                  output[h][(w*2)+b][m] += outTemp[b];
                }
              }
            }
          }
        }
      }
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }

    } break;
    default: {
      TRINNITY_ERROR("not implemented.");
    }
  }
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_2x2_5x5(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/2 + (img_w%2 > 0);
  constexpr unsigned tiledH = img_h/2 + (img_h%2 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[6*6*kernels*channels];
        transform::winograd_2x2_5x5_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*6*6*channels];
      transform::winograd_2x2_5x5_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[6*6*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 6*6, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 6*6, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 6*6; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_2x2_5x5_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}


template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels, triNNity::boundary_t bound,
          unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_3_5(const TV * __restrict__ img, const KDV * __restrict__ kernel, TV * __restrict__ out) {
  TRINNITY_ASSERT(channels%kdv_lanes == 0);
  TRINNITY_ASSERT(channels%tv_lanes == 0);
  TRINNITY_ASSERT(kernels%tv_lanes == 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      constexpr signed tiledW = img_w/3 + (img_w%3 > 0);
      constexpr signed krad = 5/2;
      // Kernel Transformation
      //
      KDV * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KDV[5 * 7 * kernels * channels/kdv_lanes];
        triNNity::spectral::cpu::transform::winograd_3_5_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, kerTransBacking);
      } else {
        kerTransBacking = const_cast<KDV*>(kernel);
      }
      auto kerTrans = triNNity::internal::memory::View4D<KDV, 5, 7, kernels, channels/kdv_lanes>(kerTransBacking);

      // Image Transformation
      //
      TV * imgTransBacking = new TV[7 * (img_h+(krad*2)) * tiledW * channels/tv_lanes];
      auto imgTrans = triNNity::internal::memory::View4D<TV, 7, (img_h+(krad*2)), tiledW, channels/tv_lanes>(imgTransBacking);
      triNNity::spectral::cpu::transform::winograd_3_5_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, imgTransBacking);
      // GEMM and output
      //
      auto output = triNNity::internal::memory::View3D<TV, img_h, img_w, kernels/tv_lanes>(out);
      TV * gemmOutBacking = new TV[7 * (img_h+(krad*2)) * tiledW * kernels/tv_lanes];
      auto gemmOut = triNNity::internal::memory::View4D<TV, 7, img_h+(krad*2), tiledW, kernels/tv_lanes>(gemmOutBacking);
      //j = 0 loop unrolled so can use = on first and += on rest
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {

        for (unsigned i = 0; i < 7; i++) {
          triNNity::dense::cpu::gemm<T, KD, T, gemm_var, triNNity::GEMM_NO_ACCUMULATE, triNNity::GEMM_A_BT>(
            channels, (img_h+(krad*2))*tiledW, channels, kernels,
            (T*)imgTrans[i][0][0], (KD*)kerTrans[0][i][0], (T*)gemmOut[i][0][0]);
        }
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < tiledW; w++) {
            for (unsigned m = 0; m < kernels/tv_lanes; m++) {
              TV outTemp[3];
              unsigned j = 0;
              outTemp[0] =  + ( + gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m] + gemmOut[5][h+j][w][m]);
              outTemp[1] =  + ( + gemmOut[1][h+j][w][m] - gemmOut[2][h+j][w][m]) + static_cast<T>(2.0/1.0)*( + gemmOut[3][h+j][w][m] - gemmOut[4][h+j][w][m]) + static_cast<T>(3.0/1.0)*( + gemmOut[5][h+j][w][m]);
              outTemp[2] =  + ( + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[6][h+j][w][m]) + static_cast<T>(4.0/1.0)*( + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m]) + static_cast<T>(9.0/1.0)*( + gemmOut[5][h+j][w][m]);

              for (unsigned b = 0; b+(w*3) < img_w && b < 3; b++) {
                output[h][(w*3)+b][m] = outTemp[b];
              }
            }
          }
        }
        for (unsigned j = 1; j < 5; j++) {
          for (unsigned i = 0; i < 7; i++) {
            triNNity::dense::cpu::gemm<T, KD, T, gemm_var, triNNity::GEMM_NO_ACCUMULATE, triNNity::GEMM_A_BT>(
              channels, (img_h+(krad*2))*tiledW, channels, kernels,
              (T*)imgTrans[i][0][0], (KD*)kerTrans[j][i][0], (T*)gemmOut[i][0][0]);
          }
          for (unsigned h = 0; h < img_h; h++) {
            for (unsigned w = 0; w < tiledW; w++) {
              for (unsigned m = 0; m < kernels/tv_lanes; m++) {
                TV outTemp[3];
                outTemp[0] =  + ( + gemmOut[0][h+j][w][m] + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m] + gemmOut[5][h+j][w][m]);
                outTemp[1] =  + ( + gemmOut[1][h+j][w][m] - gemmOut[2][h+j][w][m]) + static_cast<T>(2.0/1.0)*( + gemmOut[3][h+j][w][m] - gemmOut[4][h+j][w][m]) + static_cast<T>(3.0/1.0)*( + gemmOut[5][h+j][w][m]);
                outTemp[2] =  + ( + gemmOut[1][h+j][w][m] + gemmOut[2][h+j][w][m] + gemmOut[6][h+j][w][m]) + static_cast<T>(4.0/1.0)*( + gemmOut[3][h+j][w][m] + gemmOut[4][h+j][w][m]) + static_cast<T>(9.0/1.0)*( + gemmOut[5][h+j][w][m]);

                for (unsigned b = 0; b+(w*3) < img_w && b < 3; b++) {
                  output[h][(w*3)+b][m] += outTemp[b];
                }
              }
            }
          }
        }
      }
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }

    } break;
    default: {
      TRINNITY_ERROR("not implemented.");
    }
  }
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_3x3_5x5(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/3 + (img_w%3 > 0);
  constexpr unsigned tiledH = img_h/3 + (img_h%3 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[7*7*kernels*channels];
        transform::winograd_3x3_5x5_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*7*7*channels];
      transform::winograd_3x3_5x5_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[7*7*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 7*7, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 7*7, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 7*7, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 7*7; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_3x3_5x5_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}


template <typename T, typename KD, unsigned img_w, unsigned img_h, triNNity::gemm_variant_t gemm_var, unsigned channels, unsigned kernels,
          triNNity::boundary_t bound, unsigned tv_lanes=1, typename TV=T, unsigned kdv_lanes=1, typename KDV=KD, bool transform_kernel=true>
static TRINNITY_INLINE void winograd_4x4_5x5(const TV * __restrict__ img,  KDV * __restrict__ kernel, TV * __restrict__ out) {
  constexpr unsigned tiledW = img_w/4 + (img_w%4 > 0);
  constexpr unsigned tiledH = img_h/4 + (img_h%4 > 0);
  switch (bound) {
    case triNNity::BOUND_UNDEF:
    case triNNity::BOUND_IMPLICIT_PAD: {
      KD * kerTransBacking = nullptr;
      if (transform_kernel) {
        kerTransBacking = new KD[8*8*kernels*channels];
        transform::winograd_4x4_5x5_kernel_to_winograd<KD, channels, kernels, kdv_lanes, KDV>(kernel, (KDV*)kerTransBacking);
      } else {
        kerTransBacking = (KD*)kernel;
      }
      T* imgTransBacking = new T[tiledH*tiledW*8*8*channels];
      transform::winograd_4x4_5x5_image_to_winograd<T, img_h, img_w, channels, tv_lanes, TV>(img, (TV*)imgTransBacking);
      T * gemmOutBacking = new T[8*8*tiledH*tiledW*kernels];
      auto gemmOutS = triNNity::internal::memory::View4D<T, 8*8, tiledH, tiledW, kernels>(gemmOutBacking);
      auto imgTransS = triNNity::internal::memory::View4D<T, 8*8, tiledH, tiledW, channels>(imgTransBacking);
      auto kerTransS = triNNity::internal::memory::View3D<KD, 8*8, kernels, channels>(kerTransBacking);
      for (signed i=0; i < 8*8; i+=1){
      triNNity::dense::cpu::gemm<T, KD, T, gemm_var, GEMM_NO_ACCUMULATE, GEMM_A_BT>(
        channels, tiledH*tiledW, channels, kernels,
        imgTransS[i][0][0], kerTransS[i][0], gemmOutS[i][0][0]);
      }
      transform::winograd_4x4_5x5_winograd_to_output<T, img_h, img_w, kernels, tv_lanes, TV>((TV*)gemmOutBacking, out);
      delete [] gemmOutBacking;
      delete [] imgTransBacking;
      if (transform_kernel) {
        delete [] kerTransBacking;
      }
      break;
    }
    default: { TRINNITY_ERROR("not implemented"); break; }
  }
}





} //namespace

}

}

}

#endif // TRINNITY_SPECTRAL_CPU_IMPL_H
