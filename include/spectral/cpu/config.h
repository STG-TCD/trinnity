/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPECTRAL_CPU_CONFIG_H
#define TRINNITY_SPECTRAL_CPU_CONFIG_H

#include <triNNity/config.h>

namespace triNNity {

/**
 * Names for different kinds of Winograd convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_WINOGRAD_2,
  CONV_MULTI_WINOGRAD_2x2,
  CONV_MULTI_WINOGRAD_3,
  CONV_MULTI_WINOGRAD_3x3,
  CONV_MULTI_WINOGRAD_4x4
} conv_winograd_impl_t;

/**
 * Names for different kinds of FFT convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_FFT_STITCHED_REAL,
  CONV_MULTI_FFT_STITCHED_COMPLEX,
  CONV_MULTI_FFT_SIMPLE_GEMM_REAL,
  CONV_MULTI_FFT_SIMPLE_GEMM_COMPLEX,
  CONV_MULTI_FFT_FULL_GEMM_REAL,
  CONV_MULTI_FFT_FULL_GEMM_COMPLEX
} conv_fourier_impl_t;

/**
 * What implementation of FFT/IFFT transformation to use
 */

typedef enum {
  HANDWRITTEN_REAL,
  HANDWRITTEN_COMPLEX,
  FFTW_REAL,
  FFTW_COMPLEX
} spectral_fft_transform_t;


typedef enum {
  FFT_R2C,  /*!< Real to Complex FFT.*/
  FFT_C2C,  /*!< Complex to Complex FFT.*/
} spectral_fft_type_t;

namespace layout {

/**
 * Describes the layout of an input or output feature map.
 * The feature map is a multi-dimensional tensor with some named dimensions:
 *
 * C is the number of channels (or feature maps),
 * H is the height of the feature maps,
 * W is the width of the feature maps.
 * F is the Frequency Domain Length
 */

typedef enum {
  CHFP,   /*!< Tensor layout [channels][height][frequency domain length][P] where the width
               dimension has been replaced by a 1D frequency domain representation using complex numbers.
               The inner dimension P=2 stores pairs of numbers representing complex numbers
               in Cartesian form. To access real and imaginary components of an individual
               complex number, use [c][h][f][0] (real) and [c][h][f][1] (imaginary).*/
  FHCP,   /*!< Tensor layout [frequency domain length][height][channels][P] where the
               inner dimension P=2 stores pairs of numbers representing complex numbers
               in Cartesian form. To access real and imaginary components of an individual
               complex number, use [f][h][c][0] (real) and [f][h][c][1] (imaginary). */
  PCHF,   /*!< Tensor layout [P][channels][height][frequency domain length] where the
               outer dimension P=2 stores pairs of tensors representing complex numbers
               in Cartesian form. To access real and imaginary components of an individual
               complex number, use [0][c][h][f] (real) and [1][c][h][f] (imaginary). */
} spectral_feature_map_layout_t;

/**
 * Describes the layout of the multi-dimensional parameter tensor for a multi-channel multi-kernel convolution.
 *
 * O is the number of output feature maps (ofms),
 * I is the number of input feature maps (ifms),
 * H is the kernel height,
 * W is the kernel width.
 * F is the Fourier domain length
 */

typedef enum {
  OIHF,   /*!< Tensor layout [ofms][ifms][height][frequency domain length] where the width
               dimension has been replaced by a complex 1D frequency domain representation. */
  FOHI,   /*!< Tensor layout [frequency domain length][ofms][height][ifms] where the layout
               has been changed to accommodate GEMM. */
} spectral_parameter_layout_t;

}

}

#endif // TRINNITY_SPECTRAL_CPU_CONFIG_H
