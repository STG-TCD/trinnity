/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_WRAPPERS_MKLDNN_LAYER_H
#define TRINNITY_WRAPPERS_MKLDNN_LAYER_H

#include <cstddef>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/layer.h>

#include <vector>
#include <numeric>

#if defined(TRINNITY_WRAP_MKLDNN)
#include <mkldnn.hpp>

namespace triNNity {

namespace layer {

/**
 * A layer which wraps the MKLDNN library direct convolution.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam direct_method
 * \parblock
 * The direct convolution method the layer should use to compute the output
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 *
 */

typedef enum {
  MKLDNN_DIRECT = 0,
  MKLDNN_WINOGRAD = 1
} mkldnn_algo_t;

template<typename T, typename KD,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = triNNity::BOUND_IMPLICIT_PAD,
         mkldnn_algo_t wrap_algo = MKLDNN_DIRECT>
struct MKLDNNConvolutionalLayer: public triNNity::layer::Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    constexpr unsigned batch = 1;

    // Run the convolution
    try {
      auto cpu_engine = mkldnn::engine(mkldnn::engine::cpu, 0);
      std::vector<mkldnn::primitive> net;
      auto stream = mkldnn::stream(mkldnn::stream::kind::lazy);

      // Set up the input image(s)
      std::vector<float> net_src(batch * ifms * ifm_h * ifm_w);
      T* input_image = this->input;
      std::copy(input_image, input_image+(ifms * ifm_h * ifm_w), net_src.begin());

      mkldnn::memory::dims conv_src_tz = {batch, ifms, ifm_h, ifm_w};
      auto conv_user_src_md = mkldnn::memory::desc({conv_src_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);
      auto conv_src_md = mkldnn::memory::desc({conv_src_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);
      auto conv_user_src_memory = mkldnn::memory({conv_user_src_md, cpu_engine}, net_src.data());

      // Set up the kernel
      std::vector<float> conv_weights(ofms * ifms * k * k);
      KD* input_weights = this->kernel;
      std::copy(input_weights, input_weights+(ofms * ifms * k * k), conv_weights.begin());

      mkldnn::memory::dims conv_weights_tz = {ofms, ifms, k, k};
      auto conv_user_weights_md = mkldnn::memory::desc({conv_weights_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::oihw);
      auto conv_weights_md = mkldnn::memory::desc({conv_weights_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::oihw);
      auto conv_user_weights_memory = mkldnn::memory({conv_user_weights_md, cpu_engine}, conv_weights.data());

      // Set up the strides and padding
      mkldnn::memory::dims conv_strides = {stride_h, stride_w};
      signed padding_size = (signed) k/2;
      auto conv_padding = {padding_size, padding_size};

      // Set up the output (we don't allocate storage for this, mkldnn does)
      mkldnn::memory::dims conv_dst_tz = {batch, ofms, (triNNity::uceil<ifm_h, stride_h>()), (triNNity::uceil<ifm_w, stride_w>())};
      auto conv_dst_md = mkldnn::memory::desc({conv_dst_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);

      mkldnn::algorithm mkldnn_conv_algo;

      switch (wrap_algo) {
        case (MKLDNN_DIRECT): {
          mkldnn_conv_algo = mkldnn::convolution_direct;
        } break;

        case (MKLDNN_WINOGRAD): {
          mkldnn_conv_algo = mkldnn::convolution_winograd;
        } break;
      }

      /* create a convolution */
      auto conv_desc = mkldnn::convolution_forward::desc(
        mkldnn::prop_kind::forward_inference,
        mkldnn_conv_algo,
        conv_src_md, conv_weights_md, conv_dst_md,
        conv_strides, conv_padding, conv_padding,
        mkldnn::padding_kind::zero);

      auto conv_prim_desc =
          mkldnn::convolution_forward::primitive_desc(conv_desc, cpu_engine);

      /* create reorders between user and data if it is needed and
       *  add it to net before convolution */
      auto conv_src_memory = conv_user_src_memory;
      if (mkldnn::memory::primitive_desc(conv_prim_desc.src_primitive_desc()) !=
          conv_user_src_memory.get_primitive_desc()) {
          conv_src_memory = mkldnn::memory(conv_prim_desc.src_primitive_desc());
          net.push_back(mkldnn::reorder(conv_user_src_memory, conv_src_memory));
      }

      auto conv_weights_memory = conv_user_weights_memory;
      if (mkldnn::memory::primitive_desc(conv_prim_desc.weights_primitive_desc()) !=
          conv_user_weights_memory.get_primitive_desc()) {
          conv_weights_memory = mkldnn::memory(conv_prim_desc.weights_primitive_desc());
          net.push_back(mkldnn::reorder(conv_user_weights_memory, conv_weights_memory));
      }

      auto conv_dst_memory = mkldnn::memory(conv_prim_desc.dst_primitive_desc());

      /* create convolution primitive and add it to net */
      net.push_back(mkldnn::convolution_forward(conv_prim_desc, conv_src_memory, conv_weights_memory, conv_dst_memory));

      stream.submit(net);
      stream.wait();

      std::copy_n((T*)conv_dst_memory.get_data_handle(), (batch * ofms * (triNNity::uceil<ifm_h, stride_h>()) * (triNNity::uceil<ifm_w, stride_w>())), this->output);
    }
    catch(mkldnn::error& e) {
      TRINNITY_ERROR("Error in MKL-DNN wrapper");
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*triNNity::uceil<ifm_h, stride_h>()*triNNity::uceil<ifm_w, stride_w>()*ifms*ofms*k*k);
    return conv_flops;
  }

  virtual double working_space() { return 0; }

  MKLDNNConvolutionalLayer(T *in, KD *weights) {
    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    kernel = weights;
  }

  MKLDNNConvolutionalLayer(T *in, KD *weights, T *out) {
    this->input = in;
    if constexpr (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    kernel = weights;
  }

  ~MKLDNNConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

}

}
#endif // TRINNITY_WRAP_MKLDNN

#endif // TRINNITY_WRAPPERS_MKLDNN_LAYER_H
