/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_WRAPPERS_ARMCL_LAYER_H
#define TRINNITY_WRAPPERS_ARMCL_LAYER_H

#include <cstddef>

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/layer.h>

#include <vector>
#include <numeric>

#if defined(TRINNITY_WRAP_ARMCL)
#define ARM_COMPUTE_CL
#include "arm_compute/core/Types.h"
#include "arm_compute/runtime/Tensor.h"

#include "arm_compute/runtime/CL/CLFunctions.h"
#include "arm_compute/runtime/CL/CLScheduler.h"

namespace triNNity {

namespace layer {

/**
 * A layer which wraps the ARMCL library convolution.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam direct_method
 * \parblock
 * The direct convolution method the layer should use to compute the output
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 *
 */

typedef enum {
  ARMCL_DIRECT = 0,
  ARMCL_PATCH_GEMM = 1,
  ARMCL_WINOGRAD = 2
} armcl_algo_t;

template<typename T, typename KD,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = triNNity::BOUND_IMPLICIT_PAD,
         armcl_algo_t wrap_algo = ARMCL_DIRECT>
struct ARMCLConvolutionalLayer: public triNNity::layer::Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  //! ARMCL objects
  arm_compute::CLTensor armcl_input;
  arm_compute::CLTensor armcl_kernel;
  arm_compute::CLTensor armcl_output;
  arm_compute::BorderMode armcl_bound;

  // We use a base class pointer here because we need to set this later
  arm_compute::IFunction *conv;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    armcl_input.map(true);
    std::copy_n(reinterpret_cast<T*>(this->input), (ifms * ifm_h * ifm_w), armcl_input.buffer());
    armcl_input.unmap();

    #if defined(TRINNITY_ARMCL_NUM_THREADS) && (TRINNITY_ARMCL_NUM_THREADS > 1)
    constexpr unsigned split_dimension = 0;
    arm_compute::CLScheduler::get().schedule(reinterpret_cast<arm_compute::ICLKernel*>(conv), split_dimension);
    #endif

    conv->run();

    armcl_output.map(true);
    std::copy_n(reinterpret_cast<T*>(armcl_output.buffer()), (ofms * ofm_h * ofm_w), this->output);
    armcl_output.unmap();

    arm_compute::CLScheduler::get().sync();

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*triNNity::uceil<ifm_h, stride_h>()*triNNity::uceil<ifm_w, stride_w>()*ifms*ofms*k*k);
    return conv_flops;
  }

  virtual double working_space() { return 0; }

  ARMCLConvolutionalLayer(T *in, KD *weights) {
    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    kernel = weights;

    switch(bound_type) {
      case triNNity::BOUND_UNDEF: { armcl_bound = arm_compute::BorderMode::UNDEFINED; } break;
      case triNNity::BOUND_IMPLICIT_MIRROR: { armcl_bound = arm_compute::BorderMode::REPLICATE; } break;
      case triNNity::BOUND_IMPLICIT_PAD: { armcl_bound = arm_compute::BorderMode::CONSTANT; } break;
      case triNNity::BOUND_EXPLICIT: { armcl_bound = arm_compute::BorderMode::UNDEFINED; } break;
    }

    armcl_input.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(ifm_w, ifm_h, ifms), arm_compute::Format::F32));
    armcl_kernel.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(k, k, ifms, ofms), arm_compute::Format::F32));
    armcl_output.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(ofm_w, ofm_h, ofms), arm_compute::Format::F32));

    switch(wrap_algo) {
      case ARMCL_DIRECT: {
        conv = new arm_compute::CLDirectConvolutionLayer;
      } break;

      case ARMCL_WINOGRAD: {
        conv = new arm_compute::CLWinogradConvolutionLayer;
      } break;

      case ARMCL_PATCH_GEMM: {
        conv = new arm_compute::CLGEMMConvolutionLayer;
      } break;
    }

    reinterpret_cast<arm_compute::CLConvolutionLayer*>(conv)->configure(&armcl_input, &armcl_kernel, nullptr, &armcl_output, arm_compute::PadStrideInfo((signed) stride_w, (signed) stride_h, (signed) k/2, (signed) k/2));

    armcl_input.allocator()->allocate();
    armcl_kernel.allocator()->allocate();
    armcl_output.allocator()->allocate();
  }

  ARMCLConvolutionalLayer(T *in, KD *weights, T *out) {
    this->input = in;
    if constexpr (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    kernel = weights;

    switch(bound_type) {
      case triNNity::BOUND_UNDEF: { armcl_bound = arm_compute::BorderMode::UNDEFINED; } break;
      case triNNity::BOUND_IMPLICIT_MIRROR: { armcl_bound = arm_compute::BorderMode::REPLICATE; } break;
      case triNNity::BOUND_IMPLICIT_PAD: { armcl_bound = arm_compute::BorderMode::CONSTANT; } break;
      case triNNity::BOUND_EXPLICIT: { armcl_bound = arm_compute::BorderMode::UNDEFINED; } break;
    }

    arm_compute::CLScheduler::get().default_init();

    armcl_input.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(ifm_w, ifm_h, ifms), arm_compute::Format::F32));
    armcl_kernel.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(k, k, ifms, ofms), arm_compute::Format::F32));
    armcl_output.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(ofm_w, ofm_h, ofms), arm_compute::Format::F32));

    switch(wrap_algo) {
      case ARMCL_DIRECT: {
        conv = new arm_compute::CLDirectConvolutionLayer;
      } break;

      case ARMCL_WINOGRAD: {
        conv = new arm_compute::CLWinogradConvolutionLayer;
      } break;

      case ARMCL_PATCH_GEMM: {
        conv = new arm_compute::CLGEMMConvolutionLayer;
      } break;
    }

    reinterpret_cast<arm_compute::CLConvolutionLayer*>(conv)->configure(&armcl_input, &armcl_kernel, nullptr, &armcl_output, arm_compute::PadStrideInfo((signed) stride_w, (signed) stride_h, (signed) k/2, (signed) k/2));

    armcl_input.allocator()->allocate();
    armcl_kernel.allocator()->allocate();
    armcl_output.allocator()->allocate();
  }

  ~ARMCLConvolutionalLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }

    TRINNITY_DELETE(conv);

    armcl_input.allocator()->free();
    armcl_kernel.allocator()->free();
    armcl_output.allocator()->free();
  }
};

}

}
#endif // TRINNITY_WRAP_ARMCL

#endif // TRINNITY_WRAPPERS_ARMCL_LAYER_H
