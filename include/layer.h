/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_LAYER_H
#define TRINNITY_LAYER_H

#include <cstddef>
#include <ctime>

#include <triNNity/config.h>

namespace triNNity {

/**
 * This namespace contains the layer-based interface to triNNity.
 */

namespace layer {

/**
 * This is the superclass for all Layer types.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input tensors of the layer.
 * \endparblock
 * \tparam O
 * \parblock
 * The primitive type of the data in the output tensors of the layer.
 * \endparblock
 *
 */

template<typename T, typename O>
struct Layer {

  //! Pointer to the layer input tensor.
  T *input;

  //! Pointer to the layer output tensor.
  O *output;

  //! Whether the output is managed by us or externally
  bool output_inplace;

  #ifndef TRINNITY_DISABLE_LAYER_TIMING
  //! Timer variable
  struct timespec start;
  //! Timer variable
  struct timespec end;

  /**
   *  Return the time in milliseconds used by the last execute() call.
   */
  double time_ms() {
    return (((double) (end.tv_sec - start.tv_sec)) * 1000.0)
           +
           (((double) (end.tv_nsec - start.tv_nsec)) * 0.000001);
  }
  #endif // TRINNITY_DISABLE_LAYER_TIMING

  /**
   *  Execute the layer.
   */
  virtual void execute() = 0;

  /**
   *  Return the total number of floating point operations used in the layer computations.
   */
  virtual double flops() = 0;

  /**
   *  Return the total number of bytes of working space allocated by the layer computations.
   */
  virtual double working_space() = 0;

  virtual ~Layer() {}
};


}

}

#endif // TRINNITY_LAYER_H
