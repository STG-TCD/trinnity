/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPARSE_CPU_IMPL_H
#define TRINNITY_SPARSE_CPU_IMPL_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/sparse/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/sparse/cpu/primitive.h>

#include <triNNity/dense/cpu/primitive.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/gemm.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace sparse {

namespace cpu {

namespace impl {

/* The basic sparse direct convolution loop */
template <typename T, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void sparse_loop_mhw(const T * __restrict__ img, const triNNity::sparse::cpu::primitive::sparse_kernel<KD> * __restrict__ kernel_sparse, T * __restrict__ out) {

  auto localImageLayout  = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>()>(out);

  // Zero-initialize our output
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m = 0; m < kernels; m++) {
    for (signed h = 0; h < triNNity::sceil<img_h, stride_h>(); h++) {
      for (signed w = 0; w < triNNity::sceil<img_w, stride_w>(); w++) {
        localOutputLayout[m][h][w] = static_cast<T>(0);
      }
    }
  }

  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed count = 0; count < kernel_sparse->first; count++) {
    signed m = kernel_sparse->second[count].ofm_idx;
    signed d = kernel_sparse->second[count].ifm_idx;
    signed y = kernel_sparse->second[count].ky_idx;
    signed x = kernel_sparse->second[count].kx_idx;

    for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < (img_h-(k/2)); h+=stride_h) {
      for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
        localOutputLayout[m][h/stride_h][w/stride_w] +=
        localImageLayout[d][(h + y) - (k/2)][(w + x) - (k/2)]
        *
        (T)kernel_sparse->second[count].value;
      }
    }
  }
}

/* The basic sparse direct convolution loop with inner loops exchanged */
template <typename T, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void sparse_loop_mwh(const T * __restrict__ img, const triNNity::sparse::cpu::primitive::sparse_kernel<KD> * __restrict__ kernel_sparse, T * __restrict__ out) {

  auto localImageLayout  = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>()>(out);

  // Zero-initialize our output
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m = 0; m < kernels; m++) {
    for (signed h = 0; h < triNNity::sceil<img_h, stride_h>(); h++) {
      for (signed w = 0; w < triNNity::sceil<img_w, stride_w>(); w++) {
        localOutputLayout[m][h][w] = static_cast<T>(0);
      }
    }
  }

  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed count = 0; count < kernel_sparse->first; count++) {
    signed m = kernel_sparse->second[count].ofm_idx;
    signed d = kernel_sparse->second[count].ifm_idx;
    signed y = kernel_sparse->second[count].ky_idx;
    signed x = kernel_sparse->second[count].kx_idx;

    for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
      for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < (img_h-(k/2)); h+=stride_h) {
        localOutputLayout[m][h/stride_h][w/stride_w] +=
        localImageLayout[d][(h + y) - (k/2)][(w + x) - (k/2)]
        *
        (T)kernel_sparse->second[count].value;
      }
    }
  }
}

/* A h-wise unrolled version of the simple direct sparse convolution loop */
template <typename T, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void sparse_loop_mhw_unroll_w(const T * __restrict__ img, const triNNity::sparse::cpu::primitive::sparse_kernel<KD> * __restrict__ kernel_sparse, T * __restrict__ out) {

  auto localImageLayout  = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>()>(out);

  // Zero-initialize our output
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m = 0; m < kernels; m++) {
    for (signed h = 0; h < triNNity::sceil<img_h, stride_h>(); h++) {
      for (signed w = 0; w < triNNity::sceil<img_w, stride_w>(); w++) {
        localOutputLayout[m][h][w] = static_cast<T>(0);
      }
    }
  }

  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed count = 0; count < kernel_sparse->first; count++) {
    signed m = kernel_sparse->second[count].ofm_idx;
    signed d = kernel_sparse->second[count].ifm_idx;
    signed y = kernel_sparse->second[count].ky_idx;
    signed x = kernel_sparse->second[count].kx_idx;

    static constexpr signed h_trip_count = ((img_h-(k/2)) - (stride_h*triNNity::sceil<k/2, stride_h>()))/stride_h;

    for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < ((img_h-(k/2)) - stride_h); h+=2*stride_h) {
      for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
        localOutputLayout[m][h/stride_h][w/stride_w] +=
        localImageLayout[d][(h + y) - (k/2)][(w + x) - (k/2)]
        *
        (T)kernel_sparse->second[count].value;

        localOutputLayout[m][h+stride_h/stride_h][w/stride_w] +=
        localImageLayout[d][(h + stride_h + y) - (k/2)][(w + x) - (k/2)]
        *
        (T)kernel_sparse->second[count].value;
      }
    }

    if constexpr (h_trip_count % 2) {
      static constexpr signed h = (img_h-(k/2)) - stride_h;
      for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
        localOutputLayout[m][h/stride_h][w/stride_w] +=
        localImageLayout[d][(h + y) - (k/2)][(w + x) - (k/2)]
        *
        (T)kernel_sparse->second[count].value;
      }
    }
  }
}

template <typename T, typename KD, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE void sparse_loop_mhw_unroll_m(const T * __restrict__ img, const triNNity::sparse::cpu::primitive::sparse_kernel<KD> * __restrict__ kernel_sparse, T * __restrict__ out) {

  auto localImageLayout  = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(img);
  auto localOutputLayout = triNNity::internal::memory::View3D<T, kernels, triNNity::uceil<img_h, stride_h>(), triNNity::uceil<img_w, stride_w>()>(out);

  // Zero-initialize our output
  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed m = 0; m < kernels; m++) {
    for (signed h = 0; h < triNNity::sceil<img_h, stride_h>(); h++) {
      for (signed w = 0; w < triNNity::sceil<img_w, stride_w>(); w++) {
        localOutputLayout[m][h][w] = static_cast<T>(0);
      }
    }
  }

  #if defined(TRINNITY_USE_OPENMP)
  #pragma omp parallel for
  #endif
  for (signed count = 0; count < (kernel_sparse->first)-1; count += 2) {
    signed m0 = kernel_sparse->second[count].ofm_idx;
    signed d0 = kernel_sparse->second[count].ifm_idx;
    signed y0 = kernel_sparse->second[count].ky_idx;
    signed x0 = kernel_sparse->second[count].kx_idx;

    signed m1 = kernel_sparse->second[count+1].ofm_idx;
    signed d1 = kernel_sparse->second[count+1].ifm_idx;
    signed y1 = kernel_sparse->second[count+1].ky_idx;
    signed x1 = kernel_sparse->second[count+1].kx_idx;


    if (m0 != m1) {
      for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < (img_h-(k/2)); h+=stride_h) {
        for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
          localOutputLayout[m0][h/stride_h][w/stride_w] +=
          localImageLayout[d0][(h + y0) - (k/2)][(w + x0) - (k/2)]
          *
          (T)kernel_sparse->second[count].value;

          localOutputLayout[m1][h/stride_h][w/stride_w] +=
          localImageLayout[d1][(h + y1) - (k/2)][(w + x1) - (k/2)]
          *
          (T)kernel_sparse->second[count+1].value;
        }
      }
    } else {
      for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < (img_h-(k/2)); h+=stride_h) {
        for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
          localOutputLayout[m0][h/stride_h][w/stride_w] +=
          localImageLayout[d0][(h + y0) - (k/2)][(w + x0) - (k/2)]
          *
          (T)kernel_sparse->second[count].value;
        }
      }

      for (signed h = stride_h*triNNity::sceil<k/2, stride_h>(); h < (img_h-(k/2)); h+=stride_h) {
        for (signed w = stride_w*triNNity::sceil<k/2, stride_w>(); w < (img_w-(k/2)); w+=stride_w) {
          localOutputLayout[m1][h/stride_h][w/stride_w] +=
          localImageLayout[d1][(h + y1) - (k/2)][(w + x1) - (k/2)]
          *
          (T)kernel_sparse->second[count+1].value;
        }
      }
    }
  }
}

}

}

}

}

#endif // TRINNITY_SPARSE_CPU_IMPL_H
