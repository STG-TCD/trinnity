/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPARSE_CPU_CONFIG_H
#define TRINNITY_SPARSE_CPU_CONFIG_H

#include <triNNity/config.h>

namespace triNNity {

/**
 * Names for different kinds of sparse convolution implemented by the library.
 */

typedef enum {
  CONV_MULTI_SPARSE_DIRECT_MHW,
  CONV_MULTI_SPARSE_DIRECT_MWH,
  CONV_MULTI_SPARSE_DIRECT_MHW_UNROLL_W,
  CONV_MULTI_SPARSE_DIRECT_MHW_UNROLL_M,
} conv_sparse_impl_t;

/**
 * Names for different kinds of sparse FC implemented by the library.
 */

typedef enum {
  FC_SPARSE_DIRECT,
  FC_SPARSE_HAMMING,
} fc_sparse_impl_t;

}

#endif // TRINNITY_SPARSE_CPU_CONFIG_H
