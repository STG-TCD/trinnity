/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPARSE_CPU_PRIMITIVE_H
#define TRINNITY_SPARSE_CPU_PRIMITIVE_H

#include <cmath>
#include <cstdlib>
#include <random>
#include <algorithm>

#include <triNNity/config.h>
#include <triNNity/sparse/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/dense/cpu/primitive.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/gemm.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace sparse {

namespace cpu {

namespace primitive {

// Coordinate form data representation
template <typename KD>
struct sparse_kernel_point {
  unsigned ofm_idx;
  unsigned ifm_idx;
  unsigned ky_idx;
  unsigned kx_idx;
  KD value;
};

template <typename KD>
using sparse_kernel = std::pair<unsigned, sparse_kernel_point<KD>*>;

template <typename KD, unsigned channels, unsigned kernels, unsigned k>
static TRINNITY_INLINE const sparse_kernel<KD>* create_packed_oihw(const KD * __restrict__ kernel) {
  auto localKernelLayout = triNNity::internal::memory::View4D<const KD, kernels, channels, k, k>(kernel);
  unsigned count = 0;
  sparse_kernel_point<KD> *compacted = new sparse_kernel_point<KD>[kernels * channels * k * k];
  sparse_kernel_point<KD> *result;

  for (unsigned m = 0; m < kernels; m++) {
    for (unsigned c = 0; c < channels; c++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          if (localKernelLayout[m][c][ky][kx] != static_cast<KD>(0)) {
            compacted[count].ofm_idx = m;
            compacted[count].ifm_idx = c;
            compacted[count].ky_idx = ky;
            compacted[count].kx_idx = kx;
            compacted[count].value = localKernelLayout[m][c][ky][kx];
            count += 1;
          }
        }
      }
    }
  }

  result = new sparse_kernel_point<KD>[count];
  std::copy_n(compacted, count, result);
  delete [] compacted;

  auto retval = new std::pair<unsigned, sparse_kernel_point<KD>*>(count, result);
  return retval;
}

template <typename KD>
static TRINNITY_INLINE void destroy_packed_oihw(const sparse_kernel<KD> *final_slices) {
  delete [] final_slices->second;
}

}

}

}

}

#endif // TRINNITY_SPARSE_CPU_PRIMITIVE_H
