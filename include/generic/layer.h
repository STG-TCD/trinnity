/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_GENERIC_LAYER_H
#define TRINNITY_GENERIC_LAYER_H

#include <cstddef>

#include <triNNity/config.h>
#include <triNNity/layer.h>

#include <triNNity/dense/cpu/config.h>
#include <triNNity/dense/cpu/gemv.h>
#include <triNNity/dense/cpu/mcmk.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/memory.h>
#include <triNNity/dense/cpu/primitive.h>
#include <triNNity/dense/cpu/layer.h>

#if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/spectral/cpu/layer.h>
#endif

#if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
#include <triNNity/sparse/cpu/config.h>
#include <triNNity/sparse/cpu/layer.h>
#endif

#if defined(TRINNITY_WRAP_MKLDNN)
#include <triNNity/wrappers/mkldnn/layer.h>
#endif

#if defined(TRINNITY_WRAP_ARMCL)
#include <triNNity/wrappers/armcl/layer.h>
#endif

#include <triNNity/generic/config.h>

namespace triNNity {

namespace generic {

typedef enum {
  #if defined(TRINNITY_WRAP_MKLDNN)
  CONV_MKLDNN_DIRECT,
  CONV_MKLDNN_WINOGRAD,
  #endif

  #if defined(TRINNITY_WRAP_ARMCL)
  CONV_ARMCL_DIRECT,
  CONV_ARMCL_PATCH_GEMM,
  CONV_ARMCL_WINOGRAD,
  #endif

  #if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
  CONV_SPARSE_DIRECT_MHW,
  CONV_SPARSE_DIRECT_MWH,
  #endif

  CONV_DIRECT_SUM_2D_CHW,
  CONV_DIRECT_SUM_2D_HWC,

  CONV_IM2COL_SCAN_AT_B_I_K,
  CONV_IM2COL_SCAN_AT_B_K_I,
  CONV_IM2COL_SCAN_AT_BT_I_K,
  CONV_IM2COL_SCAN_A_B_K_I,

  CONV_IM2COL_COPY_SHORT_AT_B_I_K,
  CONV_IM2COL_COPY_SHORT_AT_B_K_I,
  CONV_IM2COL_COPY_SHORT_AT_BT_I_K,
  CONV_IM2COL_COPY_SHORT_A_B_K_I,

  CONV_IM2COL_COPY_LONG_AT_B_I_K,
  CONV_IM2COL_COPY_LONG_AT_B_K_I,
  CONV_IM2COL_COPY_LONG_AT_BT_I_K,
  CONV_IM2COL_COPY_LONG_A_B_K_I,

  CONV_IM2COL_COPY_SELF_AT_B_I_K,
  CONV_IM2COL_COPY_SELF_AT_B_K_I,
  CONV_IM2COL_COPY_SELF_AT_BT_I_K,
  CONV_IM2COL_COPY_SELF_A_B_K_I,

  CONV_IM2ROW_SCAN_A_B_I_K,
  CONV_IM2ROW_SCAN_AT_BT_K_I,
  CONV_IM2ROW_SCAN_A_BT_I_K,
  CONV_IM2ROW_SCAN_A_BT_K_I,

  CONV_IM2ROW_COPY_SHORT_A_B_I_K,
  CONV_IM2ROW_COPY_SHORT_AT_BT_K_I,
  CONV_IM2ROW_COPY_SHORT_A_BT_I_K,
  CONV_IM2ROW_COPY_SHORT_A_BT_K_I,

  CONV_KN2ROW,
  CONV_KN2COL,
  CONV_KN2ROW_AS,
  CONV_KN2COL_AS,
  CONV_KN2ROW_AA_AB,
  CONV_KN2ROW_AA_ABT,
  CONV_KN2ROW_AA_ATB,
  CONV_KN2ROW_AA_ATBT,

  CONV_MEC_COL,
  CONV_MEC_ROW,

  CONV_1X1_AB_IK,
  CONV_1X1_ABT_IK,
  CONV_1X1_ATB_IK,
  CONV_1X1_ATBT_IK,
  CONV_1X1_AB_KI,
  CONV_1X1_ABT_KI,
  CONV_1X1_ATB_KI,
  CONV_1X1_ATBT_KI,

  #if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
  CONV_WINO_2_3,
  CONV_WINO_2_3_VEC_4,
  CONV_WINO_2_3_VEC_8,

  CONV_WINO_2X2_3X3,
  CONV_WINO_2X2_3X3_VEC_4,
  CONV_WINO_2X2_3X3_VEC_8,

  CONV_WINO_3_3,
  CONV_WINO_3_3_VEC_4,
  CONV_WINO_3_3_VEC_8,

  CONV_WINO_3X3_3X3,
  CONV_WINO_3X3_3X3_VEC_4,
  CONV_WINO_3X3_3X3_VEC_8,

  CONV_WINO_4X4_3X3,
  CONV_WINO_4X4_3X3_VEC_4,
  CONV_WINO_4X4_3X3_VEC_8,

  CONV_WINO_2_5,
  CONV_WINO_2_5_VEC_4,
  CONV_WINO_2_5_VEC_8,

  CONV_WINO_2X2_5X5,
  CONV_WINO_2X2_5X5_VEC_4,
  CONV_WINO_2X2_5X5_VEC_8,

  CONV_WINO_3_5,
  CONV_WINO_3_5_VEC_4,
  CONV_WINO_3_5_VEC_8,

  CONV_WINO_3X3_5X5,
  CONV_WINO_3X3_5X5_VEC_4,
  CONV_WINO_3X3_5X5_VEC_8,

  CONV_WINO_4X4_5X5,
  CONV_WINO_4X4_5X5_VEC_4,
  CONV_WINO_4X4_5X5_VEC_8,

  CONV_FFT_STITCHED_COMPLEX,
  CONV_FFT_STITCHED_REAL,
  CONV_FFT_FULL_COMPLEX,
  CONV_FFT_FULL_REAL,
  #endif
} conv_generic_impl_t;

namespace internal {

template <typename T, typename A, typename KD, unsigned IMG_W, unsigned IMG_H, unsigned CHANNELS, unsigned KERNELS, unsigned K_W, unsigned K_H, unsigned STRIDE_W, unsigned STRIDE_H, triNNity::boundary_t BOUND, conv_generic_impl_t method, gemm_variant_t gemm_var, unsigned GBI=1, unsigned GBJ=1, unsigned GBK=1>
triNNity::layer::Layer<T, T>* buildLayerForMethod(T * __restrict__ input, KD * __restrict__ kernel, T * __restrict__ output) {
  switch (method) {

    #if defined(TRINNITY_WRAP_MKLDNN)
    case CONV_MKLDNN_DIRECT: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::MKLDNNConvolutionalLayer<T, KD,
                                                                triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND, triNNity::layer::MKLDNN_DIRECT>
                                                                (input, kernel, output);
      return result;
    } break;

    case CONV_MKLDNN_WINOGRAD: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::MKLDNNConvolutionalLayer<T, KD,
                                                                triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND, triNNity::layer::MKLDNN_WINOGRAD>
                                                                (input, kernel, output);
      return result;
    } break;
    #endif

    #if defined(TRINNITY_WRAP_ARMCL)
    case CONV_ARMCL_DIRECT: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::ARMCLConvolutionalLayer<T, KD,
                                                               triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                               CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                               triNNity::uceil<IMG_W, STRIDE_W>(),
                                                               triNNity::uceil<IMG_H, STRIDE_H>(),
                                                               BOUND, triNNity::layer::ARMCL_DIRECT>
                                                               (input, kernel, output);
      return result;
    } break;

    case CONV_ARMCL_PATCH_GEMM: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::ARMCLConvolutionalLayer<T, KD,
                                                               triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                               CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                               triNNity::uceil<IMG_W, STRIDE_W>(),
                                                               triNNity::uceil<IMG_H, STRIDE_H>(),
                                                               BOUND, triNNity::layer::ARMCL_PATCH_GEMM>
                                                               (input, kernel, output);
      return result;
    } break;

    case CONV_ARMCL_WINOGRAD: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::ARMCLConvolutionalLayer<T, KD,
                                                               triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                               CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                               triNNity::uceil<IMG_W, STRIDE_W>(),
                                                               triNNity::uceil<IMG_H, STRIDE_H>(),
                                                               BOUND, triNNity::layer::ARMCL_WINOGRAD>
                                                               (input, kernel, output);
      return result;
    } break;
    #endif

    #if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
    case CONV_SPARSE_DIRECT_MHW: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::DirectSparseConvolutionalLayer<T, KD, triNNity::CONV_MULTI_SPARSE_DIRECT_MHW,
                                                                triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND>
                                                                (input, kernel, output);
      return result;
    } break;

    case CONV_SPARSE_DIRECT_MWH: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::DirectSparseConvolutionalLayer<T, KD, triNNity::CONV_MULTI_SPARSE_DIRECT_MWH,
                                                                triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND>
                                                                (input, kernel, output);
      return result;
    } break;
    #endif

    case CONV_DIRECT_SUM_2D_CHW: {
      auto result = new triNNity::layer::DirectConvolutionalLayer<T, A, T, KD, triNNity::CONV_MULTI_SUM_SINGLE,
                                                                triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                CHANNELS, IMG_W, IMG_H, K_W, K_H, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND>
                                                                (input, kernel, output);
      return result;
    } break;

    case CONV_DIRECT_SUM_2D_HWC: {
      auto result = new triNNity::layer::DirectConvolutionalLayer<T, A, T, KD, triNNity::CONV_MULTI_SUM_SINGLE,
                                                                triNNity::layout::HWC, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                CHANNELS, IMG_W, IMG_H, K_W, K_H, STRIDE_W, STRIDE_H, KERNELS,
                                                                triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                BOUND>
                                                                (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_SCAN_AT_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_AT_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_SCAN_AT_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_SCAN_AT_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_AT_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_SCAN_A_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SHORT_AT_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_AT_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SHORT_AT_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SHORT_AT_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_AT_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SHORT_A_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_LONG_AT_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_LONG, triNNity::GEMM_AT_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_LONG_AT_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_LONG, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_LONG_AT_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_LONG, triNNity::GEMM_AT_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_LONG_A_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_LONG, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SELF_AT_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SELF, triNNity::GEMM_AT_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SELF_AT_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SELF, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SELF_AT_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SELF, triNNity::GEMM_AT_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2COL_COPY_SELF_A_B_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2COL_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SELF, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_SCAN_A_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_A_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_SCAN_AT_BT_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_AT_BT, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_SCAN_A_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_A_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_SCAN_A_BT_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_SCAN, triNNity::GEMM_A_BT, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_COPY_SHORT_A_B_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::HWC, triNNity::layout::HWIO, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_A_B, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_COPY_SHORT_AT_BT_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::HWC, triNNity::layout::HWIO, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_AT_BT, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_COPY_SHORT_A_BT_I_K: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::HWC, triNNity::layout::OHWI, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_A_BT, triNNity::GEMM_IK,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_IM2ROW_COPY_SHORT_A_BT_K_I: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_IM2ROW_GEMM, gemm_var,
                                                                   triNNity::layout::HWC, triNNity::layout::OHWI, triNNity::layout::CHW,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_COPY_SHORT, triNNity::GEMM_A_BT, triNNity::GEMM_KI,
                                                                   GBI, GBJ, GBK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_BT, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2COL: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2COL_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::IHWO, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_B, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW_AS: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_AS_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2COL_AS: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2COL_AS_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_BT, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW_AA_AB: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_AA_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW_AA_ABT: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_AA_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_BT, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW_AA_ATB: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_AA_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWIO, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_KN2ROW_AA_ATBT: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_KN2ROW_AA_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWIO, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_BT, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_MEC_COL: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_MEC_COL, gemm_var,
                                                                   triNNity::layout::HCW, triNNity::layout::OHIW, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_ANY, triNNity::GEMM_A_B, triNNity::GEMM_KI>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_MEC_ROW: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::PatchGEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_MEC_ROW_PARTITION, gemm_var,
                                                                   triNNity::layout::HWC, triNNity::layout::OHWI, triNNity::layout::HWC,
                                                                   CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                   triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                   triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                   BOUND, triNNity::PATCH_ANY, triNNity::GEMM_A_BT, triNNity::GEMM_IK>
                                                                   (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_AB_IK: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWIO, triNNity::layout::HWC,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_B, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ABT_IK: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_BT, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ATB_IK: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWIO, triNNity::layout::HWC,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_B, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ATBT_IK: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_BT, triNNity::GEMM_IK,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_AB_KI: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_B, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ABT_KI: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_A_BT, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ATB_KI: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::CHW, triNNity::layout::HWIO, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_B, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    case CONV_1X1_ATBT_KI: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::GEMMConvolutionalLayer<T, KD, triNNity::CONV_MULTI_CONV_1x1_GEMM, gemm_var,
                                                              triNNity::layout::HWC, triNNity::layout::HWIO, triNNity::layout::CHW,
                                                              CHANNELS, IMG_W, IMG_H, K_W, KERNELS, IMG_W, IMG_H,
                                                              BOUND, triNNity::GEMM_AT_BT, triNNity::GEMM_KI,
                                                              GBI, GBJ, GBK>
                                                              (input, kernel, output);
      return result;
    } break;

    #if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
    case CONV_WINO_2_3: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2_3_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2_3_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_3X3: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_3X3_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_3X3_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_3: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_3_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_3_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_3X3: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_3X3_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_3X3_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_3X3: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_3X3_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_3X3_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2_5: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2_5_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2_5_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_5X5: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_5X5_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_2X2_5X5_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_2x2, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_5: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_5_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3_5_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_5X5: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_5X5_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_3X3_5X5_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_3x3, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_5X5: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_5X5_VEC_4: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 4>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_WINO_4X4_5X5_VEC_8: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::WinogradConvolutionalLayer<T, KD, triNNity::CONV_MULTI_WINOGRAD_4x4, gemm_var,
                                                                  triNNity::layout::HWC, triNNity::layout::HWOI, triNNity::layout::HWC,
                                                                  CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                                  triNNity::uceil<IMG_W, STRIDE_W>(),
                                                                  triNNity::uceil<IMG_H, STRIDE_H>(),
                                                                  BOUND, 8>
                                                                  (input, kernel, output);
      return result;
    } break;

    case CONV_FFT_STITCHED_REAL: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::FFTConvolutionalLayer<T, KD, triNNity::CONV_MULTI_FFT_STITCHED_REAL, gemm_var,
                                                             triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                             CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                             triNNity::uceil<IMG_W, STRIDE_W>(),
                                                             triNNity::uceil<IMG_H, STRIDE_H>(),
                                                             BOUND>
                                                             (input, kernel, output);
      return result;
    } break;

    case CONV_FFT_STITCHED_COMPLEX: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::FFTConvolutionalLayer<T, KD, triNNity::CONV_MULTI_FFT_STITCHED_COMPLEX, gemm_var,
                                                             triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                             CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                             triNNity::uceil<IMG_W, STRIDE_W>(),
                                                             triNNity::uceil<IMG_H, STRIDE_H>(),
                                                             BOUND>
                                                             (input, kernel, output);
      return result;
    } break;

    case CONV_FFT_FULL_REAL: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::FFTConvolutionalLayer<T, KD, triNNity::CONV_MULTI_FFT_FULL_GEMM_REAL, gemm_var,
                                                             triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                             CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                             triNNity::uceil<IMG_W, STRIDE_W>(),
                                                             triNNity::uceil<IMG_H, STRIDE_H>(),
                                                             BOUND>
                                                             (input, kernel, output);
      return result;
    } break;

    case CONV_FFT_FULL_COMPLEX: {
      TRINNITY_STATIC_ASSERT(K_W == K_H);
      auto result = new triNNity::layer::FFTConvolutionalLayer<T, KD, triNNity::CONV_MULTI_FFT_FULL_GEMM_COMPLEX, gemm_var,
                                                             triNNity::layout::CHW, triNNity::layout::OIHW, triNNity::layout::CHW,
                                                             CHANNELS, IMG_W, IMG_H, K_W, STRIDE_W, STRIDE_H, KERNELS,
                                                             triNNity::uceil<IMG_W, STRIDE_W>(),
                                                             triNNity::uceil<IMG_H, STRIDE_H>(),
                                                             BOUND>
                                                             (input, kernel, output);
      return result;
    } break;
    #endif

    default: {
      TRINNITY_ERROR("Unsupported convolution method chosen");
    } break;
  }

  return nullptr;
}

template <typename T, unsigned IMG_W, unsigned IMG_H, unsigned KERNELS, unsigned K_W, unsigned K_H, unsigned STRIDE_W, unsigned STRIDE_H, triNNity::layout::feature_map_layout_t data_layout>
static TRINNITY_INLINE void discard(T * __restrict__ input, T * __restrict__ output) {
  static constexpr unsigned current_h = triNNity::uceil<IMG_H, STRIDE_H>();
  static constexpr unsigned current_w = triNNity::uceil<IMG_W, STRIDE_W>();
  static constexpr unsigned target_h = triNNity::uceil<IMG_H, STRIDE_H>() - (K_H - 1);
  static constexpr unsigned target_w = triNNity::uceil<IMG_W, STRIDE_W>() - (K_W - 1);

  TRINNITY_ASSERT(STRIDE_H == 1);
  TRINNITY_ASSERT(STRIDE_W == 1);

  switch (data_layout) {
    case triNNity::layout::CHW: {
      auto localInputLayout = triNNity::internal::memory::View3D<T, KERNELS, current_h, current_w>(input);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, KERNELS, target_h, target_w>(output);
      for (unsigned m = 0; m < KERNELS; m++) {
        for (unsigned h = 0; h < target_h; h++) {
          for (unsigned w = 0; w < target_w; w++) {
            localOutputLayout[m][h][w] = localInputLayout[m][h+(K_H/2)][w+(K_W/2)];
          }
        }
      }
    } break;

    case triNNity::layout::HCW: {
      auto localInputLayout = triNNity::internal::memory::View3D<T, current_h, KERNELS, current_w>(input);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, target_h, KERNELS, target_w>(output);
      for (unsigned h = 0; h < target_h; h++) {
        for (unsigned m = 0; m < KERNELS; m++) {
          for (unsigned w = 0; w < target_w; w++) {
            localOutputLayout[h][m][w] = localInputLayout[h+(K_H/2)][m][w+(K_W/2)];
          }
        }
      }
    } break;

    case triNNity::layout::HWC: {
      auto localInputLayout = triNNity::internal::memory::View3D<T, current_h, current_w, KERNELS>(input);
      auto localOutputLayout = triNNity::internal::memory::View3D<T, target_h, target_w, KERNELS>(output);
      for (unsigned h = 0; h < target_h; h++) {
        for (unsigned w = 0; w < target_w; w++) {
          for (unsigned m = 0; m < KERNELS; m++) {
            localOutputLayout[h][w][m] = localInputLayout[h+(K_H/2)][w+(K_W/2)][m];
          }
        }
      }
    } break;

    default: {
      TRINNITY_ERROR("Unsupported layout");
    } break;
  }
}

}

namespace layer {

/**
 * A generic convolution layer.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam A
 * \parblock
 * The primitive type in which accumulation should happen
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam method
 * \parblock
 * The convolution method the layer should use to compute the output
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 *  \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */

template<typename T, typename A, typename KD,
         conv_generic_impl_t method, gemm_variant_t gemm_var,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k_w, unsigned k_h,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms,
         triNNity::boundary_t bound_type = BOUND_IMPLICIT_PAD,
         unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
struct GenericConvolutionalLayer: public triNNity::layer::Layer<T, T> {

  /**
   * Returns the feature map layout required for the input, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::feature_map_layout_t get_in_layout() {
    switch (method) {
      #if defined(TRINNITY_WRAP_MKLDNN)
      case CONV_MKLDNN_DIRECT:                  { return triNNity::layout::CHW; } break;
      case CONV_MKLDNN_WINOGRAD:                { return triNNity::layout::CHW; } break;
      #endif

      #if defined(TRINNITY_WRAP_ARMCL)
      case CONV_ARMCL_DIRECT:                   { return triNNity::layout::CHW; } break;
      case CONV_ARMCL_PATCH_GEMM:               { return triNNity::layout::CHW; } break;
      case CONV_ARMCL_WINOGRAD:                 { return triNNity::layout::CHW; } break;
      #endif

      #if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
      case CONV_SPARSE_DIRECT_MHW:              { return triNNity::layout::CHW; } break;
      case CONV_SPARSE_DIRECT_MWH:              { return triNNity::layout::CHW; } break;
      #endif

      case CONV_DIRECT_SUM_2D_CHW:              { return triNNity::layout::CHW; } break;
      case CONV_DIRECT_SUM_2D_HWC:              { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_SCAN_AT_B_I_K:           { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_SCAN_AT_B_K_I:           { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_SCAN_AT_BT_I_K:          { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_SCAN_A_B_K_I:            { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_I_K:     { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_K_I:     { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_BT_I_K:    { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_A_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_I_K:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_BT_I_K:     { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_A_B_K_I:       { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_I_K:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_BT_I_K:     { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_A_B_K_I:       { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_A_B_I_K:            { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_AT_BT_K_I:          { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_A_BT_I_K:           { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_A_BT_K_I:           { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_COPY_SHORT_A_B_I_K:      { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_COPY_SHORT_AT_BT_K_I:    { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_I_K:     { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_K_I:     { return triNNity::layout::HWC; } break;
      case CONV_KN2ROW:                         { return triNNity::layout::HWC; } break;
      case CONV_KN2COL:                         { return triNNity::layout::HWC; } break;
      case CONV_KN2ROW_AS:                      { return triNNity::layout::CHW; } break;
      case CONV_KN2COL_AS:                      { return triNNity::layout::HWC; } break;
      case CONV_KN2ROW_AA_AB:                   { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_ABT:                  { return triNNity::layout::HWC; } break;
      case CONV_KN2ROW_AA_ATB:                  { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_ATBT:                 { return triNNity::layout::HWC; } break;
      case CONV_MEC_COL:                        { return triNNity::layout::HCW; } break;
      case CONV_MEC_ROW:                        { return triNNity::layout::HWC; } break;
      case CONV_1X1_AB_IK:                      { return triNNity::layout::HWC; } break;
      case CONV_1X1_ABT_IK:                     { return triNNity::layout::HWC; } break;
      case CONV_1X1_ATB_IK:                     { return triNNity::layout::CHW; } break;
      case CONV_1X1_ATBT_IK:                    { return triNNity::layout::CHW; } break;
      case CONV_1X1_AB_KI:                      { return triNNity::layout::CHW; } break;
      case CONV_1X1_ABT_KI:                     { return triNNity::layout::HWC; } break;
      case CONV_1X1_ATB_KI:                     { return triNNity::layout::CHW; } break;
      case CONV_1X1_ATBT_KI:                    { return triNNity::layout::HWC; } break;

      #if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
      case CONV_WINO_2_3:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_3_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_3_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_2X2_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_3_3:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_3_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_3_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_3X3_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_4X4_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_2_5:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_5_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_5_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_2X2_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_3_5:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_5_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_5_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_3X3_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_4X4_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_FFT_STITCHED_REAL:              { return triNNity::layout::CHW; } break;
      case CONV_FFT_STITCHED_COMPLEX:           { return triNNity::layout::CHW; } break;
      case CONV_FFT_FULL_REAL:                  { return triNNity::layout::CHW; } break;
      case CONV_FFT_FULL_COMPLEX:               { return triNNity::layout::CHW; } break;
      #endif
    }
    return triNNity::layout::CHW;
  }

  /**
   * Returns the feature map layout required for the output, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::feature_map_layout_t get_out_layout() {
    switch (method) {
      #if defined(TRINNITY_WRAP_MKLDNN)
      case CONV_MKLDNN_DIRECT:                  { return triNNity::layout::CHW; } break;
      case CONV_MKLDNN_WINOGRAD:                { return triNNity::layout::CHW; } break;
      #endif

      #if defined(TRINNITY_WRAP_ARMCL)
      case CONV_ARMCL_DIRECT:                   { return triNNity::layout::CHW; } break;
      case CONV_ARMCL_PATCH_GEMM:               { return triNNity::layout::CHW; } break;
      case CONV_ARMCL_WINOGRAD:                 { return triNNity::layout::CHW; } break;
      #endif

      #if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
      case CONV_SPARSE_DIRECT_MHW:              { return triNNity::layout::CHW; } break;
      case CONV_SPARSE_DIRECT_MWH:              { return triNNity::layout::CHW; } break;
      #endif

      case CONV_DIRECT_SUM_2D_CHW:              { return triNNity::layout::CHW; } break;
      case CONV_DIRECT_SUM_2D_HWC:              { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_SCAN_AT_B_I_K:           { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_SCAN_AT_B_K_I:           { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_SCAN_AT_BT_I_K:          { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_SCAN_A_B_K_I:            { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_I_K:     { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_K_I:     { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_BT_I_K:    { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_SHORT_A_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_I_K:      { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_BT_I_K:     { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_LONG_A_B_K_I:       { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_I_K:      { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_K_I:      { return triNNity::layout::CHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_BT_I_K:     { return triNNity::layout::HWC; } break;
      case CONV_IM2COL_COPY_SELF_A_B_K_I:       { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_A_B_I_K:            { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_SCAN_AT_BT_K_I:          { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_SCAN_A_BT_I_K:           { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_SCAN_A_BT_K_I:           { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_COPY_SHORT_A_B_I_K:      { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_COPY_SHORT_AT_BT_K_I:    { return triNNity::layout::CHW; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_I_K:     { return triNNity::layout::HWC; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_K_I:     { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW:                         { return triNNity::layout::CHW; } break;
      case CONV_KN2COL:                         { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AS:                      { return triNNity::layout::CHW; } break;
      case CONV_KN2COL_AS:                      { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_AB:                   { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_ABT:                  { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_ATB:                  { return triNNity::layout::CHW; } break;
      case CONV_KN2ROW_AA_ATBT:                 { return triNNity::layout::CHW; } break;
      case CONV_MEC_COL:                        { return triNNity::layout::HWC; } break;
      case CONV_MEC_ROW:                        { return triNNity::layout::HWC; } break;
      case CONV_1X1_AB_IK:                      { return triNNity::layout::HWC; } break;
      case CONV_1X1_ABT_IK:                     { return triNNity::layout::HWC; } break;
      case CONV_1X1_ATB_IK:                     { return triNNity::layout::HWC; } break;
      case CONV_1X1_ATBT_IK:                    { return triNNity::layout::HWC; } break;
      case CONV_1X1_AB_KI:                      { return triNNity::layout::CHW; } break;
      case CONV_1X1_ABT_KI:                     { return triNNity::layout::CHW; } break;
      case CONV_1X1_ATB_KI:                     { return triNNity::layout::CHW; } break;
      case CONV_1X1_ATBT_KI:                    { return triNNity::layout::CHW; } break;

      #if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
      case CONV_WINO_2_3:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_3_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_3_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_2X2_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_3_3:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_3_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_3_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_3X3_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_4X4_3X3:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_3X3_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_3X3_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_2_5:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_5_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_2_5_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_2X2_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_2X2_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_3_5:                       { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_5_VEC_4:                 { return triNNity::layout::HWC; } break;
      case CONV_WINO_3_5_VEC_8:                 { return triNNity::layout::HWC; } break;

      case CONV_WINO_3X3_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_3X3_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_WINO_4X4_5X5:                   { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_5X5_VEC_4:             { return triNNity::layout::HWC; } break;
      case CONV_WINO_4X4_5X5_VEC_8:             { return triNNity::layout::HWC; } break;

      case CONV_FFT_STITCHED_REAL:              { return triNNity::layout::CHW; } break;
      case CONV_FFT_STITCHED_COMPLEX:           { return triNNity::layout::CHW; } break;
      case CONV_FFT_FULL_REAL:                  { return triNNity::layout::CHW; } break;
      case CONV_FFT_FULL_COMPLEX:               { return triNNity::layout::CHW; } break;
      #endif
    }
    return triNNity::layout::CHW;
  }

  /**
   * Returns the parameter layout required for the kernel, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::parameter_layout_t get_kernel_layout() {
    switch (method) {
      #if defined(TRINNITY_WRAP_MKLDNN)
      case CONV_MKLDNN_DIRECT:                  { return triNNity::layout::OIHW; } break;
      case CONV_MKLDNN_WINOGRAD:                { return triNNity::layout::OIHW; } break;
      #endif

      #if defined(TRINNITY_WRAP_ARMCL)
      case CONV_ARMCL_DIRECT:                   { return triNNity::layout::OIHW; } break;
      case CONV_ARMCL_PATCH_GEMM:               { return triNNity::layout::OIHW; } break;
      case CONV_ARMCL_WINOGRAD:                 { return triNNity::layout::OIHW; } break;
      #endif

      #if defined(TRINNITY_ENABLE_SPARSE_GENERIC_LAYER)
      case CONV_SPARSE_DIRECT_MHW:              { return triNNity::layout::OIHW; } break;
      case CONV_SPARSE_DIRECT_MWH:              { return triNNity::layout::OIHW; } break;
      #endif

      case CONV_DIRECT_SUM_2D_CHW:              { return triNNity::layout::OIHW; } break;
      case CONV_DIRECT_SUM_2D_HWC:              { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_SCAN_AT_B_I_K:           { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_SCAN_AT_B_K_I:           { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_SCAN_AT_BT_I_K:          { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_SCAN_A_B_K_I:            { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_I_K:     { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_SHORT_AT_B_K_I:     { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_SHORT_AT_BT_I_K:    { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_SHORT_A_B_K_I:      { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_I_K:      { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_LONG_AT_B_K_I:      { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_LONG_AT_BT_I_K:     { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_LONG_A_B_K_I:       { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_I_K:      { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_SELF_AT_B_K_I:      { return triNNity::layout::IHWO; } break;
      case CONV_IM2COL_COPY_SELF_AT_BT_I_K:     { return triNNity::layout::OIHW; } break;
      case CONV_IM2COL_COPY_SELF_A_B_K_I:       { return triNNity::layout::OIHW; } break;
      case CONV_IM2ROW_SCAN_A_B_I_K:            { return triNNity::layout::IHWO; } break;
      case CONV_IM2ROW_SCAN_AT_BT_K_I:          { return triNNity::layout::IHWO; } break;
      case CONV_IM2ROW_SCAN_A_BT_I_K:           { return triNNity::layout::OIHW; } break;
      case CONV_IM2ROW_SCAN_A_BT_K_I:           { return triNNity::layout::OIHW; } break;
      case CONV_IM2ROW_COPY_SHORT_A_B_I_K:      { return triNNity::layout::HWIO; } break;
      case CONV_IM2ROW_COPY_SHORT_AT_BT_K_I:    { return triNNity::layout::HWIO; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_I_K:     { return triNNity::layout::OHWI; } break;
      case CONV_IM2ROW_COPY_SHORT_A_BT_K_I:     { return triNNity::layout::OHWI; } break;
      case CONV_KN2ROW:                         { return triNNity::layout::HWOI; } break;
      case CONV_KN2COL:                         { return triNNity::layout::IHWO; } break;
      case CONV_KN2ROW_AS:                      { return triNNity::layout::HWOI; } break;
      case CONV_KN2COL_AS:                      { return triNNity::layout::HWOI; } break;
      case CONV_KN2ROW_AA_AB:                   { return triNNity::layout::HWOI; } break;
      case CONV_KN2ROW_AA_ABT:                  { return triNNity::layout::HWOI; } break;
      case CONV_KN2ROW_AA_ATB:                  { return triNNity::layout::HWIO; } break;
      case CONV_KN2ROW_AA_ATBT:                 { return triNNity::layout::HWIO; } break;
      case CONV_MEC_COL:                        { return triNNity::layout::OHIW; } break;
      case CONV_MEC_ROW:                        { return triNNity::layout::OHWI; } break;
      case CONV_1X1_AB_IK:                      { return triNNity::layout::HWIO; } break;
      case CONV_1X1_ABT_IK:                     { return triNNity::layout::HWOI; } break;
      case CONV_1X1_ATB_IK:                     { return triNNity::layout::HWIO; } break;
      case CONV_1X1_ATBT_IK:                    { return triNNity::layout::HWOI; } break;
      case CONV_1X1_AB_KI:                      { return triNNity::layout::HWOI; } break;
      case CONV_1X1_ABT_KI:                     { return triNNity::layout::HWOI; } break;
      case CONV_1X1_ATB_KI:                     { return triNNity::layout::HWIO; } break;
      case CONV_1X1_ATBT_KI:                    { return triNNity::layout::HWIO; } break;

      #if defined(TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER)
      case CONV_WINO_2_3:                       { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2_3_VEC_4:                 { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2_3_VEC_8:                 { return triNNity::layout::HWOI; } break;

      case CONV_WINO_2X2_3X3:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2X2_3X3_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2X2_3X3_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_WINO_3_3:                       { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3_3_VEC_4:                 { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3_3_VEC_8:                 { return triNNity::layout::HWOI; } break;

      case CONV_WINO_3X3_3X3:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3X3_3X3_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3X3_3X3_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_WINO_4X4_3X3:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_4X4_3X3_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_4X4_3X3_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_WINO_2_5:                       { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2_5_VEC_4:                 { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2_5_VEC_8:                 { return triNNity::layout::HWOI; } break;

      case CONV_WINO_2X2_5X5:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2X2_5X5_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_2X2_5X5_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_WINO_3_5:                       { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3_5_VEC_4:                 { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3_5_VEC_8:                 { return triNNity::layout::HWOI; } break;

      case CONV_WINO_3X3_5X5:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3X3_5X5_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_3X3_5X5_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_WINO_4X4_5X5:                   { return triNNity::layout::HWOI; } break;
      case CONV_WINO_4X4_5X5_VEC_4:             { return triNNity::layout::HWOI; } break;
      case CONV_WINO_4X4_5X5_VEC_8:             { return triNNity::layout::HWOI; } break;

      case CONV_FFT_STITCHED_REAL:              { return triNNity::layout::OIHW; } break;
      case CONV_FFT_STITCHED_COMPLEX:           { return triNNity::layout::OIHW; } break;
      case CONV_FFT_FULL_REAL:                  { return triNNity::layout::OIHW; } break;
      case CONV_FFT_FULL_COMPLEX:               { return triNNity::layout::OIHW; } break;
      #endif
    }
    return triNNity::layout::OIHW;
  }

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  //! Pointer to the implementation layer
  triNNity::layer::Layer<T, T> *impl;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);
    impl->execute();
    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    return impl->flops();
  }

  virtual double working_space() {
    return impl->working_space();
  }

  GenericConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_ASSERT(in != nullptr);
    TRINNITY_ASSERT(weights != nullptr);
    this->input = in;
    this->kernel = weights;
    this->impl = internal::buildLayerForMethod<T, A, KD, ifm_w, ifm_h, ifms, ofms, k_w, k_h, stride_w, stride_h, bound_type, method, gemm_var, gemm_block_i, gemm_block_j, gemm_block_k>(this->input, this->kernel, nullptr);
    this->output = this->impl->output;
  }

  GenericConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_ASSERT(in != nullptr);
    TRINNITY_ASSERT(weights != nullptr);
    TRINNITY_ASSERT(out != nullptr);
    this->input = in;
    this->output = out;
    this->kernel = weights;
    this->impl = internal::buildLayerForMethod<T, A, KD, ifm_w, ifm_h, ifms, ofms, k_w, k_h, stride_w, stride_h, bound_type, method, gemm_var, gemm_block_i, gemm_block_j, gemm_block_k>(this->input, this->kernel, this->output);
  }

  ~GenericConvolutionalLayer() {
    TRINNITY_DELETE(this->impl);
  }
};


/**
 * A generic fused convolution layer, performing an input transformation, convolution, bias, and activation.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam A
 * \parblock
 * The primitive type in which accumulation should happen
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam method
 * \parblock
 * The convolution method the layer should use to compute the output
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam in_layout
 * The layout that the input data to this layer is in
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 * \tparam bound_mode
 * \parblock
 * The boundary handling mode the layer should use
 * \endparblock
 * \tparam act_type
 * \parblock
 * The type of activation that the layer should use
 * \endparblock
 * \tparam gemm_block_i
 * \parblock
 * I blocking factor for GEMM
 * \endparblock
 * \tparam gemm_block_j
 * \parblock
 * J blocking factor for GEMM
 * \endparblock
 * \tparam gemm_block_k
 * \parblock
 * K blocking factor for GEMM
 * \endparblock
 *
 */

template<typename T, typename A, typename KD,
         conv_generic_impl_t method, gemm_variant_t gemm_var,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k_w, unsigned k_h,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms,
         triNNity::layout::feature_map_layout_t in_layout,
         triNNity::boundary_t bound_type = BOUND_IMPLICIT_PAD,
         triNNity::boundary_mode_t bound_mode = PRESERVE,
         triNNity::activation_t act_type = ACTIVATION_RELU,
         unsigned gemm_block_i = 1, unsigned gemm_block_j = 1, unsigned gemm_block_k = 1>
struct GenericFusedConvolutionalLayer: public triNNity::layer::Layer<T, T> {

  /**
   * Returns the feature map layout required for the input, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::feature_map_layout_t get_in_layout() {
    return triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_in_layout();
  }

  /**
   * Returns the feature map layout required for the output, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::feature_map_layout_t get_out_layout() {
    return triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_out_layout();
  }

  /**
   * Returns the parameter layout required for the kernel, given the method selected for the convolution.
   */
  static constexpr triNNity::layout::parameter_layout_t get_kernel_layout() {
    return triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_kernel_layout();
  }

  KD *conv_kernel;
  KD *bias_kernel;
  T *discard_buffer;
  bool has_bias, has_activation;

  static constexpr unsigned ofm_w() {
    if (bound_mode == DISCARD) {
      return triNNity::uceil<ifm_w, stride_w>() - (k_w - 1);
    } else {
      return triNNity::uceil<ifm_w, stride_w>();
    }
  }

  static constexpr unsigned ofm_h() {
    if (bound_mode == DISCARD) {
      return triNNity::uceil<ifm_h, stride_h>() - (k_h - 1);
    } else {
      return triNNity::uceil<ifm_h, stride_h>();
    }
  }

  triNNity::layer::Layer<T, T> *trans;
  triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k> *conv;
  triNNity::layer::Layer<T, T> *bias;
  triNNity::layer::Layer<T, T> *act;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);
    this->trans->execute();
    this->conv->input = this->trans->output;
    this->conv->kernel = this->conv_kernel;
    this->conv->execute();

    if (bound_mode == DISCARD) {
      triNNity::generic::internal::discard<T, ifm_w, ifm_h, ofms, k_w, k_h, stride_w, stride_h, this->get_out_layout()>(this->conv->output, this->discard_buffer);
    }

    if (this->has_bias) {
      this->bias->execute();
    }

    if (this->has_activation) {
      this->act->execute();
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double bflops = this->has_bias ? this->bias->flops() : 0.0;
    double aflops = this->has_activation ? this->act->flops() : 0.0;
    return (this->conv->flops() + bflops + aflops);
  }

  virtual double working_space() {
    return this->conv->working_space();
  }

  GenericFusedConvolutionalLayer(T *in, KD *weights, KD *biases) {
    TRINNITY_ASSERT(in != nullptr);
    TRINNITY_ASSERT(weights != nullptr);

    this->input = in;
    this->conv_kernel = weights;

    if (biases == nullptr) {
      this->bias_kernel = nullptr;
      this->has_bias = false;
    } else {
      this->bias_kernel = biases;
      this->has_bias = true;
    }

    if (act_type == ACTIVATION_NONE) {
      this->has_activation = false;
    } else {
      this->has_activation = true;
    }

    if (bound_mode == DISCARD) {
      this->discard_buffer = new T[ofms * this->ofm_h() * this->ofm_w()];
    } else {
      this->discard_buffer = nullptr;
    }

    static constexpr triNNity::layout::feature_map_layout_t conv_in_layout = triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_in_layout();
    static constexpr triNNity::layout::feature_map_layout_t conv_out_layout = triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_out_layout();

    this->trans = new triNNity::layer::TransformLayer<T, ifms, ifm_w, ifm_h, in_layout, conv_in_layout>(this->input);
    this->conv = new triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>(this->trans->output, this->conv_kernel);

    if (bound_mode == DISCARD) {
      if (this->has_bias) { // bias input comes from discard buffer
        this->bias = new triNNity::layer::ConvolutionalBiasLayer<T, KD, ofms, this->ofm_w(), this->ofm_h(), conv_out_layout>(this->discard_buffer, this->bias_kernel);
      }
    } else {
      if (this->has_bias) { // bias input comes from conv output
        this->bias = new triNNity::layer::ConvolutionalBiasLayer<T, KD, ofms, this->ofm_w(), this->ofm_h(), conv_out_layout>(this->conv->output, this->bias_kernel);
      }
    }

    if (this->has_activation) {
      if (this->has_bias) { // act input comes from bias output
        this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->bias->output);
      } else { // act input comes from conv output or discard buffer
        if (bound_mode == DISCARD) { // act input comes from discard buffer
          this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->discard_buffer);
        } else { // act input comes from conv output
          this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->conv->output);
        }
      }
      this->output = this->act->output;
    } else {
      if (this->has_bias) {
        this->output = this->bias->output;
      } else {
        if (bound_mode == DISCARD) {
          this->output = this->discard_buffer;
        } else {
          this->output = this->conv->output;
        }
      }
    }

  }

  GenericFusedConvolutionalLayer(T *in, KD *weights, KD *biases, T *out) {
    TRINNITY_ASSERT(in != nullptr);
    TRINNITY_ASSERT(weights != nullptr);
    TRINNITY_ASSERT(out != nullptr);
    this->input = in;
    this->conv_kernel = weights;

    if (biases == nullptr) {
      this->bias_kernel = nullptr;
      this->has_bias = false;
    } else {
      this->bias_kernel = biases;
      this->has_bias = true;
    }

    if (act_type == ACTIVATION_NONE) {
      this->has_activation = false;
    } else {
      this->has_activation = true;
    }

    this->output = out;

    if (bound_mode == DISCARD) {
      this->discard_buffer = new T[ofms * this->ofm_h() * this->ofm_w()];
    } else {
      this->discard_buffer = nullptr;
    }

    static constexpr triNNity::layout::feature_map_layout_t conv_in_layout = triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_in_layout();
    static constexpr triNNity::layout::feature_map_layout_t conv_out_layout = triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>::get_out_layout();

    this->trans = new triNNity::layer::TransformLayer<T, ifms, ifm_w, ifm_h, in_layout, conv_in_layout>(this->input);

    if (this->has_bias || this->has_activation || bound_mode == DISCARD) { // need to post-process conv output
      this->conv = new triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>(this->trans->output, this->conv_kernel);
    } else { // wire conv output directly to output
      this->conv = new triNNity::generic::layer::GenericConvolutionalLayer<T, A, KD, method, gemm_var, ifms, ifm_w, ifm_h, k_w, k_h, stride_w, stride_h, ofms, bound_type, gemm_block_i, gemm_block_j, gemm_block_k>(this->trans->output, this->conv_kernel, this->output);
    }

    if (bound_mode == DISCARD) {
      if (this->has_bias) { // bias input comes from discard buffer
        this->bias = new triNNity::layer::ConvolutionalBiasLayer<T, KD, ofms, this->ofm_w(), this->ofm_h(), conv_out_layout>(this->discard_buffer, this->bias_kernel);
      }
    } else {
      if (this->has_bias) { // bias input comes from conv output
        this->bias = new triNNity::layer::ConvolutionalBiasLayer<T, KD, ofms, this->ofm_w(), this->ofm_h(), conv_out_layout>(this->conv->output, this->bias_kernel);
      }
    }

    if (this->has_activation) {
      if (this->has_bias) { // act input comes from bias output
        this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->bias->output);
      } else { // act input comes from conv output or discard buffer
        if (bound_mode == DISCARD) { // act input comes from discard buffer
          this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->discard_buffer);
        } else { // act input comes from conv output
          this->act = new triNNity::layer::ActivationLayer<T, ofms, this->ofm_w(), this->ofm_h(), act_type>(this->conv->output);
        }
      }
      this->output = this->act->output;
    } else {
      if (this->has_bias) {
        this->output = this->bias->output;
      } else {
        if (bound_mode == DISCARD) {
          this->output = this->discard_buffer;
        } else {
          this->output = this->conv->output;
        }
      }
    }
  }

  ~GenericFusedConvolutionalLayer() {
    TRINNITY_DELETE(this->trans);
    TRINNITY_DELETE(this->conv);

    if (this->has_bias) {
      TRINNITY_DELETE(this->bias);
    }

    if (this->has_activation) {
      TRINNITY_DELETE(this->act);
    }

    if (bound_mode == DISCARD) {
      TRINNITY_DELETE_ARRAY(this->discard_buffer);
    }
  }
};

}

}

}

#endif // TRINNITY_GENERIC_LAYER_H
