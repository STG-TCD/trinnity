/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_GENERIC_CONFIG_H
#define TRINNITY_GENERIC_CONFIG_H

#include <triNNity/config.h>

namespace triNNity {

typedef enum {
  DISCARD,     /*!< Discard values that depend on boundary pixels (reduces output size) */
  PRESERVE,    /*!< Preserve values that depend on boundary pixels (output size matches input size) */
} boundary_mode_t;

}

#endif // TRINNITY_GENERIC_CONFIG_H
