DESTDIR=

.PHONY: clean lint

lint:
	find . -iname *.h | xargs cpplint

install:
	mkdir -p $(DESTDIR)/include/triNNity/
	cp -r include/* $(DESTDIR)/include/triNNity/
	mkdir -p $(DESTDIR)/share/doc/triNNity/
	cp -r doc/* $(DESTDIR)/share/doc/triNNity/

install-arm64:
	mkdir -p $(DESTDIR)/aarch64-linux-gnu/include/triNNity/
	cp -r include/* $(DESTDIR)/aarch64-linux-gnu/include/triNNity/

install-arm32:
	mkdir -p $(DESTDIR)/arm-linux-gnueabihf/include/triNNity/
	cp -r include/* $(DESTDIR)/arm-linux-gnueabihf/include/triNNity/

install-all: install install-arm32 install-arm64

doc:
	doxygen

inplace:
	ln -s include triNNity

clean:
	rm -f triNNity
