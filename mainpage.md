triNNity Inference Primitive Library                         {#mainpage}
============

`triNNity` is a C++ library implementing a bunch of CNN primitives. Supported platforms include `x86_64` and `aarch64`.

### How do I get set up? ###

All you need is a C++ compiler. The library has been tested with `icpc`, `clang++`, and `g++`.

A `PKGBUILD` file is included for Arch Linux. To build the package do `make package`. You can install the library system-wide with `pacman -U` on the resulting `.tar.xz` file.

### Examples ###

You can find some example code in the `examples` directory showing how to use the library.

### Library Structure ###

Each module in the library exports both a low and a high level interface. The low-level interface exposes the library operations via template functions, while the high-level interface exposes the library operations as `Layer` objects. The principal difference is that the low level interface does absolutely zero memory management, while the high level interface will manage your kernel buffers for you.

To use either interface, simple `#include` the relevant header file - for the low-level interface this is `<triNNity/module/mcmk.h>`, and for the high-level interface, it is `<triNNity/module/layer.h>` You can use more than one module at once - for example, if you are building a mixed dense-sparse network on CPU, using the high-level interface, simply do:

    #include <triNNity/dense/cpu/layer.h>
    #include <triNNity/sparse/cpu/layer.h>

The two interfaces each use a namespace to logically collect all of the library operations. To use the low level interface for the dense/cpu module, for example, say `using triNNity::dense::cpu`. To use the high level interface, say `using triNNity::layer`. You can of course use both interfaces at once, if you would like to use high-level code to implement some parts of your network, but need the precision of the low-level interface in other parts.

### Available Modules ###

- dense/cpu
- sparse/cpu
- spectral/cpu

### Build Configuration ###

A number of library features are configured statically with build flags.
The following table lists these in alphabetical order.

| Flag                                          | Meaning | Value |
|-----------------------------------------------|---------|-------|
| `TRINNITY_ARMCL_NUM_THREADS`                  | How many threads to instruct ARMCL to use | integer |
| `TRINNITY_BLAS_REPORT_FILE`                   | Name of the BLAS report file to create (in the working directory of the executable) | string |
| `TRINNITY_BLAS_REPORT_PARAMS`                 | Whether to dump all the M, N and K values in calls to GEMM | boolean |
| `TRINNITY_CBLAS_COLUMN_MAJOR`                 | Use CBLAS in column-major mode | boolean |
| `TRINNITY_CBLAS_ROW_MAJOR`                    | Use CBLAS in row-major mode | boolean |
| `TRINNITY_DIRECT_MUL_PRECISION_A`             | Multiply in the accumulator format in direct convolution | boolean |
| `TRINNITY_DIRECT_MUL_PRECISION_K`             | Multiply in the kernel format in direct convolution | boolean |
| `TRINNITY_DIRECT_MUL_PRECISION_I`             | Multiply in the image format in direct convolution | boolean |
| `TRINNITY_ENABLE_SPARSE_GENERIC_LAYER`        | Enable sparse methods in generic layers | boolean |
| `TRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER`      | Enable spectral methods in generic layers | boolean |
| `TRINNITY_GEMMS_IN_PARALLEL`                  | Dispatch GEMM calls in parallel with OpenMP | boolean |
| `TRINNITY_NO_ASSERTS`                         | Disable all asserts | boolean |
| `TRINNITY_NO_EXCEPTIONS`                      | Disable all exceptions | boolean |
| `TRINNITY_USE_BLOCKED_GPU_GEMM`               | Use built-in GPU GEMM instead of cuBLAS GEMM | boolean |
| `TRINNITY_USE_CBLAS_GEMM`                     | Use CBLAS GEMM | boolean |
| `TRINNITY_USE_CBLAS_GEMV`                     | Use CBLAS GEMV | boolean |
| `TRINNITY_USE_CUBLAS_GEMM`                    | Use cuBLAS GEMM | boolean |
| `TRINNITY_USE_CUBLAS_GEMV`                    | Use cuBLAS GEMV | boolean |
| `TRINNITY_USE_FFTW`                           | Use FFTW to do FFTs | boolean |
| `TRINNITY_USE_GCC_VECTOR_EXTS`                | Enable the use of GCC vector extensions | boolean |
| `TRINNITY_USE_LIBXSMM_GEMM`                   | Use libxsmm GEMM | boolean |
| `TRINNITY_USE_MKL_GEMM`                       | Use MKL GEMM | boolean |
| `TRINNITY_USE_MKL_GEMV`                       | Use MKL GEMV | boolean |
| `TRINNITY_USE_OPENMP`                         | Enable OpenMP for thread level parallelisation | boolean |
| `TRINNITY_WRAP_ARMCL`                         | Enable ARMCL wrapper for convolution | boolean |
| `TRINNITY_WRAP_MKLDNN`                        | Enable MKLDNN wrapper for convolution | boolean |
| `TRINNITY_DISABLE_LAYER_TIMING`               | Disable per-layer timings | boolean |
| `TRINNITY_GENERIC_LAYER_NO_DEALLOCATE`        | Disable memory deallocation in generic layers | boolean |

Note: If none of `TRINNITY_DIRECT_MUL_PRECISION_A`, `TRINNITY_DIRECT_MUL_PRECISION_K`, `TRINNITY_DIRECT_MUL_PRECISION_I` are defined, direct convolution performs multiplications using the standard C++ type promotion rules. For example, if the image is in `float` precision, and the kernel is in `double` precision, the multiplications will be performed at `double` precision.

Additionally, the standard environment variables can be used to control
back-end libraries: `OMP_NUM_THREADS`, `OPENBLAS_NUM_THREADS`,
`MKL_NUM_THREADS` etc.

