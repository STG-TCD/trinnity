## v0.6

Added support for asymmetric convolutions (1D and 2D).

## v0.5

### Accelerator Support

Added support for GPGPU acceleration via cuBLAS (for NVIDIA GPU) and OpenCL
(for all other GPU vendors).

Added the `dense/cuda` and `dense/opencl` modules.

## v0.4

### GEMM Blocking Control

Added GEMM block size control -- now you can specify what blocking factors to
use in GEMM when using `triNNity::GEMM_BLOCKED` to implement any convolution.

### Precision Control in Direct Convolutions

Added controls to specify the precision at which multiplications and
accumulations are performed in all direct convolutions. For example, in an
8-bit fixed point convolution you may want to accumulate the result in 16-bit
fixed point, and so on.

## v0.3

### Zero-Cost Wrappers for External Libraries

Added zero-cost wrappers to enable the use of Intel MKL-DNN and ARM Compute
Library as back-ends for convolutions performed via generic layers.
